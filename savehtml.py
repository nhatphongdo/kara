import urllib.request,os, errno,html.parser,json, html2text
from bs4 import BeautifulSoup

class HTMLtoJSONParser(html.parser.HTMLParser):
    def __init__(self, raise_exception = True) :
        html.parser.HTMLParser.__init__(self)
        self.doc  = { }
        self.path = []
        self.cur  = self.doc
        self.line = 0
        self.raise_exception = raise_exception

    @property
    def json(self):
        return self.doc

    @staticmethod
    def to_json(content, raise_exception = True):
        parser = HTMLtoJSONParser(raise_exception = raise_exception)
        parser.feed(str(content))
        return parser.json

    def handle_starttag(self, tag, attrs):
        self.path.append(tag)
        attrs = { k:v for k,v in attrs }
        if tag in self.cur :
            if isinstance(self.cur[tag],list) :
                self.cur[tag].append(  { "__parent__": self.cur } )
                self.cur = self.cur[tag][-1]
            else :
                self.cur[tag] = [ self.cur[tag] ]
                self.cur[tag].append(  { "__parent__": self.cur } )
                self.cur = self.cur[tag][-1]
        else :
            self.cur[tag] = { "__parent__": self.cur }
            self.cur = self.cur[tag]

        for a,v in attrs.items():
            self.cur["#" + a] = v
        self.cur[""] = ""

    def handle_endtag(self, tag):
        if tag != self.path[-1] and self.raise_exception :
            raise Exception("html is malformed around line: {0} (it might be because of a tag <br>, <hr>, <img .. > not closed)".format(self.line))
        del self.path[-1]
        memo = self.cur
        self.cur = self.cur["__parent__"]
        self.clean(memo)

    def handle_data(self, data):
        self.line += data.count("\n")
        if "" in self.cur:
            self.cur[""] += data

    def clean(self, values):
        keys = list(values.keys())
        for k in keys:
            v = values[k]
            if isinstance(v, str) :
                #print ("clean", k,[v])
                c = v.strip(" \n\r\t")
                if c != v :
                    if len(c) > 0 :
                        values[k] = c
                    else :
                        del values[k]
        del values["__parent__"]


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise


urls = ['http://kinhdoanh.vnexpress.net/tin-tuc/doanh-nghiep/cap-quang-aag-lai-dut-ngay-dau-nam-moi-3129370.html',
        'http://vnexpress.net/tin-tuc/phap-luat/luat-su-de-nghi-dung-phien-xu-lam-ro-nguyen-nhan-chi-huyen-chet-3116596.html']

for idx, url in enumerate(urls):
    filename = 'link' + str(idx) + '.html'
    path = "/websiteDirectory/"
    mkdir_p(path)
    outpath = os.path.join(path, filename)
    local_filename, headers = urllib.request.urlretrieve(url, outpath)
    file = open(local_filename, 'r+', encoding='utf-8')
    content = file.read()
    file.write(content)

    soup = BeautifulSoup(content, 'html.parser')
    print(str(soup.title.encode('utf-8')))

    # data = HTMLtoJSONParser.to_json(content)
    # filename_json = 'data' + str(idx) + '.txt'
    # outpath_json = os.path.join(path, filename_json)
    # with open(outpath_json, 'w', encoding='utf-8') as outfile:
    #     json.dump(data, outfile)

    file.close()