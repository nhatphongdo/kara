from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'', include('social.apps.django_app.urls', namespace='social')),
    # url(r'', include('django.contrib.auth.urls', namespace='auth')),

    url(r'^(?i)media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

    # url for templateURL
    url(r'', include('app.urls')),

    # url for api feature
    url(r'', include('app.apis.urls')),

    url(r'^.*$', 'app.views.home', name='home'),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns(
        '',
        url(r'^(?i)debug/', include(debug_toolbar.urls)),
    )
