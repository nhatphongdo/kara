"""
Django settings for KaraExplorer project.
"""

import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
#


VERSION = '1.1.1'

PROJECT_ROOT = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

# ================================================ #
ROOT_URLCONF = 'KaraExplorer.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'KaraExplorer.wsgi.application'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n(bd1f1c%e8=_xad02x5qtfn%wgwpi492e$8_erx+d)!tpeoim'

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


# ================================================ #
LOGIN_URL = '/login'

LOGIN_REDIRECT_URL = '/'

ADMINS = (
    # ('Phong Do', 'nhatphongdo@gmail.com'),
)

MANAGERS = ADMINS

# Allow all host headers
ALLOWED_HOSTS = ['*']

SITE_ID = 1

INTERNAL_IPS = ('127.0.0.1', 'localhost')


# ================================================ #
DEBUG = True
TEMPLATE_DEBUG = DEBUG

# Debug Toolbar settings
DEBUG_TOOLBAR_PATCH_SETTINGS = False


# ================================================ #
# Application settings
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 86400  # 1 day
SHORT_PERIOD_SESSION = 15  # 15 minutes
LONG_PERIOD_SESSION = 30 * 24 * 60  # 30 days


# ================================================ #
INSTALLED_APPS = (
    'django.contrib.admin',
    # 'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.webdesign',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'djangobower',
    'compressor',
    'rest_framework',
    'rest_framework.authtoken',
    'djng',

    'app',

    'debug_toolbar',
    'django_countries',
    'social.apps.django_app.default',
)


# ================================================ #
# Database
DATABASES = {
    'default': {
        # 'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': os.path.join(PROJECT_ROOT, 'db.sqlite3'),
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'kara',
        'USER': 'kara',
        'PASSWORD': 'kara123!@#',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}


# ================================================ #
BOWER_COMPONENTS_ROOT = os.path.join(PROJECT_ROOT)

BOWER_INSTALLED_APPS = (
    'Context.js',
    # 'Snap.svg',  ##
    # 'angular',
    # 'angular-animate',  # 'angular-material'
    # 'angular-aria',  # 'angular-material'
    'angular-bootstrap',
    'angular-cookies',
    'angular-drag-and-drop-lists',
    'angular-fullscreen',
    'angular-image-crop',
    'angular-masonry-directive',
    # 'angular-material',  ##
    # 'angular-messages',  # 'angular-material'
    'angular-moment',
    'angular-resource',
    # 'angular-route',  ##
    'angular-sanitize',
    'angular-socialshare',  #angolarjs-socialshare 720kb
    'angular-ui-router',
    'autofill-event',
    # 'bootstrap',
    # 'bootstrap-material-design',  ##
    # 'bootstrap-sass',  # 'bootstrap-material-design'
    'components-font-awesome',
    # 'desandro-matches-selector',  # 'fizzy-ui-utils'
    'es5-shim',
    # 'ev-emitter',  # 'imagesloaded'  # 'outlayer'
    # 'fizzy-ui-utils',  # 'outlayer'
    # 'get-size',  # 'masonry'  # 'outlayer'
    'gridify',
    'html5shiv',
    # 'imagesloaded',  # 'angular-masonry-directive'
    # 'jquery',
    'jquery-validation',
    'jquery.inputmask',
    # 'masonry',  # 'angular-masonry-directive'
    # 'microplugin',  # 'selectize'
    # 'modernizr',  ##
    # 'moment',  # 'angular-moment'
    'ng-dialog',
    'ng-file-upload',
    'ng-tags-input',
    'oclazyload',
    # 'outlayer',  # 'masonry'
    'owl.carousel',
    'respond',
    'selectize',
    # 'sifter',  # 'selectize'
    'toastr',
    # 'velocity',  # 'gridify'
)


# ================================================ #
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django.middleware.security.SecurityMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)


# ================================================ #
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    # 'DEFAULT_PERMISSION_CLASSES': [
    # 'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    # ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'PAGINATE_BY': 10,
    'PAGINATE_BY_PARAM': 'page_size',
    'MAX_PAGINATE_BY': 100,
}


# ================================================ #
# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'uploads').replace('\\', '/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''


# ================================================ #
# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static').replace('\\', '/')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    # ex.
    # os.path.join(PROJECT_ROOT, 'dist/static'),
    # os.path.join(PROJECT_ROOT, 'static'),
)

STATIC_PRECOMPILER_OUTPUT_DIR = ''

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
    'djangobower.finders.BowerFinder',
)


# ================================================ #
# TEMPLATES = [
#     {
#         'BACKEND': 'django.template.backends.django.DjangoTemplates',
#         'DIRS': [os.path.join(PROJECT_ROOT, 'templates')],
#         'APP_DIRS': True,
#         'OPTIONS': {
#             'context_processors': [
#                 'django.template.context_processors.debug',
#                 'django.template.context_processors.request',
#                 'django.contrib.auth.context_processors.auth',
#                 'django.contrib.messages.context_processors.messages',
#             ],
#         },
#     },
# ]

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = (
    [os.path.join(PROJECT_ROOT, 'templates')]
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.template.context_processors.debug',
    'django.template.context_processors.i18n',
    'django.template.context_processors.media',
    'django.template.context_processors.static',
    'django.template.context_processors.tz',
    # 'django.template.context_processors.request',
    'django.contrib.messages.context_processors.messages',

    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)


# ================================================ #
COMPRESS_ENABLED = os.environ.get('COMPRESS_ENABLED', False)

if os.environ.get('COMPRESS_OFFLINE') is None:
    if DEBUG:
        COMPRESS_OFFLINE = False
    else:
        COMPRESS_OFFLINE = True
        # this is so that compress_offline is set to true during deployment to Heroku

COMPRESS_PRECOMPILERS = (
    ('text/less', 'lessc {infile} {outfile}'),
    ('text/coffeescript', 'coffee --compile --stdio'),
)

COMPRESS_CSS_FILTERS = [
    #creates absolute urls from relative ones
    'compressor.filters.css_default.CssAbsoluteFilter',
    #css minimizer
    'compressor.filters.cssmin.CSSMinFilter'
]

COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter'
]

# The URL that linked media will be read from and compressed
# media will be written to.
COMPRESS_URL = STATIC_URL
# The absolute file path that linked media will be read from
# and compressed media will be written to.
COMPRESS_ROOT = STATIC_ROOT


# ================================================ #
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Ho_Chi_Minh'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True


# ================================================ #
# Email settings
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False

EMAIL_HOST = 'smtp.mandrillapp.com'  # 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'ntphat691'  # 'nhatphongdo@gmail.com'
EMAIL_HOST_PASSWORD = 'P_dpIUzDdI-LuDPhHIK2rA'  # 'lbhscF7_ZMg6hpnkprK3-A'
EMAIL_SUBJECT_PREFIX = 'Kara'
MANDRILL_API_KEY = 'P_dpIUzDdI-LuDPhHIK2rA'  # 'lbhscF7_ZMg6hpnkprK3-A'


# ================================================ #
# SOCIAL_AUTH

# Keys and secrets
SOCIAL_AUTH_FACEBOOK_KEY = '1082195178497482'
SOCIAL_AUTH_FACEBOOK_SECRET = '1f063703533b1dff28102e8834e73c4c'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'user_friends', 'public_profile', 'user_birthday']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': 'id, name, email, age_range, gender, picture, cover, '
              'about, bio, birthday, first_name, last_name, middle_name, languages, location, locale',
}

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '1085358926096-cnt399g3m69ufnac7kpp7rgnlfaoimer.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = '58p3hFw0WvdkbCjnekNIWmb5'
# Google OAuth2 (google-oauth2)
# SOCIAL_AUTH_GOOGLE_OAUTH2_IGNORE_DEFAULT_SCOPE = True
# SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
#     'https://www.googleapis.com/auth/userinfo.email',
#     'https://www.googleapis.com/auth/userinfo.profile'
# ]
# SOCIAL_AUTH_GOOGLE_OAUTH2_USE_DEPRECATED_API = True

# Authentication backends
AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_PIPELINE = (
    # Get the information we can about the user and return it in a simple
    # format to create the user instance later. On some cases the details are
    # already part of the auth response from the provider, but sometimes this
    # could hit a provider API.
    'social.pipeline.social_auth.social_details',
    # Get the social uid from whichever service we're authing thru. The uid is
    # the unique identifier of the given user in the provider.
    'social.pipeline.social_auth.social_uid',
    # Verifies that the current auth process is valid within the current
    # project, this is were emails and domains whitelists are applied (if
    # defined).
    # 'social.pipeline.social_auth.auth_allowed',
    # Checks if the current social-account is already associated in the site.
    'social.pipeline.social_auth.social_user',
    # Make up a username for this person, appends a random string at the end if
    # there's any collision.
    'social.pipeline.user.get_username',
    # Send a validation email to the user to verify its email address.
    # Disabled by default.
    'social.pipeline.mail.mail_validation',
    # Associates the current social details with another user account with
    # a similar email address. Disabled by default.
    'social.pipeline.social_auth.associate_by_email',
    # Create a user account if we haven't found one yet.
    'social.pipeline.user.create_user',
    'app.auth_pipelines.create_profile',
    # Create the record that associated the social account with this user.
    'social.pipeline.social_auth.associate_user',
    # Populate the extra_data field in the social record with the values
    # specified by settings (and the default ones like access_token, etc).
    'social.pipeline.social_auth.load_extra_data',
    # Update the user record with any changed info from the auth service.
    # 'social.pipeline.user.user_details',
)

SOCIAL_AUTH_DISCONNECT_PIPELINE = (
    # Verifies that the social association can be disconnected from the current
    # user (ensure that the user login mechanism is not compromised by this
    # disconnection).
    'social.pipeline.disconnect.allowed_to_disconnect',
    # Collects the social associations to disconnect.
    'social.pipeline.disconnect.get_entries',
    # Revoke any access_token when possible.
    'social.pipeline.disconnect.revoke_tokens',
    # Removes the social associations.
    'social.pipeline.disconnect.disconnect',
)

SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True


# ================================================ #
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


# ================================================ #
# Specify the default test runner.
TEST_RUNNER = 'django.test.runner.DiscoverRunner'
