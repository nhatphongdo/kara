import datetime
import six
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.compat import MinValueValidator, MaxValueValidator
from social.apps.django_app.default.models import UserSocialAuth
from .models import *


class BigIntegerField(serializers.Field):
    default_error_messages = {
        'invalid': _('A valid integer is required.'),
        'max_value': _('Ensure this value is less than or equal to {max_value}.'),
        'min_value': _('Ensure this value is greater than or equal to {min_value}.'),
        'max_string_length': _('String value too large.')
    }
    MAX_STRING_LENGTH = 1000  # Guard against malicious string inputs.

    def __init__(self, **kwargs):
        self.max_value = kwargs.pop('max_value', None)
        self.min_value = kwargs.pop('min_value', None)
        super(BigIntegerField, self).__init__(**kwargs)
        if self.max_value is not None:
            message = self.error_messages['max_value'].format(max_value=self.max_value)
            self.validators.append(MaxValueValidator(self.max_value, message=message))
        if self.min_value is not None:
            message = self.error_messages['min_value'].format(min_value=self.min_value)
            self.validators.append(MinValueValidator(self.min_value, message=message))

    def to_internal_value(self, data):
        if isinstance(data, six.text_type) and len(data) > self.MAX_STRING_LENGTH:
            self.fail('max_string_length')

        try:
            data = int(data)
        except (ValueError, TypeError):
            self.fail('invalid')
        return data

    def to_representation(self, value):
        return int(value)


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class DynamicFieldsSerializer(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class RecursiveField(DynamicFieldsSerializer):
    def to_representation(self, value=None):
        serializer = self.parent.parent.__class__(value, context=self.context, fields=self.parent.parent.fields)
        return serializer.data


class MyItemSerializer(DynamicFieldsSerializer):
    def to_representation(self, obj=None):
        # print('===================================')
        output = {}
        for attribute_name in dir(obj):
            if attribute_name.startswith('_'):
                # Ignore private attributes.
                # print(attribute_name, 'private attributes')
                pass
            elif 'objects' == attribute_name:
                # print(attribute_name, 'objects')
                pass
            else:
                attribute = getattr(obj, attribute_name)

                if hasattr(attribute, '__call__'):
                    # Ignore methods and other callables.
                    # methodList = [method for method in dir(object) if callable(getattr(object, method))]
                    # print(attribute_name, 'methods and other callables')
                    pass
                elif isinstance(attribute, (str, int, bool, float, type(None))):
                    # Primitive types can be passed through unmodified.
                    # print(attribute_name, type(attribute))
                    output[attribute_name] = attribute
                elif isinstance(attribute, list):
                    # Recursively deal with items in lists.
                    # print(attribute_name, 'lists')
                    output[attribute_name] = [
                        self.to_representation(item) for item in attribute
                    ]
                elif isinstance(attribute, dict):
                    # Recursively deal with items in dictionaries.
                    # print(attribute_name, 'dictionaries')
                    output[attribute_name] = {
                        str(key): self.to_representation(value)
                        for key, value in attribute.items()
                    }
                else:
                    # Force anything else to its string representation.
                    # print(attribute_name, 'Force anything else to its string representation')
                    output[attribute_name] = str(attribute)
        # print('===================================')
        # print(output)
        # print('===================================')
        return output


class TagSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    person = serializers.ReadOnlyField(source='hashed_person')
    content = serializers.ReadOnlyField(source='hashed_content')

    class Meta:
        model = Tag
        fields = ('id', 'person', 'content', 'tag', 'slug', 'is_user_defined')


class ActionSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    person = serializers.ReadOnlyField(source='hashed_person')
    action = serializers.CharField(source='get_action_display')
    content_type = serializers.CharField(source='content_type.model')
    content_object = MyItemSerializer()

    class Meta:
        model = Action
        fields = ('id', 'person', 'action', 'updated_on', 'object_id', 'content_type', 'content_object')


class PostSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    poster = serializers.ReadOnlyField(source='hashed_poster')
    bookmark = serializers.ReadOnlyField(source='hashed_source')
    name = serializers.CharField(source='title')
    thumbnails = serializers.ListField(
        child=serializers.CharField()
    )

    class Meta:
        model = Post
        fields = ('id', 'poster', 'bookmark', 'name', 'author', 'slug', 'content',
                  'description', 'thumbnails', 'published_on', 'is_published', 'updated_on')


class StatisticSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    content_type = serializers.CharField(source='content_type.model')
    object_id = serializers.CharField(source='content_object.hashed_id')

    class Meta:
        model = Statistic
        fields = ('id', 'updated_on', 'content_type', 'object_id',
                  'likes', 'dislikes', 'views', 'shares', 'comments', 'links', 'folders')


def get_representation(obj=None):
    output = {'method_list': []}
    for attribute_name in dir(obj):
        if attribute_name.startswith('_'):
            # Ignore private attributes.
            pass
        elif 'objects' == attribute_name:
            pass
        else:
            try:
                attribute = getattr(obj, attribute_name)
                if hasattr(attribute, '__call__'):
                    if callable(attribute):
                        output['method_list'].append(attribute_name)
                    # Ignore methods and other callables.
                    pass
                elif isinstance(attribute, (str, int, bool, float, type(None))):
                    # Primitive types can be passed through unmodified.
                    output[attribute_name] = attribute
                elif isinstance(attribute, list):
                    # Recursively deal with items in lists.
                    output[attribute_name] = [
                        get_representation(item) for item in attribute
                    ]
                elif isinstance(attribute, dict):
                    # Recursively deal with items in dictionaries.
                    output[attribute_name] = {
                        str(key): get_representation(value)
                        for key, value in attribute.items()
                    }
                else:
                    # Force anything else to its string representation.
                    output[attribute_name] = str(attribute)
            except ObjectDoesNotExist:
                pass
    return output


class CategorySerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    parent = serializers.ReadOnlyField(source='hashed_parent')
    parents = serializers.ListField(child=serializers.DictField())

    class Meta:
            model = Category
            fields = ('id', 'name', 'slug', 'parent', 'parents')

    def to_representation(self, value=None):
        if not hasattr(value, 'parents'):
            value.parents = []

        return super(CategorySerializer, self).to_representation(value)


class BasicFeedSourceSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    name = serializers.CharField()
    url = serializers.URLField()
    icon = serializers.ReadOnlyField(source='hashed_icon_id')
    thumbnail = serializers.ReadOnlyField(source='hashed_thumbnail_id')
    cover = serializers.ReadOnlyField(source='hashed_cover_id')

    class Meta:
        model = FeedSource
        fields = ('id', 'name', 'url', 'icon', 'thumbnail', 'cover')


class BasicInterestSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    parent = serializers.ReadOnlyField(source='hashed_parent')
    thumbnail = serializers.ReadOnlyField(source='hashed_thumbnail_id')

    class Meta:
        model = Interest
        fields = ('id', 'name', 'slug', 'parent', 'thumbnail')


class InterestSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    parent = serializers.ReadOnlyField(source='hashed_parent')
    parents = serializers.ListField(child=serializers.DictField())
    thumbnail = serializers.ReadOnlyField(source='hashed_thumbnail_id')

    class Meta:
            model = Interest
            fields = ('id', 'name', 'slug', 'parent', 'parents', 'thumbnail')

    def to_representation(self, value=None):
        if not hasattr(value, 'parents'):
            value.parents = []

        return super(InterestSerializer, self).to_representation(value)


class UserFolderSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    parent = serializers.ReadOnlyField(source='hashed_parent')
    owner = serializers.ReadOnlyField(source='hashed_owner')

    class Meta:
        model = Folder
        fields = ('id', 'owner', 'name', 'slug', 'description', 'is_user_defined',
                  'parent', 'updated_on')
        ordering = ['name']


class FolderSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    parent = serializers.ReadOnlyField(source='hashed_parent')
    owner = serializers.ReadOnlyField(source='hashed_owner')
    parents = serializers.ListField(child=serializers.DictField())
    public = serializers.IntegerField()

    class Meta:
        model = Folder
        fields = ('id', 'owner', 'name', 'slug', 'description', 'is_user_defined',
                  'parent', 'parents', 'public', 'updated_on')

    def to_representation(self, value=None):
        if not hasattr(value, 'parents'):
            value.parents = []

        return super(FolderSerializer, self).to_representation(value)


class BookmarkFeedSourceSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    icon = serializers.ReadOnlyField(source='hashed_icon_id')
    thumbnail = serializers.ReadOnlyField(source='hashed_thumbnail_id')
    cover = serializers.ReadOnlyField(source='hashed_cover_id')

    class Meta:
        model = FeedSource
        fields = ('id', 'name', 'url', 'feed_source_url', 'icon', 'thumbnail', 'cover', 'description')


class BookmarkSerializer(DynamicFieldsModelSerializer):
    id = serializers.CharField(source='hashed_id')
    name = serializers.CharField(source='title')
    feed_source = BookmarkFeedSourceSerializer(many=False)
    folders = FolderSerializer(
        many=True,
        fields=('id', 'owner', 'name', 'slug', 'description', 'is_user_defined', 'parent'))

    class Meta:
        model = Bookmark
        fields = ('id', 'name', 'slug', 'url', 'status', 'created_on', 'updated_on', 'feed_source', 'folders')


class UserInterestParentSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    # parent = serializers.ReadOnlyField(source='hashed_parent')
    thumbnail = serializers.ReadOnlyField(source='hashed_thumbnail_id')

    class Meta:
        model = Interest
        fields = ('id', 'name', 'slug', 'description', 'thumbnail')


class UserInterestSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    # parent = UserInterestParentSerializer()
    parent = serializers.ReadOnlyField(source='hashed_parent')
    thumbnail = serializers.ReadOnlyField(source='hashed_thumbnail_id')

    class Meta:
        model = Interest
        fields = ('id', 'name', 'slug', 'description', 'parent', 'is_trash', 'thumbnail')


class UserCategorySerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    parent = serializers.ReadOnlyField(source='hashed_parent')

    class Meta:
        model = Category
        fields = ('id', 'name', 'slug', 'parent', 'thumbnail')


class UserFeedSourceSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    icon = serializers.ReadOnlyField(source='hashed_icon_id')
    thumbnail = serializers.ReadOnlyField(source='hashed_thumbnail_id')
    cover = serializers.ReadOnlyField(source='hashed_cover_id')

    class Meta:
        model = FeedSource
        fields = ('id', 'name', 'url', 'feed_source_url', 'icon', 'thumbnail', 'cover', 'description')


class UserFeedSourceAliasSerializer(DynamicFieldsModelSerializer):
    alias_name = serializers.ReadOnlyField(source='name')
    feed_source = UserFeedSourceSerializer()

    class Meta:
        model = FeedSource
        fields = ('alias_name', 'feed_source')


class UserRelationshipsSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    from_person = serializers.ReadOnlyField(source='hashed_from_person')
    to_person = serializers.ReadOnlyField(source='hashed_to_person')

    class Meta:
        model = Relationship
        fields = ('id', 'from_person', 'to_person', 'status')


class UserFriendsSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_user_id')
    gender = serializers.CharField(source='get_gender_display')
    gender_id = serializers.IntegerField(source='gender')
    # country = serializers.CharField(source='get_country_display')
    # language = serializers.CharField(source='get_language_display')
    avatar = serializers.ReadOnlyField(source='hashed_avatar_id')
    cover = serializers.ReadOnlyField(source='hashed_cover_id')
    birthday = serializers.ReadOnlyField(source='get_birthday_display')

    class Meta:
        model = Profile
        fields = ('id', 'nick_name', 'gender', 'gender_id',
                  'avatar', 'cover', 'birthday', 'biography', 'country', 'language')


class UserProfileSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_user_id')
    gender = serializers.CharField(source='get_gender_display')
    gender_id = serializers.IntegerField(source='gender')
    # country = serializers.CharField(source='get_country_display')
    # language = serializers.CharField(source='get_language_display')
    avatar = serializers.ReadOnlyField(source='hashed_avatar_id')
    cover = serializers.ReadOnlyField(source='hashed_cover_id')
    birthday = serializers.ReadOnlyField(source='get_birthday_display')

    class Meta:
        model = Profile
        fields = ('id', 'nick_name', 'gender', 'gender_id',
                  'avatar', 'cover', 'birthday', 'biography', 'country', 'language')


class AuthSerializer(serializers.ModelSerializer):
    session = serializers.CharField()
    uid = serializers.CharField()

    class Meta:
        model = User
        fields = ('uid', 'username', 'session')

    def get_full_name(self, obj):
        return obj.user.get_full_name()

    def get_username(self, obj):
        return obj.user.username

    def get_email(self, obj):
        return obj.user.email

    def get_user_id(self, obj):
        return obj.user.id


class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(many=False)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'profile', 'last_login')

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        user = User(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            # is_active = 1,
            # created_on = datetime.date.today(),
        )
        user.set_password(validated_data['password'])
        user.save()
        Profile.objects.create(user=user, **profile_data)
        return user

    def update(self, instance, validated_data):

        profile_data = validated_data.pop('profile')
        profile = instance.profile

        instance.set_password(validated_data.get('password', instance.password))
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.updated_on = datetime.date.today(),
        instance.email = validated_data.get('email', instance.email)
        instance.save()

        if hasattr(instance, 'profile'):
            profile = instance.profile
            profile.gender = profile_data.get('gender', profile.gender)
            profile.save()
        else:
            profile_new = Profile(user=instance,
                                  gender=profile_data['gender'])
            profile_new.save()

        return instance


class UserPublicFolderSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    person = serializers.ReadOnlyField(source='hashed_person')
    to_person = serializers.ReadOnlyField(source='hashed_to_person')
    action = serializers.CharField(source='get_action_display')
    permissions = serializers.CharField(source='get_permissions_display')
    is_accepted = serializers.BooleanField()
    content_type = serializers.CharField(source='content_type.model')
    content_object = UserFolderSerializer()

    class Meta:
        model = Share
        fields = ('id', 'person', 'to_person', 'action', 'permissions',
                  'is_accepted', 'updated_on', 'content_type', 'content_object')


class UserBookmarkSerializer(DynamicFieldsModelSerializer):
    id = serializers.CharField(source='hashed_id')
    name = serializers.CharField(source='title')

    class Meta:
        model = Bookmark
        fields = ('id', 'name', 'slug', 'url', 'status', 'created_on', 'updated_on')


class ProfileUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')


class ProfileSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_user_id')
    user = ProfileUserSerializer()
    gender = serializers.CharField(source='get_gender_display')
    gender_id = serializers.IntegerField(source='gender')
    # country = serializers.CharField(source='get_country_display')
    # language = serializers.CharField(source='get_language_display')
    avatar = serializers.ReadOnlyField(source='hashed_avatar_id')
    cover = serializers.ReadOnlyField(source='hashed_cover_id')
    birthday = serializers.ReadOnlyField(source='get_birthday_display')

    class Meta:
        model = Profile
        fields = ('id', 'nick_name', 'gender', 'gender_id',
                  'avatar', 'cover', 'birthday', 'biography', 'user', 'country', 'language')


class UserWallPostsSerializer(DynamicFieldsModelSerializer):
    id = serializers.CharField(source='hashed_id')
    owner = serializers.CharField(source='hashed_owner')
    poster = ProfileSerializer()
    name = serializers.CharField(source='title')

    class Meta:
        model = WallPost
        fields = ('id', 'owner', 'poster', 'name', 'content', 'updated_on')


class UserCommentsSerializer(DynamicFieldsModelSerializer):
    id = serializers.CharField(source='hashed_id')
    person = ProfileSerializer()
    name = serializers.CharField(source='title')
    content_type = serializers.CharField(source='content_type.model')
    content_object = MyItemSerializer()

    class Meta:
        model = Comment
        fields = ('id', 'person', 'name', 'content', 'content_type', 'updated_on', 'content_object')


class UserFriendsGroupsSerializer(DynamicFieldsModelSerializer):
    id = serializers.CharField(source='hashed_id')
    owner = serializers.CharField(source='hashed_owner')
    status = serializers.CharField(source='get_status_display')

    class Meta:
        model = FriendsGroup
        fields = ('id', 'owner', 'name', 'slug', 'is_user_defined', 'status', 'updated_on')


class UserTagCategorySerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    person = serializers.ReadOnlyField(source='hashed_person')
    content = serializers.ReadOnlyField(source='hashed_content')
    name = serializers.CharField(source='tag')

    class Meta:
        model = Tag
        fields = ('id', 'person', 'content', 'name', 'slug', 'is_user_defined')


class UserSocialAccountSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = UserSocialAuth
        fields = ('provider', 'uid', 'extra_data')


