import json
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import *
from .models import *


def json_response(response_dict, status=200):
    response = HttpResponse(json.dumps(response_dict), content_type="application/json", status=status)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    return response


source_category_db = FeedSource.categories.through._meta.db_table
source_db = FeedSource._meta.db_table
category_db = Category._meta.db_table
user_db = User._meta.db_table
post_db = Post._meta.db_table
bookmark_db = Bookmark._meta.db_table
post_category_db = Post.categories.through._meta.db_table
post_interest_db = Post.interests.through._meta.db_table
interest_db = Interest._meta.db_table
tag_db = Tag._meta.db_table
action_db = Action._meta.db_table
comment_db = Comment._meta.db_table
share_db = Share._meta.db_table
folder_db = Folder._meta.db_table
bookmark_folder = BookmarksInFolder._meta.db_table


def content_type_of(content):
    if content == 'profile':
        return ContentType.objects.get_for_model(Profile)
    elif content == 'folder':
        return ContentType.objects.get_for_model(Folder)
    elif content == 'bookmark':
        return ContentType.objects.get_for_model(Bookmark)
    elif content == 'category':
        return ContentType.objects.get_for_model(Category)
    elif content == 'interest':
        return ContentType.objects.get_for_model(Interest)
    elif content == 'post':
        return ContentType.objects.get_for_model(Post)
    elif content == 'feedsource':
        return ContentType.objects.get_for_model(FeedSource)
    elif content == 'feedsourceofuser':
        return ContentType.objects.get_for_model(FeedSourceOfUser)
    elif content == 'tag':
        return ContentType.objects.get_for_model(Tag)
    elif content == 'comment':
        return ContentType.objects.get_for_model(Comment)
    elif content == 'share':
        return ContentType.objects.get_for_model(Share)
    elif content == 'message':
        return ContentType.objects.get_for_model(Message)
    elif content == 'action':
        return ContentType.objects.get_for_model(Action)
    elif content == 'relationship':
        return ContentType.objects.get_for_model(Relationship)
    elif content == 'wallpost':
        return ContentType.objects.get_for_model(WallPost)
    elif content == 'photo':
        return ContentType.objects.get_for_model(Photo)
    else:
        return None


def action_id_of(action):
    """
    :param action: text
    :return: int
    """
    switcher = {
        'undefined': ACTION_UNDEFINED,
        'share': ACTION_SHARE,
        'like': ACTION_LIKE,
        'dislike': ACTION_DISLIKE,
        'unlike': ACTION_UNLIKE,
        'view': ACTION_VIEW,
        'share_social': ACTION_SHARE_SOCIAL,
        'download': ACTION_DOWNLOAD,
        'favourite': ACTION_FAVOURITE,
        'un-favourite': ACTION_UN_FAVOURITE,
    }
    return switcher.get(action, ACTION_UNDEFINED)


def gender_id(action):
    switcher = {
        'male': GENDER_MALE,
        'female': GENDER_FEMALE,
        'other': GENDER_OTHER,
    }
    return switcher.get(action, GENDER_OTHER)


def message_status_id(action):
    switcher = {
        'unread': MESSAGE_UNREAD,
        'read': MESSAGE_READ,
        'deleted': MESSAGE_DELETED,
        'important': MESSAGE_IMPORTANT,
    }
    return switcher.get(action, MESSAGE_UNREAD)


def relationship_status_id(action):
    switcher = {
        'friend_requested': RELATIONSHIP_FRIEND_REQUESTED,
        'friend': RELATIONSHIP_FRIEND,
        'blocked': RELATIONSHIP_BLOCKED,
        'following': RELATIONSHIP_FOLLOWING,
    }
    return switcher.get(action, RELATIONSHIP_FRIEND_REQUESTED)


def bookmark_status_id(action):
    switcher = {
        'none': BOOKMARK_NONE,
        'favourite': BOOKMARK_FAVOURITE,
        'important': BOOKMARK_IMPORTANT,
    }
    return switcher.get(action, BOOKMARK_NONE)


def share_action_id(action):
    switcher = {
        'none': SHARE_NONE,
    }
    return switcher.get(action, SHARE_NONE)


def share_permission_id(action):
    switcher = {
        'read': SHARE_READ,
        'write': SHARE_WRITE,
        'admin': SHARE_ADMIN,
        'public': SHARE_PUBLIC,
    }
    return switcher.get(action, SHARE_READ)


def get_photo(pk):
    try:
        photo = Photo.objects.get(pk=pk)
        if photo and photo.file:
            return str(photo.file)
    except ObjectDoesNotExist:
        pass
    return 'photos/default/default.jpg'


def get_paginator(list_object, items=10, page=1):
    paginator = Paginator(list_object, items)
    try:
        list_on_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        list_on_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page of results.
        list_on_page = paginator.page(paginator.num_pages)
    return list_on_page, paginator


def set_items(items, attr):
    output = []
    seen = set()
    for item in items:
        if item[attr] not in seen:
            output.append(item)
            seen.add(item[attr])
    return output


def set_items_value_list(items, attr):
    output = []
    seen = set()
    for item in items:
        if item[attr] not in seen:
            output.append(item[attr])
            seen.add(item[attr])
    return output


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

