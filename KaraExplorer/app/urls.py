from django.conf.urls import patterns, url, include
from . import views

layout_tpls = [
    url(r'^empty-content/$', views.empty_content, name='layout_empty_content_tpl'),
    url(r'^empty-sidebar/$', views.empty_sidebar, name='layout_empty_sidebar_tpl'),
    url(r'^footer/$', views.layout_footer, name='layout_footer_tpl'),
    url(r'^top-menu/$', views.layout_top_menu, name='layout_top_menu_tpl'),
    url(r'^right-sidebar/$', views.layout_right_sidebar, name='layout_right_sidebar_tpl'),
    url(r'^left-sidebar/$', views.layout_left_sidebar, name='layout_left_sidebar_tpl'),
    url(r'^feeds-sidebar/$', views.layout_feeds_sidebar, name='layout_feeds_sidebar_tpl'),
    url(r'^interests-sidebar/$', views.layout_interests_sidebar, name='layout_interests_sidebar_tpl'),
    url(r'^people-sidebar/$', views.layout_people_sidebar, name='layout_people_sidebar_tpl'),

    url(r'^webscreen-content/$', views.web_screen_content, name='layout_web_screen_content_tpl'),
    url(r'^linkdetail-content/$', views.link_detail_content, name='layout_link_detail_content_tpl'),
]

content_tpls = [
    url(r'^main/$', views.main_view, name='content_main_tpl'),
    url(r'^feed/$', views.feed_view, name='content_feed_tpl'),
    url(r'^interest/$', views.interest_view, name='content_interest_tpl'),
    url(r'^manage/$', views.manage_view, name='content_manage_tpl'),
    url(r'^people/$', views.people_view, name='content_people_tpl'),
    url(r'^profile/$', views.profile_view, name='content_profile_tpl'),
    url(r'^search/$', views.search, name='content_search_tpl'),
    url(r'^profile-setting/$', views.profile_setting_view, name='content_profile_setting_tpl'),
]

xenon_tpls = [
    url(r'^feed-manage/$', views.feed_manage_view, name='xenon_feed_manage_tpl'),
    url(r'^interest-manage/$', views.interest_manage_view, name='xenon_interest_manage_tpl'),

    url(r'^manage-card/$', views.manage_cards_view, name='xenon_manage_card_tpl'),
    url(r'^manage-list/$', views.manage_list_view, name='xenon_manage_list_tpl'),
    url(r'^manage-tagcloud/$', views.manage_tag, name='xenon_manage_tag_tpl'),
    url(r'^manage-content-tag/$', views.manage_content_tag, name='xenon_manage_content_tag_tpl'),

    url(r'^search-all/$', views.search_all, name='xenon_search_all_tpl'),
    url(r'^search-web/$', views.search_web, name='xenon_search_web_tpl'),
    url(r'^search-interest/$', views.search_interest, name='xenon_search_interest_tpl'),
    url(r'^search-bookmark/$', views.search_bookmark, name='xenon_search_bookmark_tpl'),
    url(r'^search-post/$', views.search_post, name='xenon_search_post_tpl'),

    url(r'^feed-card/$', views.feed_cards_view, name='xenon_feed_card_tpl'),
    url(r'^feed-list/$', views.feed_list_view, name='xenon_feed_list_tpl'),
    url(r'^feed-post-card/$', views.feed_post_cards_view, name='xenon_feed_post_card_tpl'),
    url(r'^feed-post-list/$', views.feed_post_list_view, name='xenon_feed_post_list_tpl'),

    url(r'^interest-card/$', views.interest_cards_view, name='xenon_interest_card_tpl'),
    url(r'^interest-list/$', views.interest_list_view, name='xenon_interest_list_tpl'),
    url(r'^interest-list/$', views.interest_list_view, name='xenon_interest_list_tpl'),
]

popup_tpls = [
    url(r'^linkdetail/$', views.linkdetail, name='app_link_detail'),
    url(r'^webscreen/$', views.webscreen, name='app_web_screen'),
    url(r'^editfeed/$', views.editfeed, name='app_edit_feed'),
    url(r'^add-browser-interests/$', views.add_browser_interests, name='app_add_browser_interests'),

    url(r'^addlink/$', views.addlink, name='app_addlink'),
    url(r'^addtag/$', views.add_tag, name='app_addtag'),
    url(r'^backup/$', views.backup, name='app_backup'),
    url(r'^bookmark/$', views.bookmark, name='app_bookmark'),
    url(r'^collaborate/$', views.collaborate, name='app_collaborate'),
    url(r'^createfeedfolder/$', views.createfeedfolder, name='app_create_feed_folder'),
    url(r'^createfolder/$', views.createfolder, name='app_create_folder'),
    url(r'^createlink/$', views.createlink, name='app_create_link'),
    url(r'^create-user-category/$', views.create_user_category, name='app_create_user_category'),
    url(r'^delete/$', views.delete, name='app_delete'),
    url(r'^follow/$', views.follow, name='app_follow'),
    url(r'^privacy/$', views.privacy, name='app_privacy'),
    url(r'^rename/$', views.rename, name='app_rename'),
    url(r'^rename-bookmark/$', views.rename_bookmark, name='app_bookmark_rename'),
    url(r'^rename-user-feed-source/$', views.rename_user_feed_source, name='app_rename_user_feed_source'),
    url(r'^rename-user-category/$', views.rename_user_category, name='app_rename_user_category'),
    url(r'^rename-tag/$', views.rename_tag, name='app_rename_tag'),
    url(r'^search/$', views.search, name='app_search'),
    url(r'^share/$', views.share, name='app_share'),
    url(r'^subscribe/$', views.subscribe, name='app_subscribe'),
    url(r'^tags/$', views.tags, name='app_tags'),
    url(r'^votedown/$', views.votedown, name='app_votedown'),
    url(r'^show-static/$', views.show_static, name='app_show_static'),
    url(r'^change-password/$', views.change_password, name='app_change_password'),
    url(r'^reset-password/$', views.reset_password, name='app_reset_password'),
    url(r'^change-cover/$', views.change_cover, name='app_change_cover'),
    url(r'^select_dest_folder/$', views.select_dest_folder, name='app_select_dest_folder'),
]

include_tpls = [
    url(r'^bookmark-card/$', views.bookmark_card, name='xenon_include_bookmark_card_tpl'),
    url(r'^bookmark-list/$', views.bookmark_list, name='xenon_include_bookmark_list_tpl'),
    url(r'^folder-card/$', views.folder_card, name='xenon_include_folder_card_tpl'),
    url(r'^folder-list/$', views.folder_list, name='xenon_include_folder_list_tpl'),

    url(r'^feed-card/$', views.feed_card, name='xenon_include_feed_card_tpl'),
    url(r'^feed-list/$', views.feed_list, name='xenon_include_feed_list_tpl'),
    url(r'^feed-post-card/$', views.feed_post_card, name='xenon_include_feed_post_card_tpl'),
    url(r'^feed-post-list/$', views.feed_post_list, name='xenon_include_feed_post_list_tpl'),

    url(r'^persons-list/$', views.persons_list, name='xenon_include_persons_list_tpl'),
    url(r'^persons-list-request-sent/$',
        views.person_list_request_sent, name='xenon_include_person_list_request_sent_tpl'),
    url(r'^add-people/$', views.add_people_view, name='xenon_add_people_tpl'),
    url(r'^manage-people/$', views.manage_people_view, name='xenon_manage_people_view_tpl'),

    url(r'^manage-following-list/$', views.manage_following_list, name='xenon_include_manage_following_list_tpl'),
    url(r'^manage-people-folder/$', views.manage_people_folder, name='xenon_include_manage_people_folder_tpl'),
    url(r'^people-card/$', views.people_card, name='xenon_include_people_card_tpl'),
    url(r'^people-list/$', views.people_list, name='xenon_include_people_list_tpl'),
    url(r'^people-post-card/$', views.people_post_card, name='xenon_include_people_post_card_tpl'),
    url(r'^people-post-list/$', views.people_post_list, name='xenon_include_people_post_list_tpl'),

    url(r'^interest-card/$', views.interest_card, name='xenon_include_interest_card_tpl'),
    url(r'^feed-source-list/$', views.feed_source_list, name='xenon_include_feedsource__list_tpl'),
    url(r'^interest-list/$', views.interest_list, name='xenon_include_interest_list_tpl'),
    url(r'^interest-post-card/$', views.interest_post_card, name='xenon_include_interest_post_card_tpl'),
    url(r'^interest-post-list/$', views.interest_post_list, name='xenon_include_interest_post_list_tpl'),

    url(r'^feed-popup-list/$', views.feed_popup_post_list, name='xenon_include_feed_popup_post_list_tpl')
]

templates_feeds = [
    url(r'^card/$', views.tpl_cards, name='feed_card_tpl'),
    url(r'^add-feed-source/$', views.tpl_add_feed_source, name='feed_add_source_tpl'),
]

templates_interests = [
    url(r'^add-interests/$', views.tpl_add_interest, name='interest_add_interest_tpl'),
]

templates_manage = [
    url(r'^tagcloud/$', views.tpl_tagcloud, name='manage_tag_cloud_tpl'),
    url(r'^detectbrokenlink/$', views.tpl_detectbrokenlink, name='manage_link_broken_tpl'),
    url(r'^detectduplicationlink/$', views.tpl_detectduplicationlink, name='manage_link_duplication_tpl'),
]

templates_profile = [
    url(r'^wall/$', views.tpl_wall, name='profile_wall_tpl'),
    url(r'^folder/$', views.tpl_folder, name='profile_folder_tpl'),
    url(r'^link/$', views.tpl_link, name='profile_link_tpl'),
    url(r'^web/$', views.tpl_web, name='profile_web_tpl'),
    url(r'^interest/$', views.tpl_interest, name='profile_interest_tpl'),
    url(r'^people/$', views.tpl_people, name='profile_people_tpl'),
    url(r'^dashboard/$', views.tpl_dashboard, name='profile_dashboard_tpl'),
]

urlpatterns = patterns('',
    url(r'^(?i)templates/layout/', include(layout_tpls)),
    url(r'^(?i)templates/content/', include(content_tpls)),
    url(r'^(?i)templates/xenon/', include(xenon_tpls)),
    url(r'^(?i)templates/popup/', include(popup_tpls)),
    url(r'^(?i)templates/include/', include(include_tpls)),
    url(r'^(?i)templates/feeds/', include(templates_feeds)),
    url(r'^(?i)templates/interests/', include(templates_interests)),
    url(r'^(?i)templates/manage/', include(templates_manage)),
    url(r'^(?i)templates/profile/', include(templates_profile)),
)


