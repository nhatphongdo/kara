from hashids import Hashids

hash_salt = 'zlTpa5ABTZCGiGqW lVB1FrUeExRhDc3gU'
hashids = Hashids(salt=hash_salt, min_length=8)

HASH_ACTION             = 38980
HASH_BOOKMARK           = 82498
HASH_CATEGORY           = 42248
HASH_COMMENT            = 10531
HASH_FEEDSOURCE         = 55971
HASH_FOLDER             = 79353
HASH_INTEREST           = 75527
HASH_MESSAGE            = 20535
HASH_POST               = 99177
HASH_RELATIONSHIP       = 91431
HASH_SHARE              = 71682
HASH_TAG                = 79855
HASH_UPLOADFILE         = 79906
HASH_WALLPOST           = 11136
HASH_INTERESTOFUSER     = 28153
HASH_FEEDSOURCEOFUSER   = 63937
HASH_PROFILE            = 11018
HASH_BOOKMARKINFOLDER   = 14232
HASH_SESSION            = 70044
HASH_PHOTO              = 25130
HASH_FRIENDSGROUP       = 92384
#86858


def encodeNumber(type, num):
    if isinstance(num, str):
        num = int(num)

    if isinstance(type, str):
        num = (num,)
        for c in type:
            num += (ord(c),)
        return hashids.encrypt(*num)
    elif isinstance(type, int):
        return hashids.encrypt(num, type)
    else:
        return hashids.encrypt(num)


def decodeNumber(str):
    nums = hashids.decrypt(str)
    if len(nums) == 0:
        raise ValueError('encoded string is not valid')
    else:
        return nums[0]
