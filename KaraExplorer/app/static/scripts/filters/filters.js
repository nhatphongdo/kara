'use strict';

/* Filters */

angular.module('filters', [])
    .filter('checkmark', function () {
        return function (input) {
            return input ? '\u2713' : '\u2718';
        };
    })
    .filter('youtube', function () {
        return function (input) {
            return 'http://' + input.replace("watch?v=", "embed/");
        };
    })
    .filter('truncate', function () {
        return function (text, length, end) {
            if (text == undefined) {
                return '';
            }
            if (isNaN(length)) {
                length = 66;
            }

            if (end === undefined) {
                end = "...";
            }

            if (text.length <= length || text.length - end.length <= length) {
                return text;
            }
            else {
                return String(text).substring(0, length - end.length) + end;
            }

        };
    })
    .filter('trusted', ['$sce', function ($sce) {
        return function (url) {
            return $sce.trustAsResourceUrl(url);
        };
    }])
    .filter('unsafe', function ($sce) {
        return $sce.trustAsHtml;
    })
    .filter('truncateword', [
        function () {
            return function (input, words) {
                if (isNaN(words)) return input;
                if (words <= 0) return '';
                if (input) {
                    var inputWords = input.split(/\s+/);
                    if (inputWords.length > words) {
                        input = inputWords.slice(0, words).join(' ') + ' ...';
                    }
                }
                return input;
            };
        }
    ])
    .filter('domain', function () {
        return function (input) {
            var output = "",
                matches;

            var urls = /\w+:\/\/([\w|\.]+)/;
            matches = urls.exec(input);

            if (matches !== null) {
                output = matches[1];
            }

            return output;
        };
    })
    .filter('http', function () {
        return function (input) {
            var http = /^(https?|ftp):\/\//i;
            if (!http.test(input)) {
                input = 'http://' + input;
            }
            return input;
        };
    })
    .filter('shortNameDomain', function () {
        return function (input) {
            var output = "",
                matches;

            var urls = /(www\.)?([\w|\.]+)+(\.[\w]{2,3}){1,2}/;
            matches = urls.exec(input);

            if (matches !== null) {
                output = matches[2];
            }

            return output;
        };
    })
    //sort object
    .filter('reverse', function () {
        return function (items) {
            if (items) {
                return items.slice().reverse();
            }
            else {
                return null;
            }
        };
    })
    .filter('getInterest', function () {
        return function (array, interest) {
            if (interest.id != null) {
                var obj = $.grep(array, function (item) {
                    return item.id === interest.id;
                });
                if (obj.length == 1) {
                    return obj[0].name;
                }
                else {
                    return interest.name;
                }
            }
            else {
                return array[0].name;
            }
        };
    })
    .filter('abs', function () {
        return function (val) {
            return Math.abs(val);
        }
    })
;


