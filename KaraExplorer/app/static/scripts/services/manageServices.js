﻿'use strict';

/*
 *  Services
 *
 *  Usage
 *  $resource(url, [paramDefaults], [actions], options);
 *
 *  Arguments
 *  url                         string
 *      '/path/:verb'
 *  paramDefaults (optional)    Object
 *       {verb:'greet', salutation:'Hello'} results in URL /path/greet?salutation=Hello.
 *  actions (optional)          Object.<Object>=
 *      {action1: {method:?, params:?, isArray:?, headers:?, ...},
 *      action2: {method:?, params:?, isArray:?, headers:?, ...},
 *      ...}
 *      Where:
 *          action – {string} – The name of action.
 *          method – {string} – Case insensitive HTTP method (e.g. GET, POST, PUT, DELETE, JSONP, etc).
 *          params – {Object=} – Optional set of pre-bound parameters for this action.
 *          url – {string} – action specific url override.
 *          isArray – {boolean=} – If true then the returned object for this action is an array.
 *          transformRequest – {function(data, headersGetter)|Array.<function(data, headersGetter)>} – transform function or an array of such functions.
 *          transformResponse – {function(data, headersGetter)|Array.<function(data, headersGetter)>} – transform function or an array of such functions.
 *          cache – {boolean|Cache} – If true, a default $http cache will be used to cache the GET request, otherwise if a cache instance built with $cacheFactory, this cache will be used for caching.
 *          timeout – {number|Promise} – timeout in milliseconds, or promise that should abort the request when resolved.
 *          withCredentials - {boolean} - whether to set the withCredentials flag on the XHR object. See requests with credentials for more information.
 *          responseType - {string} - see requestType.
 *          interceptor - {Object=} - The interceptor object has two optional methods - response and responseError.
 *  options                     Object
 *  */

angular.module('manage.services', [])
    .factory('FolderServices', function ($resource) {
        var api_user_object_folders = '/api/folders/';
        var api_folder              = '/api/folder/';
        return {
            folders: $resource(api_user_object_folders + ':username/',
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    structure: { method: 'POST',
                        url: api_user_object_folders + ':username/structure/'},
                    of_object: { method: 'POST',
                        url: api_user_object_folders + ':username/:pk/',
                        params: { username: '@username', pk: '@pk' }},
                    of_object_on_page: { method: 'POST',
                        url: api_user_object_folders + ':username/:pk/page:page/items:items',
                        params: { username: '@username', pk: '@pk', page: '@page' }},
                    of_object_on_page_with_items: { method: 'POST',
                        url: api_user_object_folders + ':username/:pk/page:page/items:items',
                        params: { username: '@username', pk: '@pk', page: '@page', items: '@items' }}
                }),
            folder: $resource(api_folder + ':username/',
                null,
                {
                    info: { method: 'POST',
                        url: api_folder + ':username/' + ':pk/',
                        params: { username: '@username', pk: '@pk' }},
                    create: { method: 'POST',
                        url: api_folder + ':username/' + 'add-new-to/' + ':parent/',
                        params: { username: '@username', parent: '@parent' }},
                    update: { method: 'POST',
                        url: api_folder + ':username/' + ':pk/' + ':parent/rename/',
                        params: { username: '@username', pk: '@pk', parent: '@parent' }},
                    delete: { method: 'DELETE',
                        url: api_folder + ':username/' + ':pk/' + 'delete/',
                        params: { username: '@username', pk: '@pk' }},
                    copy: { method: 'PUT',
                        url: api_folder + ':username/' + ':pk/copy-to/' + ':parent/',
                        params: { username: '@username', pk: '@pk', parent: '@parent' }},
                    move: { method: 'PUT',
                        url: api_folder + ':username/' + ':pk/move-to/' + ':parent/',
                        params: { username: '@username', pk: '@pk', parent: '@parent' }}
                })
        }
    })
    .factory('BookmarkServices', function ($resource) {
        var api_url_bookmarks       = '/api/bookmarks/';
        var api_url_bookmark_detail = '/api/bookmark/';
        return {
            bookmarks: $resource(api_url_bookmarks + ':username/' + ':folder',
                null,
                {
                    get: { method: 'POST',
                        params: { username: '@username', folderID: '@folderID' }},
                    page: { method: 'POST',
                        url: api_url_bookmarks + ':username/' + ':folderID/' + 'page:page/',
                        params: { username: '@username', folderID: '@folderID', page: '@page' }},
                    page_item: { method: 'POST',
                        url: api_url_bookmarks + ':username/' + ':folderID/' + 'page:page/' + 'items:items/',
                        params: { username: '@username', folderID: '@folderID', page: '@page', items: '@items' }}
                }),
            bookmarkDetail: $resource(api_url_bookmark_detail + ':username/',
                null,
                {
                    create: { method: 'POST',
                        url: api_url_bookmark_detail + ':username/' + 'add-new-to/' + ':folder/',
                        params: { username: '@username', folder: '@folder' }},
                    update: { method: 'POST',
                        url: api_url_bookmark_detail + ':username/' + ':pk/' + ':folder/rename/',
                        params: { username: '@username', pk: '@pk', folder: '@folder' }},
                    delete: { method: 'DELETE',
                        url: api_url_bookmark_detail + ':username/' + ':pk/' + ':folder/delete/',
                        params: { username: '@username', pk: '@pk', folder: '@folder' }},
                    copy: {
                        method: 'PUT',
                        url: api_url_bookmark_detail + ':username/' + ':pk/copy-to/' + ':folder/',
                        params: { username: '@username', pk: '@pk', folder: '@folder' }},
                    move: {
                        method: 'PUT',
                        url: api_url_bookmark_detail + ':username/' + ':pk/move-to/' + ':folder/',
                        params: { username: '@username', pk: '@pk', folder: '@folder' }}
                })
        }
    })
    .factory('ImportBookmark', function ($http) {
        var api_url_import_bookmark = '/api/importbookmarks/';
        return {
            uploadFile: function (fd, username) {
                var url = api_url_import_bookmark + username + "/";
                return $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                    .then(function (data) {
                        return data;
                    });
            }
        }
    })
    .factory('ManageServices', function ($resource) {
        var api_url_folder_bookmark = '/api/folderbookmark/';
        var api_add_bookmark        = 'api/addbookmark/';
        var api_selected            = 'api/selected/' + ':username/';
        return {
            folderBookmark: $resource(api_url_folder_bookmark + ':username/' + ':folder/',
                null,
                {
                    page: { method: 'POST',
                        url: api_url_folder_bookmark + ':username/' + ':folder/' + 'page:page/',
                        params: { username: '@username', folder: '@folder', page: '@page' }},
                    page_item: { method: 'POST',
                        url: api_url_folder_bookmark + ':username/' + ':folder/' + 'page:page/' + 'items:items/',
                        params: { username: '@username', folder: '@folder', page: '@page', items: '@items' }}
                }),
            addBookmark: $resource(api_add_bookmark + ':username/' + ':pk/' + ':folder/',
                null,
                {
                    default: { method: 'POST',
                        params: { username: '@username', pk: '@pk', folder: '@folder' }}
                }),
            selected: $resource(api_selected,
                { username: '@username' },
                {
                    delete: { method: 'POST',
                        url: api_selected + 'delete/'},
                    move_to: { method: 'POST',
                        url: api_selected + 'move-to/' + ':folder/',
                        params: { folder: '@folder' }},
                    copy_to: { method: 'POST',
                        url: api_selected + 'copy-to/' + ':folder/',
                        params: { folder: '@folder' }}
                })
        }
    })
;


