'use strict';

angular.module('layout.services', ['ngCookies'])
    .service('$menuItems', function (GlobalService) {
        this.menuItems = [];
        var menuItemObj = {
            parent: null,
            data: null,
            type: 'custom',
            id: undefined,
            slug: 'all',
            title: 'All',
            ref: 'home',
            state: 'home',
            params: {viewType: GlobalService.settings.viewType, pk: 'None'},

            isActive: false,
            isOpen: false,
            isRoot: false,
            level: 0,
            numb: {posts: 0},

            menuItems: [],

            clearType: function (type) {
                if (type === undefined) {
                    while (this.menuItems.length > 0) {
                        this.menuItems.pop();
                    }
                }
                else {
                    for (var i = 0; i < this.menuItems.length; i++) {
                        if (this.menuItems[i].type == type) {
                            if (this.menuItems.splice(i, 1)) {
                                i--;
                            }
                        }
                    }
                }
                return this;
            },

            addItem: function (item, state, params) {
                var title = item.nick_name || item.alias_name || item.name || item.title || item.tag || item,
                    itemObj = angular.extend(
                        angular.copy(menuItemObj),
                        {
                            title: title,
                            state: state,
                            params: params || {}
                        },
                        item);

                itemObj.params = angular.merge(
                    angular.copy(menuItemObj).params,
                    itemObj.params
                );
                itemObj.ref = state + '(' + JSON.stringify(itemObj.params) + ')';
                itemObj.parent = this;
                itemObj.data = item;
                itemObj.level = itemObj.parent ? itemObj.parent.level + 1 : 0;
                itemObj.isRoot = $.type(item) === "string";

                this.menuItems.push(itemObj);
                return itemObj;
            },

            isItem: function (type, id) {
                return (this.type == type && (this.id == id || this.slug == id || this.title == id))
            },

            updateItem: function () {
                var item = this;
                item = angular.extend(
                    item,
                    item.data);
                item.title = this.nick_name || this.alias_name || this.name || this.title || this.tag;
                return item;
            }
        };

        this.clearType = function (type) {
            if (type === undefined) {
                while (this.menuItems.length > 0) {
                    this.menuItems.pop();
                }
            }
            else {
                for (var i = 0; i < this.menuItems.length; i++) {
                    if (this.menuItems[i].type == type) {
                        if (this.menuItems.splice(i, 1)) {
                            i--;
                        }
                    }
                }
            }
            return this;
        };

        this.addItem = function (item, state, params) {
            var title = item.nick_name || item.alias_name || item.name || item.title || item.tag || item,
                itemObj = angular.extend(
                    angular.copy(menuItemObj),
                    {
                        title: title,
                        state: state,
                        params: params || {}
                    },
                    item);

            itemObj.params = angular.merge(
                angular.copy(menuItemObj).params,
                itemObj.params
            );
            itemObj.ref = state + '(' + JSON.stringify(itemObj.params) + ')';
            //itemObj.parent = this;
            itemObj.data = item;
            itemObj.level = itemObj.parent ? itemObj.parent.level + 1 : 0;
            itemObj.isRoot = $.type(item) === "string";

            this.menuItems.push(itemObj);
            return itemObj;
        };

        this.isItem = function (type, id) {
            return (item.type == type && (item.id == id || item.slug == id || item.title == id))
        };

        this.updateItem = function () {
            var item = this;
            item = angular.extend(
                item,
                item.data);
            item.title = this.nick_name || this.alias_name || this.name || this.title || this.tag;
            return item;
        };

        this.getAll = function () {
            return this.menuItems;
        };

        this.instantiate = function () {
            return angular.copy(this);
        };

        // default menu
        this.prepareSidebarMenu = function () {
            return this;
        };

        this.prepareHorizontalMenu = function () {
            this.addItem('Feed', 'feeds');
            this.addItem('Interest', 'interests');
            this.addItem('Bookmark', 'manage');
            //this.addItem('People', 'people');
            return this;
        };

        // load active menu by breadcrumbs
        // set Active
    })
    .factory('$layoutToggles', function ($rootScope, $layout) {
        return {
            initToggles: function () {
                // Sidebar Toggle
                $rootScope.sidebarToggle = function () {
                    $layout.setOptions('sidebar.isCollapsed', !$rootScope.layoutOptions.sidebar.isCollapsed);
                };

                // Right Sidebar Toggle
                $rootScope.rightSidebarToggle = function () {
                    $layout.setOptions('rightSidebar.isOpen', !$rootScope.layoutOptions.rightSidebar.isOpen);
                };

                $rootScope.$watch('layoutOptions.sidebar.isCollapsed', function (newVal, oldVal) {
                    if (newVal !== oldVal && public_vars.$sidebarMenu) {
                        if (newVal == true) {
                            public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar('destroy')
                        }
                        else {
                            public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar({wheelPropagation: public_vars.wheelPropagation});
                        }
                    }
                });
            }
        };
    })
    .factory('$pageLoadingBar', function ($rootScope, $window, GlobalService) {
        return {
            init: function () {
                var pl = this;

                $window.showLoadingBar = this.showLoadingBar;
                $window.hideLoadingBar = this.hideLoadingBar;

                function startLoading() {
                    pl.showLoadingBar({
                        pct: 87,
                        delay: 1.1,
                        resetOnEnd: false
                    });
                }

                $rootScope.$on('$stateChangeStart', function () {
                    console.log('$stateChangeStart', '$pageLoadingBar', $.now());
                    startLoading();
                });

                function endLoading() {
                    pl.showLoadingBar({
                        pct: 100,
                        delay: .65,
                        resetOnEnd: true
                    });
                }

                $rootScope.$on('$stateChangeSuccess', function () {
                    console.log('$stateChangeSuccess', '$pageLoadingBar', $.now());
                    if (!GlobalService.is_loading.length)
                        endLoading();
                });

                $rootScope.$on('$stateNotFound', function () {
                    console.log('$stateNotFound', '$pageLoadingBar', $.now());
                    endLoading();
                    GlobalService.is_loading = [];
                });

                $rootScope.$on('$stateChangeError', function () {
                    console.log('$stateChangeError', '$pageLoadingBar', $.now());
                    endLoading();
                    GlobalService.is_loading = [];
                });
            },

            showLoadingBar: function (options) {
                var defaults = {
                    pct: 0,
                    delay: 1.3,
                    wait: 0,
                    before: function () {
                    },
                    finish: function () {
                    },
                    resetOnEnd: true
                };

                if (typeof options == 'object')
                    defaults = jQuery.extend(defaults, options);
                else if (typeof options == 'number')
                    defaults.pct = options;

                if (defaults.pct > 100)
                    defaults.pct = 100;
                else if (defaults.pct < 0)
                    defaults.pct = 0;

                var $ = jQuery,
                    $loading_bar = $(".xenon-loading-bar");

                if ($loading_bar.length == 0) {
                    $loading_bar = $('<div class="xenon-loading-bar progress-is-hidden"><span data-pct="0"></span></div>');
                    public_vars.$body.append($loading_bar);
                }

                var $pct = $loading_bar.find('span'),
                    current_pct = $pct.data('pct'),
                    is_regress = current_pct > defaults.pct;
                defaults.before(current_pct);

                TweenMax.to($pct, defaults.delay, {css: {width: defaults.pct + '%'}, delay: defaults.wait, ease: is_regress ? Expo.easeOut : Expo.easeIn,
                    onStart: function () {
                        $loading_bar.removeClass('progress-is-hidden');
                    },
                    onComplete: function () {
                        var pct = $pct.data('pct');
                        if (pct == 100 && defaults.resetOnEnd) {
                            hideLoadingBar();
                        }
                        defaults.finish(pct);
                    },
                    onUpdate: function () {
                        $pct.data('pct', parseInt($pct.get(0).style.width, 10));
                    }});
            },

            hideLoadingBar: function () {
                var $ = jQuery,
                    $loading_bar = $(".xenon-loading-bar"),
                    $pct = $loading_bar.find('span');
                $loading_bar.addClass('progress-is-hidden');
                $pct.width(0).data('pct', 0);
            }
        };
    })
    .factory('$layout', function ($rootScope, $cookies, $cookieStore) {
        return {
            propsToCache: [
                'sidebar.isCollapsed',
                'rightSidebar.isOpen',
                'view'
            ],

            setOptions: function (options, the_value) {
                if (typeof options == 'string' && typeof the_value != 'undefined') {
                    options = this.pathToObject(options, the_value);
                }

                jQuery.extend(true, $rootScope.layoutOptions, options);

                this.saveCookies();
            },

            saveCookies: function () {
                var cookie_entries = this.iterateObject($rootScope.layoutOptions, '', {});

                angular.forEach(cookie_entries, function (value, prop) {
                    $cookies.put(prop, value);
                });
            },

            resetCookies: function () {
                var cookie_entries = this.iterateObject($rootScope.layoutOptions, '', {});

                angular.forEach(cookie_entries, function (value, prop) {
                    $cookies.remove(prop);
                });
            },

            loadOptionsFromCookies: function () {
                var dis = this,
                    cookie_entries = dis.iterateObject($rootScope.layoutOptions, '', {}),
                    loaded_props = {};

                angular.forEach(cookie_entries, function (value, prop) {
                    var cookie_val = $cookies.get(prop);

                    if (typeof cookie_val != 'undefined') {
                        jQuery.extend(true, loaded_props, dis.pathToObject(prop, cookie_val));
                    }
                });

                jQuery.extend($rootScope.layoutOptions, loaded_props);
            },

            is: function (prop, value) {
                var cookieval = this.get(prop);

                return cookieval == value;
            },

            get: function (prop) {
                var cookieval = $cookies.get(prop);

                if (cookieval && cookieval.match(/^true|false|[0-9.]+$/)) {
                    cookieval = eval(cookieval);
                }

                if (!cookieval) {
                    cookieval = this.getFromPath(prop, $rootScope.layoutOptions);
                }

                return cookieval;
            },

            getFromPath: function (path, lo) {
                var val = '',
                    current_path,
                    paths = path.split('.');

                angular.forEach(paths, function (path_id, i) {
                    var is_last = paths.length - 1 == i;

                    if (!current_path)
                        current_path = lo[path_id];
                    else
                        current_path = current_path[path_id];

                    if (is_last) {
                        val = current_path;
                    }
                });

                return val;
            },

            pathToObject: function (obj_path, the_value) {
                var new_obj = {},
                    curr_obj = null,
                    last_key;

                if (obj_path) {
                    var paths = obj_path.split('.'),
                        depth = paths.length - 1,
                        array_scls = '';

                    angular.forEach(paths, function (path_id, i) {
                        var is_last = paths.length - 1 == i;

                        array_scls += '[\'' + path_id + '\']';

                        if (is_last) {
                            if (typeof the_value == 'string' && !the_value.toString().match(/^true|false|[0-9.]+$/)) {
                                the_value = '"' + the_value + '"';
                            }

                            eval('new_obj' + array_scls + ' = ' + the_value + ';');
                        }
                        else
                            eval('new_obj' + array_scls + ' = {};');
                    });
                }

                return new_obj;
            },

            iterateObject: function (objects, append, arr) {
                var dis = this;

                angular.forEach(objects, function (obj, key) {
                    if (typeof obj == 'object') {
                        return dis.iterateObject(obj, append + key + '.', arr);
                    }
                    else if (typeof obj != 'undefined') {
                        arr[append + key] = obj;
                    }
                });

                // Filter Caching Objects
                angular.forEach(arr, function (value, prop) {
                    if (!inArray(prop, dis.propsToCache))
                        delete arr[prop];
                });

                function inArray(needle, haystack) {
                    var length = haystack.length;
                    for (var i = 0; i < length; i++) {
                        if (haystack[i] == needle) return true;
                    }
                    return false;
                }

                return arr;
            }
        };
    })
    .factory('$utils', function () {
        return {
            // Util for finding an object by its 'id' property among an array
            findById: function findById(a, id) {
                for (var i = 0; i < a.length; i++) {
                    if (a[i].id == id) {
                        return a[i];
                    }
                }
                return null;
            },

            // Util for returning a random key from a collection that also isn't the current key
            newRandomKey: function newRandomKey(coll, key, currentKey) {
                var randKey;
                do {
                    randKey = coll[Math.floor(coll.length * Math.random())][key];
                } while (randKey == currentKey);
                return randKey;
            },

            grepOut: function grepOut(arr, name) {
                return jQuery.grep(arr, function (value) {
                    return value != name;
                });
            }
        };
    })
;


