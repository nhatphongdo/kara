﻿'use strict';

angular.module('feeds.services', [])
    .factory('FeedsServices', function ($resource) {
        var api_categories      = 'api/categories/';
        var api_feeds_category  = 'api/category/';
        var api_feeds_source    = 'api/source/';
        return {
            categories: $resource(api_categories,
                null,
                {
                    default: { method: 'POST'},
                    structure: { method: 'POST',
                        url: api_categories + 'structure/'}
                }),
            category: $resource(api_feeds_category,
                null,
                {
                    info: { method: 'POST',
                        url: api_feeds_category + ':pk/',
                        params: { pk: '@pk'}},
                    posts: { method: 'POST',
                        url: api_feeds_category + ':pk/posts/',
                        params: { pk: '@pk'}},
                    posts_page: { method: 'POST',
                        url: api_feeds_category + ':pk/posts/' + 'page:page/',
                        params: { pk: '@pk', page: '@page' }},
                    posts_page_item: { method: 'POST',
                        url: api_feeds_category + ':pk/posts/' + 'page:page/' + 'items:items/',
                        params: { pk: '@pk', page: '@page', items: '@items' }},
                    sources: { method: 'POST',
                        url: api_feeds_category + ':pk/sources/',
                        params: { pk: '@pk'}},
                    sources_page: { method: 'POST',
                        url: api_feeds_category + ':pk/sources/' + 'page:page/',
                        params: { pk: '@pk', page: '@page' }},
                    sources_page_item: { method: 'POST',
                        url: api_feeds_category + ':pk/sources/' + 'page:page/' + 'items:items/',
                        params: { pk: '@pk', page: '@page', items: '@items' }}
                }),
            source: $resource(api_feeds_source,
                null,
                {
                    info: { method: 'POST',
                        url: api_feeds_source + ':src/',
                        params: { src: '@src'}},
                    key: { method: 'POST',
                        url: api_feeds_source + 'key-:key/',
                        params: { key: '@key' }},
                    key_page: { method: 'POST',
                        url: api_feeds_source + 'key-:key/' + 'page:page/',
                        params: { key: '@key', page: '@page' }},
                    key_page_item: { method: 'POST',
                        url: api_feeds_source + 'key-:key/' + 'page:page/' + 'items:items/',
                        params: { key: '@key', page: '@page', items: '@items' }},
                    contents: { method: 'POST',
                        url: api_feeds_source + ':src/contents/',
                        params: { src: '@src'}},
                    contents_page: { method: 'POST',
                        url: api_feeds_source + ':src/contents/' + 'page:page/',
                        params: { src: '@src', page: '@page' }},
                    contents_page_item: { method: 'POST',
                        url: api_feeds_source + ':src/contents/' + 'page:page/' + 'items:items/',
                        params: { src: '@src', page: '@page', items: '@items' }}
                })
        }
    })
;


