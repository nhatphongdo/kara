﻿'use strict';

angular.module('interest.services', [])
    .factory('InterestServices', function ($resource) {
        var api_structure_interests = 'api/interests/structure/';
        var api_main_interests      = 'api/interests/main/';
        var api_child_interests_pk  = 'api/interests/:pk/';
        var api_child_interests_key = 'api/interests/key-:key/';
        var api_interest            = 'api/interest/';
        return {
            interests: $resource(api_structure_interests,
                null,
                {
                    tree: { method: 'POST'},
                    main: { method: 'POST',
                        url: api_main_interests},
                    child: { method: 'POST',
                        url: api_child_interests_pk,
                        params: { pk: '@pk'}},
                    key: { method: 'POST',
                        url: api_child_interests_key,
                        params: { key: '@key' }},
                    key_page: { method: 'POST',
                        url: api_child_interests_key + 'page:page/',
                        params: { key: '@key', page: '@page' }},
                    key_page_item: { method: 'POST',
                        url: api_child_interests_key + 'page:page/' + 'items:items/',
                        params: { key: '@key', page: '@page', items: '@items' }}
                }),
            interest: $resource(api_interest,
                null,
                {
                    info: { method: 'POST',
                        url: api_interest + ':pk/',
                        params: { pk: '@pk'}},
                    posts_page: { method: 'POST',
                        url: api_interest + ':pk/posts/' + 'page:page/',
                        params: { pk: '@pk', page: '@page' }},
                    posts_page_item: { method: 'POST',
                        url: api_interest + ':pk/posts/' + 'page:page/' + 'items:items/',
                        params: { pk: '@pk', page: '@page', items: '@items' }},
                    pin: { method: 'POST',
                        url: api_interest + ':username/pin/:pk/',
                        params: { username: '@username', pk: '@pk'}}
                })
        }
    })
;


