'use strict';

angular.module('auth.services', [])
    .factory('AuthServices', function ($resource) {
        var api_authenticate    = '/api/auth/';
        var api_register        = '/api/register/';
        var api_register_verify = '/api/register-verify/';
        var api_reset_password  = '/api/reset-password/';
        var api_user_profile    = '/api/profile/' + ':username/';
        var soc_auth_login      = '/login/' + ':backend/';
        var soc_auth_disconnect = '/disconnect/' + ':backend/';
        return {
            auth: $resource(api_authenticate,
                null,
                {
                    login: { method: 'POST'},
                    verify: { method: 'PUT'},
                    logout: { method: 'DELETE'},
                    join: { method: 'POST',
                        url: api_register},
                    join_verify: { method: 'POST',
                        url: api_register_verify + ':session_key/' + ':email/',
                        params: { session_key: '@session_key', email: '@email' }},
                    reset_password: { method: 'POST',
                        url: api_reset_password + ':session_key/' + ':email/',
                        params: { session_key: '@session_key', email: '@email' }}
                }),
            user: $resource(api_user_profile,
                { username: '@username' },
                {
                    get: { method: 'POST'}
                }),
            social_auth: $resource(soc_auth_login,
                { backend: '@backend' },
                {
                    login: { method: 'POST',
                        url: soc_auth_login},
                    disconnect: { method: 'POST',
                        url: soc_auth_disconnect},
                    disconnect_individual: { method: 'POST',
                        url: soc_auth_disconnect + ':association_id/',
                        params: { association_id: '@association_id' }}
                })
        };
    })
;


