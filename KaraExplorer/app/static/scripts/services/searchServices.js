'use strict';

angular.module('search.services', [])
    .factory('SearchServices', function ($resource) {
        var api_search_all = ' api/search/' + ':keyword/';
        return {
            search_all: $resource(api_search_all,
                {keyword: '@keyword'},
                {
                    default: { method: 'POST' },
                    page: { method: 'POST',
                        url: api_search_all + 'page:page/',
                        params: {page: '@page'}},
                    page_items: { method: 'POST',
                        url: api_search_all + 'page:page/' + 'items:items/',
                        params: {page: '@page', items: '@items'}}
                })
        }
    })
;


