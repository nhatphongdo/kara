'use strict';

angular.module('people.services', [])
    .factory('ProfileServices', function ($resource) {
        var api_user_main_interests         = 'api/profile/' + ':username/' + 'main-interests/';
        var api_user_interests              = 'api/profile/' + ':username/' + 'interests/';
        var api_user_friends                = 'api/profile/' + ':username/' + 'friends/';
        var api_user_friends_groups         = 'api/profile/' + ':username/' + 'friends-groups/';
        var api_user_following_groups       = 'api/profile/' + ':username/' + 'following-groups/';
        var api_user_friend_requests        = 'api/profile/' + ':username/' + 'friend_requests/';
        var api_user_friend_requests_sent   = 'api/profile/' + ':username/' + 'friend-requests-sent/';
        var api_user_following              = 'api/profile/' + ':username/' + 'following/';
        var api_user_folders                = 'api/profile/' + ':username/' + 'folders/';
        var api_user_public_folders         = 'api/profile/' + ':username/' + 'public-folders/';
        var api_user_sources                = 'api/profile/' + ':username/' + 'sources/';
        var api_user_sources_alias          = 'api/profile/' + ':username/' + 'sources-alias/';
        var api_user_bookmarks              = 'api/profile/' + ':username/' + 'bookmarks/';
        var api_user_wall_posts             = 'api/profile/' + ':username/' + 'wall-posts/';
        var api_user_actions                = 'api/profile/' + ':username/' + 'actions/';
        var api_user_categories             = 'api/profile/' + ':username/' + 'categories/';
        var api_update_profile              = 'api/profile/' + ':username/' + 'update/';
        var api_user_password               = 'api/profile/' + ':username/' + 'password/';
        var api_user_social_account         = 'api/profile/' + ':username/' + 'social-account/';
        return {
            update_profile: $resource(api_update_profile,
                { username: '@username' },
                {
                    update: { method: 'POST' }
                }),
            interests: $resource(api_user_interests,
                { username: '@username' },
                {
                    default: { method: 'POST' },
                    page: { method: 'POST',
                        url: api_user_interests + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_interests + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }},
                    child: { method: 'POST',
                        url: api_user_interests + ':pk/',
                        params: { pk: '@pk' }},
                    child_name: { method: 'POST',
                        url: api_user_interests + 'interest-:name/',
                        params: { name: '@name' }},
                    main: { method: 'POST',
                        url: api_user_main_interests}
                }),
            friends: $resource(api_user_friends,
                { username: '@username' },
                {
                    default: { method: 'POST' },
                    page: { method: 'POST',
                        url: api_user_friends + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_friends + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }}
                }),
            friends_groups: $resource(api_user_friends_groups,
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    info: { method: 'POST',
                        url: api_user_friends_groups + 'pk:pk/',
                        params: { pk: '@pk' }},
                    friends: { method: 'POST',
                        url: api_user_friends_groups + 'pk:pk/friends/',
                        params: { pk: '@pk' }},
                    actions: { method: 'POST',
                        url: api_user_friends_groups + 'pk:pk/actions/',
                        params: { pk: '@pk' }}
                }),
            following_groups: $resource(api_user_following_groups,
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    info: { method: 'POST',
                        url: api_user_following_groups + 'pk:pk/',
                        params: { pk: '@pk' }},
                    following: { method: 'POST',
                        url: api_user_following_groups + 'pk:pk/following/',
                        params: { pk: '@pk' }},
                    actions: { method: 'POST',
                        url: api_user_following_groups + 'pk:pk/actions/',
                        params: { pk: '@pk' }}
                }),
            following: $resource(api_user_following,
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    page: { method: 'POST',
                        url: api_user_following + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_following + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }}
                }),
            public_folders: $resource(api_user_public_folders,
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    page: { method: 'POST',
                        url: api_user_public_folders + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_public_folders + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }}
                }),
            folders: $resource(api_user_folders,
                { username: '@username' },
                {
                    structure: { method: 'POST',
                        url: api_user_folders + 'structure/'},
                    default: { method: 'POST'},
                    page: { method: 'POST',
                        url: api_user_folders + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_folders + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }}
                }),
            sources: $resource(api_user_sources,
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    page: { method: 'POST',
                        url: api_user_sources + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_sources + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }}
                }),
            sources_alias: $resource(api_user_sources_alias,
                { username: '@username' },
                {
                    default: {  method: 'POST'},
                    rename: { method: 'POST',
                        url: api_user_sources_alias + ':pk/',
                        params: { pk: '@pk' }},
                    move: { method: 'POST',
                        url: api_user_sources_alias + ':pk/' + 'move-to/:category/',
                        params: { pk: '@pk', category: '@category' }},
                    page: { method: 'POST',
                        url: api_user_sources_alias + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_sources_alias + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }}
                }),
            bookmarks: $resource(api_user_bookmarks,
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    page: { method: 'POST',
                        url: api_user_bookmarks + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_bookmarks + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }}
                }),
            wall_posts: $resource(api_user_wall_posts,
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    page: { method: 'POST',
                        url: api_user_wall_posts + 'page:page/',
                        params: { page: '@page' }},
                    page_items: { method: 'POST',
                        url: api_user_wall_posts + 'page:page/' + 'items:items/',
                        params: { page: '@page', items: '@items' }}
                }),
            categories: $resource(api_user_categories,
                { username: '@username' },
                {
                    default: { method: 'POST'},
                    info: { method: 'POST',
                        url: api_user_categories + 'category-:name/',
                        params: { name: '@name' }},
                    rename: { method: 'POST',
                        url: api_user_categories + 'category-:name/rename/',
                        params: { name: '@name' }},
                    sources: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'sources/',
                        params: { name: '@name' }},
                    sources_page: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'sources/' + 'page:page/',
                        params: { name: '@name', page: '@page' }},
                    sources_page_item: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'sources/' + 'page:page/' + 'items:items/',
                        params: { name: '@name', page: '@page', items: '@items' }},
                    sources_alias: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'sources-alias/',
                        params: { name: '@name' },
                        cache: true
                    },

                    sources_alias_page: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'sources-alias/' + 'page:page/',
                        params: { name: '@name', page: '@page' }},
                    sources_alias_page_item: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'sources-alias/' + 'page:page/' + 'items:items/',
                        params: { name: '@name', page: '@page', items: '@items' }},
                    posts: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'posts/',
                        params: { name: '@name' }},
                    posts_page: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'posts/' + 'page:page/',
                        params: { name: '@name', page: '@page' }},
                    posts_page_item: { method: 'POST',
                        url: api_user_categories + 'category-:name/' + 'posts/' + 'page:page/' + 'items:items/',
                        params: { name: '@name', page: '@page', items: '@items' }}
                }),
            activity: $resource(api_user_actions,
                { username: '@username' },
                {
                    default: { method: 'POST' }
                }),
            password: $resource(api_user_password,
                { username: '@username' },
                {
                    change: { method: 'POST',
                        url: api_user_password + 'change/'}
                }),
            social_account: $resource(api_user_social_account,
                { username: '@username' },
                {
                    default: { method: 'POST' }
                })
        }
    })
;


