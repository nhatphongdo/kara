﻿'use strict';

angular.module('people.controllers', ['oc.lazyLoad'])
    .controller('PeopleCtrl', function ($scope, $stateParams, GlobalService, ProfileServices) {
        $scope.ctrl_name = 'PeopleCtrl';
        var page = $stateParams.page || 1;
        var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
        var params = {
            username: GlobalService.user.username,
            is_paginator: true,
            page: page, items: itemsEachPage
        };
        ProfileServices.activity.default(params)
            .$promise
            .then(function (infoActivity) {
                GlobalService.current.paginator = infoActivity.paginator || {};
                $scope.activities = infoActivity.actions;
            });
    })
    .controller('ProfileCtrl', function ($scope, $rootScope, $state, $stateParams, GlobalService, AuthServices) {
        $scope.ctrl_name = 'ProfileCtrl';
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.fileChanged = function (e) {
                var files = e.target.files;
                var fileReader = new FileReader();
                fileReader.readAsDataURL(files[0]);

                fileReader.onload = function () {
                    $(".change-cover-image").css('display', 'block');
                    $(".cover-exist").css('display', 'none');
                    $scope.imgSrc = this.result;
                    $scope.$apply();
                };

            };
            $scope.showCrop = function () {
                console.log($scope.croppedDataUri);
            };
            $scope.clear = function () {
                $scope.imageCropStep = 1;
                delete $scope.imgSrc;
                delete $scope.result;
                delete $scope.resultBlob;
            };
            if ($stateParams.profile == GlobalService.user.id) {
                $rootScope.wall_user = GlobalService.user;
                if ($state.is('profile')) {
                    $state.go('profile.wall', {profile: GlobalService.user.id});
                }
            }
            else {
                var params = {username: $stateParams.profile};
                AuthServices.user.get(params)
                    .$promise
                    .then(function (data) {
                        if (data.error === 0) {
                            // Set information
                            $rootScope.wall_user = data.user;
                            if ($state.is('profile')) {
                                $state.go('profile.wall', {profile: $rootScope.wall_user.username});
                            }
                        }
                        else {
                            if ($state.is('profile')) {
                                $state.go('profile.wall', {profile: GlobalService.user.id});
                            }
                        }
                    });
            }
        }
    })
    .controller('ProfileSettingCtrl', function ($scope, $state, $window, $location, $timeout, $interval, Upload, GlobalService, AuthServices, ProfileServices) {
        $scope.ctrl_name = 'ProfileSettingCtrl';
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.notificationNotworking = function () {
                toastr.info('This function is not avaiable right now ! <br/>Please try again later ', null, GlobalService.opts_toast);
            };
            $scope.uploadFiles = function (file, errFiles) {
                $scope.f = file;
                $scope.errFile = errFiles && errFiles[0];
                if (file) {
                    file.upload = Upload.upload({
                        url: 'http://static.karaplus.com/api/upload',
                        data: {file: file, type: 'avatar'}
                    });

                    file.upload.then(function (response) {
                        $timeout(function () {
                            $scope.avatar = response.data.image.id;
                            file.result = response.data;
                        });
                    }, function (response) {
                        if (response.status > 0)
                            $scope.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 *
                            evt.loaded / evt.total));
                    });
                }
            };
            $scope.user = GlobalService.user;
            $scope.updateProfile = function () {
                var full_name = $scope.user.first_name;
                var birthday = $scope.user.profile.birthday;
                var language = $scope.user.profile.language;
                var country = $scope.user.profile.country;
                var avatar = $scope.avatar;
                var gender = $scope.user.profile.gender_id;
                var biography = $scope.user.profile.biography;
                var update = {
                    first_name: full_name,
                    gender: gender,
                    avatar: avatar,
                    nick_name: full_name,
                    biography: biography,
                    birthday: birthday,
                    language: language,
                    country: country
                };
                var params = {
                    username: GlobalService.user.username,
                    update: update
                };
                ProfileServices.update_profile.update(params)
                    .$promise
                    .then(function (result) {
                        toastr.success('Update your profile successfully ! ', null, GlobalService.opts_toast);
                        $state.reload();
                    });
            };
            $scope.social_connecting = function (account) {
                var params = {
                    backend: account.provider
                };
                account.is_connecting = !account.is_connecting;
                if (account.is_connecting) {
                    AuthServices.social_auth.disconnect(params)
                        .$promise
                        .then(function () {
                            var message = 'Disconnected all social account!';
                            if (account.provider == "google-oauth2")
                                message = 'Disconnected all Google account!';
                            if (account.provider == "facebook")
                                message = 'Disconnected all Facebook account!';
                            account.uid = 'Not yet connect';
                            toastr.info(message, null, GlobalService.opts_toast);
                            $scope.getSocialAccount();
                        });
                }
                else {
                    var message = 'Connecting social account!';
                    if (account.provider == "google-oauth2")
                        message = 'Connecting Google account!';
                    if (account.provider == "facebook")
                        message = 'Connecting Facebook account!';
                    var url_login = '/login/' + account.provider + '/?next=' + $location.absUrl().split('#')[0];
                    toastr.info(message + ' <a href="' + url_login + '" target="_blank"></a>', null, GlobalService.opts_toast);
                    $scope.showConnectPopupWindow(url_login);
                }
            };
            $scope.showConnectPopupWindow = function (url) {
                var left = screen.width / 2 - 200,
                    top = screen.height / 2 - 250,
                    popup = $window.open(url, 'SocCONNECT', "top=" + top + ",left=" + left + ",width=400,height=500,scrollbars=yes"),
                    interval = 1000;
                popup.focus();
                $window.$scope = {};
                var i = $interval(function () {
                    interval += 500;
                    try {
                        if ($window.$scope.reload_soc) {
                            $interval.cancel(i);
                            popup.close();
                            $scope.getSocialAccount();
                        }
                        else {
                            //console.log('waiting... connecting...');
                        }
                    } catch (e) {
                        //console.log('waiting... connecting...');
                    }
                }, interval);
            };
            $scope.getSocialAccount = function () {
                var params = {
                    username: GlobalService.user.username
                };
                ProfileServices.social_account.default(params)
                    .$promise
                    .then(function (soc_acc) {
                        $scope.globals.user.social_accounts = soc_acc.social_accounts;
                        $scope.google = {
                            is_connecting: false,
                            provider: "google-oauth2",
                            uid: 'Not yet connect'
                        };
                        var google = $.grep(soc_acc.social_accounts, function (acc) {
                            return acc.provider == "google-oauth2";
                        });
                        if (google.length) {
                            $scope.google.uid = google[0].uid;
                            $scope.google.is_connecting = true;
                        }
                        $scope.facebook = {
                            is_connecting: false,
                            provider: "facebook",
                            uid: 'Not yet connect'
                        };
                        var facebook = $.grep(soc_acc.social_accounts, function (acc) {
                            return acc.provider == "facebook";
                        });
                        if (facebook.length) {
                            $scope.facebook.uid = facebook[0].uid;
                            $scope.facebook.is_connecting = true;
                        }
                    });
            };

            $scope.getSocialAccount();
        }
    })
    .controller('ProfileWallCtrl', function ($scope, $rootScope, GlobalService, ProfileServices, ActionServices) {
        $scope.ctrl_name = 'ProfileWallCtrl';
        $scope.post_text = '';

        $scope.actionWallPost = function () {
            if (!GlobalService.is_authenticated) {
                $rootScope.goLogin();
            }
            if ($scope.post_text) {
                var params = {
                    username: $rootScope.wall_user.username,//wall_owner
                    wall_post: {
                        title: '',
                        content: $scope.post_text
                    }
                };
                ActionServices.post_on_wall.add(params)
                    .$promise
                    .then(function (wall_post) {
                        if (wall_post.success) {
                            $scope.wall_posts.unshift(wall_post.wall_post);
                            $scope.post_text = '';
                        }
                        else {
                            $rootScope.msg = wall_post.msg;
                        }
                    });
            }
        };

        if (GlobalService.is_authenticated) {
            $scope.get_user_public_folders = function () {
                var page = 1;
                var itemsEachPage = 6;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.public_folders.page_items(params)
                    .$promise
                    .then(function (public_folders) {
                        $rootScope.wall_user.profile.public_folders = public_folders.folders;
                        $scope.public_folders = public_folders.folders;
                        $scope.total_public_folders = public_folders.paginator.totalItems;
                    });
            };
            $scope.get_user_folders = function () {
                var page = 1;
                var itemsEachPage = 6;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.folders.page_items(params)
                    .$promise
                    .then(function (folders) {
                        $rootScope.wall_user.profile.folders = folders.folders;
                        $scope.folders = folders.folders;
                        $scope.total_folders = folders.paginator.totalItems;
                    });
            };
            $scope.get_user_friends = function () {
                var page = 1;
                var itemsEachPage = 6;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.friends.page_items(params)
                    .$promise
                    .then(function (friends) {
                        $rootScope.wall_user.profile.friends = friends.friends;
                        $scope.friends = friends.friends;
                        $scope.total_friends = friends.paginator.totalItems;
                    });
            };
            $scope.get_user_interests = function () {
                var page = 1;
                var itemsEachPage = 6;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.interests.page_items(params)
                    .$promise
                    .then(function (interests) {
                        $rootScope.wall_user.profile.interests = interests.interests;
                        $scope.interests = interests.interests;
                        $scope.total_interests = interests.paginator.totalItems;
                    });
            };
            $scope.get_user_sources = function () {
                var page = 1;
                var itemsEachPage = 6;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.sources.page_items(params)
                    .$promise
                    .then(function (sources) {
                        $rootScope.wall_user.profile.sources = sources.subscribes;
                        $scope.sources = sources.subscribes;
                        $scope.total_sources = sources.paginator.totalItems;
                    });
            };
            $scope.get_user_sources_alias = function () {
                var page = 1;
                var itemsEachPage = 6;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.sources_alias.page_items(params)
                    .$promise
                    .then(function (sources) {
                        var subscribes = sources.subscribes;
                        for (var i = 0; i < subscribes.length; i++) {
                            subscribes[i].feed_source.alias_name = subscribes[i].alias_name;
                            subscribes[i] = subscribes[i].feed_source;
                        }
                        $rootScope.wall_user.profile.sources = subscribes;
                        $scope.sources = subscribes;
                        $scope.total_sources = sources.paginator.totalItems;
                    });
            };
            $scope.get_user_bookmarks = function () {
                var page = 1;
                var itemsEachPage = 4;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.bookmarks.page_items(params)
                    .$promise
                    .then(function (bookmarks) {
                        $rootScope.wall_user.profile.bookmarks = bookmarks.bookmarks;
                        $scope.bookmarks = bookmarks.bookmarks;
                        $scope.total_bookmarks = bookmarks.paginator.totalItems;
                    });
            };
            $scope.get_user_wall_posts = function () {
                var page = GlobalService.current.paginator.currentPage || 1;
                var itemsEachPage = 10;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.wall_posts.page_items(params)
                    .$promise
                    .then(function (wall_posts) {
                        $rootScope.wall_user.profile.wall_posts = wall_posts.wall_posts;
                        $scope.wall_posts = wall_posts.wall_posts;
                        $scope.total_wall_posts = wall_posts.paginator.totalItems;
                        GlobalService.current.paginator = wall_posts.paginator;
                    });
            };

            $scope.$watch('wall_user.username', function () {
                if ($rootScope.wall_user) {
                    $scope.friends = $rootScope.wall_user.profile.friends || [];
                    $scope.interests = $rootScope.wall_user.profile.interests || [];
                    //$scope.public_folders = $rootScope.wall_user.profile.public_folders || [];
                    $scope.folders = $rootScope.wall_user.profile.folders || [];
                    $scope.sources = $rootScope.wall_user.profile.sources || [];
                    $scope.bookmarks = $rootScope.wall_user.profile.bookmarks || [];

                    $scope.get_user_interests();
                    if (!$scope.friends.length) {
                        $scope.get_user_friends();
                    }
                    //$scope.get_user_public_folders();
                    $scope.get_user_folders();
                    //$scope.get_user_sources();
                    $scope.get_user_sources_alias();
                    $scope.get_user_bookmarks();
                    $scope.get_user_wall_posts();
                }
            }, true);
        }
    })
    .controller('ProfileFolderCtrl', function ($scope, $rootScope, $stateParams, GlobalService, ProfileServices) {
        $scope.ctrl_name = 'ProfileFolderCtrl';
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.get_user_public_folders = function () {
                var page = $stateParams.page || 1;
                var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.public_folders.page_items(params)
                    .$promise
                    .then(function (public_folders) {
                        $scope.public_folders = public_folders.folders;
                        GlobalService.current.paginator = public_folders.paginator;
                    });
            };
            $scope.get_user_folders = function () {
                var page = $stateParams.page || 1;
                var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.folders.page_items(params)
                    .$promise
                    .then(function (folders) {
                        $scope.folders = folders.folders;
                        GlobalService.current.paginator = folders.paginator;
                    });
            };
            $scope.$watch('wall_user.username', function () {
                if ($rootScope.wall_user) {
                    //$scope.public_folders = $rootScope.wall_user.profile.public_folders || [];
                    //$scope.get_user_public_folders();
                    $scope.folders = $rootScope.wall_user.profile.folders || [];
                    $scope.get_user_folders();
                }
            }, true);
        }
    })
    .controller('ProfileBookmarkCtrl', function ($scope, $rootScope, $stateParams, GlobalService, ProfileServices) {
        $scope.ctrl_name = 'ProfileBookmarkCtrl';
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.get_user_bookmarks = function () {
                var page = $stateParams.page || 1;
                var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.bookmarks.page_items(params)
                    .$promise
                    .then(function (bookmarks) {
                        $scope.bookmarks = bookmarks.bookmarks;
                        $scope.total_bookmarks = bookmarks.paginator.totalItems;
                    });
            };
            $scope.$watch('wall_user.username', function () {
                if ($rootScope.wall_user) {
                    $scope.bookmarks = $rootScope.wall_user.profile.bookmarks || [];
                    $scope.get_user_bookmarks();
                }
            }, true);
        }
    })
    .controller('ProfileInterestCtrl', function ($scope, $rootScope, $stateParams, GlobalService, ProfileServices) {
        $scope.ctrl_name = 'ProfileInterestCtrl';
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.get_user_interests = function () {
                var page = $stateParams.page || 1;
                var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.interests.page_items(params)
                    .$promise
                    .then(function (interests) {
                        $scope.interests = interests.interests;
                        $scope.total_interests = interests.paginator.totalItems;
                    });
            };
            $scope.$watch('wall_user.username', function () {
                if ($rootScope.wall_user) {
                    $scope.interests = $rootScope.wall_user.profile.interests || [];
                    $scope.get_user_interests();
                }
            }, true);
        }
    })
    .controller('ProfileSourceCtrl', function ($scope, $rootScope, $stateParams, GlobalService, ProfileServices) {
        $scope.ctrl_name = 'ProfileSourceCtrl';
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.get_user_sources_alias = function () {
                var page = $stateParams.page || 1;
                var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
                var params = {
                    username: $rootScope.wall_user.username,
                    is_paginator: true,
                    page: page, items: itemsEachPage
                };
                ProfileServices.sources_alias.page_items(params)
                    .$promise
                    .then(function (sources) {
                        var subscribes = sources.subscribes;
                        for (var i = 0; i < subscribes.length; i++) {
                            subscribes[i].feed_source.alias_name = subscribes[i].alias_name;
                            subscribes[i] = subscribes[i].feed_source;
                        }
                        $scope.sources = subscribes;
                        $scope.total_sources = sources.paginator.totalItems;
                    });
            };
            $scope.$watch('wall_user.username', function () {
                if ($rootScope.wall_user) {
                    $scope.sources = $rootScope.wall_user.profile.sources || [];
                    $scope.get_user_sources_alias();
                }
            }, true);
        }
    })
    .controller('SidebarPeopleMenuCtrl', function ($scope, $rootScope, $stateParams, $timeout, $menuItems, GlobalService, ProfileServices) {
        $scope.ctrl_name = 'SidebarPeopleMenuCtrl';
        // Trigger menu setup
        public_vars.$sidebarMenu = public_vars.$body.find('.sidebar-menu');
        $rootScope.$sidebarMenuItems = $menuItems.instantiate();

        $scope.getMenuPeople = function () {
            $rootScope.people_menu = $rootScope.$sidebarMenuItems;
            var friends_menu = {
                is_friend_root: true,
                name: 'Friends'
            };
            var following_menu = {
                is_follow_root: true,
                name: 'Following'
            };

            $scope.menuFriends = $rootScope.people_menu.addItem(friends_menu, 'people.friends');

            $scope.menuFollowing = $rootScope.people_menu.addItem(following_menu, 'people.following');

            $scope.menuItems = $rootScope.$sidebarMenuItems.getAll();

            $timeout(setup_sidebar_menu, 1);
            ps_init();
        };

        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.get_friends_groups = function () {
                var params = {username: GlobalService.user.username};
                ProfileServices.friends_groups.default(params)
                    .$promise
                    .then(function (friends_groups) {
                        $scope.menuFriends.clearType('fi-group');
                        GlobalService.user.profile.friends_groups = friends_groups.friends_groups;
                        $scope.friends_groups = friends_groups.friends_groups;

                        for (var i = 0; i < $scope.friends_groups.length; i++) {
                            $scope.friends_groups[i].type = 'fi-group';
                            $scope.menuFriends.addItem($scope.friends_groups[i], 'people.group', {group: $scope.friends_groups[i].id});
                        }
                    });
            };

            $scope.get_friends_in_groups = function (group) {
                var params = {username: GlobalService.user.username, pk: group};
                ProfileServices.friends_groups.friends(params)
                    .$promise
                    .then(function (friends_in_group) {
                        $scope.menuFriends.clearType('friend');
                        GlobalService.user.profile.friends = friends_in_group.friends_in_group;
                        $scope.friends = friends_in_group.friends_in_group;

                        for (var i = 0; i < $scope.friends.length; i++) {
                            $scope.friends[i].type = 'friend';
                            $scope.menuFriends.addItem($scope.friends[i], 'people.view', {pk: $scope.friends[i].id});
                        }
                    });
            };

            $scope.get_following_groups = function () {
                var params = {username: GlobalService.user.username};
                ProfileServices.following_groups.default(params)
                    .$promise
                    .then(function (following_groups) {
                        $scope.menuFollowing.clearType('fo-group');
                        GlobalService.user.profile.following_groups = following_groups.following_groups;
                        $scope.following_groups = following_groups.following_groups;

                        for (var i = 0; i < $scope.following_groups.length; i++) {
                            $scope.following_groups[i].type = 'fo-group';
                            $scope.menuFollowing.addItem($scope.following_groups[i], 'people.group', {group: $scope.following_groups[i].id});
                        }
                    });
            };

            $scope.get_following_in_groups = function (group) {
                var params = {username: GlobalService.user.username, pk: group};
                ProfileServices.following_groups.following(params)
                    .$promise
                    .then(function (following_in_group) {
                        $scope.menuFollowing.clearType('following');
                        GlobalService.user.profile.following = following_in_group.following_in_group;
                        $scope.following = following_in_group.following_in_group;

                        for (var i = 0; i < $scope.following.length; i++) {
                            $scope.following[i].type = 'following';
                            $scope.menuFollowing.addItem($scope.following[i], 'people.view', {pk: $scope.following[i].id});
                        }
                    });
            };

            $scope.get_friends = function () {
                var page = 1 || $stateParams.page;
                var itemsEachPage = 6;
                var params = {
                    username: GlobalService.user.username,
                    is_paginator: false,
                    page: page,
                    items: itemsEachPage
                };
                ProfileServices.friends.page_items(params)
                    .$promise
                    .then(function (friends) {
                        GlobalService.user.profile.friends = friends.friends;
                        $scope.friends = friends.friends;
                        //$scope.total_friends = friends.paginator.totalItems;

                        for (var i = 0; i < $scope.friends.length; i++) {
                            $scope.menuFriends.addItem($scope.friends[i], 'people.view', {pk: $scope.friends[i].id});
                        }
                    });
            };

            $scope.get_following = function () {
                var page = 1 || $stateParams.page;
                var itemsEachPage = 6;
                var params = {
                    username: GlobalService.user.username,
                    is_paginator: false,
                    page: page,
                    items: itemsEachPage
                };
                ProfileServices.following.page_items(params)
                    .$promise
                    .then(function (following) {
                        GlobalService.user.profile.following = following.following;
                        $scope.following = following.following;
                        //$scope.total_following = following.paginator.totalItems;

                        for (var i = 0; i < $scope.following.length; i++) {
                            $scope.menuFollowing.addItem($scope.following[i], 'people.view', {pk: $scope.following[i].id});
                        }
                    });
            };

            $scope.getMenuPeople();
            //$scope.get_friends_groups();
            //$scope.get_friends_in_groups('None');
            //$scope.get_following_groups();
            //$scope.get_following_in_groups('None');

            $scope.friend_root = function (item) {
                if (!item.isOpen) {
                    $scope.get_friends_groups();
                    $scope.get_friends_in_groups('None');
                }
                item.isOpen = !item.isOpen;
            };
            $scope.following_root = function (item) {
                if (!item.isOpen) {
                    $scope.get_following_groups();
                    $scope.get_following_in_groups('None');
                }
                item.isOpen = !item.isOpen;
            };
            $scope.friendInGroup = function (item) {
                if (!item.isOpen) {
                    var params = {username: GlobalService.user.username, pk: item.data.id};
                    ProfileServices.friends_groups.friends(params)
                        .$promise
                        .then(function (friends_in_group) {
                            item.clearType('friend');
                            GlobalService.user.profile.friends = friends_in_group.friends_in_group;
                            item.friends = friends_in_group.friends_in_group;

                            for (var i = 0; i < item.friends.length; i++) {
                                item.friends[i].type = 'friend';
                                item.addItem(item.friends[i], 'people.view', {pk: item.friends[i].id});
                            }
                        });
                }
                item.isOpen = !item.isOpen;
            };
            $scope.followingInGroup = function (item) {
                if (!item.isOpen) {
                    var params = {username: GlobalService.user.username, pk: item.data.id};
                    ProfileServices.following_groups.following(params)
                        .$promise
                        .then(function (following_in_group) {
                            item.clearType('following');
                            item.following = following_in_group.following_in_group;

                            for (var i = 0; i < item.following.length; i++) {
                                item.following[i].type = 'following';
                                item.addItem(item.following[i], 'people.view', {pk: item.following[i].id});
                            }
                        });
                }
                item.isOpen = !item.isOpen;
            };
        }
    })
    .controller('AddPeopleCtrl', function ($scope, $stateParams, GlobalService, ActionServices) {
        $scope.ctrl_name = 'AddPeopleCtrl';
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            var page = $stateParams.page || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                username: GlobalService.user.username,
                is_paginator: true,
                page: page, items: itemsEachPage
            };
            ActionServices.find_user.friends(params)
                .$promise
                .then(function (findFriends) {
                    $scope.listFindFriends = findFriends.users;
                });
        }
    })
    .controller('PeopleManageCtrl', function ($scope, $rootScope) {
        $scope.ctrl_name = 'PeopleManageCtrl';
        $scope.models = {
            selected: null,
            lists: {"A": [], "B": []}
        };

        // Generate initial model
        for (var i = 1; i <= 3; ++i) {
            $scope.models.lists.A.push({label: "Item A" + i});
            $scope.models.lists.B.push({label: "Item B" + i});
        }

        // Model to JSON for demo purpose
        $scope.$watch('models', function (model) {
            $scope.modelAsJson = angular.toJson(model, true);
        }, true);
        $scope.dragoverCallback = function (event, index, external, type) {
            // $scope.logListEvent('dragged over', event, index, external, type);
            // Disallow dropping in the third row. Could also be done with dnd-disable-if.
            return index < 10;
        };

        $scope.dropCallback = function (event, index, item, external, type, allowedType) {
            var non_content = $('.dndDragover').attr('data-new');
            if (non_content === 'folder') {
                if ($rootScope.modalDismiss === true) {
                    console.log(event);
                }
            }
            //$scope.logListEvent('dropped at', event, index, external, type);
            if (external) {
                if (allowedType === 'itemType' && !item.label) return false;
                if (allowedType === 'containerType' && !angular.isArray(item)) return false;
            }

            return item;
        };

        $scope.logListEvent = function (action, event, index, external, type) {
            var message = external ? 'External ' : '';
            message += type + ' element is ' + action + ' position ' + index;

            $scope.logEvent(message, event);
        };
        $scope.logEvent = function (message, event) {
            //console.log(message, '(triggered by the following', event.type, 'event)');
            //console.log(event);
        };
    })
;


