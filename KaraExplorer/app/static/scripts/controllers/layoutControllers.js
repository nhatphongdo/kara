'use strict';

angular.module('layout.controllers', [])
    .controller('HorizontalMenuCtrl', function ($scope, $rootScope, $menuItems) {
        $scope.ctrl_name = 'HorizontalMenuCtrl';
        $rootScope.top_menu = $menuItems.instantiate().prepareHorizontalMenu().getAll();
    })
    .controller('SidebarMenuCtrl', function ($scope, $rootScope, $menuItems, $timeout) {
        $scope.ctrl_name = 'SidebarMenuCtrl';
        $rootScope.lsb_menu = $menuItems.instantiate().prepareSidebarMenu().getAll();
        public_vars.$sidebarMenu = public_vars.$body.find('.sidebar-menu');
    })
;


