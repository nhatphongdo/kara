﻿'use strict';

angular.module('kara.controllers', [])
    .controller('EmptyController', function ($scope) {
        $scope.ctrl_name = 'EmptyController';
    })
    .controller('KaraController', function ($scope, $rootScope, $state, $stateParams, $cookies, $window, $layout, $layoutToggles, $pageLoadingBar, $utils, GlobalService, AuthServices, ProfileServices, ActionServices, TagServices, ManageServices, $location, $anchorScroll, $modal, $log, ngDialog, Fullscreen) {
        $scope.ctrl_name = 'KaraController';
        //My Clock
        var updateClock = function () {
            $rootScope.clock = new Date();
        };
        var timer = setInterval(function () {
            $scope.$apply(updateClock);
        }, 1000);
        updateClock();

        // initial
        $scope.initialize = function (username, is_authenticated, is_anonymous, version) {
            GlobalService.version = version;
            GlobalService.is_authenticated = (is_authenticated === 'True');
            GlobalService.is_anonymous = (is_anonymous === 'True');

            if (!GlobalService.is_authenticated) {
                // If not logged in in this session, auto re-sign in with cookie's data
                if ($cookies.get('uid_token')) {
                    var func_name = 'initialize_verify';
                    GlobalService.is_loading.push(func_name);
                    AuthServices.auth.verify()
                        .$promise
                        .then(function (data) {
                            GlobalService.error = data;
                            $scope.wrong_username = false;
                            $scope.wrong_password = false;
                            GlobalService.error.alert = '';
                            if (data.error === 0) {
                                // Set information
                                GlobalService.is_authenticated = true;
                                GlobalService.user = data.user;
                            }
                            else if(data.error === 3){
                                GlobalService.error.alert = 'Please check email to verify your account before continue with us!';
                                toastr.warning(GlobalService.error.alert,
                                    'Warning!!!', GlobalService.opts_toast);
                            }
                            else {
                                if (data.message == 'Email availability') {
                                    $scope.wrong_username = true;
                                    GlobalService.error.alert = 'Email is not found , retype to sign in or sign up below';
                                    $scope.wrong_password = false;
                                }
                                if (data.message == 'Wrong authentication') {
                                    $scope.wrong_username = false;
                                    $scope.wrong_password = true;
                                    GlobalService.error.alert = 'Password is incorrect . Please , try again !';
                                }
                                window.location.reload();
                            }
                            GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, func_name);
                        })
                }
            }
            else {
                // Load user's information
                func_name = 'initialize_get';
                GlobalService.is_loading.push(func_name);
                var params = {username: username};
                AuthServices.user.get(params)
                    .$promise
                    .then(function (data) {
                        GlobalService.error = data;
                        $scope.wrong_username = false;
                        $scope.wrong_password = false;
                        GlobalService.error.alert = '';
                        if (data.error === 0) {
                            // Set information
                            GlobalService.is_authenticated = true;
                            GlobalService.user = data.user;
                        }
                        else if(data.error === 3){
                            GlobalService.error.alert = 'Please check email to verify your account before continue with us!';
                            toastr.warning(GlobalService.error.alert,
                                'Warning!!!', GlobalService.opts_toast);
                        }
                        else {
                            if (data.message == 'Email availability') {
                                $scope.wrong_username = true;
                                GlobalService.error.alert = 'Email is not found , retype to sign in or sign up below';
                                $scope.wrong_password = false;
                            }
                            if (data.message == 'Wrong authentication') {
                                $scope.wrong_username = false;
                                $scope.wrong_password = true;
                                GlobalService.error.alert = 'Password is incorrect . Please , try again !';
                            }
                            window.location.reload();
                        }
                        GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, func_name);
                    });
            }
        };

        $rootScope.reloadPage = function () {
            $state.reload();
            GlobalService.settings.reload = false;
        };

        $rootScope.changeStateParamsView = function (view) {
            if ($.inArray(view, GlobalService.views) + 1) {
                $stateParams.viewType = view;
                GlobalService.settings.viewType = view;
                $layout.setOptions('view', view);
                $state.go($state.current, $stateParams);
            }
        };

        $rootScope.goLogin = function () {
            GlobalService.is_need_login = true;
        };

        if ($window.opener && $window.name == 'SocCONNECT') {
            $window.opener.$scope.reload_soc = true;
        }

        //XENON//BEGIN
        $rootScope.layoutOptions = {
            sidebar: {
                isCollapsed: false
            },
            rightSidebar: {
                isOpen: false
            },
            view: 'card'
        };
        $layout.loadOptionsFromCookies();

        public_vars.$body = jQuery("body");
        $layoutToggles.initToggles();
        $pageLoadingBar.init();

        // Set Scroll to 0 When page is changed
        var old_state_parent = 'home';
        $rootScope.$on('$stateChangeStart', function () {
            console.log('$stateChangeStart', 'KaraController', $.now());
            old_state_parent = $state.current.name.split('.')[0] || 'home';
        });
        $rootScope.$on('$stateChangeSuccess', function () {
            console.log('$stateChangeSuccess', 'KaraController', $.now());
            if (!$state.includes(old_state_parent)) {
                var obj = {pos: jQuery(window).scrollTop()};

                TweenLite.to(obj, .25, {
                    pos: 0, ease: Power4.easeOut, onUpdate: function () {
                        $(window).scrollTop(obj.pos);
                    }
                });
            }

            GlobalService.selection = {
                hand_select: false, select_all: false, select_starred: false,
                selectList: {
                    tags: [], folders: [], bookmarks: []
                }
            };
        });
        //XENON//END

        /*
         * Selection
         */
        var sltitem = null;
        $scope.resetSelection = function () {
            GlobalService.selection.hand_select = !GlobalService.selection.hand_select;
            GlobalService.selection.select_all = false;
            GlobalService.selection.select_starred = false;
            $scope.selectNone();
            GlobalService.selection.selectList = {tags: [], folders: [], bookmarks: []};
        };

        $scope.checkSelection = function () {
            GlobalService.selection.selectList = {tags: [], folders: [], bookmarks: []};
            angular.forEach(GlobalService.current.links, function (item) {
                item.selected = GlobalService.selection.select_starred ? item.status.favourited : GlobalService.selection.select_all;
                if (GlobalService.selection.select_starred && item.status.favourited) {
                    if ($state.includes('manage.view', {viewType: 'list'})) {
                        sltitem = item.id;
                        GlobalService.selection.selectList.bookmarks.push(sltitem);
                    }
                    else if ($state.includes('manage.tags')) {
                        sltitem = item.slug;
                        GlobalService.selection.selectList.tags.push(sltitem);
                    }
                }
            });
            angular.forEach(GlobalService.current.folders, function (item) {
                item.selected = GlobalService.selection.select_starred ? item.status.favourited : GlobalService.selection.select_all;
                if (GlobalService.selection.select_starred && item.status.favourited) {
                    if ($state.includes('manage.view', {viewType: 'list'})) {
                        sltitem = item.id;
                        GlobalService.selection.selectList.folders.push(sltitem);
                    }
                    else if ($state.includes('manage.tags')) {
                        sltitem = item.slug;
                        GlobalService.selection.selectList.tags.push(sltitem);
                    }
                }
            });
        };

        $scope.selectAll = function () {
            GlobalService.selection.select_all = GlobalService.selection.hand_select;
            GlobalService.selection.select_starred = false;
            angular.forEach(GlobalService.current.links, function (item) {
                if (!item.selected) {
                    item.selected = GlobalService.selection.select_all;
                    if ($state.includes('manage.view', {viewType: 'list'})) {
                        sltitem = item.id;
                        GlobalService.selection.selectList.bookmarks.push(sltitem);
                    }
                    else if ($state.includes('manage.tags')) {
                        sltitem = item.slug;
                        GlobalService.selection.selectList.tags.push(sltitem);
                    }
                }
            });
            angular.forEach(GlobalService.current.folders, function (item) {
                if (!item.selected) {
                    item.selected = GlobalService.selection.select_all;
                    if ($state.includes('manage.view', {viewType: 'list'})) {
                        sltitem = item.id;
                        GlobalService.selection.selectList.folders.push(sltitem);
                    }
                    else if ($state.includes('manage.tags')) {
                        sltitem = item.slug;
                        GlobalService.selection.selectList.tags.push(sltitem);
                    }
                }
            });
        };

        $scope.selectNone = function () {
            GlobalService.selection.select_all = false;
            GlobalService.selection.select_starred = false;
            angular.forEach(GlobalService.current.links, function (item) {
                item.selected = GlobalService.selection.select_all;
            });
            angular.forEach(GlobalService.current.folders, function (item) {
                item.selected = GlobalService.selection.select_all;
            });
            GlobalService.selection.selectList = {tags: [], folders: [], bookmarks: []};
        };

        $scope.selectStarred = function () {
            GlobalService.selection.select_starred = GlobalService.selection.hand_select;
            GlobalService.selection.select_all = false;
            $scope.checkSelection();
        };

        $scope.selectItem = function (item, type) {
            GlobalService.selection.select_all = false;
            GlobalService.selection.select_starred = false;
            item.selected = !item.selected;
            if (item.selected) {
                if ($state.includes('manage.view', {viewType: 'list'})) {
                    sltitem = item.id;
                }
                else if ($state.includes('manage.tags')) {
                    sltitem = item.slug;
                }

                switch (type) {
                    case 'folders':
                        GlobalService.selection.selectList.folders.push(sltitem);
                        break;
                    case 'bookmarks':
                        GlobalService.selection.selectList.bookmarks.push(sltitem);
                        break;
                    case 'tags':
                        GlobalService.selection.selectList.tags.push(sltitem);
                        break;
                }
            }
            else {
                if ($state.includes('manage.view', {viewType: 'list'})) {
                    sltitem = item.id;
                }
                else if ($state.includes('manage.tags')) {
                    sltitem = item.slug;
                }

                switch (type) {
                    case 'folders':
                        GlobalService.selection.selectList.folders.splice(
                            GlobalService.selection.selectList.folders.indexOf(sltitem), 1);
                        break;
                    case 'bookmarks':
                        GlobalService.selection.selectList.bookmarks.splice(
                            GlobalService.selection.selectList.bookmarks.indexOf(sltitem), 1);
                        break;
                    case 'tags':
                        GlobalService.selection.selectList.tags.splice(
                            GlobalService.selection.selectList.tags.indexOf(sltitem), 1);
                        break;
                }
            }
        };

        GlobalService.tools.select = [];
        GlobalService.tools.select.push({name: 'Delete', func: 'deleteSelected', expr: 'true',
            dis_expr: '!(globals.selection.selectList.tags.length || globals.selection.selectList.folders.length || globals.selection.selectList.bookmarks.length)'});
        GlobalService.tools.select.push({name: 'Move to', func: 'moveSelectedTo', expr: "!$state.includes('manage.tags.**')",
            dis_expr: '!(globals.selection.selectList.folders.length || globals.selection.selectList.bookmarks.length)'});
        GlobalService.tools.select.push({name: 'Copy to', func: 'copySelectedTo', expr: "!$state.includes('manage.tags.**')",
            dis_expr: '!(globals.selection.selectList.folders.length || globals.selection.selectList.bookmarks.length)'});
        $scope.deleteSelected = function (params) {
            ManageServices.selected.delete(params)
                .$promise
                .then(function (data) {
                    toastr.success("Deleted!", 'Success', GlobalService.opts_toast);
                    $state.reload();
                });
        };
        $scope.callSelectTool = function (func_name) {
            if (!GlobalService.is_authenticated) {
                $rootScope.goLogin();
                return false;
            }
            var params = {
                username: GlobalService.user.username,
                selected_list: GlobalService.selection.selectList,
                parent_id: GlobalService.current.info.id || 'None'
            };
            switch (func_name) {
                case 'deleteSelected':
                    $scope.deleteSelected(params);
                    break;
                case 'moveSelectedTo':
                    if (!$state.includes('manage.tags.**')) {
                        $scope.showSelectFolderPopup(params, func_name);
                    }
                    break;
                case 'copySelectedTo':
                    if (!$state.includes('manage.tags.**')) {
                        $scope.showSelectFolderPopup(params, func_name);
                    }
                    break;
                default:
                    break;
            }
        };
        $scope.showSelectFolderPopup = function (params, func_name) {
            if (!GlobalService.is_authenticated) {
                $rootScope.goLogin();
                return false;
            }
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/select_dest_folder/',
                controller: function ($scope, $modalInstance) {
                    $scope.modalTitle = 'Select destination folder';
                    $scope.params = params;
                    ProfileServices.folders.structure({username: GlobalService.user.username})
                        .$promise
                        .then(function (folders) {
                            $scope.folders_structure = folders.folders;
                        });
                    $scope.banList = angular.copy(params.selected_list.folders);
                    $scope.callSubmit = function () {
                        if (params.folder == undefined) {
                            toastr.warning("Select destination for action, plz!", null, GlobalService.opts_toast);
                        }
                        else {
                            if ($scope.inBanList(params.folder)) {
                                toastr.warning("Select another destination for action, plz!", null, GlobalService.opts_toast);
                            }
                            else {
                                switch (func_name) {
                                    case 'moveSelectedTo':
                                        $scope.moveSelectedTo(params);
                                        break;
                                    case 'copySelectedTo':
                                        $scope.copySelectedTo(params);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    };
                    $scope.moveSelectedTo = function (params) {
                        ManageServices.selected.move_to(params)
                            .$promise
                            .catch(function (error) {
                                $modalInstance.dismiss(error);
                            })
                            .then(function (result) {
                                toastr.success("Moved!", 'Success', GlobalService.opts_toast);
                                $modalInstance.close(result);
                            });
                    };
                    $scope.copySelectedTo = function (params) {
                        ManageServices.selected.copy_to(params)
                            .$promise
                            .catch(function (error) {
                                $modalInstance.dismiss(error);
                            })
                            .then(function (result) {
                                toastr.success("Copied!", 'Success', GlobalService.opts_toast);
                                $modalInstance.close(result);
                            });
                    };
                    // dismissReason
                    $scope.cancel = function (reason) {
                        $modalInstance.dismiss(reason);
                    };
                    $scope.setDestFolder = function (id) {
                        if (id == params.parent_id) {
                            toastr.warning("Select another destination for action, plz!", null, GlobalService.opts_toast);
                        }
                        else {
                            params.folder = id;
                        }
                    };
                    $scope.inBanList = function (id) {
                        return $.inArray(id, $scope.banList) + 1;
                    };
                }
            });

            modalInstance.result
                .then(function (result) {
                    $state.reload();
                },
                function (reason) {
                    console.log(reason);
                });
        };

        /*
         * END Selection
         */

        // Interactive
        $rootScope.interactive = function (item, type) {
            GlobalService.interactive.type = item.type || type;
            GlobalService.interactive.item = {
                id: item.id || 'None',
                name: item.name || 'None',
                slug: item.slug || 'None',
                description: item.description || '',
                url: item.url || 'None'
            };

            switch (type) {
                case 'folder':
                case 'people':
                case 'bookmark':
                    break;
                case 'post':
                    GlobalService.interactive.item.id = item.bookmark.id;
                    GlobalService.interactive.item.url = item.bookmark.url;
                    break;
                case 'object':
                    GlobalService.interactive.item = item.data;
                    break;
                default :
                    GlobalService.interactive.item = item;
            }
        };

        // Search
        $rootScope.searchSubmit = function (search) {
            if (!search.keyword.length)
                $('#input_keyword').focus();
            else {
                switch (search.types.length) {
                    case 0:
                        $('#search-select').style({display: 'block'});
                        break;
                    case 1:
                        if (search.web)
                            $state.go('search.web', {'keyword': search.keyword });
                        if (search.interest)
                            $state.go('search.interest', {'keyword': search.keyword });
                        if (search.post)
                            $state.go('search.post', {'keyword': search.keyword });
                        if (search.bookmark)
                            $state.go('search.bookmark', {'keyword': search.keyword });
                        break;
                    default :
                        $state.go('search.all', {'keyword': search.keyword });
                        break;
                }
            }
        };
        $rootScope.$watch('globals.search.all', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                if (newVal) {
                    GlobalService.search.post = false;
                    GlobalService.search.web = false;
                    GlobalService.search.interest = false;
                    GlobalService.search.bookmark = false;
                    GlobalService.search.types = ["post", "feedsource", "interest", "bookmark"];
                }
                else {
                    if (!(GlobalService.search.web || GlobalService.search.interest
                        || GlobalService.search.post || GlobalService.search.bookmark)) {
                        GlobalService.search.post = true;
                        GlobalService.search.web = true;
                        GlobalService.search.interest = true;
                        GlobalService.search.bookmark = true;
                    }
                }
            }
        });

        $rootScope.$watch('globals.search.post', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                if (newVal) {
                    if (GlobalService.search.all)
                        GlobalService.search.types = [];
                    GlobalService.search.all = false;
                    GlobalService.search.types = $utils.grepOut(GlobalService.search.types, 'post');
                    GlobalService.search.types.push('post');
                    if (GlobalService.search.types.length == 4)
                        GlobalService.search.all = true;
                }
                else {
                    if (!GlobalService.search.all)
                        GlobalService.search.types = $utils.grepOut(GlobalService.search.types, 'post');
                }
            }
        });

        $rootScope.$watch('globals.search.web', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                if (newVal) {
                    if (GlobalService.search.all)
                        GlobalService.search.types = [];
                    GlobalService.search.all = false;
                    GlobalService.search.types = $utils.grepOut(GlobalService.search.types, 'feedsource');
                    GlobalService.search.types.push('feedsource');
                    if (GlobalService.search.types.length == 4)
                        GlobalService.search.all = true;
                }
                else {
                    if (!GlobalService.search.all)
                        GlobalService.search.types = $utils.grepOut(GlobalService.search.types, 'feedsource');
                }
            }
        });

        $rootScope.$watch('globals.search.interest', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                if (newVal) {
                    if (GlobalService.search.all)
                        GlobalService.search.types = [];
                    GlobalService.search.all = false;
                    GlobalService.search.types = $utils.grepOut(GlobalService.search.types, 'interest');
                    GlobalService.search.types.push('interest');
                    if (GlobalService.search.types.length == 4)
                        GlobalService.search.all = true;
                }
                else {
                    if (!GlobalService.search.all)
                        GlobalService.search.types = $utils.grepOut(GlobalService.search.types, 'interest');
                }
            }
        });

        $rootScope.$watch('globals.search.bookmark', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                if (newVal) {
                    if (GlobalService.search.all)
                        GlobalService.search.types = [];
                    GlobalService.search.all = false;
                    GlobalService.search.types = $utils.grepOut(GlobalService.search.types, 'bookmark');
                    GlobalService.search.types.push('bookmark');
                    if (GlobalService.search.types.length == 4)
                        GlobalService.search.all = true;
                }
                else {
                    if (!GlobalService.search.all)
                        GlobalService.search.types = $utils.grepOut(GlobalService.search.types, 'bookmark');
                }
            }
        });

        // Statistic
        $rootScope.get_statistic = function (item, type) {
            if (GlobalService.is_authenticated) {
                var params = {content_type: type, pk: item.id};
                ActionServices.statistic.default(params)
                    .$promise
                    .then(function (statistic) {
                        item.statistic = statistic.statistic;
                        item.status = statistic.status;
                        item.numb = statistic.numb;
                    });
            }
        };

        $rootScope.get_tag_statistic = function (item, type) {
            if (GlobalService.is_authenticated) {
                var params = {content_type: type, slug: item.slug};
                ActionServices.tag_statistic.default(params)
                    .$promise
                    .then(function (statistic) {
                        item.statistic = statistic.statistic;
                        item.status = statistic.status;
                        item.numb = statistic.numb;
                    });
            }
        };

        $rootScope.get_comments = function (item, type) {
            if (GlobalService.is_authenticated) {
                if (item.paginator_comment === undefined)
                    item.paginator_comment = {};
                var page = item.paginator_comment.currentPage || 1;
                var itemsEachPage = 10;
                var params = {
                    content_type: type, pk: item.id,
                    is_paginator: true,
                    page: page,
                    items: itemsEachPage
                };
                ActionServices.comments.page_items(params)
                    .$promise
                    .then(function (comments) {
                        item.comments = comments.comments;
                        item.paginator_comment = comments.paginator;
                    });
            }
        };

        // ACTIONS and LOGS
        $rootScope.action = function (item, type, action) {
            if (GlobalService.is_authenticated) {
                var params = {
                    username: GlobalService.user.username,
                    action: {
                        action: action,
                        content_type: type,
                        object: item.id
                    }
                };
                ActionServices.action.act(params)
                    .$promise
                    .then(function (action) {
                        if (action.error == 'SUCCESS') {
                            $rootScope.get_statistic(item, type);
                        }
                    });
            }
        };
        $rootScope.actionLike = function (item, type) {
            $rootScope.action(item, type, 'like');
        };
        $rootScope.actionDislike = function (item, type) {
            $rootScope.action(item, type, 'dislike');
        };
        $rootScope.actionUnlike = function (item, type) {
            $rootScope.action(item, type, 'unlike');
        };
        $rootScope.actionView = function (item, type) {
            $rootScope.action(item, type, 'view');
        };

        $rootScope.actionSubscribe = function (item) {
            if (GlobalService.is_authenticated) {
                var params = {
                    username: GlobalService.user.username,
                    subscribe: {
                        object: item.id,
                        name: item.name,
                        tag_category: item.categories[0].name
                    }
                };
                ActionServices.subscribe.add(params)
                    .$promise
                    .then(function (action) {
                        if (action.error === 'SUCCESS') {
                            item.subscribed = true;
                        }
                        else if (action.error === 'FAILED') {
                            item.subscribed = false;
                        }
                    });
            }
        };

        $rootScope.actionUnSubscribe = function (item) {
            if (GlobalService.is_authenticated) {
                var params = {
                    username: GlobalService.user.username,
                    subscribe: {
                        object: item.id,
                        name: item.name,
                        is_deleted: true
                    }
                };
                ActionServices.subscribe.add(params)
                    .$promise
                    .then(function () {
                        $rootScope.get_statistic(item, 'feedsource');
                        toastr.warning("Unsubscribed successful ! ", 'Notification', GlobalService.opts_toast);
                        $state.reload();
                    });
            }
        };

        $rootScope.actionFavourite = function (item) {
            if (GlobalService.is_authenticated) {
                var params = {
                    content_type: item.type,
                    pk: item.id
                };
                if (item.status.favourited) {
                    ActionServices.favorite.delete(params)
                        .$promise
                        .then(function (action) {
                            item.status.favourited = action.favourited;
                            if (!action.favourited)
                                toastr.warning("Un-Favourite! ", 'Notification', GlobalService.opts_toast);
                        });
                }
                else {
                    params.username = GlobalService.user.username;
                    ActionServices.favorite.default(params)
                        .$promise
                        .then(function (action) {
                            item.status.favourited = action.favourited;
                            if (action.favourited)
                                toastr.warning("Favourite! ", 'Notification', GlobalService.opts_toast);
                        });
                }
            }
        };

        $rootScope.sendMailResetPassword = function (email) {
            if(email == undefined || email == '') {
                $('#email-to-reset').focus();
                return;
            }
            var params = {email: email};
            ActionServices.send_mail.reset_pwd(params)
                .$promise
                .then(function (result) {
                    if (result.result.status == 'sent')
                        toastr.success('Sent to ' + email + '!' , 'Success', GlobalService.opts_toast);
                });
        };

        //show modals
        $rootScope.showAddLinkPopup = function () {
            $rootScope.showFolderStatic = false;
            $rootScope.showFolder = function () {
                $rootScope.showFolderStatic = true;
            };
            if ($rootScope.showFolderStatic == true) {
                $(".down").click(function () {
                });
            }

            $scope.getUserFoldersStructure = function () {
                var params = {username: GlobalService.user.username};
                ProfileServices.folders.structure(params)
                    .$promise
                    .then(function (folders) {
                        GlobalService.user.folders_structure = folders.folders;
                    });
            };
            $scope.getUserFoldersStructure();

            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/addlink/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Add Link';
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    toastr.success('Add link successfully !', null, GlobalService.opts_toast);
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Add Link dismissed.');
                });
        };
        $scope.showBackUpPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/backup/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Backup Online';
                    }, modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });
        };
        $scope.showImportBookmarkPopup = function () {

            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/bookmark/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Import bookmark from';
                    }, modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }

            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    toastr.success('Import bookmark successfully ! ', null, GlobalService.opts_toast);
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Backup Online dismissed.');
                });
        };
        $scope.showPopupChangePassword = function () {

            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/change-password/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Change password';
                    }, modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }

            });

            modalInstance.result
                .then(function () {
                    $rootScope.logout();
                },
                function () {
                    $log.info('Change password dismissed.');
                });
        };
        $rootScope.showPopupResetPassword = function (session_key, email) {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/reset-password/',
                controller: function ($scope, $modalInstance, AuthServices) {
                    $scope.modalTitle = 'Reset password';
                    $scope.resetpwd = function (pwd) {
                        var func_name = 'reset_password';
                        GlobalService.is_loading.push(func_name);
                        $scope.email = email;
                        var params = {
                            session_key: session_key,
                            email: email,
                            password: pwd
                        };
                        AuthServices.auth.reset_password(params)
                            .$promise
                            .catch(function (error) {
                                $modalInstance.dismiss(error);
                            })
                            .then(function (data) {
                                GlobalService.error = data;
                                $scope.wrong_username = false;
                                $scope.wrong_password = false;
                                GlobalService.error.alert = '';
                                if (data.error === 0) {
                                    // Set information
                                    GlobalService.user = data.user;
                                    $modalInstance.close();
                                    GlobalService.error.alert = 'Sign in Now and Enjoy using Kara.';
                                    toastr.success(GlobalService.error.alert,
                                        'SUCCESSFULLY!!!', GlobalService.opts_toast);
                                    $state.go('home');
                                }
                                else {
                                    if (data.message == 'Email availability') {
                                        $scope.wrong_username = true;
                                        GlobalService.error.alert = 'Email is not found , retype to sign in or sign up below';
                                        $scope.wrong_password = false;
                                    }
                                    if (data.message == 'Wrong authentication') {
                                        $scope.wrong_username = false;
                                        $scope.wrong_password = true;
                                        GlobalService.error.alert = 'Password is incorrect . Please , try again !';
                                    }
                                    $modalInstance.dismiss(data.message);
                                    $state.go('home');
                                }
                                GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, func_name);
                            });
                    };
                    // dismissReason
                    $scope.cancel = function (reason) {
                        $modalInstance.dismiss(reason);
                    };
                },
                size: 'sm'
            });

            modalInstance.result
                .then(function () {
                    toastr.success('Reset password successfully!', null, GlobalService.opts_toast);
                },
                function (reason) {
                    toastr.success('Password Reset: Failure. ' + reason, null, GlobalService.opts_toast);
                });
        };
        $scope.showImportBookmarkPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/bookmark/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Import bookmark from';
                    }, modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Import Bookmarks dismissed.');
                });
        };
        $scope.showCollaboratePopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/collaborate/',
                controller: 'ModalInstance',
                resolve: {
                    modalTitle: function () {
                        return 'Collaborate';
                    }, modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Collaborate dismissed.');
                });
        };
        $rootScope.showCreateFeedFolderPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/createfeedfolder/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Create Feed-Folder';
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    toastr.success('Created successfully !', null, GlobalService.opts_toast)
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Create Feed-Folder dismissed.');
                });
        };
        $rootScope.showCreateFolderPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/createfolder/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Create New folder';
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result.info;
                    toastr.success('Created folder successfully !', null, GlobalService.opts_toast);

                    $state.go('manage.view', {pk: result.folder_id, viewType: GlobalService.settings.viewType});
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Create New Folder dismissed.');
                });
        };
        $rootScope.showCreateLinkPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/createlink/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Create New Bookmark';
                    },
                    modalItem: function () {
                        return {}
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    if (result.error == 'SUCCESS') {
                        GlobalService.interactive.item = result;
                        $state.reload();
                    }
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Create New Bookmark dismissed.');
                });
        };
        $rootScope.showCreateUserCategory = function (item) {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/create-user-category/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Create New Category';
                    },
                    modalItem: function () {
                        return item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    $state.reload();
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Create New Bookmark dismissed.');
                    $rootScope.modalDismiss = true;
                });
        };
        $scope.showDeletePopup = function () {
            var type;
            var item;
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/delete/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Delete ' + GlobalService.interactive.type;
                    },
                    modalItem: function () {
                        type = GlobalService.interactive.type;
                        item = GlobalService.interactive.item;
                        return GlobalService.interactive.item;
                    }
                }
            });

            modalInstance.result
                .then(function () {
                    if (type == 'folder') {
                        if (item.parent.length > 0) {
                            $state.go('manage.view', {viewType: GlobalService.settings.viewType, pk: item.parent});
                            return;

                        } else {
                            $state.go('manage.view', {viewType: GlobalService.settings.viewType, pk: 'None'});
                            return;
                        }
                    }
                    else {
                        $state.reload();
                    }
                    $state.reload();
                },
                function (reason) {
                    console.log(reason);
                });
        };
        $scope.showFollowPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/follow/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Follow'
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Follow dismissed.');
                });
        };
        $scope.showPrivacyPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/privacy/',
                controller: 'ModalInstance',
                resolve: {
                    modalTitle: function () {
                        return 'Edit Privacy'
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Edit Privacy dismissed.');
                });
        };
        $scope.showRenamePopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/rename/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Rename folder';
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    toastr.success('Change a name succesfully !', 'Notification', GlobalService.opts_toast);
                    $state.reload();
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Rename Folder dismissed.');
                });
        };
        $scope.showRenameUserFeedSource = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/rename-user-feed-source/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Rename feed source';
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    toastr.success('Change a name succesfully !', 'Notification', GlobalService.opts_toast);
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Rename Folder dismissed.');
                });
        };
        $scope.showBookmarkRenamePopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/rename-bookmark/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Rename bookmark';
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    toastr.success('You bookmark this link succesfully !', 'Notification', GlobalService.opts_toast);
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Rename Bookmark dismissed.');
                });
        };
        $scope.showTagCategoryRenamePopup = function () {

            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/rename-user-category/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Rename your category';
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    // reload notification
                    toastr.success('Change the name successful !', 'Notification', GlobalService.opts_toast);
                    $state.reload();
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Rename your category dismissed.');
                });
        };
        $scope.showSearchPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/search/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Search'
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    //$state.go();
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Search dismissed.');
                });
        };
        $rootScope.showPopupStatic = function (type, item) {
            $rootScope.people_static = null;
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/show-static/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        var title;
                        switch (type) {
                            case 'liked':
                                title = 'People upvoted this';
                                break;
                            case 'disliked':
                                title = 'People downvoted this';
                                break;
                            case 'shared':
                                title = 'People shared this';
                                break;
                            case 'viewed':
                                title = 'People viewed this';
                                break;
                            case 'commented':
                                title = 'People commented this';
                                break;
                        }

                        return title;
                    },
                    modalItem: function () {
                        var params = {
                            content_type: item.type,
                            pk: item.id
                        };
                        switch (type) {
                            case 'liked':
                                ActionServices.find_user.liked(params).$promise.then(function (result) {
                                    $rootScope.people_static = result;
                                });
                                break;
                            case 'disliked':
                                ActionServices.find_user.disliked(params).$promise.then(function (result) {
                                    $rootScope.people_static = result;
                                });
                                break;
                            case 'shared':
                                ActionServices.find_user.shared(params).$promise.then(function (result) {
                                    $rootScope.people_static = result;
                                });
                                break;
                            case 'viewed':
                                ActionServices.find_user.viewed(params).$promise.then(function (result) {
                                    $rootScope.people_static = result;
                                });
                                break;
                            case 'commented':
                                ActionServices.find_user.commented(params).$promise.then(function (result) {
                                    $rootScope.people_static = result;
                                });
                                break;
                        }
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Search dismissed.');
                });
        };
        $rootScope.showSharePopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/share/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Share'
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Share dismissed.');
                });
        };
        $rootScope.showSubscribePopup = function (item) {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/subscribe/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Subscribe'
                    },
                    modalItem: function () {
                        return item || GlobalService.interactive.item
                    }
                }
            });
            $scope.user_categories = GlobalService.categories;
            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                    // reload notification
                    item.status.subscribed = true;
                    toastr.success('Subscribed succesfully !', 'Notification', GlobalService.opts_toast);
                    $state.reload();
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Subscribe dismissed.');
                });
        };
        $scope.showManageTagsPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/tags/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Manage Tags';
                    },
                    modalItem: function () {
                        var params = {
                            username: GlobalService.user.username,
                            content_type: GlobalService.interactive.type,
                            pk: GlobalService.interactive.item.id
                        };
                        TagServices.find_tag.default(params)
                            .$promise
                            .then(function (tags) {
                                GlobalService.interactive.item.tags = tags.tags || [];
                            });
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item.tags = result;
                },
                function (reason) {
                    GlobalService.interactive.item.tags = reason;
                    $log.info('Modal Manage Tags dismissed.');
                });
        };
        $scope.showVoteDownPopup = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/votedown/',
                controller: 'ModalInstance',
                resolve: {
                    modalTitle: function () {
                        return 'Why'
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    GlobalService.interactive.item = result;
                },
                function (reason) {
                    $log.info('' + reason + '. Modal Why Vote Down dismissed.');
                });
        };
        $scope.markAsRead = function (item) {
            item.status.viewed = true
        };
        $rootScope.addTagsIntoObject = function (content_type, object_hashed, tags_array) {
            return TagServices.userTags.list({
                username: GlobalService.user.username,
                content_object: {
                    content_type: content_type,
                    object: object_hashed
                },
                tags: tags_array
            }).$promise;
        };
        $rootScope.loadUserTags = function (query) {
            return TagServices.userTags.query({
                username: GlobalService.user.username,
                query_tag: query
            }).$promise;
        };
        $rootScope.removeTagFromObject = function (content_type, object_hashed, tags_array) {
            return TagServices.userTags.list({
                username: GlobalService.user.username,
                content_object: {
                    content_type: content_type,
                    object: object_hashed
                },
                tags: tags_array,
                is_deleted: true
            }).$promise;
        };

        //showLinkDetailPopup
        $rootScope.hideRightSidebarPopup = function () {
            $(".right-sidebar-edit").addClass('hidden-sidebar');
            $rootScope.showRightSidebar = true;
        };
        $rootScope.showRightSidebarPopup = function () {
            $(".right-sidebar-edit").removeClass('hidden-sidebar');
            $rootScope.showRightSidebar = false;
        };

        $rootScope.showLinkDetailPopup = function (item) {
            $scope.link = 'templates/popup/linkdetail/';
            ngDialog.open({
                template: $scope.link,
                controller: 'PopupDetailPostCtrl',
                className: $rootScope.dialog,
                resolve: {
                    OpenItem: function () {
                        item.read = true;
                        return item;
                    }
                }
            });
            $rootScope.ngCloseStatus = true;
            $rootScope.$on('ngDialog.closing', function (e, $dialog) {
                $rootScope.ngCloseStatus = false;
            });
            $rootScope.$on('ngDialog.opened', function (e, $dialog) {
                var width = $(".block-content-edit").width();
                var popup = $(".popup-container").width();
                var window = angular.element($window);
                var window_size = window.width();
                var left = parseInt(window_size) - parseInt(popup);
                var block_content = $(".body-content").width();
                var detail_content = $(".content-entry").width();

                $(".ngdialog-close").css('left', window_size - popup - 23);
                localStorage.setItem("prevpost", window_size - popup + ((width - block_content) / 4 - 10));
                localStorage.setItem("nextpost", window_size - popup + ((width - block_content) / 4) + detail_content + 10);

                document.addEventListener('scroll',
                    function (event) {
                        var $el = $(".right-sidebar-edit");
                        $el.css('position', 'fixed');
                        $el.css('left', width + left + 11);
                        var $elm = $(event.target);
                        if ($elm.context.className == 'ng-dialog ng-scope') {
                            // or any other filtering condition
                            // do some stuff
                        }
                    },
                    true // Capture event
                );

                //$('.right-sidebar-edit').innerstaticHeightSidebar();
                //
                //$('.block-content-edit').innerstaticHeight();
            });
        };
        $rootScope.scrollToComment = function () {
            $location.hash('comment-block');
            // call $anchorScroll()
            $anchorScroll();
        };

        $rootScope.showWebScreenPopup = function (item) {
            $scope.link = 'templates/popup/webscreen/';
            ngDialog.open({
                template: $scope.link,
                controller: 'PopupSourceCtrl',
                className: $rootScope.dialog,
                resolve: {
                    OpenItem: function () {
                        return item;
                    }
                }
            });
            $scope.$on('ngDialog.opened', function (e, $dialog) {
                var width = $(".block-content-edit").width();
                var popup = $(".popup-container").width();
                var window = angular.element($window);
                var window_size = window.width();
                var left = parseInt(window_size) - parseInt(popup);
                var block_content = $(".body-content").width();
                var detail_content = $(".detail-content").width();

                $(".ngdialog-close").css('left', window_size - popup - 23);
                localStorage.setItem("nextpost", window_size - popup + ((width - block_content) / 4));
                localStorage.setItem("prevpost", window_size - popup + ((width - block_content) / 4) + detail_content);

                document.addEventListener('scroll',
                    function (event) {
                        var $el = $(".right-sidebar-edit");
                        $el.css('position', 'fixed');
                        $el.css('left', width + left + 11);
                        var $elm = $(event.target);
                        if ($elm.context.className == 'ng-dialog ng-scope') {
                            // or any other filtering condition
                            // do some stuff
                        }
                    }, true);
            });
        };
        $scope.showEditFeedPopup = function (view) {
            if ($state.includes('feeds')) {
                $scope.link = 'templates/popup/editfeed/';
                ngDialog.open({template: $scope.link});
            }
        };
        $scope.showEditInterestPopup = function () {
            if ($state.includes('interests')) {
                $scope.link = '';
                ngDialog.open({template: $scope.link});
            }
        };
    })
    .controller('CommentCtrl', function ($scope, $rootScope, ActionServices, GlobalService) {
        $scope.ctrl_name = 'CommentCtrl';
        $rootScope.actionComment = function (item, type) {
            if (item.comment_tex && GlobalService.is_authenticated) {
                var params = {
                    username: GlobalService.user.username,
                    comment: {
                        title: '',
                        content: item.comment_tex,
                        content_type: type,
                        object: item.id
                    }
                };
                ActionServices.comment.default(params)
                    .$promise
                    .then(function (comment) {
                        if (comment.error == 'SUCCESS') {
                            if (type == 'wallpost' || type == 'comment') {
                                item.comments.unshift(comment.comment);
                            }
                            else {
                                item.comments.unshift(comment.comment);
                            }
                            item.comment_tex = '';
                            $rootScope.get_statistic(item, type);
                        }
                    });
            }
        };
    })
    .controller('ModalInstance', function ($scope, $stateParams, $modal, $modalInstance, modalTitle, modalItem, TagServices, FolderServices, BookmarkServices, ManageServices, GlobalService, ImportBookmark, ActionServices, ProfileServices) {
        $scope.ctrl_name = 'ModalInstance';
        var time = Date.now || function () {
            return +new Date;
        };
        var start = time();

        $scope.modalTitle = modalTitle;
        $scope.modalItem = modalItem;
        $scope.modalItem.to_folder = {};
        $scope.newFolder = {};
        $scope.newBookmarkLink = {};

        //!!!collaborate
        $scope.users = [];
        $scope.suggestionUser = function (query) {
            if ((query.length > 2) && (time() - start > 200)) {
                start = time();
            }
        };
        $scope.onSelect = function ($item) {
            $scope.$item = $item;
            $scope.selectedUser = '';
        };

        // dismissReason
        $scope.cancel = function (reason) {
            $modalInstance.dismiss(reason);
        };

        // closeResult
        $scope.submit = function (result) {
            $modalInstance.close(result);
        };

        //save folder and close modal
        $scope.createNewFolder = function (newFolder) {
            var folderID = 'None';
            if ($stateParams.pk != undefined) {
                folderID = $stateParams.pk;
            }
            var params = {
                username: GlobalService.user.username,
                parent: folderID,
                folder: newFolder,
                tags: newFolder.tags
            };
            FolderServices.folder.create(params)
                .$promise
                .then(function (folderCreated) {
                    // close and return new folder
                    $modalInstance.close(folderCreated);
                })
                .catch(function (err) {
                    $modalInstance.dismiss(err.data.error);
                });
        };

        //save bookmark and close modal
        $scope.createBookmark = function (newBookmarkLink) {
            var folderID = 'None';
            if ($stateParams.pk != undefined) {
                folderID = $stateParams.pk;
            }
            newBookmarkLink.folder = folderID;
            var params = {
                username: GlobalService.user.username,
                folder: folderID,
                bookmark: newBookmarkLink,
                tags: newBookmarkLink.tags
            };
            BookmarkServices.bookmarkDetail.create(params)
                .$promise
                .then(function (bookmarkCreated) {
                    toastr.success('Create bookmark "<u>' + newBookmarkLink.title + '</u>" successfully !', null, GlobalService.opts_toast);
                    $modalInstance.close(bookmarkCreated);
                })
                .catch(function (err) {
                    $modalInstance.dismiss(err);
                });
        };
        $scope.showPeopleAction = function (action, object) {
        };

        //rename folder and close modal
        $scope.renameFolder = function () {
            var folderID = 'None';
            if ($stateParams.pk != undefined) {
                folderID = $stateParams.pk;
            }
            var params = {
                username: GlobalService.user.username,
                pk: $scope.modalItem.id, parent: folderID,
                folder: {
                    name: $scope.modalItem.name, description: $scope.modalItem.description
                }
            };
            FolderServices.folder.update(params)
                .$promise
                .then(function (folderRenamed) {
                    // close and return renamed folder
                    $modalInstance.close(folderRenamed);
                })
                .catch(function (err) {
                    $modalInstance.dismiss(err.data.error);
                });
        };

        //rename folder and close modal
        $scope.renameBookmark = function () {
            var folderID = 'None';
            if ($stateParams.pk != undefined) {
                folderID = $stateParams.pk;
            }
            var params = {
                username: GlobalService.user.username,
                folder: folderID, pk: $scope.modalItem.id,
                bookmark: {
                    title: $scope.modalItem.name, url: $scope.modalItem.url
                }
            };
            BookmarkServices.bookmarkDetail.update(params)
                .$promise
                .then(function (renameBookmark) {
                    // close and return renamed bookmark
                    $modalInstance.close(renameBookmark);
                })
                .catch(function (err) {
                    $modalInstance.dismiss(err.data.error);
                });
        };

        $scope.renameTagCategory = function () {
            var params = {
                username: GlobalService.user.username,
                name: $scope.modalItem.slug,
                rename: $scope.modalItem.name
            };
            ProfileServices.categories.rename(params)
                .$promise
                .then(function (tag_category) {
                    // close and return renamed folder
                    $modalInstance.close(tag_category);
                })
                .catch(function (err) {
                    $modalInstance.dismiss(err.data.error);
                });
        };

        $scope.reTagCategory = function () {
            var params = {
                username: GlobalService.user.username,
                pk: $scope.modalItem.id,
                category: true,
                category_name: $scope.new_category
            };
            ProfileServices.sources_alias.move(params)
                .$promise
                .then(function (category_source) {
                    var subscribe = category_source.subscribe;
                    for (var i = 0; i < subscribe.length; i++) {
                        subscribe[i].feed_source.alias_name = subscribe[i].alias_name;
                        subscribe[i] = subscribe[i].feed_source;
                    }
                    // close and return
                    $modalInstance.close(subscribe);
                })
                .catch(function (err) {
                    $modalInstance.dismiss(err.data.error);
                });
        };

        $scope.renameUserFeedSource = function () {
            var params = {
                username: GlobalService.user.username,
                pk: $scope.modalItem.id,
                rename: $scope.modalItem.alias_name
            };
            ProfileServices.sources_alias.rename(params)
                .$promise
                .then(function (user_source) {
                    var subscribe = user_source.subscribe;
                    for (var i = 0; i < subscribe.length; i++) {
                        subscribe[i].feed_source.alias_name = subscribe[i].alias_name;
                        subscribe[i] = subscribe[i].feed_source;
                    }
                    // close and return renamed folder
                    $modalInstance.close(subscribe[0]);
                })
                .catch(function (err) {
                    $modalInstance.dismiss(err.data.error);
                });
        };

        //delete folder and close modal
        $scope.deleteFolder = function () {
            var params = {
                username: GlobalService.user.username, pk: $scope.modalItem.id
            };
            FolderServices.folder.delete(params)
                .$promise
                .catch(function () {
                    $modalInstance.dismiss();
                })
                .then(function () {
                    toastr.warning('Deleted successfully !', null, GlobalService.opts_toast);
                    $modalInstance.close();
                });
        };

        $scope.deleteBookmark = function () {
            var folderID = 'None';
            if ($stateParams.pk != undefined) {
                folderID = $stateParams.pk;
            }
            var params = {
                username: GlobalService.user.username, folder: folderID, pk: $scope.modalItem.id
            };
            BookmarkServices.bookmarkDetail.delete(params)
                .$promise
                .then(function (bookmarkDeleted) {
                    // close and return deleted bookmark
                    var options = GlobalService.opts_toast;
                    toastr.warning('<p class="remove-notification">"' +
                        $scope.modalItem.name +
                        '" has been removed sucessfully !</p>' +
                        '<button" class="btn btn-trans btn-undo">' +
                        '<i class="fa fa-repeat"></i> Undo</button>', null, options);

                    $modalInstance.close(bookmarkDeleted);
                });

            $(".btn-undo").on('click', function () {
                //var params = {'list_object_id': list_object_id, restore: true};
                //ActionServices.restore.sources(params)
                //    .$promise
                //    .then(function () {
                //        $scope.getSourcesAlias(item);
                //    });
            });
        };

        $scope.importBookmarkSubmit = function (fileBookmark) {
            var form_data = new FormData();
            form_data.append('file', $('#file-upload')[0].files[0]);
            form_data.append('folderName', fileBookmark.folderName);

            ImportBookmark.uploadFile(form_data, GlobalService.user.username).then(function (result) {
                if (result.data.error == 'SUCCESS') {
                    fileBookmark.messageSuccess = "Successful!";
                    fileBookmark.isSuccess = true;
                    fileBookmark.isError = false;
                } else {
                    fileBookmark.messageError = result.data.message;
                    fileBookmark.isError = true;
                    fileBookmark.isSuccess = false;
                }

            });
        };
        //delete folder or delete bookmark/link
        $scope.deleteItem = function () {
            if (GlobalService.interactive.type === 'folder') {
                $scope.deleteFolder();
            }
            if (GlobalService.interactive.type === 'bookmark'
                || GlobalService.interactive.type === 'link') {
                $scope.deleteBookmark();
            }
        };

        $scope.removeTag = function (tag) {
            var params = {
                username: GlobalService.user.username,
                content_object: {
                    content_type: GlobalService.interactive.type,
                    object: GlobalService.interactive.item.id
                },
                tags: [tag],
                is_deleted: true
            };
            TagServices.userTags.list(params)
                .$promise
                .then(function (folderTags) {
                    //{
                    //    'error': 'SUCCESS',
                    //    'user_object_tags': user_object_tags
                    //}
                    $scope.modalItem.tags = folderTags.user_object_tags;
                });
        };

        $scope.addTagModal = function () {
            var modalInstance = $modal.open({
                animation: GlobalService.settings.animationsPopupEnabled,
                templateUrl: 'templates/popup/addtag/',
                controller: 'ModalInstance',
                size: 'sm',
                resolve: {
                    modalTitle: function () {
                        return 'Add Tag';
                    },
                    modalItem: function () {
                        return GlobalService.interactive.item
                    }
                }
            });

            modalInstance.result
                .then(function (result) {
                    $scope.modalItem.tags = result;
                    GlobalService.interactive.item.tags = result;
                },
                function (reason) {
                    console.log(reason);
                });
        };

        $scope.addTag = function (tag) {
            var params = {
                username: GlobalService.user.username,
                content_object: {
                    content_type: GlobalService.interactive.type,
                    object: GlobalService.interactive.item.id
                },
                tags: [tag]
            };
            TagServices.userTags.list(params)
                .$promise
                .catch(function (error) {
                    $modalInstance.dismiss(error);
                })
                .then(function (folderTags) {
                    $modalInstance.close(folderTags.user_object_tags);
                });
        };

        $scope.addBookmark = function () {
            var params = {
                username: GlobalService.user.username,
                pk: modalItem.id,
                folder: $scope.modalItem.to_folder.id || 'None',
                tags: $scope.tags
            };
            ManageServices.addBookmark.default(params)
                .$promise
                .then(function (result) {
                    switch (result.error) {
                        case 'SUCCESS':
                            modalItem.bookmarked = true;
                            $modalInstance.close(result.message);
                            break;
                        case 'FAILED':
                            // $modalInstance.dismiss(result.message);
                            break;
                    }
                })
            ;
        };

        $scope.subscribeSource = function (item) {
            if (GlobalService.is_authenticated) {
                var params = {
                    username: GlobalService.user.username,
                    subscribe: {
                        object: item.id,
                        name: item.name,
                        tag_category: [item.categories[0].name]
                    }
                };
                ActionServices.subscribe.add(params)
                    .$promise
                    .then(function (result) {
                        switch (result.error) {
                            case 'SUCCESS':
                                item.subscribed = true;
                                $modalInstance.close(result.message);
                                break;
                            case 'FAILED':
                                item.subscribed = false;
                                $modalInstance.dismiss(result.message);
                                break;
                        }
                    });
            }
        };

        $scope.pwd = {
            current: '', new: '', confirm: ''
        };
        $scope.changePassword = function (pwd) {
            if (GlobalService.is_authenticated) {
                var params = {
                    username: GlobalService.user.username,
                    password: pwd
                };
                ProfileServices.password.change(params)
                    .$promise
                    .then(function (result) {
                        switch (result.error) {
                            case 'SUCCESS':
                                $modalInstance.close(true);
                                break;
                            case 'FAILED':
                                $scope.pwd.error = result.message;
                                break;
                        }
                    });
            }
        };
    })
    .controller('PopupDetailPostCtrl', function ($scope, $rootScope, $state, GlobalService, OpenItem, TagServices) {
        $scope.ctrl_name = 'PopupDetailPostCtrl';
        var $doc = angular.element(document);
        $scope.item = OpenItem;
        $scope.popupType = 'post';
        $scope.item.type = 'post';
        $scope.left_next = localStorage.getItem("nextpost");
        $scope.left_prev = localStorage.getItem("prevpost");

        var share = {
            token: 'Kara179',
            user: GlobalService.user.username,
            hashed_id: $scope.item.id,
            type: $scope.item.type
        };
        $scope.item.share_url = location.origin + '/'
            + $state.href('share', {content_type: share.type, pk: share.hashed_id, user: share.user, token: share.token});

        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $rootScope.actionView($scope.item, 'post');
            $rootScope.get_comments($scope.item, 'post');
        }
        $scope.plusObject = function () {
            console.log('plusObject', $scope.popupType);
            $rootScope.interactive($scope.item, 'post');
            if ($scope.item.status.bookmarked) {
                //$rootScope.showUnAddLinkPopup();
                //$rootScope.actionUnSubscribe($scope.item);
            }
            else
                $rootScope.showAddLinkPopup();
        };
        $scope.upObject = function () {
            console.log('upObject', $scope.popupType);
            if ($scope.item.status.liked)
                $rootScope.actionUnlike($scope.item, $scope.popupType);
            else
                $rootScope.actionLike($scope.item, $scope.popupType);
        };
        $scope.downObject = function () {
            console.log('downObject', $scope.popupType);
            if ($scope.item.status.disliked)
                $rootScope.actionUnlike($scope.item, $scope.popupType);
            else
                $rootScope.actionDislike($scope.item, $scope.popupType);
        };

        $scope.get_tag = function () {
            $scope.loading = true;
            var params = {
                content_type: $scope.item.type,
                pk: $scope.item.id
            };
            TagServices.find_tag.default(params)
                .$promise
                .then(function (result) {
                    $scope.item.tags = result.tags;
                });
        };
        $scope.get_tag();

        $scope.parentList = GlobalService.current.links;
        $scope.related_post = GlobalService.current.posts;
        $scope.changePageComment = function () {
            $rootScope.get_comments($scope.item, 'post');
        };

        $scope.nextPrevPost = function () {
            var posts = GlobalService.current.posts;

            for (var i in posts) {
                //$scope.related_post.push(posts[i]);
                if ($scope.item.id == posts[i].id) {
                    var j = parseInt(i) + 1;
                    var z = parseInt(i) - 1;
                    if (i == 0) {
                        $scope.nextItem = null;
                    }
                    if (z == 0) {
                        $scope.prevItem = null;
                    }
                    $scope.nextItem = posts[j];
                    $scope.prevItem = posts[z];
                }
            }
        };
        $scope.nextPrevPost();
        $scope.showNextPost = function (nextItem) {
            jQuery(window).scrollTop();
            nextItem.read = true;
            $scope.item = $scope.nextItem;
            $scope.nextPrevPost();
        };
        $scope.showPrevPost = function (prevItem) {
            $scope.item = $scope.prevItem;
            $scope.nextPrevPost();
        };
        var handler = function (e) {
            switch (e.keyCode) {
                case 37:
                    if ($scope.prevItem) {
                        $scope.showPrevPost($scope.prevItem);
                    }
                    break;
                case 39:
                    if ($scope.nextItem) {
                        $scope.showNextPost($scope.nextItem);
                    }
                    break;
            }
        };

        $doc.on('keydown', handler);
        $scope.$on('$destroy', function () {
            $doc.off('keydown', handler);
        });
    })
    .controller('PopupSourceCtrl', function ($scope, $rootScope, $state, GlobalService, FeedsServices, OpenItem, ActionServices, TagServices) {
        $scope.ctrl_name = 'PopupSourceCtrl';
        $scope.item = OpenItem;
        $scope.popupType = 'feedsource';
        $scope.item.type = 'feedsource';

        var share = {
            token: 'Kara179',
            user: GlobalService.user.username,
            hashed_id: $scope.item.id,
            type: $scope.item.type
        };
        $scope.item.share_url = location.origin + '/'
            + $state.href('share', {content_type: share.type, pk: share.hashed_id, user: share.user, token: share.token});

        $scope.item.paginator = {};
        $scope.templateview = true;
        $scope.templatename = 'popup_list';
        $scope.plusObject = function () {
            console.log('plusObject', $scope.popupType);
            if ($scope.item.status.subscribed) {
                //$rootScope.showUnSubscribePopup($scope.item);
                $rootScope.actionUnSubscribe($scope.item);
            }
            else
                $rootScope.showSubscribePopup($scope.item);
        };
        $scope.upObject = function () {
            console.log('upObject', $scope.popupType);
            if ($scope.item.status.liked)
                $rootScope.actionUnlike($scope.item, $scope.popupType);
            else
                $rootScope.actionLike($scope.item, $scope.popupType);
        };
        $scope.downObject = function () {
            console.log('downObject', $scope.popupType);
            if ($scope.item.status.disliked)
                $rootScope.actionUnlike($scope.item, $scope.popupType);
            else
                $rootScope.actionDislike($scope.item, $scope.popupType);
        };
        $scope.compassObject = function () {
            console.log('compassObject', $scope.popupType);
        };

        $scope.get_source_posts_popup = function () {
            var page = $scope.item.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage;
            var params = {
                src: $scope.item.id,
                page: page,
                items: itemsEachPage
            };
            if ($scope.item.id != undefined) {
                FeedsServices.source.contents_page_item(params)
                    .$promise
                    .then(function (contents) {
                        $scope.item.posts = contents.links || [];
                        $scope.item.paginator = contents.paginator || {};
                    });
            }
        };
        $scope.get_source_posts_popup();

        $scope.get_source_subscriber = function () {
            $scope.loading = true;
            var params = {
                pk: $scope.item.id
            };
            ActionServices.find_user.subscriber(params)
                .$promise
                .then(function (result) {
                    $scope.item.subscriber = result.users;
                });
        };
        $scope.get_source_subscriber();

        $scope.get_tag = function () {
            $scope.loading = true;
            var params = {
                content_type: $scope.item.type,
                pk: $scope.item.id
            };
            TagServices.find_tag.default(params)
                .$promise
                .then(function (result) {
                    $scope.item.tags = result.tags;
                });
        };
        $scope.get_tag();

        $scope.changePage = function () {
            if (GlobalService.is_authenticated && GlobalService.user.username) {
                $scope.get_source_posts_popup();
            }
        };
        $scope.changePageComment = function () {
            if (GlobalService.is_authenticated && GlobalService.user.username) {
                $rootScope.get_comments($scope.item, 'feedsource');
            }
        };

        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $rootScope.actionView($scope.item, 'feedsource');
            $rootScope.get_comments($scope.item, 'feedsource');
        }
    })
    .controller('RightSideBar', function ($scope, $rootScope, $state, GlobalService) {
        $scope.ctrl_name = 'RightSideBar';
        if ($state.is('feeds.view.src') || $state.is('feeds.view.src.page')) {
            $scope.plusObject = function () {
                if (GlobalService.current.info.status.subscribed) {
                    //$rootScope.showUnSubscribePopup($scope.item);
                    $rootScope.actionUnSubscribe(GlobalService.current.info);
                    toastr.warning("Unsubscribed successful ! ", 'Notification', GlobalService.opts_toast);
                }
                else
                    $rootScope.showSubscribePopup(GlobalService.current.info);
            };
            $scope.upObject = function () {
                console.log('upObject', GlobalService.current.type, GlobalService.current.info.name);
                $rootScope.actionLike(GlobalService.current.info, GlobalService.current.type);
            };
            $scope.downObject = function () {
                console.log('downObject', GlobalService.current.type, GlobalService.current.info.name);
                $rootScope.actionDislike(GlobalService.current.info, GlobalService.current.type);
            };
            $scope.compassObject = function () {
                console.log('compassObject', GlobalService.current.type, GlobalService.current.info.name);
            };
        }
    })
    .controller('ExtraController', function ($scope, $rootScope, $stateParams, GlobalService, ActionServices, FeedsServices, FolderServices, ManageServices) {
        $scope.ctrl_name = 'ExtraController';
        if ($stateParams.content_type != undefined && $stateParams.pk != undefined) {
            if ($stateParams.content_type == 'post') {
                var params = {pk: $stateParams.pk};
                ActionServices.get_post.default(params)
                    .$promise
                    .then(function (data) {
                        $scope.item = data.post;
                        $rootScope.showLinkDetailPopup($scope.item);
                    });
            }
            if ($stateParams.content_type == 'feedsource') {
                params = {src: $stateParams.pk};
                FeedsServices.source.info(params)
                    .$promise
                    .then(function (infoSource) {
                        $scope.item = infoSource.info || {};

                        $scope.item.parents = [];
                        var categories = infoSource.info.categories;
                        for (var i = 0; i < categories.length; i++) {
                            if (categories[i].id == $rootScope.$stateParams.pk) {
                                $scope.item.parents.push(categories[i]);
                                $scope.item.parent = categories[i];
                                break;
                            }
                        }
                        $scope.item.parents.push({
                            'id': infoSource.info.id,
                            'name': infoSource.info.name
                        });
                        $scope.item.parents.reverse();

                        $rootScope.showWebScreenPopup($scope.item);
                    });
            }
            if ($stateParams.content_type == 'folder') {
                $scope.type = 'folder';
                $scope.links = [];
                $scope.folders = [];
                $scope.get_folder_info = function () {
                    var params = {username: $stateParams.user, pk: $stateParams.pk};
                    FolderServices.folder.info(params)
                        .$promise
                        .then(function (infoFolder) {
                            GlobalService.current.type = $scope.type || 'undefined';
                            GlobalService.current.info = infoFolder.info || {};
                            GlobalService.current.breadcrumbs = infoFolder.breadcrumbs || {};
                            $scope.$watch('GlobalService.current.breadcrumbs', function () {
                                console.log('$watch GlobalService.current.breadcrumbs');
                            }, true);

                            GlobalService.interactive.type = $scope.type || 'undefined';
                            GlobalService.interactive.item = infoFolder.info || {};
                        });
                };
                $scope.get_folder_links = function () {
                    var page = $stateParams.page || 1;
                    var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
                    var params = {
                        username: $stateParams.user,
                        folder: $stateParams.pk,
                        page: page,
                        items: itemsEachPage
                    };
                    ManageServices.folderBookmark.page_item(params)
                        .$promise
                        .then(function (folderBookmark) {
                            GlobalService.current.folders = folderBookmark.folders || [];
                            GlobalService.current.links = folderBookmark.links || [];
                            GlobalService.current.paginator = folderBookmark.paginator || {};
                            $scope.folders = GlobalService.current.folders;
                            $scope.links = GlobalService.current.links;
                            $scope.mix = GlobalService.current.mix;
                        });
                };
                if (GlobalService.is_authenticated && GlobalService.user.username) {
                    $scope.get_folder_info();
                    $scope.get_folder_links();
                }
            }
        }
    })
    .controller('ShareController', function ($scope, $rootScope, $state, $stateParams, GlobalService, ActionServices, FeedsServices, FolderServices, ManageServices) {
        $scope.ctrl_name = 'ShareController';
        if ($stateParams.content_type != undefined && $stateParams.pk != undefined) {
            if ($stateParams.content_type == 'post') {
                var params = {pk: $stateParams.pk};
                ActionServices.get_post.default(params)
                    .$promise
                    .then(function (data) {
                        $scope.item = data.post;

                        //$rootScope.showLinkDetailPopup($scope.item);
                        $scope.popupType = 'post';
                        $scope.item.type = 'post';

                        var share = {
                            token: 'Kara179',
                            user: GlobalService.user.username,
                            hashed_id: $scope.item.id,
                            type: $scope.item.type
                        };
                        $scope.share_url = location.origin + '/'
                            + $state.href('share', {content_type: share.type, pk: share.hashed_id, user: share.user, token: share.token});

                        if (GlobalService.is_authenticated && GlobalService.user.username) {
                            $rootScope.actionView($scope.item, 'post');
                            $rootScope.get_comments($scope.item, 'post');
                        }
                        $scope.related_post = GlobalService.current.posts;
                        $scope.changePageComment = function () {
                            $rootScope.get_comments($scope.item, 'post');
                        };
                    });
            }
            if ($stateParams.content_type == 'feedsource') {
                params = {src: $stateParams.pk};
                FeedsServices.source.info(params)
                    .$promise
                    .then(function (infoSource) {
                        $scope.item = infoSource.info || {};

                        $scope.item.parents = [];
                        var categories = infoSource.info.categories;
                        for (var i = 0; i < categories.length; i++) {
                            if (categories[i].id == $rootScope.$stateParams.pk) {
                                $scope.item.parents.push(categories[i]);
                                $scope.item.parent = categories[i];
                                break;
                            }
                        }
                        $scope.item.parents.push({
                            'id': infoSource.info.id,
                            'name': infoSource.info.name
                        });
                        $scope.item.parents.reverse();

                        //$rootScope.showWebScreenPopup($scope.item);
                        $scope.popupType = 'feedsource';
                        $scope.item.type = 'feedsource';

                        var share = {
                            token: 'Kara179',
                            user: GlobalService.user.username,
                            hashed_id: $scope.item.id,
                            type: $scope.item.type
                        };
                        $scope.share_url = location.origin + '/'
                            + $state.href('share', {content_type: share.type, pk: share.hashed_id, user: share.user, token: share.token});

                        if (GlobalService.is_authenticated && GlobalService.user.username) {
                            $rootScope.actionView($scope.item, 'feedsource');
                            $rootScope.get_comments($scope.item, 'feedsource');
                        }
                        $scope.item.paginator = {};
                        $scope.templateview = true;
                        $scope.templatename = 'popup_list';

                        $scope.get_source_posts_popup = function () {
                            var page = $scope.item.paginator.currentPage || 1;
                            var itemsEachPage = GlobalService.settings.itemsEachPage;
                            var params = {
                                src: $scope.item.id,
                                page: page,
                                items: itemsEachPage
                            };
                            if ($scope.item.id != undefined) {
                                FeedsServices.source.contents_page_item(params)
                                    .$promise
                                    .then(function (contents) {
                                        $scope.item.posts = contents.links || [];
                                        $scope.item.paginator = contents.paginator || {};
                                    });
                            }
                        };
                        $scope.get_source_posts_popup();
                        $scope.changePage = function () {
                            $scope.get_source_posts_popup();
                        };
                        $scope.changePageComment = function () {
                            $rootScope.get_comments($scope.item, 'feedsource');
                        };
                    });
            }
            if ($stateParams.content_type == 'folder') {
                $scope.type = 'folder';
                $scope.links = [];
                $scope.folders = [];
                $scope.get_folder_info = function () {
                    var params = {username: $stateParams.user, pk: $stateParams.pk};
                    FolderServices.folder.info(params)
                        .$promise
                        .then(function (infoFolder) {
                            GlobalService.current.type = $scope.type || 'undefined';
                            GlobalService.current.info = infoFolder.info || {};
                            GlobalService.current.breadcrumbs = infoFolder.breadcrumbs || {};
                            $scope.$watch('GlobalService.current.breadcrumbs', function () {
                                console.log('$watch GlobalService.current.breadcrumbs');
                            }, true);

                            GlobalService.interactive.type = $scope.type || 'undefined';
                            GlobalService.interactive.item = infoFolder.info || {};
                        });
                };
                $scope.get_folder_links = function () {
                    var page = $stateParams.page || 1;
                    var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
                    var params = {
                        username: $stateParams.user,
                        folder: $stateParams.pk,
                        page: page,
                        items: itemsEachPage
                    };
                    ManageServices.folderBookmark.page_item(params)
                        .$promise
                        .then(function (folderBookmark) {
                            GlobalService.current.folders = folderBookmark.folders || [];
                            GlobalService.current.links = folderBookmark.links || [];
                            GlobalService.current.paginator = folderBookmark.paginator || {};
                            $scope.folders = GlobalService.current.folders;
                            $scope.links = GlobalService.current.links;
                            $scope.mix = GlobalService.current.mix;
                        });
                };
                if (GlobalService.is_authenticated && GlobalService.user.username) {
                    $scope.get_folder_info();
                    $scope.get_folder_links();
                }
            }
        }
    })
;


