﻿'use strict';

angular.module('search.controllers', ['oc.lazyLoad'])
    .controller('SearchCtrl', function ($scope, $stateParams, $cookies, SearchServices, GlobalService) {
        $scope.ctrl_name = 'SearchCtrl';
        var keyword = $stateParams.keyword;
        GlobalService.current.info.name = 'Search';

        $scope.search_all = function () {
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                keyword: keyword,
                is_paginator: true, page: page, items: itemsEachPage,
                types: GlobalService.search.types
            };
            if (keyword) {
                SearchServices.search_all.page_items(params)
                    .$promise
                    .then(function (data) {
                        var total = 0;
                        if ($.inArray('bookmark', GlobalService.search.types)+1) {
                            $scope.folders = data.mix.folders;
                            $scope.links = data.mix.links;
                            total += parseInt($scope.folders.length) + parseInt($scope.links.length);
                        }
                        if ($.inArray('feedsource', GlobalService.search.types)+1) {
                            $scope.sources = data.sources[0];
                            total += parseInt($scope.sources.length);
                        }
                        if ($.inArray('interest', GlobalService.search.types)+1) {
                            $scope.interests = data.interests[0];
                            total += parseInt($scope.interests.length);
                        }
                        if ($.inArray('post', GlobalService.search.types)+1) {
                            $scope.posts = data.posts[0];
                            total += parseInt($scope.posts.length);
                        }
                        GlobalService.current.paginator.totalItems = total;
                    });
            }
        };
        $scope.search_all();
    })
    .controller('SearchSourcesCtrl', function ($scope, $stateParams, SearchServices, GlobalService) {
        $scope.ctrl_name = 'SearchSourcesCtrl';
        var keyword = $stateParams.keyword;
        GlobalService.current.info.name = 'Search Web';

        $scope.search_web = function () {
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                keyword: keyword,
                is_paginator: true, page: page, items: itemsEachPage,
                types: ["feedsource"]
            };
            if (keyword) {
                SearchServices.search_all.page_items(params)
                    .$promise
                    .then(function (data) {
                        $scope.sources = data.sources[0];
                        GlobalService.current.paginator = data.paginator_sources[0];
                    });
            }
        };
        $scope.search_web();
        $scope.changePage = function () {
            $scope.search_web();
        };
    })
    .controller('SearchInterestsCtrl', function ($scope, $stateParams, SearchServices, GlobalService) {
        $scope.ctrl_name = 'SearchInterestsCtrl';
        var keyword = $stateParams.keyword;
        GlobalService.current.info.name = 'Search Interest';

        $scope.search_interest = function () {
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                keyword: keyword,
                is_paginator: true, page: page, items: itemsEachPage,
                types: ["interest"]
            };
            if (keyword) {
                SearchServices.search_all.page_items(params)
                    .$promise
                    .then(function (data) {
                        $scope.interests = data.interests[0];
                        GlobalService.current.paginator = data.paginator_interests;
                    });
            }
        };
        $scope.search_interest();
        $scope.changePage = function () {
            $scope.search_interest();
        };
    })
    .controller('SearchBookmarksCtrl', function ($scope, $stateParams, SearchServices, GlobalService) {
        $scope.ctrl_name = 'SearchBookmarksCtrl';
        var keyword = $stateParams.keyword;
        GlobalService.current.info.name = 'Search bookmark';

        $scope.search_bookmark = function () {
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                keyword: keyword,
                is_paginator: true, page: page, items: itemsEachPage,
                types: ["bookmark"]
            };
            if (keyword) {
                SearchServices.search_all.page_items(params)
                    .$promise
                    .then(function (data) {
                        $scope.folders = data.mix.folders;
                        $scope.links = data.mix.links;
                        GlobalService.current.paginator = data.paginator_mix;
                    });
            }
        };
        $scope.search_bookmark();
        $scope.changePage = function () {
            $scope.search_bookmark();
        };
    })
    .controller('SearchPostsCtrl', function ($scope, $stateParams, SearchServices, GlobalService) {
        $scope.ctrl_name = 'SearchPostsCtrl';
        var keyword = $stateParams.keyword;
        GlobalService.current.info.name = 'Search Post';

        $scope.search_post = function () {
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                keyword: keyword,
                is_paginator: true, page: page, items: itemsEachPage,
                types: ["post"]
            };
            if (keyword) {
                SearchServices.search_all.page_items(params)
                    .$promise
                    .then(function (data) {
                        $scope.posts = data.posts[0];
                        GlobalService.current.paginator = data.paginator_posts;
                    });
            }
        };
        $scope.search_post();
        $scope.changePage = function () {
            $scope.search_post();
        };
    })
;


