﻿'use strict';

angular.module('manage.controllers', ['oc.lazyLoad'])
    .controller('TagsContentCtrl', function ($scope, $state, $stateParams, TagServices, GlobalService) {
        $scope.ctrl_name = 'TagsContentCtrl';
        GlobalService.current.info.name = $stateParams.slug;
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            GlobalService.current.info.name = $stateParams.slug;
            var params = {
                slug: $stateParams.slug,
                is_paginator: true, page: page, items: itemsEachPage
            };
            TagServices.with_tag.get_page_item(params)
                .$promise
                .then(function (content) {
                    GlobalService.current.folders = content.folders || [];
                    GlobalService.current.links = content.links || [];
                    GlobalService.current.paginator = content.paginator || {};
                    $scope.folders = GlobalService.current.folders;
                    $scope.links = GlobalService.current.links;
                    $scope.mix = GlobalService.current.mix;
                });
        }
    })
    .controller('TagsCloud', function ($scope, $state, $stateParams, $modal, TagServices, GlobalService) {
        $scope.ctrl_name = 'TagsCloud';
        GlobalService.current.info.name = 'Tags';
        var page = GlobalService.current.paginator.currentPage
            || $state.params.page || 1;
        var itemsEachPage = GlobalService.settings.itemsEachPage;
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            var user = GlobalService.user;
            var params = {
                username: user.username,
                page: page, items: itemsEachPage,
                is_extra: true, is_paginator: true
            };
            TagServices.userTags.page_item(params)
                .$promise
                .then(function (tags) {
                    GlobalService.current.links = tags.user_tags;
                    for (var i = 0; i < GlobalService.current.links.length; i++) {
                        GlobalService.current.links[i].tools = GlobalService.tools.tag;
                    }
                    GlobalService.current.paginator = tags.paginator;
                });

            $scope.deleteTag = function (item) {
                var params = {slug: item.slug};
                TagServices.with_tag.delete(params)
                    .$promise
                    .then(function (result) {
                        toastr.success('" <u>' + item.tag + '</u> " has been deleted successfully ! ', null, GlobalService.opts_toast);
                        $state.reload();
                    });
            };
            $scope.showRenameTagPopup = function (item) {
                var modalInstance = $modal.open({
                    animation: GlobalService.settings.animationsPopupEnabled,
                    templateUrl: 'templates/popup/rename-tag/',
                    controller: function ($scope, $modalInstance) {
                        $scope.modalTitle = 'Rename tag';
                        $scope.item = item;
                        $scope.renameTag = function (item) {
                            var params = {slug: item.slug, rename: item.rename};
                            TagServices.with_tag.rename(params)
                                .$promise
                                .catch(function (error) {
                                    $modalInstance.dismiss(error);
                                })
                                .then(function (result) {
                                    toastr.success('" <u>' + item.tag + '</u> " has been renamed successfully ! ', null, GlobalService.opts_toast);
                                    $modalInstance.close(result);
                                });
                        };
                        // dismissReason
                        $scope.cancel = function (reason) {
                            $modalInstance.dismiss(reason);
                        };
                    },
                    size: 'sm'
                });

                modalInstance.result
                    .then(function (result) {
                        $state.reload();
                    },
                    function (reason) {
                        console.log(reason);
                    });
            };

            GlobalService.tools.tag = [];
            GlobalService.tools.tag.push({name: 'Delete', func: 'deleteTag'});
            GlobalService.tools.tag.push({name: 'Rename', func: 'showRenameTagPopup'});
            $scope.callTool = function (func_name, item) {
                switch (func_name) {
                    case 'deleteTag':
                        $scope.deleteTag(item);
                        break;
                    case 'showRenameTagPopup':
                        $scope.showRenameTagPopup(item);
                        break;
                    default:
                        break;
                }
            };

            //paginator
            $scope.changeState = function () {
                var currentPage = GlobalService.current.paginator.currentPage || 1;
                $state.go('manage.tags.page', {page: currentPage});
            };
            $scope.changePage = function () {
                $scope.load();
            };
        }
    })
    .controller('TagsOtherCloud', function ($scope, $state, $stateParams, TagServices, GlobalService) {
        $scope.ctrl_name = 'TagsOtherCloud';
        GlobalService.current.info.name = 'Other Tags';
        var page = GlobalService.current.paginator.currentPage
            || $state.params.page || 1;
        var itemsEachPage = GlobalService.settings.itemsEachPage;
        var params = {
            page: page, items: itemsEachPage,
            is_extra: true, is_paginator: true
        };
        TagServices.sysTags.page_item(params)
            .$promise
            .then(function (tags) {
                GlobalService.current.links = tags.system_tags;
                for (var i = 0; i < GlobalService.current.links.length; i++) {
                    GlobalService.current.links[i].tools = GlobalService.tools.sys_tag;
                }
                GlobalService.current.paginator = tags.paginator;
            });

        //paginator
        $scope.changeState = function () {
            var currentPage = GlobalService.current.paginator.currentPage || 1;
            $state.go('manage.tags.other.page', {page: currentPage});
        };
        $scope.changePage = function () {
            $scope.load();
        };
    })
    .controller('ManageCtrl', function ($scope, $rootScope, $state, $stateParams, GlobalService, FolderServices, BookmarkServices, ManageServices) {
        $scope.ctrl_name = 'ManageCtrl';
        $scope.type = 'folder';
        $scope.links = [];
        $scope.folders = [];
        $scope.get_folder_info = function () {
            var params = {username: GlobalService.user.username, pk: $rootScope.$stateParams.pk || 'None'};
            FolderServices.folder.info(params)
                .$promise
                .then(function (infoFolder) {
                    GlobalService.current.type = $scope.type || 'undefined';
                    GlobalService.current.info = infoFolder.info || {};
                    GlobalService.current.breadcrumbs = infoFolder.breadcrumbs || {};
                    $scope.$watch('GlobalService.current.breadcrumbs', function () {
                        console.log('$watch GlobalService.current.breadcrumbs');
                    }, true);

                    GlobalService.interactive.type = $scope.type || 'undefined';
                    GlobalService.interactive.item = infoFolder.info || {};
                });
        };
        $scope.get_folder_links = function () {
            var page = $stateParams.page || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                username: GlobalService.user.username,
                folder: $rootScope.$stateParams.pk || 'None',
                page: page,
                items: itemsEachPage
            };
            ManageServices.folderBookmark.page_item(params)
                .$promise
                .then(function (folderBookmark) {
                    GlobalService.current.folders = folderBookmark.folders || [];
                    GlobalService.current.links = folderBookmark.links || [];
                    GlobalService.current.paginator = folderBookmark.paginator || {};
                    $scope.folders = GlobalService.current.folders;
                    $scope.links = GlobalService.current.links;
                    $scope.mix = GlobalService.current.mix;
                });
        };
        $scope.changeState = function () {
            $state.go('manage.view.page', {page: GlobalService.current.paginator.currentPage});
        };
        $scope.changePage = function () {
            $scope.load();
        };

        //feature of menu
        $rootScope.copy = function () {
            GlobalService.interactive.action = true;
            GlobalService.interactive.actionType = 1;
            GlobalService.interactive.from = $stateParams.pk || GlobalService.current.info.id || 'None';
            GlobalService.interactive.objectType = GlobalService.interactive.type;
            GlobalService.interactive.object = GlobalService.interactive.item;
        };
        $rootScope.cut = function () {
            GlobalService.interactive.action = true;
            GlobalService.interactive.actionType = 2;
            GlobalService.interactive.from = $stateParams.pk || GlobalService.current.info.id || 'None';
            GlobalService.interactive.objectType = GlobalService.interactive.type;
            GlobalService.interactive.object = GlobalService.interactive.item;
        };
        $rootScope.paste = function (iscurrent) {
            if (typeof iscurrent === 'undefined')
                var message = '';
            if (!GlobalService.interactive.action) {
                message = 'không có hành động copy hoặc cut !';
                console.log(message);
                return false;
            }
            if (GlobalService.interactive.type === 2) {
                message = 'Không đưa vào link được !';
                console.log(message);
                return false;
            }
            if (!GlobalService.interactive.object) {
                message = 'đối dượng hành động không có !';
                console.log(message);
                return false;
            }
            if ((GlobalService.interactive.actionType == 2 && GlobalService.interactive.object.id == GlobalService.interactive.item.id)) {
                message = 'cut: đối tượng cùng đích là 1 !';
                console.log(message);
                return false;
            }
            if (GlobalService.interactive.item.id == GlobalService.interactive.from) {
                message = 'cùng 1 nơi !';
                console.log(message);
                return false;
            }
            GlobalService.interactive.to = GlobalService.interactive.item.id;
            if (GlobalService.interactive.object.type === 'bookmark') {
                params = {
                    username: GlobalService.user.username,
                    pk: GlobalService.interactive.object.id,
                    folder: GlobalService.interactive.to || 'None'
                };
                if (GlobalService.interactive.actionType === 1) {
                    BookmarkServices.bookmarkDetail.copy(params)
                        .$promise
                        .then(function (data) {
                            toastr.success('Copy bookmark successful !', null, GlobalService.opts_toast);
                            if (iscurrent == 1) {
                                $state.reload();
                                return;
                            }
                            GlobalService.interactive.actionType = false;
                            GlobalService.interactive.objectType = false;
                            GlobalService.interactive.object = {};
                            GlobalService.interactive.to = false;
                            GlobalService.interactive.from = false;
                            GlobalService.interactive.action = false;

                            GlobalService.interactive.item = {};
                            $state.go('manage.view',
                                {viewType: GlobalService.settings.viewType, pk: data.parent_id});
                        });
                }
                else {
                    params.is_move = true;
                    BookmarkServices.bookmarkDetail.move(params)
                        .$promise
                        .then(function (data) {
                            toastr.success('Move bookmark successful !', null, GlobalService.opts_toast);
                            if (iscurrent == 1) {
                                $state.reload();
                                return;
                            }
                            GlobalService.interactive.actionType = false;
                            GlobalService.interactive.objectType = false;
                            GlobalService.interactive.object = {};
                            GlobalService.interactive.to = false;
                            GlobalService.interactive.from = false;
                            GlobalService.interactive.action = false;

                            GlobalService.interactive.item = {};
                            $state.go('manage.view',
                                {viewType: GlobalService.settings.viewType, pk: data.parent_id});
                        });
                }
            }
            if (GlobalService.interactive.object.type === "folder") {
                var params = {
                    username: GlobalService.user.username,
                    pk: GlobalService.interactive.object.id,
                    parent: GlobalService.interactive.to || 'None'
                };
                if (GlobalService.interactive.actionType === 1) {
                    FolderServices.folder.copy(params)
                        .$promise
                        .then(function (data) {
                            toastr.success('Copy folder successful !', null, GlobalService.opts_toast);
                            if (iscurrent == 1) {
                                $state.reload();
                                return;
                            }
                            GlobalService.interactive.actionType = false;
                            GlobalService.interactive.objectType = false;
                            GlobalService.interactive.object = {};
                            GlobalService.interactive.to = false;
                            GlobalService.interactive.from = false;
                            GlobalService.interactive.action = false;

                            GlobalService.interactive.item = {};
                            $state.go('manage.view',
                                {viewType: GlobalService.settings.viewType, pk: data.parent_id});
                        });
                }
                else {
                    params.is_move = true;
                    FolderServices.folder.move(params)
                        .$promise
                        .then(function (data) {
                            toastr.success('Move folder successful !', null, GlobalService.opts_toast);
                            if (iscurrent == 1) {
                                $state.reload();
                                return;
                            }
                            GlobalService.interactive.actionType = false;
                            GlobalService.interactive.objectType = false;
                            GlobalService.interactive.object = {};
                            GlobalService.interactive.to = false;
                            GlobalService.interactive.from = false;
                            GlobalService.interactive.action = false;

                            GlobalService.interactive.item = {};
                            $state.go('manage.view',
                                {viewType: GlobalService.settings.viewType, pk: data.parent_id});
                        });
                }
            }
        };
        $scope.deleteBookmark = function (item) {

        };
        $scope.publish = function () {
            GlobalService.interactive.actionType = 'public_un_public';
            var folderID = 'None';
            if ($stateParams.pk != undefined) {
                folderID = $stateParams.pk;
            }
            var folder_params = {
                username: GlobalService.user.username, folderID: folderID, item: {
                    id: GlobalService.interactive.item.id,
                    name: GlobalService.interactive.item.name,
                    description: GlobalService.interactive.item.description || null
                }, publish: GlobalService.interactive.actionType
            };
            FolderServices.folder.update(folder_params)
                .$promise
                .then(function (data) {
                    if (data.error == 'SUCCESS') {
                        GlobalService.interactive.actionType = false;
                        GlobalService.interactive.item.public = 1;
                    }
                });
        };
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.get_folder_info();
            $scope.get_folder_links();
        }
    })
    .controller('SidebarManageMenuCtrl', function ($scope, $rootScope, $menuItems, GlobalService, FolderServices, TagServices) {
        $scope.ctrl_name = 'SidebarManageMenuCtrl';
        public_vars.$sidebarMenu = public_vars.$body.find('.sidebar-menu');
        $rootScope.$sidebarMenuItems = $menuItems.instantiate();

        $scope.getMenuFolder = function () {
            var user_folder_params = {
                username: GlobalService.user.username,
                is_extra: true,
                is_paginator: false, page: 1, items: 10
            };
            FolderServices.folders.default(user_folder_params)
                .$promise
                .then(function (response) {
                    var list_menu = response.object_folders;
                    $rootScope.folder_menu = $rootScope.$sidebarMenuItems;
                    $rootScope.folder_menu.addItem('All Folders', 'manage.view');

                    for (var i = 0; i < list_menu.length; i++) {
                        var menu = $rootScope.folder_menu.addItem(list_menu[i], 'manage.view', {pk: list_menu[i].id}
                        );
                    }
                    $scope.menuItems = $rootScope.$sidebarMenuItems.getAll();
                });
        };

        $rootScope.$sidebarMenuTags = $menuItems.instantiate();

        $scope.getMenuTag = function () {
            var tags_params = {
                username: GlobalService.user.username,
                page: 1, items: 4,
                is_extra: false, is_paginator: true
            };
            TagServices.userTags.page_item(tags_params)
                .$promise
                .then(function (userTagsList) {
                    var list_menu = userTagsList.user_tags;
                    var user_tags_menu = $rootScope.$sidebarMenuTags;
                    user_tags_menu.addItem('Your Tags', 'manage.tags');

                    for (var i = 0; i < list_menu.length; i++) {
                        var menu = user_tags_menu.addItem(list_menu[i], 'manage.content_tag', {slug: list_menu[i].slug});
                    }
                    $scope.menuTags = $rootScope.$sidebarMenuTags.getAll();
                });
        };

        $rootScope.$sidebarMenuOther = $menuItems.instantiate();

        $scope.getMenuOther = function () {
            var sys_tags_params = {
                page: 1, items: 4,
                is_extra: false, is_paginator: true
            };
            TagServices.sysTags.page_item(sys_tags_params)
                .$promise
                .then(function (otherTagsList) {
                    var list_menu = otherTagsList.system_tags;
                    var tags_menu = $rootScope.$sidebarMenuOther;
                    tags_menu.addItem('Other Tags', 'manage.tags.other');

                    for (var i = 0; i < list_menu.length; i++) {
                        var menu = tags_menu.addItem(list_menu[i], 'manage.content_tag', {slug: list_menu[i].slug});
                    }
                    $scope.menuOther = $rootScope.$sidebarMenuOther.getAll();
                });
        };

        $rootScope.getChildFolderForItemMenu = function (menuItem) {
            var object_params = {
                username: GlobalService.user.username,
                pk: menuItem.data.id,
                is_extra: true,
                is_paginator: false, page: 1, items: 10
            };
            FolderServices.folders.of_object(object_params)
                .$promise
                .then(function (response) {
                    menuItem.clearType();
                    menuItem.data.childrent = response.object_folders;
                    var list_objects = response.object_folders;
                    for (var i = 0; i < list_objects.length; i++) {
                        var menu = menuItem.addItem(list_objects[i], 'manage.view', {pk: list_objects[i].id}
                        );
                    }
                });
        };

        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.getMenuFolder();
            $scope.getMenuTag();
        }
        $scope.getMenuOther();
    })
;


