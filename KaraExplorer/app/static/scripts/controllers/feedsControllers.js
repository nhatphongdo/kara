﻿'use strict';

angular.module('feeds.controllers', ['oc.lazyLoad'])
    .controller('FeedsCtrl', function ($scope, $rootScope, $state, $stateParams, GlobalService, ProfileServices, FeedsServices, ActionServices, TagServices) {
        $scope.ctrl_name = 'FeedsCtrl';
        if ($state.is('feeds.view') || $state.is('feeds.view.page')) {
            $scope.type = 'category';
            $scope.posts = [];

            $scope.get_category_info = function () {
                var params = {username: GlobalService.user.username, name: $rootScope.$stateParams.pk || 'None'};
                ProfileServices.categories.info(params)
                    .$promise
                    .then(function (infoCategory) {
                        GlobalService.current.type = $scope.type || 'undefined';
                        GlobalService.current.info = infoCategory.user_main_category || {};

                        GlobalService.interactive.type = $scope.type || 'undefined';
                        GlobalService.interactive.item = infoCategory.user_main_category || {};
                    });
            };
            $scope.get_category_feed_sources = function () {
                GlobalService.current.feed_sources = [];
            };
            $scope.get_category_posts = function () {
                $scope.loading = true;
                var page = $stateParams.page || 1;
                var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
                var params = {
                    username: GlobalService.user.username,
                    name: $rootScope.$stateParams.pk || 'None',
                    is_paginator: true,
                    page: page,
                    items: itemsEachPage
                };
                ProfileServices.categories.posts_page_item(params)
                    .$promise
                    .then(function (sources) {
                        GlobalService.current.posts = sources.posts || [];
                        GlobalService.current.paginator = sources.paginator || {};
                        $scope.posts = GlobalService.current.posts;
                        $scope.loading = false;
                    });
            };
            $scope.changeState = function () {
                if ($rootScope.$stateParams.pk == undefined || $rootScope.$stateParams.pk == '')
                    $rootScope.$stateParams.pk = 'None';
                $state.go('feeds.view.page', {page: GlobalService.current.paginator.currentPage});
            };
            $scope.changePage = function () {
                $scope.get_category_posts();
            };

            if (GlobalService.is_authenticated && GlobalService.user.username) {
                $scope.get_category_info();
                $scope.get_category_feed_sources();
                $scope.get_category_posts();
            }
        }
        else if ($state.is('feeds.view.src') || $state.is('feeds.view.src.page')) {
            $scope.type = 'feedsource';
            $scope.posts = [];
            $scope.get_source_info = function () {
                var params = {src: $rootScope.$stateParams.src};
                FeedsServices.source.info(params)
                    .$promise
                    .then(function (infoSource) {
                        GlobalService.current.type = $scope.type || 'undefined';
                        GlobalService.current.info = infoSource.info || {};
                        GlobalService.current.info.type = $scope.type || 'undefined';

                        var share = {
                            token: 'Kara179',
                            user: GlobalService.user.username,
                            hashed_id: GlobalService.current.info.id,
                            type: GlobalService.current.info.type
                        };
                        GlobalService.current.info.share_url = location.origin + '/'
                            + $state.href('share',
                                {content_type: share.type, pk: share.hashed_id, user: share.user, token: share.token});

                        GlobalService.current.info.parents = [];
                        var categories = infoSource.info.categories;
                        for (var i = 0; i < categories.length; i++) {
                            if (categories[i].id == $rootScope.$stateParams.pk) {
                                GlobalService.current.info.parents.push(categories[i]);
                                GlobalService.current.info.parent = categories[i];
                                break;
                            }
                        }
                        GlobalService.current.info.parents.push({
                            'id': infoSource.info.id,
                            'name': infoSource.info.name
                        });
                        GlobalService.current.info.parents.reverse();

                        GlobalService.interactive.type = $scope.type || 'undefined';
                        GlobalService.interactive.item = infoSource.info || {};
                    });
            };
            $scope.get_source_posts = function () {
                $scope.loading = true;
                var page = $stateParams.page || 1;
                var itemsEachPage = GlobalService.settings.itemsEachPage;
                var params = {
                    src: $rootScope.$stateParams.src,
                    page: page,
                    items: itemsEachPage
                };
                FeedsServices.source.contents_page_item(params)
                    .$promise
                    .then(function (contents) {
                        GlobalService.current.posts = contents.links || [];
                        GlobalService.current.paginator = contents.paginator || {};
                        $scope.posts = GlobalService.current.posts;
                        $scope.loading = false;
                    });
            };
            $scope.get_source_subscriber = function () {
                $scope.loading = true;
                var params = {
                    pk: $rootScope.$stateParams.src
                };
                ActionServices.find_user.subscriber(params)
                    .$promise
                    .then(function (result) {
                        GlobalService.current.subscriber = result.users;
                    });
            };
            $scope.get_tag = function () {
                $scope.loading = true;
                var params = {
                    content_type: $scope.type,
                    pk: $rootScope.$stateParams.src
                };
                TagServices.find_tag.default(params)
                    .$promise
                    .then(function (result) {
                        GlobalService.current.tags = result.tags;
                    });
            };
            $scope.changeState = function () {
                if ($rootScope.$stateParams.pk == undefined || $rootScope.$stateParams.pk == '')
                    $rootScope.$stateParams.pk = 'None';
                $state.go('feeds.view.src.page', {page: GlobalService.current.paginator.currentPage});
            };
            $scope.changePage = function () {
                $scope.get_source_posts();
            };

            $rootScope.actionView({id: $stateParams.src}, $scope.type);
            if (GlobalService.is_authenticated && GlobalService.user.username) {
                $scope.get_source_info();
                $scope.get_source_posts();
                $scope.get_source_subscriber();
                $scope.get_tag();
            }
        }
    })
    .controller('SidebarFeedsMenuCtrl', function ($scope, $rootScope, $menuItems, GlobalService, ProfileServices, FeedsServices) {
        $scope.ctrl_name = 'SidebarFeedsMenuCtrl';
        // Trigger menu setup
        public_vars.$sidebarMenu = public_vars.$body.find('.sidebar-menu');
        $rootScope.$sidebarMenuItems = $menuItems.instantiate();

        $scope.getMainCategories = function () {
            $rootScope.categories_menu = $rootScope.$sidebarMenuItems;
            $rootScope.categories_menu.addItem('All', 'feeds.view');
            FeedsServices.categories.default()
                .$promise
                .then(function (main_categories) {
                    GlobalService.categories = main_categories.main_categories;
                    $scope.categories = main_categories.main_categories;

                    for (var i = 0; i < $scope.categories.length; i++) {
                        $rootScope.categories_menu.addItem($scope.categories[i], 'feeds.view', {pk: $scope.categories[i].id});
                    }
                });

            $scope.menuItems = $rootScope.$sidebarMenuItems.getAll();
        };

        $scope.getUserMainCategories = function () {
            $rootScope.categories_menu = $rootScope.$sidebarMenuItems;
            $rootScope.categories_menu.addItem('All', 'feeds.view');
            var params = {
                username: GlobalService.user.username
            };
            ProfileServices.categories.default(params)
                .$promise
                .then(function (main_categories) {
                    GlobalService.categories = main_categories.user_main_categories;
                    $scope.categories = main_categories.user_main_categories;

                    for (var i = 0; i < $scope.categories.length; i++) {
                        $rootScope.categories_menu.addItem($scope.categories[i], 'feeds.view', {pk: $scope.categories[i].slug});
                    }
                });

            $scope.menuItems = $rootScope.$sidebarMenuItems.getAll();
        };

        $rootScope.getChildSourcesForMenu = function (item) {
            if (item.isOpen) {
                if (item.menuItems.length) {
                    for (var j = 0; j < item.menuItems.length; j++) {
                        $rootScope.get_statistic(item.menuItems[j].data, 'feedsource');
                        item.menuItems[j].updateItem();
                    }
                }
                else {
                    var params = {username: GlobalService.user.username, name: item.params.pk};
                    ProfileServices.categories.sources_alias(params)
                        .$promise
                        .then(function (category_sources) {
                            item.clearType('feedsource');
                            var subscribes = category_sources.subscribes;
                            item.data.feeds_src = subscribes;
                            var feeds_src = subscribes;
                            for (var j = 0; j < feeds_src.length; j++) {
                                feeds_src[j].type = 'feedsource';
                                item.addItem(feeds_src[j], 'feeds.view.src', {pk: item.params.pk, src: feeds_src[j].id});
                            }
                        });
                }
            }
        };

        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.getUserMainCategories();
        }
    })
    .controller('AddFeedsCtrl', function ($scope, $rootScope, $state, GlobalService, FeedsServices) {
        $scope.ctrl_name = 'AddFeedsCtrl';
        GlobalService.current.info.name = 'Add feeds';
        GlobalService.current.paginator = {};
        $scope.search_filter = 'Search';
        $scope.search_input = '';

        $scope.get_sys_categories = function () {
            FeedsServices.categories.default()
                .$promise
                .then(function (main_categories) {
                    GlobalService.main_categories = main_categories.main_categories;
                    if ($state.current.name == 'feeds.add.category') {
                        $scope.search_input = '';
                        $scope.search_filter = $state.params.category;
                        var categs = GlobalService.main_categories;
                        for (var i = 0; i < categs.length; i++) {
                            if (categs[i].name == $state.params.category) {
                                $scope.get_sources_of_category(categs[i]);
                            }
                        }
                    }
                });
        };
        $scope.get_sources_of_category = function (item) {
            $scope.search_filter = item.name;
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                pk: item.id,
                is_paginator: true,
                page: page,
                items: itemsEachPage
            };
            FeedsServices.category.sources_page_item(params)
                .$promise
                .then(function (sources) {
                    $scope.sources_result = sources.sources;
                    GlobalService.current.paginator = sources.paginator || {};
                    $scope.typeOfGet = 'by_category';
                    $scope.search_category = item;
                });
            $scope.search_input = '';
            $state.go("feeds.add.category", {category: $scope.search_filter});
        };
        $scope.get_sources_with_key = function () {
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                key: $scope.search_input,
                is_paginator: true,
                page: page,
                items: itemsEachPage
            };
            if ($scope.search_input == undefined || $scope.search_input.length) {
                FeedsServices.source.key_page_item(params)
                    .$promise
                    .then(function (sources) {
                        $scope.sources_result = sources.sources;
                        GlobalService.current.paginator = sources.paginator || {};
                        $scope.typeOfGet = 'by_search_key';
                        $scope.search_input = params.key;
                    });
                $state.go("feeds.add.search", {keyword: $scope.search_input});
            }
        };

        $scope.get_sys_categories();

        $scope.changePage = function () {
            switch ($scope.typeOfGet) {
                case 'by_category':
                    $scope.get_sources_of_category($scope.search_category);
                    break;
                case 'by_search_key':
                    $scope.get_sources_with_key();
                    break;
                default :
                    break;
            }
        };

        if ($state.current.name == 'feeds.add.search') {
            $scope.search_input = $state.params.keyword;
            $scope.get_sources_with_key();
        }

        $scope.listTour = [
            {
                element: '.step1',
                status: true,
                position: 'right'
            },
            {
                element: '.step2',
                status: false,
                position: 'right'
            },
            {
                element: '.step3',
                status: false,

                position: 'left'
            },
            {
                element: '.step4',
                status: false,

                position: 'bottom'
            },
            {
                element: '.step5',
                status: false,
                intro: 'Get it, use it.'
            }
        ];
        if (localStorage.getItem('someData')) {
            $scope.listTour = JSON.parse(localStorage.getItem('someData'));
        }

        $scope.first_time = false;
        // $scope.start = true;
        if ($scope.first_time) {
            $scope.step1 = true;
        }
        $scope.nextStep = function (current, optional) {
            for (var i in $scope.listTour) {
                if ($scope.listTour[i].element == current) {
                    $(current).css('display', 'none');
                    $scope.listTour[i].status = false;
                    var j = parseInt(i) + 1;
                    var nextElement = $scope.listTour[j].element;
                    $scope.listTour[j].status = true;
                    $(nextElement).css('display', 'block');
                }
            }
            if (optional == 'reload') {
                var dataToStore = JSON.stringify($scope.listTour);
                localStorage.setItem('someData', dataToStore);
                $state.reload();
            }
            //$scope.step1 = false;
            //$scope.step2 = true;
            //$(current).css('display', 'none');
        };
    })
    .controller('FeedsManageCtrl', function ($scope, $rootScope, GlobalService, ProfileServices, ActionServices) {
        $scope.ctrl_name = 'FeedsManageCtrl';
        GlobalService.current.info.name = 'Manage feeds';
        GlobalService.current.paginator = {};

        $scope.selected = {};
        $scope.newzone = {sources: []};
        $scope.dragStart = function (item) {
            console.log('start drag ', item.name);
        };
        $scope.dragEnd = function (item) {
            console.log('end drag ', item.name);
        };
        $scope.dragSelected = function (item) {
            console.log('selected ', item.name);
            $scope.selected = item;
        };
        $scope.dragMoved = function (list, index) {
            console.log('moved ', list[index].name);
            list.splice(index, 1);
        };
        $scope.dragCopied = function (item) {
            item.name = 'Copy of ' + item.name;
            console.log('copied ', item.name);
        };
        $scope.dragCanceled = function (item) {
            console.log('canceled ', item.name);
        };
        $scope.dragOver = function (event, index, type, external) {
            console.log('dragged over the list.', event, index, item, type);
            return true;
        };
        $scope.dropOver = function (event, index, item, type, external, zone) {
            console.log('dropped over the list.');
            console.log(type, zone);
            var params = {
                username: GlobalService.user.username,
                pk: item.id,
                category: zone.slug,
                category_name: zone.name
            };
            ProfileServices.sources_alias.move(params)
                .$promise
                .then(function (category_source) {
                    var subscribe = category_source.subscribe;
                    for (var i = 0; i < subscribe.length; i++) {
                        subscribe[i].feed_source.alias_name = subscribe[i].alias_name;
                        subscribe[i] = subscribe[i].feed_source;
                    }
                    return subscribe;
                });
            return item;
        };
        $scope.dropToNewZone = function (event, index, item, type, external, zone) {
            $rootScope.showCreateUserCategory(item);
            console.log('dropped to new zone.');
            console.log(type, zone);
            return item;
        };
        $scope.inserted = function (event, index, item, type, external) {
            console.log('inserted into the list.');
        };

        $scope.getZones = function () {
            var params = {
                username: GlobalService.user.username
            };
            ProfileServices.categories.default(params)
                .$promise
                .then(function (main_categories) {
                    GlobalService.categories = main_categories.user_main_categories;
                    $scope.categories = main_categories.user_main_categories;
                    for (var i = 0; i < $scope.categories.length; i++) {
                        //$scope.getSources($scope.categories[i]);
                        $scope.getSourcesAlias($scope.categories[i]);
                    }
                });
        };

        $scope.getSources = function (item) {
            var params = {username: GlobalService.user.username, name: item.slug};
            ProfileServices.categories.sources(params)
                .$promise
                .then(function (category_sources) {
                    item.sources = category_sources.subscribes;
                });
        };

        $scope.getSourcesAlias = function (item) {
            var params = {username: GlobalService.user.username, name: item.slug};
            ProfileServices.categories.sources_alias(params)
                .$promise
                .then(function (category_sources) {
                    var subscribes = category_sources.subscribes;
                    for (var i = 0; i < subscribes.length; i++) {
                        subscribes[i].feed_source.alias_name = subscribes[i].alias_name;
                        subscribes[i] = subscribes[i].feed_source;
                    }
                    item.sources = subscribes;
                });
        };

        $scope.moveSourcesAlias = function (item, zone) {
            var params = {
                username: GlobalService.user.username,
                pk: item.id,
                category: zone.slug,
                category_name: zone.name
            };
            ProfileServices.sources_alias.move(params)
                .$promise
                .then(function (category_source) {
                });
        };

        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.getZones();
        }

        $scope.delAllSources = function (item) {
            var list_object_id = [];
            for (var i = 0; i < item.sources.length; i++) {
                list_object_id.push(item.sources[i].id);
            }
            var params = {'list_object_id': list_object_id};
            ActionServices.unsubscribe.sources(params)
                .$promise
                .then(function () {
                    $scope.getSourcesAlias(item);
                });
            var options = GlobalService.opts_toast;
            toastr.warning('<p class="remove-notification">"' + item.name + '" has been removed sucessfully !</p>' +
                '   <button" class="btn btn-trans btn-undo">' +
                '           <i class="fa fa-repeat"></i> Undo</button>', null, options);

            $(".btn-undo").on('click', function () {
                var params = {'list_object_id': list_object_id, restore: true};
                ActionServices.restore.sources(params)
                    .$promise
                    .then(function () {
                        $scope.getSourcesAlias(item);
                    });
            });
        };
        $scope.restoreAllSources = function (item) {
            var list_object_id = [];
            for (var i = 0; i < item.sources.length; i++) {
                list_object_id.push(item.sources[i].id);
            }
            var params = {'list_object_id': list_object_id};
            ActionServices.restore.sources(params)
                .$promise
                .then(function () {
                    $scope.getSourcesAlias(item);
                });
        };
        $scope.delOneSource = function (zone, $index) {
            var list_object_id = [];
            for (var i = 0; i < zone.sources.length; i++) {
                if (i == $index) {
                    list_object_id.push(zone.sources[i].id);
                    var item = zone.sources[i]
                }
            }
            var options = GlobalService.opts_toast;
            toastr.warning('<p class="remove-notification">"' + item.name + '" has been removed sucessfully !</p>' +
                '   <button" class="btn btn-trans btn-undo">' +
                '           <i class="fa fa-repeat"></i> Undo</button>', null, options);
            var params = {'list_object_id': list_object_id};
            ActionServices.unsubscribe.sources(params)
                .$promise
                .then(function () {
                    $scope.getSourcesAlias(zone);
                });

            $(".btn-undo").on('click', function () {
                var params = {'list_object_id': list_object_id, restore: true};
                ActionServices.restore.sources(params)
                    .$promise
                    .then(function () {
                        $scope.getSourcesAlias(zone);
                    });
            });
        };
    })
;


