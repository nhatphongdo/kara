﻿'use strict';

angular.module('interest.controllers', ['oc.lazyLoad'])
    .controller('InterestsCtrl', function ($scope, $rootScope, $state, $stateParams, GlobalService, InterestServices) {
        $scope.ctrl_name = 'InterestsCtrl';
        $scope.type = 'interest';
        $scope.posts = [];
        $scope.get_interest_info = function () {
            var params = {pk: $rootScope.$stateParams.pk || 'None'};
            InterestServices.interest.info(params)
                .$promise
                .then(function (infoInterest) {
                    GlobalService.current.type = $scope.type || 'undefined';
                    GlobalService.current.info = infoInterest.info || {};

                    GlobalService.interactive.type = $scope.type || 'undefined';
                    GlobalService.interactive.item = infoInterest.info || {};
                });
        };
        $scope.get_interest_posts = function () {
            $scope.loading = true;
            var page = $stateParams.page || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage;
            var params = {
                pk: $rootScope.$stateParams.pk || 'None',
                page: page,
                items: itemsEachPage
            };
            InterestServices.interest.posts_page_item(params)
                .$promise
                .then(function (sources) {
                    GlobalService.current.posts = sources.links || [];
                    GlobalService.current.paginator = sources.paginator || {};
                    $scope.posts = GlobalService.current.posts;
                    $scope.loading = false;
                });
        };
        $scope.changeState = function () {
            $state.go('interests' + '.view.page', { page: GlobalService.current.paginator.currentPage });
        };
        $scope.changePage = function () {
            $scope.get_interest_posts();
        };
        $scope.get_interest_info();
        $scope.get_interest_posts();
    })
    .controller('SidebarInterestsMenuCtrl', function ($scope, $rootScope, $menuItems, $timeout, InterestServices, GlobalService, ProfileServices) {
        $scope.ctrl_name = 'SidebarInterestsMenuCtrl';
        // Trigger menu setup
        public_vars.$sidebarMenu = public_vars.$body.find('.sidebar-menu');
        $rootScope.$sidebarMenuItems = $menuItems.instantiate();

        $scope.getMainInterest = function () {
            $rootScope.interests_menu = $rootScope.$sidebarMenuItems;
            $rootScope.interests_menu.addItem('All', 'interests.view');

            InterestServices.interests.main()
                .$promise
                .then(function (main_interests) {
                    GlobalService.interests = main_interests.main_interests;
                    $scope.interests = main_interests.main_interests;

                    for (var i = 0; i < $scope.interests.length; i++) {
                        $rootScope.interests_menu.addItem($scope.interests[i], 'interests.view', { pk: $scope.interests[i].id });
                    }
                });

            $scope.menuItems = $rootScope.$sidebarMenuItems.getAll();
        };

        $scope.getUserMainInterest = function () {
            $rootScope.interests_menu = $rootScope.$sidebarMenuItems;
            $rootScope.interests_menu.addItem('All', 'interests.view');
            var params = {
                username: GlobalService.user.username
            };
            ProfileServices.interests.main(params)
                .$promise
                .then(function (main_interests) {
                    GlobalService.interests = main_interests.user_main_interest;
                    $scope.interests = main_interests.user_main_interest;

                    for (var i = 0; i < $scope.interests.length; i++) {
                        $rootScope.interests_menu.addItem($scope.interests[i], 'interests.view', { pk: $scope.interests[i].id });
                    }
                });

            $scope.menuItems = $rootScope.$sidebarMenuItems.getAll();

            // Set Active Menu Item

            $timeout(setup_sidebar_menu, 1);
            ps_init();
        };

        $rootScope.getChildInterestsForMenu = function (item) {
            var params = {
                username: GlobalService.user.username,
                is_paginator: false,
                page: 1, items: 10,
                pk: item.params.pk
            };
            ProfileServices.interests.child(params)
                .$promise
                .then(function (interests) {
                    item.clearType('interest');
                    item.data.interests = interests.interests;
                    interests = interests.interests;
                    for (var j = 0; j < interests.length; j++) {
                        interests[j].type = 'interest';
                        item.addItem(interests[j], 'interests.view', {pk: interests[j].id});
                    }
                });
        };
        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.getUserMainInterest();
        }

        $rootScope.trash_interest = function (item, list, index) {
            var params = {username: GlobalService.user.username, pk: item.id, is_trash: true};
            InterestServices.interest.pin(params)
                .$promise
                .then(function (error) {
                    if (error.error === 'SUCCESS') {
                        item.is_trash = true;
                        list.splice(index, 1);
                    }
                    else if (error.error === 'FAILED') {
                        item.is_trash = false;
                    }
                });
        };
    })
    .controller('AddInterestsCtrl', function ($scope, $rootScope, $state, ngDialog, InterestServices, GlobalService) {
        $scope.ctrl_name = 'AddInterestsCtrl';
        GlobalService.current.info.name = 'Add interests';
        GlobalService.current.paginator = {};
        $scope.search_filter = 'Search';
        $scope.search_input = '';

        $scope.get_sys_interests = function () {
            InterestServices.interests.main()
                .$promise
                .then(function (main_interests) {
                    GlobalService.main_interests = main_interests.main_interests;
                    if ($state.current.name == 'interests.add.interest') {
                        $scope.search_input = '';
                        $scope.search_filter = $state.params.interest;
                        var interests = GlobalService.main_interests;
                        for (var i = 0; i < interests.length; i++) {
                            if (interests[i].name == $state.params.interest) {
                                $scope.get_child_of_interest(interests[i]);
                            }
                        }
                    }
                });
        };
        $rootScope.showPopupInterests = function (item) {
            $scope.link = 'templates/popup/add-browser-interests/';
            ngDialog.open({
                template: $scope.link,
                controller: 'PopupInterestCtrl',
                resolve: {
                    OpenItem: function () {
                        return item;
                    }
                }
            });
        };
        $rootScope.get_child_of_interest = function (item) {
            $scope.search_filter = item.name;
            var params = {pk: item.id};
            InterestServices.interests.child(params)
                .$promise
                .then(function (interests) {
                    $scope.interests = interests.interests;
                });
            $scope.search_input = '';
            $state.go("interests.add.interest", {interest: $scope.search_filter});
        };
        $scope.get_interests_with_key = function () {
            var page = GlobalService.current.paginator.currentPage || 1;
            var itemsEachPage = GlobalService.settings.itemsEachPage || 10;
            var params = {
                key: $scope.search_input,
                is_paginator: true,
                page: page,
                items: itemsEachPage
            };
            if ($scope.search_input == undefined || $scope.search_input.length) {
                InterestServices.interests.key_page_item(params)
                    .$promise
                    .then(function (interests) {
                        $scope.interests = interests.interests;
                        GlobalService.current.paginator = interests.paginator || {};
                    });
                $state.go("interests.add.search", {keyword: $scope.search_input});
            }
        };
        $rootScope.pin_interest = function (item) {
            var params = {username: GlobalService.user.username, pk: item.id};
            InterestServices.interest.pin(params)
                .$promise
                .then(function (error) {
                    if (error.error === 'SUCCESS') {
                        var options = GlobalService.opts_toast;
                        toastr.success('Subscribe ' + item.name + ' successfully !', null, options);
                        item.pined = true;
                    }
                    else if (error.error === 'FAILED') {
                        item.pined = false;
                    }
                });
        };
        $scope.get_sys_interests();

        if ($state.current.name == 'interests.add.search') {
            $scope.search_input = $state.params.keyword;
            $scope.get_interests_with_key();
        }
    })
    .controller('InterestsManageCtrl', function ($scope, GlobalService, ProfileServices, ActionServices) {
        $scope.ctrl_name = 'InterestsManageCtrl';
        GlobalService.current.info.name = 'Manage interests';
        GlobalService.current.paginator = {};

        $scope.dragoverCallback = function (event, index, external, type) {
            $scope.logListEvent('dragged over', event, index, external, type);
            // Disallow dropping in the third row. Could also be done with dnd-disable-if.
            return index < 10;
        };

        $scope.dropCallback = function (event, index, item, external, type, allowedType) {
            $scope.logListEvent('dropped at', event, index, external, type);
            if (external) {
                if (allowedType === 'itemType' && !item.name) return false;
                if (allowedType === 'containerType' && !angular.isArray(item)) return false;
            }

            return item;
        };
        $scope.logListEvent = function (action, event, index, external, type) {
            var message = external ? 'External ' : '';
            message += type + ' element is ' + action + ' position ' + index;
            console.log(type);

            $scope.logEvent(message, event);
        };
        $scope.logEvent = function (message, event) {
            //console.log(message, '(triggered by the following', event.type, 'event)');
            //console.log(event);
        };

        $scope.getZones = function () {
            var params = {
                username: GlobalService.user.username
            };
            ProfileServices.interests.main(params)
                .$promise
                .then(function (main_interests) {
                    GlobalService.interests = main_interests.user_main_interest;

                    $scope.interests = main_interests.user_main_interest;

                    for (var i = 0; i < $scope.interests.length; i++) {
                        $scope.getInterests($scope.interests[i]);
                    }
                });
        };

        $scope.getInterests = function (item) {
            var params = {
                username: GlobalService.user.username,
                is_paginator: false,
                page: 1, items: 10,
                pk: item.id
            };
            ProfileServices.interests.child(params)
                .$promise
                .then(function (interests) {
                    item.interests = interests.interests;
                });
        };

        if (GlobalService.is_authenticated && GlobalService.user.username) {
            $scope.getZones();
        }

        $scope.delAllInterests = function (item) {
            var list_object_id = [];
            for (var i = 0; i < item.interests.length; i++) {
                list_object_id.push(item.interests[i].id);
            }
            var options = GlobalService.opts_toast;
            toastr.warning('<p class="remove-notification">"' + item.name + '" has been removed sucessfully !</p>' +
                '<button" class="btn btn-trans btn-undo">' +
                '<i class="fa fa-repeat"></i> Undo</button>', null, options);
            var params = {'list_object_id': list_object_id};
            ActionServices.unsubscribe.interests(params)
                .$promise
                .then(function () {
                    $scope.getInterests(item);
                });

            $(".btn-undo").on('click', function () {
                var params = {'list_object_id': list_object_id, restore: true};
                ActionServices.restore.interests(params)
                    .$promise
                    .then(function () {
                        $scope.getInterests(item);
                    });
            });
        };
        $scope.restoreAllInterests = function (item) {
            var list_object_id = [];
            for (var i = 0; i < item.interests.length; i++) {
                list_object_id.push(item.interests[i].id);
            }
            var params = {'list_object_id': list_object_id};
            ActionServices.restore.interests(params)
                .$promise
                .then(function () {
                    $scope.getInterests(item);
                });
        };
        $scope.delOneInterest = function (zone, $index) {
            var list_object_id = [];
            for (var i = 0; i < zone.interests.length; i++) {
                if (i == $index) {
                    list_object_id.push(zone.interests[i].id);
                    var item = zone.interests[i]
                }
            }
            var options = GlobalService.opts_toast;
            toastr.warning('<p class="remove-notification">"' + item.name + '" has been removed sucessfully !</p>' +
                '<button" class="btn btn-trans btn-undo">' +
                '<i class="fa fa-repeat"></i> Undo</button>', null, options);
            var params = {'list_object_id': list_object_id};
            ActionServices.unsubscribe.interests(params)
                .$promise
                .then(function () {
                    $scope.getInterests(zone);
                });

            $(".btn-undo").on('click', function () {
                var params = {'list_object_id': list_object_id, restore: true};
                ActionServices.restore.interests(params)
                    .$promise
                    .then(function () {
                        $scope.getInterests(zone);
                    });
            });
        };
    })
    .controller('PopupInterestCtrl', function ($scope, $rootScope, OpenItem) {
        $scope.ctrl_name = 'PopupInterestCtrl';
        $scope.popupType = 'interest';
        $scope.actionView(OpenItem, 'interest');
        $scope.item = OpenItem;
        $rootScope.get_child_of_interest(OpenItem);
    })
;


