﻿'use strict';

angular.module('auth.controllers', [])
    .controller('AuthController', function ($scope, $rootScope, $state, $stateParams, $cookies, $utils, GlobalService, AuthServices) {
        $scope.ctrl_name = 'AuthController';
        $scope.wrong_username = false;
        $scope.wrong_password = false;
        $scope.reset_password = false;

        $scope.showResetPasswordForm = function () {
            $scope.reset_password = true;
        };
        $scope.closeResetPasswordForm = function () {
            $scope.reset_password = false;
        };

        $scope.join_kara = function () {
            var func_name = 'join_kara';
            GlobalService.is_loading.push(func_name);
            var params = {
                email: $scope.email,
                password: $scope.password
            };
            AuthServices.auth.join(params)
                .$promise
                .then(function (data) {
                    GlobalService.error = data;
                    $scope.wrong_username = false;
                    $scope.wrong_password = false;
                    GlobalService.error.alert = '';
                    if (data.error === 0) {
                        // Set information
                        GlobalService.is_authenticated = true;
                        GlobalService.user = data.user;
                        window.location.reload();
                    }
                    else if (data.error === 3) {
                        GlobalService.error.alert = 'Please check email to verify your account before continue with us!';
                        toastr.warning(GlobalService.error.alert,
                            'Warning!!!', GlobalService.opts_toast);
                    }
                    else {
                        if (data.message == 'Email availability') {
                            $scope.wrong_username = true;
                            GlobalService.error.alert = 'Email is not found , retype to sign in or sign up below';
                            $scope.wrong_password = false;
                        }
                        if (data.message == 'Wrong authentication') {
                            $scope.wrong_username = false;
                            $scope.wrong_password = true;
                            GlobalService.error.alert = 'Password is incorrect . Please , try again !';
                        }
                        window.location.reload();
                    }
                    GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, func_name);
                });
        };
        $scope.login = function () {
            var func_name = 'login';
            GlobalService.is_loading.push(func_name);
            var params = {
                email: $scope.email,
                password: $scope.password
            };
            AuthServices.auth.login(params)
                .$promise
                .then(function (data) {
                    GlobalService.error = data;
                    $scope.wrong_username = false;
                    $scope.wrong_password = false;
                    GlobalService.error.alert = '';
                    if (data.error === 0) {
                        // Set information
                        GlobalService.is_authenticated = true;
                        GlobalService.user = data.user;
                        window.location.reload();
                    }
                    else if (data.error === 3) {
                        GlobalService.error.alert = 'Please check email to verify your account before continue with us!';
                        toastr.warning(GlobalService.error.alert,
                            'Warning!!!', GlobalService.opts_toast);
                    }
                    else {
                        if (data.message == 'Email availability') {
                            $scope.wrong_username = true;
                            GlobalService.error.alert = 'Email is not found , retype to sign in or sign up below';
                            $scope.wrong_password = false;
                        }
                        if (data.message == 'Wrong authentication') {
                            $scope.wrong_username = false;
                            $scope.wrong_password = true;
                            GlobalService.error.alert = 'Password is incorrect . Please , try again !';
                        }
                    }
                    GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, func_name);
                });
        };
        $rootScope.logout = function () {
            var func_name = 'logout';
            GlobalService.is_loading.push(func_name);
            AuthServices.auth.logout(function () {
                $cookies.remove('uid_token');
                $cookies.remove('sessionid');
                GlobalService.is_authenticated = false;
                GlobalService.user = null;
                GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, func_name);
                window.location.reload();
            });
        };
        $rootScope.join_verify = function (session_key, email) {
            var func_name = 'join_verify';
            GlobalService.is_loading.push(func_name);
            $scope.email = email;
            var params = {
                session_key: session_key,
                email: email
            };
            AuthServices.auth.join_verify(params)
                .$promise
                .then(function (data) {
                    GlobalService.error = data;
                    $scope.wrong_username = false;
                    $scope.wrong_password = false;
                    GlobalService.error.alert = '';
                    if (data.error === 0) {
                        // Set information
                        GlobalService.user = data.user;
                        GlobalService.error.alert = 'Sign in Now and Enjoy using Kara.';
                        toastr.success(GlobalService.error.alert,
                            'SUCCESSFULLY!!!', GlobalService.opts_toast);
                        $state.go('home');
                    }
                    else if (data.error === 3) {
                        GlobalService.error.alert = 'Please check email to verify your account before continue with us!';
                        toastr.warning(GlobalService.error.alert,
                            'Warning!!!', GlobalService.opts_toast);
                    }
                    else {
                        if (data.message == 'Email availability') {
                            $scope.wrong_username = true;
                            GlobalService.error.alert = 'Email is not found , retype to sign in or sign up below';
                            $scope.wrong_password = false;
                        }
                        if (data.message == 'Wrong authentication') {
                            $scope.wrong_username = false;
                            $scope.wrong_password = true;
                            GlobalService.error.alert = 'Password is incorrect . Please , try again !';
                        }
                        $state.go('home');
                    }
                    GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, func_name);
                });
        };
        $rootScope.resetPassword = function (session_key, email) {
            var func_name = 'reset_password';
            GlobalService.is_loading.push(func_name);
            $scope.email = email;
            var params = {
                session_key: session_key,
                email: email
            };
            AuthServices.auth.reset_password(params)
                .$promise
                .then(function (data) {
                    GlobalService.error = data;
                    $scope.wrong_username = false;
                    $scope.wrong_password = false;
                    GlobalService.error.alert = '';
                    if (data.error === 0) {
                        // Set information
                        GlobalService.user = data.user;
                        $state.go('home');
                    }
                    else if (data.error === 3) {
                        GlobalService.error.alert = 'Please check email to verify your account before continue with us!';
                        toastr.warning(GlobalService.error.alert,
                            'Warning!!!', GlobalService.opts_toast);
                    }
                    else {
                        if (data.message == 'Email availability') {
                            $scope.wrong_username = true;
                            GlobalService.error.alert = 'Email is not found , retype to sign in or sign up below';
                            $scope.wrong_password = false;
                        }
                        if (data.message == 'Wrong authentication') {
                            $scope.wrong_username = false;
                            $scope.wrong_password = true;
                            GlobalService.error.alert = 'Password is incorrect . Please , try again !';
                        }
                        $state.go('home');
                    }
                    GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, func_name);
                });
        };
    })
;


