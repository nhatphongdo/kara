'use strict';

/* Directives */

angular.module('directives', [])
    .directive('xeBreadcrumb', function () {
        return {
            restrict: 'A',
            link: function (scope, el) {
                var $bc = angular.element(el);

                if ($bc.hasClass('auto-hidden')) {
                    var $as = $bc.find('li a'),
                        collapsed_width = $as.width(),
                        expanded_width = 0;

                    $as.each(function (i, el) {
                        var $a = $(el);

                        expanded_width = $a.outerWidth(true);
                        $a.addClass('collapsed').width(expanded_width);

                        $a.hover(function () {
                                $a.removeClass('collapsed');
                            },
                            function () {
                                $a.addClass('collapsed');
                            }
                        );
                    });
                }
            }
        }
    })
    .directive('cellHighlight', function () {
        return {
            restrict: 'C',
            link: function postLink(scope, iElement, iAttrs) {
                iElement.find('td')
                    .mouseover(function () {
                        $(this).parent('tr').css('opacity', '0.7');
                    })
                    .mouseout(function () {
                        $(this).parent('tr').css('opacity', '1.0');
                    });
            }
        };
    })
    .directive('context', function () {
        return {
            restrict: 'A',
            scope: '@&',
            compile: function compile(tElement, tAttrs, transclude) {
                return {
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        var ul = $('#' + iAttrs.context),
                            last = null;

                        ul.css({
                            'display': 'none'
                        });
                        var child = $(iElement).find('li');
                        console.log(child);
                        $(iElement).bind('contextmenu', function (event) {
                            event.preventDefault();
                            ul.css({
                                position: "fixed",
                                display: "block",
                                left: event.clientX + 'px',
                                top: event.clientY + 'px'
                            });
                            last = event.timeStamp;
                        });
                        //$(iElement).click(function(event) {
                        //  ul.css({
                        //    position: "fixed",
                        //    display: "block",
                        //    left: event.clientX + 'px',
                        //    top: event.clientY + 'px'
                        //  });
                        //  last = event.timeStamp;
                        //});

                        $(document).click(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp)
                                    return;
                                ul.css({
                                    'display': 'none'
                                });
                            }
                        });
                    }
                };
            }
        };
    })
    .directive('title', function ($rootScope, $timeout) {
        return {
            restrict: 'E',
            link: function (scope, element, attr) {
                var listener = function (event, toState) {
                    console.log('$stateChangeSuccess', 'title', $.now());
                    var title = 'Homepage';
                    if (toState.data && toState.data.title)
                        title = toState.data.title;
                    $timeout(function () {
                        element.text(title);
                    }, 0, false);
                };

                $rootScope.$on('$stateChangeSuccess', listener);
            }
        };
    })
    .directive('load', function ($rootScope, $ocLazyLoad) {
        return {
            restrict: 'EA',
            link: function (scope, element, attrs) {
                var url = attrs.load || attrs.url || attrs.src || '';
                $rootScope.winOpen = function (url) {
                    window.open(url, '', 'top=500, left=600, width=400, height=200');
                };
                if (url.length)
                    $rootScope.winOpen(url);
            }
        };
    })
    .directive('scrollable', function () {
        return {
            restrict: 'AC',
            link: function (scope, el, attr) {
                if (!jQuery.isFunction(jQuery.fn.perfectScrollbar)) {
                    return false;
                }

                var $this = angular.element(el),
                    max_height = parseInt(attrDefault($this, 'max-height', 200), 10);

                max_height = max_height < 0 ? 200 : max_height;

                $this.css({maxHeight: max_height}).perfectScrollbar({
                    wheelPropagation: true
                });
            }
        }
    })
    .directive('loginForm', function () {
        return {
            restrict: 'AC',
            link: function (scope, el) {

                jQuery(el).find(".form-group:has(label)").each(function (i, el) {
                    var $this = angular.element(el),
                        $label = $this.find('label'),
                        $input = $this.find('.form-control');

                    $input.on('focus', function () {
                        $this.addClass('is-focused');
                    });

                    $input.on('keydown', function () {
                        $this.addClass('is-focused');
                    });

                    $input.on('blur', function () {
                        $this.removeClass('is-focused');

                        if ($input.val().trim().length > 0) {
                            $this.addClass('is-focused');
                        }
                    });

                    $label.on('click', function () {
                        $input.focus();
                    });

                    if ($input.val().trim().length > 0) {
                        $this.addClass('is-focused');
                    }
                });
            }
        };
    })
    .directive('uiView', function ($rootScope, $state, $pageLoadingBar, $interpolate, $utils, GlobalService) {
        return {
            restrict: 'ECA',
            link: function (scope, element, attrs) {
                function getUiViewName(scope, attrs, element, $interpolate) {
                    var name = $interpolate(attrs.uiView || attrs.name || '')(scope);
                    var inherited = element.inheritedData('$uiView');
                    return name.indexOf('@') >= 0 ?  name :  (name + '@' + (inherited ? inherited.state.name : ''));
                }

                var name = $interpolate(attrs.uiView || attrs.name || '')(scope);

                scope.$on('$viewContentLoading', function () {
                    GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, name);
                    //GlobalService.is_loading.push(name);
                });

                scope.$on('$viewContentLoaded', function (event) {
                    GlobalService.is_loading = $utils.grepOut(GlobalService.is_loading, name);
                    if (!GlobalService.is_loading.length) {
                        $pageLoadingBar.showLoadingBar({
                            pct: 100,
                            delay: .65,
                            resetOnEnd: true
                        });
                    }
                });
            }
        };
    })
    .directive("owlCarousel", function () {
        return {
            restrict: 'E',
            transclude: false,
            link: function (scope) {
                scope.initCarousel = function (element) {
                    // provide any default options you want
                    var defaultOptions = {};
                    var customOptions = scope.$eval($(element).attr('data-options'));
                    // combine the two options objects
                    for (var key in customOptions) {
                        defaultOptions[key] = customOptions[key];
                    }
                    // init carousel
                    $(element).owlCarousel(defaultOptions);
                };
            }
        };
    })
    .directive('owlCarouselItem', function () {
        return {
            restrict: 'A',
            transclude: false,
            link: function (scope, element) {
                // wait for the last item in the ng-repeat then call init
                if (scope.$last) {
                    scope.initCarousel(element.parent());
                }
            }
        };
    })
    .directive('inputmask', function () {
        return {
            restrict: 'AC',
            link: function (scope, el, attr) {
                if (!jQuery.isFunction(jQuery.fn.inputmask))
                    return false;

                var $this = angular.element(el),
                    mask = $this.data('mask').toString(),
                    opts = {
                        numericInput: attrDefault($this, 'numeric', false),
                        radixPoint: attrDefault($this, 'radixPoint', ''),
                        rightAlign: attrDefault($this, 'numericAlign', 'left') == 'right'
                    },
                    placeholder = attrDefault($this, 'placeholder', ''),
                    is_regex = attrDefault($this, 'isRegex', '');

                if (placeholder.length) {
                    opts[placeholder] = placeholder;
                }

                switch (mask.toLowerCase()) {
                    case "phone":
                        mask = "(999) 999-9999";
                        break;

                    case "currency":
                    case "rcurrency":

                        var sign = attrDefault($this, 'sign', '$');
                        ;

                        mask = "999,999,999.99";

                        if ($this.data('mask').toLowerCase() == 'rcurrency') {
                            mask += ' ' + sign;
                        }
                        else {
                            mask = sign + ' ' + mask;
                        }

                        opts.numericInput = true;
                        opts.rightAlignNumerics = false;
                        opts.radixPoint = '.';
                        break;

                    case "email":
                        mask = 'Regex';
                        opts.regex = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}";
                        break;

                    case "fdecimal":
                        mask = 'decimal';
                        $.extend(opts, {
                            autoGroup: true,
                            groupSize: 3,
                            radixPoint: attrDefault($this, 'rad', '.'),
                            groupSeparator: attrDefault($this, 'dec', ',')
                        });
                }

                if (is_regex) {
                    opts.regex = mask;
                    mask = 'Regex';
                }

                $this.inputmask(mask, opts);
            }
        }
    })
    .directive('validate', function () {
        return {
            restrict: 'AC',
            link: function (scope, el, attr) {
                if (!jQuery.isFunction(jQuery.fn.validate))
                    return false;

                var $this = angular.element(el),
                    opts = {
                        rules: {},
                        messages: {},
                        errorElement: 'span',
                        errorClass: 'validate-has-error',
                        highlight: function (element) {
                            $(element).closest('.form-group').addClass('validate-has-error');
                        },
                        unhighlight: function (element) {
                            $(element).closest('.form-group').removeClass('validate-has-error');
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.has-switch').length) {
                                error.insertAfter(element.closest('.has-switch'));
                            }
                            else if (element.parent('.checkbox, .radio').length || element.parent('.input-group').length) {
                                error.insertAfter(element.parent());
                            }
                            else {
                                error.insertAfter(element);
                            }
                        }
                    },
                    $fields = $this.find('[data-validate]');

                $fields.each(function (j, el2) {
                    var $field = $(el2),
                        name = $field.attr('name'),
                        validate = attrDefault($field, 'validate', '').toString(),
                        _validate = validate.split(',');

                    for (var k in _validate) {
                        var rule = _validate[k],
                            params,
                            message;

                        if (typeof opts['rules'][name] == 'undefined') {
                            opts['rules'][name] = {};
                            opts['messages'][name] = {};
                        }

                        if ($.inArray(rule, ['required', 'url', 'email', 'number', 'date', 'creditcard']) != -1) {
                            opts['rules'][name][rule] = true;

                            message = $field.data('message-' + rule);

                            if (message) {
                                opts['messages'][name][rule] = message;
                            }
                        }
                        // Parameter Value (#1 parameter)
                        else if (params = rule.match(/(\w+)\[(.*?)\]/i)) {
                            if ($.inArray(params[1], ['min', 'max', 'minlength', 'maxlength', 'equalTo']) != -1) {
                                opts['rules'][name][params[1]] = params[2];
                                message = $field.data('message-' + params[1]);
                                if (message) {
                                    opts['messages'][name][params[1]] = message;
                                }
                            }
                        }
                    }
                });

                $this.validate(opts);
            }
        }
    })
    .directive('validate', function(){
        return {
            restrict: 'AC',
            link: function (scope, el, attr) {
                if (!jQuery.isFunction(jQuery.fn.validate))
                    return false;

                var $this = angular.element(el),
                    opts = {
                        rules: {},
                        messages: {},
                        errorElement: 'span',
                        errorClass: 'validate-has-error',
                        highlight: function (element) {
                            $(element).closest('.form-group').addClass('validate-has-error');
                        },
                        unhighlight: function (element) {
                            $(element).closest('.form-group').removeClass('validate-has-error');
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.has-switch').length) {
                                error.insertAfter(element.closest('.has-switch'));
                            }
                            else if (element.parent('.checkbox, .radio').length || element.parent('.input-group').length) {
                                error.insertAfter(element.parent());
                            }
                            else {
                                error.insertAfter(element);
                            }
                        }
                    },
                    $fields = $this.find('[data-validate]');

                $fields.each(function (j, el2) {
                    var $field = $(el2),
                        name = $field.attr('name'),
                        validate = attrDefault($field, 'validate', '').toString(),
                        _validate = validate.split(',');

                    for (var k in _validate) {
                        var rule = _validate[k],
                            params,
                            message;

                        if (typeof opts['rules'][name] == 'undefined') {
                            opts['rules'][name] = {};
                            opts['messages'][name] = {};
                        }

                        if ($.inArray(rule, ['required', 'url', 'email', 'number', 'date', 'creditcard']) != -1) {
                            opts['rules'][name][rule] = true;

                            message = $field.data('message-' + rule);

                            if (message) {
                                opts['messages'][name][rule] = message;
                            }
                        }
                        // Parameter Value (#1 parameter)
                        else if (params = rule.match(/(\w+)\[(.*?)\]/i)) {
                            if ($.inArray(params[1], ['min', 'max', 'minlength', 'maxlength', 'equalTo']) != -1) {
                                opts['rules'][name][params[1]] = params[2];
                                message = $field.data('message-' + params[1]);
                                if (message) {
                                    opts['messages'][name][params[1]] = message;
                                }
                            }
                        }
                    }
                });

                $this.validate(opts);
            }
        }
    })
    .directive('tooltip', function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                $(element).hover(function(){
                    // on mouseenter
                    $(element).tooltip('show');
                }, function(){
                    // on mouseleave
                    $(element).tooltip('hide');
                });
            }
        };
    })
    .directive('winHref', function($rootScope, $window, $location){
        $rootScope.social={};
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                if(attrs.class=='facebook') {
                    $rootScope.social.fb_link = attrs.winHref;
                    $rootScope.fb_login = function(){
                        $window.open($rootScope.social.fb_link, '_self');
                    };
                }
                if(attrs.class=='gplus') {
                    $rootScope.social.gp_link = attrs.winHref;
                    $rootScope.gp_login = function(){
                        $window.open($rootScope.social.gp_link, '_self');
                    };
                }
            }
        };
    })
    .directive('errSrc', function () {
        return {
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        }
    })
;


