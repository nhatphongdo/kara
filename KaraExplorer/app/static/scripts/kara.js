'use strict';

var kara = angular.module('kara', [
        'ngResource', 'ngCookies', 'ngSanitize',
        'djng.forms', 'ngTagsInput', 'ngDialog',
        'ui.router', 'ui.bootstrap',
        'oc.lazyLoad', 'masonry', 'angularMoment',
        'filters', 'directives',
        'layout.services', 'layout.controllers', 'search.controllers',
        'kara.services', 'kara.controllers',
        'auth.services', 'auth.controllers',
        'manage.services', 'manage.controllers',
        'feeds.services', 'feeds.controllers',
        'interest.services', 'interest.controllers',
        'people.services', 'people.controllers', 'search.services',
        'dndLists', 'ngFileUpload', 'ImageCropper', '720kb.socialshare',
        'FBAngular'/*Added in v1.3*/
    ],
    function ($interpolateProvider, $httpProvider, $resourceProvider, $rootScopeProvider) {
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';

        $interpolateProvider.startSymbol("{$");
        $interpolateProvider.endSymbol("$}");

        $resourceProvider.defaults.stripTrailingSlashes = false;

        $rootScopeProvider.digestTtl(20);
    });

kara.factory('AuthInterceptor', function ($rootScope, $q, $window, $location, $cookies) {
    var COOKIE_TOKEN_NAME = 'uid_token';
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($cookies.get(COOKIE_TOKEN_NAME)) {
                config.headers.Authorization = 'Token ' + $cookies.get(COOKIE_TOKEN_NAME);
            }
            return config;
        },

        response: function (config) {
            if (config.status == 200 && config.data && config.data.session) {
                var now = new Date();
                now.setMinutes(now.getMinutes() + 15);
                $cookies.put('uid_token', config.data.session, {
                    expires: config.data.expire_time || now
                });
            }
            return config;
        },

        responseError: function (response) {
            if (response.status === 401) {
                $cookies.remove(COOKIE_TOKEN_NAME);
                $location.path('/');
                return;
            }
            return $q.reject(response);
        }
    };
});

kara.run(function ($http, $cookies, $rootScope, $state, $stateParams, GlobalService) {
    $http.defaults.headers.common['X-CSRFToken'] = $cookies.get('csrftoken');
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.globals = GlobalService;

    // Page Loading Overlay
    public_vars.$pageLoadingOverlay = jQuery('.page-loading-overlay');

    jQuery(window).load(function () {
        public_vars.$pageLoadingOverlay.addClass('loaded');
    })
});

kara.config(function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
    //$httpProvider.interceptors.push('AuthInterceptor');

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');

    $urlRouterProvider
        .when('/feeds', function ($state, GlobalService) {
            $state.go('feeds.view', {viewType: GlobalService.settings.viewType});
        })
        .when('/interests', function ($state, GlobalService) {
            $state.go('interests.view', {viewType: GlobalService.settings.viewType});
        })
        .when('/manage', function ($state, GlobalService) {
            $state.go('manage.view', {viewType: GlobalService.settings.viewType});
        })
        .otherwise('/');

    $stateProvider
        // Home
        .state('home', {
            url: '/',
            views: {
                '': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                    controller: function ($state, GlobalService) {
                        if (GlobalService.is_authenticated)
                            $state.go('feeds');
                    }
                }
            },
            data: {
                title: 'Welcome',
                state_root: 'home'
            }
        })

        // Manage //
        .state('manage', {
            url: '/manage',
            views: {
                '': {
                    templateUrl: '/templates/content/manage/'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/left-sidebar/',
                    controller: 'SidebarManageMenuCtrl'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                    controller: 'EmptyController'
                }
            },
            data: {
                title: 'Bookmark',
                state_root: 'manage'
            }
        })
        .state('manage.tags', {
            url: '/tags',
            views: {
                '@': {
                    templateUrl: '/templates/xenon/manage-tagcloud/',
                    controller: 'TagsCloud'
                }
            }
        })
        .state('manage.tags.page', {
            url: '/page{page:[0-9]+}',
            views: {
                '@': {
                    templateUrl: '/templates/xenon/manage-tagcloud/',
                    controller: 'TagsCloud'
                }
            }
        })
        .state('manage.tags.other', {
            url: '/other',
            views: {
                '@': {
                    templateUrl: 'templates/xenon/manage-tagcloud/',
                    controller: 'TagsOtherCloud'
                }
            }
        })
        .state('manage.tags.other.page', {
            url: '/page{page:[0-9]+}',
            views: {
                '@': {
                    templateUrl: 'templates/xenon/manage-tagcloud/',
                    controller: 'TagsOtherCloud'
                }
            }
        })

        .state('manage.content_tag', {
            url: '/content-tag/{viewType:card|list}/{slug}',
            views: {
                '@manage': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/manage-' + viewType + '/';
                    },
                    controller: 'TagsContentCtrl'
                }
            }
        })
        .state('manage.content_tag.page', {
            url: '/page{page:[0-9]+}',
            views: {
                '@manage': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/manage-' + viewType + '/';
                    },
                    controller: 'TagsContentCtrl'
                }
            }
        })

        .state('manage.detectbrokenlink', {
            url: '/detectbrokenlink',
            views: {
                '@': {
                    templateUrl: '/templates/manage/detectbrokenlink/'
                }
            }
        })
        .state('manage.detectduplicationlink', {
            url: '/detectduplicationlink',
            views: {
                '@': {
                    templateUrl: '/templates/manage/detectduplicationlink/'
                }
            }
        })

        .state('manage.view', {
            url: '/{viewType}/{pk:[a-zA-Z0-9-]*}',
            views: {
                '': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/manage-' + viewType + '/';
                    },
                    controller: 'ManageCtrl'
                }
            }
        })
        .state('manage.view.page', {
            url: '/page{page:[0-9]*}',
            views: {
                '@manage': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/manage-' + viewType + '/';
                    },
                    controller: 'ManageCtrl'
                }
            }
        })

        // Feed //
        .state('feeds', {
            url: '/feeds',
            views: {
                '': {
                    templateUrl: '/templates/content/feed/'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/feeds-sidebar/',
                    controller: 'SidebarFeedsMenuCtrl'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/right-sidebar/',
                    controller: 'RightSideBar'
                },
                'extra': {
                    controller: 'EmptyController'
                }
            },
            data: {
                title: 'Feeds',
                state_root: 'feeds'
            }
        })
        .state('feeds.view', {
            url: '/{viewType:card|list}/{pk:[a-zA-Z0-9-]*}',
            views: {
                '': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/feed-post-' + viewType + '/';
                    },
                    controller: 'FeedsCtrl'
                }
            }
        })
        .state('feeds.view.page', {
            url: '/page{page:[0-9]+}',
            views: {
                '@feeds': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/feed-post-' + viewType + '/';
                    },
                    controller: 'FeedsCtrl'
                }
            }
        })
        .state('feeds.view.src', {
            url: '/{src:[a-zA-Z0-9-]+}',
            views: {
                '@feeds': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/feed-post-' + viewType + '/';
                    },
                    controller: 'FeedsCtrl'
                }
            }
        })
        .state('feeds.view.src.page', {
            url: '/page{page:[0-9]+}',
            views: {
                '@feeds': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/feed-post-' + viewType + '/';
                    },
                    controller: 'FeedsCtrl'
                }
            }
        })

        .state('feeds.manage', {
            url: '/manage',
            views: {
                '': {
                    templateUrl: '/templates/xenon/feed-manage/',
                    controller: 'FeedsManageCtrl'
                }
            }
        })
        .state('feeds.add', {
            url: '/add-feed-source',
            views: {
                '': {
                    templateUrl: '/templates/feeds/add-feed-source/',
                    controller: 'AddFeedsCtrl'
                }
            }
        })
        .state('feeds.add.search', {
            url: '/{keyword}'
        })
        .state('feeds.add.category', {
            url: '/category/{category}'
        })

        // Interest //
        .state('interests', {
            url: '/interests',
            views: {
                '': {
                    templateUrl: '/templates/content/interest/'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/interests-sidebar/',
                    controller: 'SidebarInterestsMenuCtrl'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                    controller: 'EmptyController'
                }
            },
            data: {
                title: 'Interests',
                state_root: 'interests'
            }
        })
        .state('interests.view', {
            url: '/{viewType:card|list}/{pk:[a-zA-Z0-9-]*}',
            views: {
                '': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/interest-' + viewType + '/';
                    },
                    controller: 'InterestsCtrl'
                }
            }
        })
        .state('interests.view.page', {
            url: '/page{page:[0-9]+}',
            views: {
                '@interests': {
                    templateUrl: function ($stateParams, GlobalService) {
                        var viewType = $stateParams.viewType || GlobalService.settings.viewType;
                        return '/templates/xenon/interest-' + viewType + '/';
                    },
                    controller: 'InterestsCtrl'
                }
            }
        })

        .state('interests.manage', {
            url: '/manage',
            views: {
                '': {
                    templateUrl: '/templates/xenon/interest-manage/',
                    controller: 'InterestsManageCtrl'
                }
            }
        })
        .state('interests.add', {
            url: '/add-interests',
            views: {
                '': {
                    templateUrl: '/templates/interests/add-interests/',
                    controller: 'AddInterestsCtrl'
                }
            }
        })
        .state('interests.add.search', {
            url: '/{keyword}'
        })
        .state('interests.add.interest', {
            url: '/interest/{interest}'
        })

        // Profile //
        .state('profile', {
            url: '/profile/{profile:[a-zA-Z0-9.]+}',
            views: {
                '': {
                    templateUrl: '/templates/content/profile/',
                    controller: 'ProfileCtrl'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/people-sidebar/',
                    controller: 'SidebarPeopleMenuCtrl'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                    controller: 'EmptyController'
                }
            },
            data: {
                title: 'Profile',
                state_root: 'profile'
            }
        })
        .state('profile.wall', {
            url: '/wall',
            views: {
                '': {
                    templateUrl: '/templates/profile/wall/',
                    controller: 'ProfileWallCtrl'
                }
            }
        })
        .state('profile.folder', {
            url: '/folder',
            views: {
                '': {
                    templateUrl: '/templates/profile/folder/',
                    controller: 'ProfileFolderCtrl'
                }
            }
        })
        .state('profile.link', {
            url: '/link',
            views: {
                '': {
                    templateUrl: '/templates/profile/link/',
                    controller: 'ProfileBookmarkCtrl'
                }
            }
        })
        .state('profile.web', {
            url: '/web',
            views: {
                '': {
                    templateUrl: '/templates/profile/web/',
                    controller: 'ProfileSourceCtrl'
                }
            }
        })
        .state('profile.interest', {
            url: '/interest',
            views: {
                '': {
                    templateUrl: '/templates/profile/interest/',
                    controller: 'ProfileInterestCtrl'
                }
            }
        })
        .state('profile.people', {
            url: '/people',
            views: {
                '': {
                    templateUrl: '/templates/profile/people/'
                }
            }
        })
        .state('profile.dashboard', {
            url: '/dashboard',
            views: {
                '': {
                    templateUrl: '/templates/profile/dashboard/'
                }
            }
        })

        .state('profile-setting', {
            url: '/profile-setting',
            views: {
                '': {
                    templateUrl: '/templates/content/profile-setting/',
                    controller: 'ProfileSettingCtrl'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/people-sidebar/',
                    controller: 'SidebarPeopleMenuCtrl'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                    controller: 'EmptyController'
                }
            },
            data: {
                title: 'Profile Setting',
                state_root: 'profile-setting'
            }
        })

        .state('people', {
            url: '/people',
            views: {
                '': {
                    templateUrl: '/templates/content/people/'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/people-sidebar/',
                    controller: 'SidebarPeopleMenuCtrl'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                    controller: 'EmptyController'
                }
            },
            data: {
                title: 'People',
                state_root: 'profile'
            }

        })
        .state('people.view', {
            url: '/{pk:[a-zA-Z0-9-]+}/activity',
            views: {
                '': {
                    templateUrl: '/templates/include/people-card/'
                }
            }
        })
        .state('people.friends', {
            url: '^/friends/activity',
            views: {
                '': {
                    templateUrl: '/templates/include/people-card/',
                    controller: 'PeopleCtrl'
                }
            }
        })
        .state('people.following', {
            url: '^/following/activity',
            views: {
                '': {
                    templateUrl: '/templates/include/people-card/',
                    controller: 'PeopleCtrl'
                }
            }
        })
        .state('people.group', {
            url: '/group/{group:[a-zA-Z0-9-]+}/activity',
            views: {
                '': {
                    templateUrl: '/templates/include/people-card/'
                }
            }
        })
        .state('people.persons', {
            url: '/friend-request',
            views: {
                '': {
                    templateUrl: '/templates/include/persons-list/'
                }
            }
        })
        .state('people.friend-request-sent', {
            url: '/friend-request-sent',
            views: {
                '': {
                    templateUrl: '/templates/include/persons-list-request-sent/'
                }
            }
        })
        .state('people.manage_following', {
            url: '/manage-following',
            views: {
                '': {
                    templateUrl: '/templates/include/manage-following-list/'
                }
            }
        })
        .state('people.add_people', {
            url: '/add-people',
            views: {
                '': {
                    templateUrl: '/templates/include/add-people/',
                    controller: 'AddPeopleCtrl'
                }
            }
        })
        .state('people.manage_friends', {
            url: '/manage-people',
            views: {
                '': {
                    templateUrl: '/templates/include/manage-people/',
                    controller: 'PeopleManageCtrl'
                }
            }
        })
        .state('people.manage_people_folder', {
            url: '/manage-people-folder',
            views: {
                '': {
                    templateUrl: '/templates/include/manage-people-folder/'
                }
            }
        })

        .state('search', {
            url: '/search',
            views: {
                '': {
                    templateUrl: '/templates/content/search/'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/empty-sidebar/'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                    controller: 'EmptyController'
                }
            },
            data: {
                title: 'Search',
                state_root: 'search'
            }

        })
        .state('search.all', {
            url: '/all/{keyword}',
            views: {
                '': {
                    templateUrl: '/templates/xenon/search-all/',
                    controller: 'SearchCtrl'
                }
            }
        })
        .state('search.web', {
            url: '/web/{keyword}',
            views: {
                '': {
                    templateUrl: '/templates/xenon/search-web/',
                    controller: 'SearchSourcesCtrl'
                }
            }
        })
        .state('search.interest', {
            url: '/interest/{keyword}',
            views: {
                '': {
                    templateUrl: '/templates/xenon/search-interest/',
                    controller: 'SearchInterestsCtrl'
                }
            }
        })
        .state('search.bookmark', {
            url: '/bookmark/{keyword}',
            views: {
                '': {
                    templateUrl: '/templates/xenon/search-bookmark/',
                    controller: 'SearchBookmarksCtrl'
                }
            }
        })
        .state('search.post', {
            url: '/post/{keyword}',
            views: {
                '': {
                    templateUrl: '/templates/xenon/search-post/',
                    controller: 'SearchPostsCtrl'
                }
            }
        })

        .state('share', {
            url: '/share/{content_type:post|feedsource|folder}/{pk:[a-zA-Z0-9-]+}/by-{user:[a-zA-Z0-9@_.-]+}/{token:[a-zA-Z0-9-]+}',
            views: {
                '': {
                    templateUrl: function ($stateParams) {
                        if ($stateParams.content_type != undefined && $stateParams.pk != undefined) {
                            if ($stateParams.content_type == 'post') {
                                return 'templates/layout/linkdetail-content/';
                            }
                            if ($stateParams.content_type == 'feedsource') {
                                return 'templates/layout/webscreen-content/';
                            }
                            if ($stateParams.content_type == 'folder') {
                                return 'templates/layout/empty-content/';
                            }
                        }
                        return 'templates/layout/empty-content/';
                    },
                    controller: 'ShareController'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/empty-sidebar/'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                }
            },
            data: {
                title: 'Share',
                state_root: 'share'
            },
            onEnter: function ($state, $stateParams, GlobalService) {
                if ($stateParams.content_type != undefined && $stateParams.pk != undefined) {
                    GlobalService.is_need_login = false;
                }
            }
        })

        .state('verify', {
            url: '/verify/{session_key:[a-zA-Z0-9-]+}/{email:[a-zA-Z0-9@_.-]+}',
            views: {
                '': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/empty-sidebar/'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                }
            },
            data: {
                title: 'Verify',
                state_root: 'verify'
            },
            onEnter: function ($state, $stateParams, $rootScope) {
                $rootScope.join_verify($stateParams.session_key, $stateParams.email);
            }
        })
        .state('reset-password', {
            url: '/reset-password/{session_key:[a-zA-Z0-9-]+}/{email:[a-zA-Z0-9@_.-]+}',
            views: {
                '': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'sidebar-menu': {
                    templateUrl: 'templates/layout/empty-sidebar/'
                },
                'site-footer': {
                    templateUrl: 'templates/layout/footer/'
                },
                'user-info-navbar': {
                    templateUrl: 'templates/layout/top-menu/',
                    controller: 'HorizontalMenuCtrl'
                },
                'side-bar-chat': {
                    templateUrl: 'templates/layout/empty-content/'
                },
                'extra': {
                }
            },
            data: {
                title: 'Reset Password',
                state_root: 'reset-password'
            },
            onEnter: function ($state, $stateParams, $rootScope) {
                $rootScope.showPopupResetPassword($stateParams.session_key, $stateParams.email);
            }
        })
    ;
});


