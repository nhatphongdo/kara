# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0048_auto_20150923_1414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 179745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 179745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 185745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 185745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 186745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 186745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 171744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 171744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 182745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 182745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 172744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 172744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 183745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 183745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 175744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 175744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 167744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 167744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 194746, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 194746, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 188745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='slug',
            field=models.SlugField(default='', max_length=1024),
        ),
        migrations.AlterField(
            model_name='post',
            name='title',
            field=models.TextField(default='', max_length=1024),
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 188745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 165744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 165744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 170744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 170744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 193745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 193745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 193745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 191745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 191745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 177745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 177745, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 169744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 168744, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 11, 6, 40, 2, 168744, tzinfo=utc)),
        ),
    ]
