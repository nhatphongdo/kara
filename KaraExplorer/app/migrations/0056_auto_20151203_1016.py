# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('app', '0055_auto_20151126_1853'),
    ]

    operations = [
        migrations.CreateModel(
            name='Statistic',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField()),
                ('likes', models.IntegerField(default=0)),
                ('dislikes', models.IntegerField(default=0)),
                ('views', models.IntegerField(default=0)),
                ('shares', models.IntegerField(default=0)),
                ('comments', models.IntegerField(default=0)),
                ('links', models.IntegerField(default=0)),
                ('folders', models.IntegerField(default=0)),
                ('created_on', models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 416508, tzinfo=utc))),
                ('updated_on', models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 416508, tzinfo=utc))),
                ('is_deleted', models.BooleanField(default=False)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'ordering': ['content_type', 'object_id', '-updated_on'],
            },
        ),
        migrations.AlterModelOptions(
            name='action',
            options={'ordering': ['person', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='bookmark',
            options={'ordering': ['owner', 'feed_source', 'title']},
        ),
        migrations.AlterModelOptions(
            name='bookmarksinfolder',
            options={'ordering': ['folder', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='comment',
            options={'ordering': ['person', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='feedsourceofuser',
            options={'ordering': ['owner', 'feed_source']},
        ),
        migrations.AlterModelOptions(
            name='folder',
            options={'ordering': ['owner', 'name']},
        ),
        migrations.AlterModelOptions(
            name='interestsofuser',
            options={'ordering': ['owner', 'interest']},
        ),
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ['sender', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='photo',
            options={'ordering': ['-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='post',
            options={'ordering': ['poster', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='profile',
            options={'ordering': ['-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='relationship',
            options={'ordering': ['from_person', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='session',
            options={'ordering': ['expire_time', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='share',
            options={'ordering': ['person', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='tag',
            options={'ordering': ['tag', '-updated_on']},
        ),
        migrations.AlterModelOptions(
            name='uploadfile',
            options={'ordering': ['owner', '-created_on']},
        ),
        migrations.AlterModelOptions(
            name='wallpost',
            options={'ordering': ['owner', '-updated_on']},
        ),
        migrations.AddField(
            model_name='profile',
            name='nick_name',
            field=models.CharField(max_length=256, default=''),
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 400507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 400507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 405507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 405507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 407507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 407507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 391506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 391506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 402507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 402507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 392506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 392506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 404507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 404507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 395506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 395506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 382506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 382506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 415508, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 415508, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 408507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 408507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 379506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 379506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 387506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 387506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 413507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 413507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 413507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 412507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 412507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 399507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 399507, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 385506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 383506, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 3, 3, 16, 52, 383506, tzinfo=utc)),
        ),
    ]
