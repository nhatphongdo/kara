# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import app.models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0050_auto_20151116_1848'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedsource',
            name='cover',
            field=models.FileField(default='', max_length=1024, upload_to=app.models.feed_source_cover_filename),
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 263188, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 263284, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 267495, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 267538, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 268977, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 269020, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 257205, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 257243, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 264677, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 264724, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 258224, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 258259, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 266051, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 266091, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 260025, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 260070, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 252072, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 252106, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 275868, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 275910, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 270117, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 270161, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 250790, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 250833, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 256241, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 256281, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 274883, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 274838, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 274914, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 273475, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 273518, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 261916, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 261953, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 255202, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 253084, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 19, 11, 39, 5, 253125, tzinfo=utc)),
        ),
    ]
