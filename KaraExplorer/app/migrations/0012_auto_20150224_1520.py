# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20150224_1501'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmark',
            name='status',
            field=models.IntegerField(choices=[(0, 'None'), (1, 'Favourite'), (2, 'Important')], default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='action',
            field=models.IntegerField(choices=[(0, 'Undefined'), (1, 'Share'), (2, 'Like')], default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='person',
            field=models.ForeignKey(to='app.Profile'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='action',
            field=models.IntegerField(choices=[(0, 'None')], default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='permissions',
            field=models.IntegerField(choices=[(1, 'Read'), (2, 'Write'), (3, 'Admin')], default=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='person',
            field=models.ForeignKey(to='app.Profile'),
            preserve_default=True,
        ),
    ]
