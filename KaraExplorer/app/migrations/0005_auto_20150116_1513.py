# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('app', '0004_auto_20150116_1458'),
    ]

    operations = [
        migrations.CreateModel(
            name='Share',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('object_id', models.PositiveIntegerField()),
                ('action', models.PositiveIntegerField()),
                ('permissions', models.PositiveIntegerField()),
                ('is_accepted', models.NullBooleanField(default=None)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('person', models.ForeignKey(to='app.Profile', related_name='from_person')),
                ('to_person', models.ForeignKey(to='app.Profile', related_name='to_person')),
            ],
            options={
                'ordering': ['person'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='folder',
            name='is_user_defined',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
