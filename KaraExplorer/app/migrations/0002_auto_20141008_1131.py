# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bookmark',
            options={'ordering': ['title']},
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='feedsource',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='folder',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='interest',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='tag',
            options={'ordering': ['tag']},
        ),
        migrations.AddField(
            model_name='message',
            name='content',
            field=models.TextField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='message',
            name='ip_address',
            field=models.GenericIPAddressField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='message',
            name='status',
            field=models.IntegerField(choices=[(1, 'Unread'), (2, 'Read'), (3, 'Deleted'), (4, 'Important')], default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='message',
            name='title',
            field=models.CharField(max_length=2048, default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=None),
            preserve_default=False,
        ),
    ]
