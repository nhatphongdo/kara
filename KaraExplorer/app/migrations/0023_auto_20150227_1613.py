# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
import app.models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0022_auto_20150227_1445'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', app.models.BigAutoField(serialize=False, primary_key=True)),
                ('caption', models.CharField(max_length=2048, default='')),
                ('file', models.FileField(max_length=1024, upload_to=app.models.photo_filename)),
                ('created_on', models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 834118, tzinfo=utc))),
                ('updated_on', models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 835125, tzinfo=utc))),
                ('ip_address', models.GenericIPAddressField(default='0.0.0.0')),
                ('is_deleted', models.BooleanField(default=False)),
                ('owner', models.ForeignKey(to='app.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='action',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookmark',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookmarksinfolder',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='category',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feedsource',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='folder',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='interest',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='interestsofuser',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='relationship',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 832129, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 832129, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='share',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tag',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='uploadfile',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wallpost',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 799105, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 800106, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 812111, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 812111, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='bookmark',
            field=models.ForeignKey(to='app.Bookmark'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='folder',
            field=models.ForeignKey(to='app.Folder'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 779111, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 779111, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 804108, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 804108, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 783100, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 783100, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 808112, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 808112, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 788105, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 788105, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interestsofuser',
            name='interest',
            field=models.ForeignKey(to='app.Interest'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interestsofuser',
            name='owner',
            field=models.ForeignKey(to='app.Profile'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 767094, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 767094, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 819119, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 819119, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 763105, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 764104, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 776106, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 776106, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 832129, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 828116, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 828116, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 795105, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 795105, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 773110, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='owner',
            field=models.ForeignKey(to='app.Profile'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='post',
            field=models.ForeignKey(to='app.WallPost'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 771109, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 9, 13, 14, 771109, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
