# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import app.models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0059_auto_20151223_1038'),
    ]

    operations = [
        migrations.CreateModel(
            name='FriendsGroup',
            fields=[
                ('id', app.models.BigAutoField(primary_key=True, serialize=False)),
                ('name', models.SlugField(max_length=256, default='')),
                ('slug', models.SlugField(max_length=256, default='')),
                ('is_user_defined', models.BooleanField(default=False)),
                ('created_on', models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 419522, tzinfo=utc))),
                ('updated_on', models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 419522, tzinfo=utc))),
                ('ip_address', models.GenericIPAddressField(default='0.0.0.0')),
                ('is_deleted', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['name', '-updated_on'],
            },
        ),
        migrations.AlterModelOptions(
            name='interest',
            options={'ordering': ['parent__name', 'name']},
        ),
        migrations.AddField(
            model_name='wallpost',
            name='poster',
            field=models.ForeignKey(to='app.Profile', default=1, related_name='poster'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 431523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 431523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 436523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 436523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 438523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 438523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 422522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 422522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 433523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 433523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 424523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 424523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 435523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 435523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 426523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 426523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 413522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 413522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 446524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 446524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 439523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 439523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 411522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 411522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 420522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='status',
            field=models.IntegerField(choices=[(1, 'Friend requested'), (2, 'Friend'), (3, 'Blocked'), (4, 'Following')], default=1),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 420522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 444524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 444524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 444524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 443524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 443524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 447524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 447524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 430523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 430523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 418522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 416522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='owner',
            field=models.ForeignKey(related_name='owner', to='app.Profile'),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 28, 11, 8, 28, 416522, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='friendsgroup',
            name='owner',
            field=models.ForeignKey(to='app.Profile'),
        ),
        migrations.AddField(
            model_name='relationship',
            name='groups',
            field=models.ManyToManyField(to='app.FriendsGroup', related_name='friends_in_groups'),
        ),
    ]
