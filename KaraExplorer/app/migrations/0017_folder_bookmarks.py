# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20150225_1123'),
    ]

    operations = [
        migrations.AddField(
            model_name='folder',
            name='bookmarks',
            field=models.ManyToManyField(to='app.Bookmark', related_name='folder_contains_bookmarks', through='app.BookmarksInFolder'),
            preserve_default=True,
        ),
    ]
