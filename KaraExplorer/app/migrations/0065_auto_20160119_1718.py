# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0064_auto_20160106_1441'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='bookmark',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='bookmarksinfolder',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='category',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='comment',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='feedsource',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='feedsourceofuser',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='folder',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='friendsgroup',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='interest',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='interestsofuser',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='message',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='photo',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='post',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='share',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='tag',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='uploadfile',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='wallpost',
            name='is_trash',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 342630, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 342676, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 346269, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 346303, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 347965, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 348015, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 335135, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 335182, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 343936, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 343973, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 336300, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 336341, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 345108, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 345141, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='friendsgroup',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 331648, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='friendsgroup',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 331694, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 338154, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 338192, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 327912, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 327960, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 355650, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 355700, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 349411, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 349457, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 326013, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 326070, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 333093, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 333143, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 354544, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 354502, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 354579, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 352994, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 353041, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 357088, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 357132, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 341261, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 341312, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 330489, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 329148, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 19, 10, 18, 5, 329189, tzinfo=utc)),
        ),
    ]
