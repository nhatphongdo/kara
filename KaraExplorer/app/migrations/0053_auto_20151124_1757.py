# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import app.models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0052_auto_20151121_1707'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeedSourceOfUser',
            fields=[
                ('id', app.models.BigAutoField(primary_key=True, serialize=False)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField(default='0.0.0.0')),
                ('is_deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 113081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 113081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 118082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 118082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 120082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 120082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 105081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 105081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 115081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 115081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 106081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 106081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 117081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 117081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 108081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 108081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 98080, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 98080, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 128082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 128082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 121082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 121082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 96080, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 96080, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 102081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 102081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 126082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 126082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 126082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 125082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 125082, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 112081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 112081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 100081, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 99080, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 24, 10, 57, 51, 99080, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='feedsourceofuser',
            name='feed_source',
            field=models.ForeignKey(to='app.FeedSource'),
        ),
        migrations.AddField(
            model_name='feedsourceofuser',
            name='owner',
            field=models.ForeignKey(to='app.Profile'),
        ),
        migrations.AddField(
            model_name='profile',
            name='subscribes',
            field=models.ManyToManyField(through='app.FeedSourceOfUser', to='app.FeedSource', related_name='feedsource_of_user'),
        ),
    ]
