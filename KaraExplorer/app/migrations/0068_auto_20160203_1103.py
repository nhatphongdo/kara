# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-03 04:03
from __future__ import unicode_literals

import app.models
import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0067_auto_20160202_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmarksinfolder',
            name='name',
            field=models.CharField(default='', max_length=1024),
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 708279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 709279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 715279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 715279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 716279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 716279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 698278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 698278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 711279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 711279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 699279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 699279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 713279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 713279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='friendsgroup',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 693278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='friendsgroup',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 694278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 702279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 702279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 689278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 689278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 726280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 726280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 718280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 718280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='biography',
            field=models.TextField(default='', null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='birthday',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 659276, tzinfo=utc), null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 659276, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='language',
            field=app.models.LanguageField(choices=[('ab', 'Abkhaz - аҧсуа'), ('aa', 'Afar - Afaraf'), ('af', 'Afrikaans - Afrikaans'), ('ak', 'Akan - Akan'), ('sq', 'Albanian - Shqip'), ('am', 'Amharic - አማርኛ'), ('ar', 'Arabic - العربية'), ('an', 'Aragonese - Aragonés'), ('hy', 'Armenian - Հայերեն'), ('as', 'Assamese - অসমীয়া'), ('av', 'Avaric - авар мацӀ, магӀарул мацӀ'), ('ae', 'Avestan - avesta'), ('ay', 'Aymara - aymar aru'), ('az', 'Azerbaijani - azərbaycan dili'), ('bm', 'Bambara - bamanankan'), ('ba', 'Bashkir - башҡорт теле'), ('eu', 'Basque - euskara, euskera'), ('be', 'Belarusian - Беларуская'), ('bn', 'Bengali - বাংলা'), ('bh', 'Bihari - भोजपुरी'), ('bi', 'Bislama - Bislama'), ('bs', 'Bosnian - bosanski jezik'), ('br', 'Breton - brezhoneg'), ('bg', 'Bulgarian - български език'), ('my', 'Burmese - ဗမာစာ'), ('ca', 'Catalan; Valencian - Català'), ('ch', 'Chamorro - Chamoru'), ('ce', 'Chechen - нохчийн мотт'), ('ny', 'Chichewa; Chewa; Nyanja - chiCheŵa, chinyanja'), ('zh', 'Chinese - 中文 (Zhōngwén), 汉语, 漢語'), ('cv', 'Chuvash - чӑваш чӗлхи'), ('kw', 'Cornish - Kernewek'), ('co', 'Corsican - corsu, lingua corsa'), ('cr', 'Cree - ᓀᐦᐃᔭᐍᐏᐣ'), ('hr', 'Croatian - hrvatski'), ('cs', 'Czech - česky, čeština'), ('da', 'Danish - dansk'), ('dv', 'Divehi; Dhivehi; Maldivian; - ދިވެހި'), ('nl', 'Dutch - Nederlands, Vlaams'), ('en', 'English - English'), ('eo', 'Esperanto - Esperanto'), ('et', 'Estonian - eesti, eesti keel'), ('ee', 'Ewe - Eʋegbe'), ('fo', 'Faroese - føroyskt'), ('fj', 'Fijian - vosa Vakaviti'), ('fi', 'Finnish - suomi, suomen kieli'), ('fr', 'French - français, langue française'), ('ff', 'Fula; Fulah; Pulaar; Pular - Fulfulde, Pulaar, Pular'), ('gl', 'Galician - Galego'), ('ka', 'Georgian - ქართული'), ('de', 'German - Deutsch'), ('el', 'Greek, Modern - Ελληνικά'), ('gn', 'Guaraní - Avañeẽ'), ('gu', 'Gujarati - ગુજરાતી'), ('ht', 'Haitian; Haitian Creole - Kreyòl ayisyen'), ('ha', 'Hausa - Hausa, هَوُسَ'), ('he', 'Hebrew (modern) - עברית'), ('hz', 'Herero - Otjiherero'), ('hi', 'Hindi - हिन्दी, हिंदी'), ('ho', 'Hiri Motu - Hiri Motu'), ('hu', 'Hungarian - Magyar'), ('ia', 'Interlingua - Interlingua'), ('id', 'Indonesian - Bahasa Indonesia'), ('ie', 'Interlingue - Originally called Occidental; then Interlingue after WWII'), ('ga', 'Irish - Gaeilge'), ('ig', 'Igbo - Asụsụ Igbo'), ('ik', 'Inupiaq - Iñupiaq, Iñupiatun'), ('io', 'Ido - Ido'), ('is', 'Icelandic - Íslenska'), ('it', 'Italian - Italiano'), ('iu', 'Inuktitut - ᐃᓄᒃᑎᑐᑦ'), ('ja', 'Japanese - 日本語 (にほんご／にっぽんご)'), ('jv', 'Javanese - basa Jawa'), ('kl', 'Kalaallisut, Greenlandic - kalaallisut, kalaallit oqaasii'), ('kn', 'Kannada - ಕನ್ನಡ'), ('kr', 'Kanuri - Kanuri'), ('ks', 'Kashmiri - कश्मीरी, كشميري\u200e'), ('kk', 'Kazakh - Қазақ тілі'), ('km', 'Khmer - ភាសាខ្មែរ'), ('ki', 'Kikuyu, Gikuyu - Gĩkũyũ'), ('rw', 'Kinyarwanda - Ikinyarwanda'), ('ky', 'Kirghiz, Kyrgyz - кыргыз тили'), ('kv', 'Komi - коми кыв'), ('kg', 'Kongo - KiKongo'), ('ko', 'Korean - 한국어 (韓國語), 조선말 (朝鮮語)'), ('ku', 'Kurdish - Kurdî, كوردی\u200e'), ('kj', 'Kwanyama, Kuanyama - Kuanyama'), ('la', 'Latin - latine, lingua latina'), ('lb', 'Luxembourgish, Letzeburgesch - Lëtzebuergesch'), ('lg', 'Luganda - Luganda'), ('li', 'Limburgish, Limburgan, Limburger - Limburgs'), ('ln', 'Lingala - Lingála'), ('lo', 'Lao - ພາສາລາວ'), ('lt', 'Lithuanian - lietuvių kalba'), ('lu', 'Luba-Katanga - '), ('lv', 'Latvian - latviešu valoda'), ('gv', 'Manx - Gaelg, Gailck'), ('mk', 'Macedonian - македонски јазик'), ('mg', 'Malagasy - Malagasy fiteny'), ('ms', 'Malay - bahasa Melayu, بهاس ملايو\u200e'), ('ml', 'Malayalam - മലയാളം'), ('mt', 'Maltese - Malti'), ('mi', 'Māori - te reo Māori'), ('mr', 'Marathi (Marāṭhī) - मराठी'), ('mh', 'Marshallese - Kajin M̧ajeļ'), ('mn', 'Mongolian - монгол'), ('na', 'Nauru - Ekakairũ Naoero'), ('nv', 'Navajo, Navaho - Diné bizaad, Dinékʼehǰí'), ('nb', 'Norwegian Bokmål - Norsk bokmål'), ('nd', 'North Ndebele - isiNdebele'), ('ne', 'Nepali - नेपाली'), ('ng', 'Ndonga - Owambo'), ('nn', 'Norwegian Nynorsk - Norsk nynorsk'), ('no', 'Norwegian - Norsk'), ('ii', 'Nuosu - ꆈꌠ꒿ Nuosuhxop'), ('nr', 'South Ndebele - isiNdebele'), ('oc', 'Occitan - Occitan'), ('oj', 'Ojibwe, Ojibwa - ᐊᓂᔑᓈᐯᒧᐎᓐ'), ('cu', 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic - ѩзыкъ словѣньскъ'), ('om', 'Oromo - Afaan Oromoo'), ('or', 'Oriya - ଓଡ଼ିଆ'), ('os', 'Ossetian, Ossetic - ирон æвзаг'), ('pa', 'Panjabi, Punjabi - ਪੰਜਾਬੀ, پنجابی\u200e'), ('pi', 'Pāli - पाऴि'), ('fa', 'Persian - فارسی'), ('pl', 'Polish - polski'), ('ps', 'Pashto, Pushto - پښتو'), ('pt', 'Portuguese - Português'), ('qu', 'Quechua - Runa Simi, Kichwa'), ('rm', 'Romansh - rumantsch grischun'), ('rn', 'Kirundi - kiRundi'), ('ro', 'Romanian, Moldavian, Moldovan - română'), ('ru', 'Russian - русский язык'), ('sa', 'Sanskrit (Saṁskṛta) - संस्कृतम्'), ('sc', 'Sardinian - sardu'), ('sd', 'Sindhi - सिन्धी, سنڌي، سندھی\u200e'), ('se', 'Northern Sami - Davvisámegiella'), ('sm', 'Samoan - gagana faa Samoa'), ('sg', 'Sango - yângâ tî sängö'), ('sr', 'Serbian - српски језик'), ('gd', 'Scottish Gaelic; Gaelic - Gàidhlig'), ('sn', 'Shona - chiShona'), ('si', 'Sinhala, Sinhalese - සිංහල'), ('sk', 'Slovak - slovenčina'), ('sl', 'Slovene - slovenščina'), ('so', 'Somali - Soomaaliga, af Soomaali'), ('st', 'Southern Sotho - Sesotho'), ('es', 'Spanish; Castilian - español, castellano'), ('su', 'Sundanese - Basa Sunda'), ('sw', 'Swahili - Kiswahili'), ('ss', 'Swati - SiSwati'), ('sv', 'Swedish - svenska'), ('ta', 'Tamil - தமிழ்'), ('te', 'Telugu - తెలుగు'), ('tg', 'Tajik - тоҷикӣ, toğikī, تاجیکی\u200e'), ('th', 'Thai - ไทย'), ('ti', 'Tigrinya - ትግርኛ'), ('bo', 'Tibetan Standard, Tibetan, Central - བོད་ཡིག'), ('tk', 'Turkmen - Türkmen, Түркмен'), ('tl', 'Tagalog - Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔'), ('tn', 'Tswana - Setswana'), ('to', 'Tonga (Tonga Islands) - faka Tonga'), ('tr', 'Turkish - Türkçe'), ('ts', 'Tsonga - Xitsonga'), ('tt', 'Tatar - татарча, tatarça, تاتارچا\u200e'), ('tw', 'Twi - Twi'), ('ty', 'Tahitian - Reo Tahiti'), ('ug', 'Uighur, Uyghur - Uyƣurqə, ئۇيغۇرچە\u200e'), ('uk', 'Ukrainian - українська'), ('ur', 'Urdu - اردو'), ('uz', 'Uzbek - zbek, Ўзбек, أۇزبېك\u200e'), ('ve', 'Venda - Tshivenḓa'), ('vi', 'Vietnamese - Tiếng Việt'), ('vo', 'Volapük - Volapük'), ('wa', 'Walloon - Walon'), ('cy', 'Welsh - Cymraeg'), ('wo', 'Wolof - Wollof'), ('fy', 'Western Frisian - Frysk'), ('xh', 'Xhosa - isiXhosa'), ('yi', 'Yiddish - ייִדיש'), ('yo', 'Yoruba - Yorùbá'), ('za', 'Zhuang, Chuang - Saɯ cueŋƅ, Saw cuengh')], default='en-us', max_length=5),
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 659276, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 695278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 695278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 724280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 724280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 724280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 722280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 722280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 727280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 727280, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 707279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 707279, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 692278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 690278, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 3, 4, 3, 3, 690278, tzinfo=utc)),
        ),
    ]
