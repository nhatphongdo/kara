# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
import app.models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0035_auto_20150330_1504'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedsource',
            name='icon',
            field=models.FileField(default='', max_length=1024, upload_to=app.models.feed_source_icon_filename),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 981864, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 982847, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 984851, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 984851, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 977849, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 978860, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 982847, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 982847, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 978860, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 978860, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 983850, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 983850, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 980844, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 980844, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 973864, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 973864, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 988863, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 988863, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 985861, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 985861, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 971864, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 971864, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 977849, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 977849, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 988863, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 988863, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 988863, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 987863, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 987863, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 981864, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 981864, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 976849, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 974856, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 8, 12, 33, 974856, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
