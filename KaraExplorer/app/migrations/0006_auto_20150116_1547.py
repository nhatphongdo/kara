# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20150116_1513'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='parent',
            field=models.ForeignKey(to='app.Category', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='parent',
            field=models.ForeignKey(to='app.Folder', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='parent',
            field=models.ForeignKey(to='app.Interest', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='source',
            field=models.ForeignKey(to='app.Bookmark', null=True),
            preserve_default=True,
        ),
    ]
