# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20150224_1528'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmark',
            name='slug',
            field=models.SlugField(max_length=1024, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='url',
            field=models.URLField(max_length=4096, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(unique=True, max_length=256, default='', db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(unique=True, max_length=256, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='name',
            field=models.CharField(max_length=1024, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='url',
            field=models.URLField(max_length=4096, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='name',
            field=models.CharField(max_length=256, default='', db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='slug',
            field=models.SlugField(max_length=256, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='name',
            field=models.CharField(unique=True, max_length=256, default='', db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='slug',
            field=models.SlugField(unique=True, max_length=256, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='content',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='tag',
            field=models.SlugField(unique=True, max_length=256, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='content',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='title',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
