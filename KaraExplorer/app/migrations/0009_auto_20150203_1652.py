# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20150203_1011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(primary_key=True, to=settings.AUTH_USER_MODEL, serialize=False, related_name='profile'),
            preserve_default=True,
        ),
    ]
