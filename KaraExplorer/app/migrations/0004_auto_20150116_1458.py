# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('app', '0003_auto_20141009_1128'),
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField()),
                ('action', models.PositiveIntegerField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('person', models.ForeignKey(to='app.Profile')),
            ],
            options={
                'ordering': ['person'],
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='read',
            name='content_type',
        ),
        migrations.DeleteModel(
            name='Read',
        ),
        migrations.AddField(
            model_name='folder',
            name='is_user_defined',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='post',
            name='source',
            field=models.ForeignKey(to='app.Bookmark', default=None),
            preserve_default=False,
        ),
    ]
