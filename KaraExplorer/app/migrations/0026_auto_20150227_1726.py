# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_auto_20150227_1726'),
    ]

    operations = [
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 456616, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 456616, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 467624, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 467624, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 439611, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 439611, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 459621, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 460621, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 442625, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 442625, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 463622, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 463622, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 446626, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 447627, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 428620, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 429620, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 488632, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 488632, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 472626, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 473626, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 424618, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 425617, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 436622, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 436622, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 484629, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 484629, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 485631, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 481629, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 481629, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 453628, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 453628, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 434622, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 431621, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 27, 10, 26, 47, 431621, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
