# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_auto_20150224_1537'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='slug',
            field=models.SlugField(max_length=256, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='tag',
            field=models.SlugField(max_length=256, default=''),
            preserve_default=True,
        ),
    ]
