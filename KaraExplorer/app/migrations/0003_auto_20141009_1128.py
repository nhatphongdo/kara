# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20141008_1131'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookmark',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='post',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='wallpost',
            name='slug',
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='title',
            field=models.CharField(max_length=1024, db_index=True),
        ),
        migrations.AlterField(
            model_name='folder',
            name='name',
            field=models.CharField(max_length=256, db_index=True),
        ),
        migrations.AlterField(
            model_name='folder',
            name='slug',
            field=models.SlugField(max_length=256),
        ),
        migrations.AlterField(
            model_name='post',
            name='title',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='title',
            field=models.TextField(),
        ),
    ]
