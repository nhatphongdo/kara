# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0062_auto_20151231_1450'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedsourceofuser',
            name='name',
            field=models.CharField(max_length=1024, default=''),
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 169395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 169395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 175396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 175396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 177396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 177396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 157395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 157395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 170396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 170396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 159395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 159395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 172396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 173396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='friendsgroup',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 152394, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='friendsgroup',
            name='status',
            field=models.IntegerField(default=1, choices=[(1, 'Friend'), (2, 'Following')]),
        ),
        migrations.AlterField(
            model_name='friendsgroup',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 152394, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 161395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 161395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 145394, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 145394, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 186396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='photo',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 186396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 179396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 179396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 143394, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 143394, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 153395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 153395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 185396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_time',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 185396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='session',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 185396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 183396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 183396, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 188397, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 188397, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 167395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 167395, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 148394, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 147394, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 4, 7, 27, 47, 147394, tzinfo=utc)),
        ),
    ]
