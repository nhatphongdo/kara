# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20150116_1547'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='is_user_defined',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tag',
            name='person',
            field=models.ForeignKey(to='app.Profile', default=-1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='person',
            field=models.ForeignKey(to='app.Profile', default=-1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='title',
            field=models.CharField(max_length=1024, default='', db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='parent',
            field=models.ForeignKey(to='app.Interest', related_name='parent_id', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interestsofuser',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='content',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.IntegerField(choices=[(1, 'Unread'), (2, 'Read'), (3, 'Deleted'), (4, 'Important')], default=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='title',
            field=models.CharField(max_length=2048, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='status',
            field=models.IntegerField(choices=[(1, 'Friend requested'), (2, 'Friend'), (3, 'Blocked')], default=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='person',
            field=models.ForeignKey(to='app.Profile', related_name='from_person', default=-1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='to_person',
            field=models.ForeignKey(to='app.Profile', related_name='to_person', default=-1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 1, 30)),
            preserve_default=True,
        ),
    ]
