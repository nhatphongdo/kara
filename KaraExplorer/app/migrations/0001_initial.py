# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bookmark',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=1024, db_index=True, unique=True)),
                ('slug', models.SlugField(max_length=1024, unique=True)),
                ('url', models.URLField(max_length=4096)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BookmarksInFolder',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('bookmark', models.ForeignKey(to='app.Bookmark', related_name='bookmark_id')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256, db_index=True, unique=True)),
                ('slug', models.SlugField(max_length=256, unique=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('parent', models.ForeignKey(to='app.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FeedSource',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=1024)),
                ('url', models.URLField(max_length=4096)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('categories', models.ManyToManyField(to='app.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Folder',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256, db_index=True, unique=True)),
                ('slug', models.SlugField(max_length=256, unique=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Interest',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256, db_index=True, unique=True)),
                ('slug', models.SlugField(max_length=256, unique=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('parent', models.ForeignKey(to='app.Interest')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='InterestsOfUser',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('interest', models.ForeignKey(to='app.Interest', related_name='interest_id')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=1024, db_index=True, unique=True)),
                ('slug', models.SlugField(max_length=1024, unique=True)),
                ('content', models.TextField()),
                ('published_on', models.DateTimeField()),
                ('is_published', models.BooleanField(default=False)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('categories', models.ManyToManyField(to='app.Category')),
                ('interests', models.ManyToManyField(to='app.Interest')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, primary_key=True, serialize=False)),
                ('gender', models.PositiveSmallIntegerField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('interests', models.ManyToManyField(related_name='interests_of_user', through='app.InterestsOfUser', to='app.Interest')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Read',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relationship',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('status', models.IntegerField(choices=[(1, 'Friend requested'), (2, 'Friend'), (3, 'Blocked')])),
                ('from_person', models.ForeignKey(to='app.Profile', related_name='from_people')),
                ('to_person', models.ForeignKey(to='app.Profile', related_name='to_people')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('tag', models.SlugField()),
                ('object_id', models.PositiveIntegerField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UploadFile',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('file', models.FileField(upload_to='users/%Y/%m/%d', max_length=1024)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('owner', models.ForeignKey(to='app.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WallPost',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=1024, db_index=True, unique=True)),
                ('slug', models.SlugField(max_length=1024, unique=True)),
                ('content', models.TextField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('ip_address', models.GenericIPAddressField()),
                ('owner', models.ForeignKey(to='app.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='uploadfile',
            name='post',
            field=models.ForeignKey(to='app.WallPost'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='relationships',
            field=models.ManyToManyField(related_name='related_to', through='app.Relationship', to='app.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='poster',
            field=models.ForeignKey(to='app.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='receiver',
            field=models.ForeignKey(to='app.Profile', related_name='+'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='sender',
            field=models.ForeignKey(to='app.Profile', related_name='+'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='interestsofuser',
            name='owner',
            field=models.ForeignKey(to='app.Profile', related_name='profile_id'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='folder',
            name='owner',
            field=models.ForeignKey(to='app.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='folder',
            name='parent',
            field=models.ForeignKey(to='app.Folder'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookmarksinfolder',
            name='folder',
            field=models.ForeignKey(to='app.Folder', related_name='folder_id'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookmark',
            name='folders',
            field=models.ManyToManyField(related_name='bookmarks_in_folder', through='app.BookmarksInFolder', to='app.Folder'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookmark',
            name='owner',
            field=models.ForeignKey(to='app.Profile'),
            preserve_default=True,
        ),
    ]
