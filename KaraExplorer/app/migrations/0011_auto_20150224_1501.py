# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20150209_1712'),
    ]

    operations = [
        migrations.AddField(
            model_name='relationship',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='action',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookmarksinfolder',
            name='ip_address',
            field=models.GenericIPAddressField(default='0.0.0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedsource',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='interest',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profile',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relationship',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='share',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallpost',
            name='updated_on',
            field=models.DateTimeField(auto_now=True, default=datetime.date(2015, 2, 24)),
            preserve_default=True,
        ),
    ]
