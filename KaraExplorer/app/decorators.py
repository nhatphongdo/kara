import uuid
from datetime import timedelta
from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.views import redirect_to_login
from django.utils import timezone
from rest_framework.response import Response
from .errors import *
from .models import Session


def token_required(check_session=True, check_user=True, login_url=None):
    def wrapper(func):
        def validateUser(user):
            if user is not None:
                # the authentication verified for the user
                if not user.is_active:
                    return Response({
                        'error': AUTHENTICATION_ERROR_SUSPENDED,
                        'message': 'User is suspended'
                    })
                return None
            else:
                # the authentication system was unable to verify the user
                return Response({
                    'error': AUTHENTICATION_ERROR_WRONG,
                    'message': 'Wrong authentication'
                })

        def decorator(view, request, *args, **kwargs):
            if request.method == 'OPTIONS':
                return func(view, request, None, *args, **kwargs)
            auth_header = request.META.get('HTTP_AUTHORIZATION', None)
            if auth_header is not None:
                tokens = auth_header.split(' ')
                if len(tokens) == 2 and tokens[0] == 'Token':
                    token = tokens[1]
                    user_session = Session.verify(token, request.user)
                    if user_session is not None and user_session.owner is not None and not request.user.is_authenticated:
                        # Login if needed
                        login(request, user_session.owner)

                    if check_user:
                        result = validateUser(request.user)
                        if result is not None:
                            return result

                    if check_session and user_session is not None:
                        return func(view, request, user_session, *args, **kwargs)

            # No token, check if user still logged in
            if request.user.is_authenticated:
                if check_user:
                    result = validateUser(request.user)
                    if result is not None:
                        return result

                # Auto generate another token
                session = Session(
                    owner_id=request.user.id,
                    session_key=uuid.uuid4(),
                    expire_time=timezone.now() + timedelta(minutes=settings.SHORT_PERIOD_SESSION)
                )

                return func(view, request, session, *args, **kwargs)

            # No token, no authentication
            if login_url is not None:
                return redirect_to_login(login_url)
            else:
                if check_session:
                    return Response({
                        'error': AUTHENTICATION_ERROR_WRONG,
                        'message': 'Wrong authentication'
                    })
                return func(view, request, None, *args, **kwargs)

        return decorator

    return wrapper
