from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.template.defaultfilters import slugify
from django_countries.fields import CountryField
from .fields import *
from .libraries import hasher


# User class
class Profile(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name='profile')
    nick_name = models.CharField(max_length=256, default='')
    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICE, default=GENDER_OTHER)
    # avatar = models.FileField(max_length=1024, upload_to=avatar_filename, default='')
    avatar = models.IntegerField(null=True)
    cover = models.IntegerField(null=True)
    birthday = models.DateField(null=True)
    biography = models.TextField(default='', null=True)
    country = CountryField(default='US', blank_label='(select country)')
    language = LanguageField(default='en-us')
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    relationships = models.ManyToManyField('self', through='Relationship', through_fields=('from_person', 'to_person'),
                                           symmetrical=False, related_name='related_to')
    interests = models.ManyToManyField('Interest', through='InterestsOfUser', through_fields=('owner', 'interest'),
                                       symmetrical=False, related_name='interests_of_user')
    subscribes = models.ManyToManyField('FeedSource', through='FeedSourceOfUser',
                                        through_fields=('owner', 'feed_source'),
                                        symmetrical=False, related_name='feedsource_of_user')
    is_deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ['-updated_on']

    def __str__(self):
        string_display = 'anonymous'
        if self.user.email:
            string_display = self.user.email
        if self.user.username and self.nick_name:
            string_display = '%s - %s' % (self.nick_name, self.user.username)
        elif self.user.username:
            string_display = self.user.username
        elif self.nick_name:
            string_display = self.nick_name
        return string_display

    def get_profile_display(self):
        string_display = 'anonymous'
        if self.user.email:
            string_display = self.user.email
        if self.user.username:
            string_display = self.user.username
        if self.nick_name:
            string_display = self.nick_name
        return string_display

    def add_relationship(self, person, status):
        relationship, created = Relationship.objects.get_or_create(
            from_person=self,
            to_person=person,
            status=status)
        return relationship

    def remove_relationship(self, person, status):
        Relationship.objects.filter(
            from_person=self,
            to_person=person,
            status=status).delete()
        return

    def get_relationships(self, status):
        return self.relationships.filter(
            to_people__status=status,
            to_people__from_person=self)

    def get_related_to(self, status):
        return self.related_to.filter(
            from_people__status=status,
            from_people__to_person=self)

    def get_friend_requests(self):
        return self.get_related_to(RELATIONSHIP_FRIEND_REQUESTED)

    def get_friend_requests_sent(self):
        return self.get_relationships(RELATIONSHIP_FRIEND_REQUESTED)

    def get_following(self):
        return self.get_relationships(RELATIONSHIP_FOLLOWING)

    def get_follower(self):
        return self.get_related_to(RELATIONSHIP_FOLLOWING)

    def get_friends(self):
        return self.get_relationships(RELATIONSHIP_FRIEND)

    def get_relationships_in_group(self, status, group_id):
        return self.relationships.filter(
            to_people__status=status,
            to_people__from_person=self,
            to_people__groups__id=group_id)

    def get_friends_in_group(self, group_id):
        return self.get_relationships_in_group(RELATIONSHIP_FRIEND, group_id)

    def get_following_in_group(self, group_id):
        return self.get_relationships_in_group(RELATIONSHIP_FOLLOWING, group_id)

    def get_gender(self):
        return self.gender

    @property
    def hashed_user_id(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.user_id)

    @property
    def hashed_avatar_id(self):
        return hasher.encodeNumber(hasher.HASH_PHOTO, self.avatar) if self.avatar else None

    @property
    def hashed_cover_id(self):
        return hasher.encodeNumber(hasher.HASH_PHOTO, self.cover) if self.cover else None

    @property
    def get_birthday_display(self):
        return self.birthday.strftime("%d/%m/%Y")


# Message class stores in-box's messages of user
class Message(models.Model):
    id = BigAutoField(primary_key=True)
    sender = models.ForeignKey(Profile, related_name='+')
    receiver = models.ForeignKey(Profile, related_name='+')
    title = models.CharField(max_length=2048, default='')
    content = models.TextField(default='')
    status = models.IntegerField(choices=MESSAGE_STATUSES, default=MESSAGE_UNREAD)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    class Meta:
        ordering = ['sender', '-updated_on']

    def __str__(self):
        return "from %s to %s" % (self.sender.username, self.receiver.username)

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_MESSAGE, self.id)

    @property
    def hashed_sender(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.sender_id)

    @property
    def hashed_receiver(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.receiver_id)


# WallPost stores post of users
class WallPost(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile, related_name='owner')
    poster = models.ForeignKey(Profile, related_name='poster')
    title = models.TextField(default='')
    content = models.TextField(default='')
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    class Meta:
        ordering = ['-updated_on', 'owner']

    def __str__(self):
        return "%s post on wall of %s: %s" % (self.poster.nick_name, self.owner.nick_name, self.content)

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_WALLPOST, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)


    @property
    def hashed_poster(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.poster_id)


# File stores photo / video uploads
class UploadFile(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile)
    post = models.ForeignKey(WallPost)
    file = models.FileField(max_length=1024, upload_to=upload_file_filename)
    created_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    class Meta:
        ordering = ['owner', '-created_on']

    def __str__(self):
        return "%s post" % self.owner.username

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_UPLOADFILE, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)

    @property
    def hashed_post(self):
        return hasher.encodeNumber(hasher.HASH_POST, self.post_id)


class FriendsGroup(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile)
    name = models.SlugField(max_length=256, default='', db_index=True)
    slug = models.SlugField(max_length=256, default='', db_index=True)
    is_user_defined = models.BooleanField(default=False)
    status = models.IntegerField(choices=GROUP_STATUSES, default=GROUP_FRIEND)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        return "%s of %s [group of %s]" % (self.name, self.owner.nick_name, self.get_status_display())

    class Meta:
        ordering = ['name', '-updated_on']

    def save(self, *args, **kwargs):
        self.do_unique_slug()
        super(FriendsGroup, self).save(*args, **kwargs)

    def do_unique_slug(self):
        """
        Ensures that the slug is always unique for this post
        """
        if not self.id:
            # make sure we have a slug first
            if not len(self.slug.strip()):
                self.slug = slugify(self.tag)

            self.slug = self.get_unique_slug(self.slug)
            return True

        return False

    def get_unique_slug(self, slug):
        """
        Iterates until a unique slug is found
        """
        orig_slug = slug
        counter = 1

        while True:
            group = FriendsGroup.objects.filter(slug=slug)
            if not group.exists():
                return slug

            slug = '%s-%s' % (orig_slug, counter)
            counter += 1

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_FRIENDSGROUP, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)


# Relationship class stores relation between 2 users
class Relationship(models.Model):
    id = BigAutoField(primary_key=True)
    from_person = models.ForeignKey(Profile, related_name='from_people')
    to_person = models.ForeignKey(Profile, related_name='to_people')
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    status = models.IntegerField(choices=RELATIONSHIP_STATUSES, default=RELATIONSHIP_FRIEND_REQUESTED)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    groups = models.ManyToManyField(FriendsGroup, related_name='friends_in_groups')

    def __str__(self):
        return "%s - %s [%s]" % (self.from_person.nick_name, self.to_person.nick_name, self.get_status_display())

    class Meta:
        ordering = ['from_person', '-updated_on']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_RELATIONSHIP, self.id)

    @property
    def hashed_from_person(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.from_person_id)

    @property
    def hashed_to_person(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.to_person_id)

    pass


# Content classes
# Category stores group of content
class Category(models.Model):
    id = BigAutoField(primary_key=True)
    name = models.CharField(max_length=256, default='', unique=True, db_index=True)
    slug = models.SlugField(max_length=256, default='', unique=True, db_index=True)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    parent = models.ForeignKey('self', null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        if self.parent:
            return '%s [%s]' % (self.name, self.parent.name)
        else:
            return self.name

    class Meta:
        ordering = ['name']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_CATEGORY, self.id)

    @property
    def hashed_parent(self):
        if self.parent_id is None:
            return None
        else:
            return hasher.encodeNumber(hasher.HASH_CATEGORY, self.parent_id)


# FeedSource stores the source to feed news
class FeedSource(models.Model):
    id = BigAutoField(primary_key=True)
    name = models.CharField(max_length=1024, default='')
    url = models.URLField(max_length=4096, default='')
    feed_source_url = models.URLField(max_length=4096, default='')
    # icon = models.FileField(max_length=1024, upload_to=feed_source_icon_filename, default='')
    icon = models.IntegerField(null=True)
    # thumbnail = models.FileField(max_length=1024, upload_to=feed_source_thumbnail_filename, default='')
    thumbnail = models.IntegerField(null=True)
    # cover = models.FileField(max_length=1024, upload_to=feed_source_cover_filename, default='')
    cover = models.IntegerField(null=True)
    description = models.TextField(default='')
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    categories = models.ManyToManyField(Category, related_name='feed_sources')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_FEEDSOURCE, self.id)

    @property
    def hashed_icon_id(self):
        return hasher.encodeNumber(hasher.HASH_PHOTO, self.icon) if self.icon else None

    @property
    def hashed_thumbnail_id(self):
        return hasher.encodeNumber(hasher.HASH_PHOTO, self.thumbnail) if self.thumbnail else None

    @property
    def hashed_cover_id(self):
        return hasher.encodeNumber(hasher.HASH_PHOTO, self.cover) if self.cover else None


# Interest stores interests
class Interest(models.Model):
    id = BigAutoField(primary_key=True)
    name = models.CharField(max_length=256, default='', unique=True, db_index=True)
    slug = models.SlugField(max_length=256, default='', unique=True, db_index=True)
    description = models.TextField()
    # thumbnail = models.FileField(max_length=1024, upload_to=interest_thumbnail_filename, default='')
    thumbnail = models.IntegerField(null=True)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    parent = models.ForeignKey('self', null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        if self.parent:
            return '%s [%s]' % (self.name, self.parent.name)
        else:
            return self.name

    class Meta:
        ordering = ['parent__name', 'name']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_INTEREST, self.id)

    @property
    def hashed_parent(self):
        if self.parent_id is None:
            return None
        else:
            return hasher.encodeNumber(hasher.HASH_INTEREST, self.parent_id)

    @property
    def hashed_thumbnail_id(self):
        return hasher.encodeNumber(hasher.HASH_PHOTO, self.thumbnail) if self.thumbnail else None


# Stores interests of user
class InterestsOfUser(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile)
    interest = models.ForeignKey(Interest)
    created_on = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True, auto_now_add=False)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        return '%s interested in %s [%s]' % (self.owner.nick_name, self.interest.name, self.is_deleted)

    class Meta:
        ordering = ['owner', 'interest']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_INTERESTOFUSER, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)

    @property
    def hashed_interest(self):
        return hasher.encodeNumber(hasher.HASH_INTEREST, self.owner_id)

    pass


# Stores feed_source of user
class FeedSourceOfUser(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile)
    feed_source = models.ForeignKey(FeedSource)
    created_on = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True, auto_now_add=False)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)
    name = models.CharField(max_length=1024, default='')

    def __str__(self):
        return '%s subscribed web %s [original name: %s]' % (self.owner.nick_name, self.name, self.feed_source.name)

    class Meta:
        ordering = ['owner', '-id']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_FEEDSOURCEOFUSER, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)

    @property
    def hashed_source(self):
        return hasher.encodeNumber(hasher.HASH_FEEDSOURCE, self.owner_id)

    pass


# Generic actions of content
class Tag(models.Model):
    id = BigAutoField(primary_key=True)
    person = models.ForeignKey(Profile)
    tag = models.SlugField(max_length=256, default='', db_index=True)
    slug = models.SlugField(max_length=256, default='', db_index=True)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    is_user_defined = models.BooleanField(default=False)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        return "%s tag <%s> on %s [%s]" % (self.person.nick_name, self.tag, self.content_type.model, self.content_object)

    class Meta:
        ordering = ['-updated_on', 'tag']

    def save(self, *args, **kwargs):
        self.do_unique_slug()
        super(Tag, self).save(*args, **kwargs)

    def do_unique_slug(self):
        """
        Ensures that the slug is always unique for this post
        """
        if not self.id:
            # make sure we have a slug first
            if not len(self.slug.strip()):
                self.slug = slugify(self.tag)

            # self.slug = self.get_unique_slug(self.slug)
            return True

        return False

    def get_unique_slug(self, slug):
        """
        Iterates until a unique slug is found
        """
        orig_slug = slug
        counter = 1

        while True:
            tag = Tag.objects.filter(slug=slug)
            if not tag.exists():
                return slug

            slug = '%s-%s' % (orig_slug, counter)
            counter += 1

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_TAG, self.id)

    @property
    def hashed_person(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.person_id)

    @property
    def hashed_content(self):
        return hasher.encodeNumber(self.content_type_id, self.object_id)


class Action(models.Model):
    id = BigAutoField(primary_key=True)
    person = models.ForeignKey(Profile)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    action = models.IntegerField(choices=ACTION_TYPES, default=ACTION_UNDEFINED)
    note = models.TextField(default='undefined')
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s [<%s> %s] at %s" % (self.person.nick_name, self.get_action_display(), self.content_type, self.content_object, self.updated_on)

    class Meta:
        ordering = ['person', '-updated_on']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_ACTION, self.id)

    @property
    def hashed_person(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.person_id)

    @property
    def hashed_content(self):
        return hasher.encodeNumber(self.content_type_id, self.object_id)


# Comment
class Comment(models.Model):
    id = BigAutoField(primary_key=True)
    person = models.ForeignKey(Profile)
    title = models.TextField(default='')
    content = models.TextField(default='')
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['person', '-updated_on']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_COMMENT, self.id)

    @property
    def hashed_person(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.person_id)

    @property
    def hashed_content(self):
        return hasher.encodeNumber(self.content_type_id, self.object_id)


# Folder stores user defined groups
class Folder(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile)
    name = models.CharField(max_length=256, db_index=True)
    slug = models.SlugField(max_length=256, db_index=True)
    description = models.TextField()
    is_user_defined = models.BooleanField(default=False)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    parent = models.ForeignKey('self', null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    class Meta:
        ordering = ['owner', 'name']

    def __str__(self):
        if self.parent:
            return '%s [%s]' % (self.name, self.parent.name)
        else:
            return self.name

    def save(self, *args, **kwargs):
        self.do_unique_slug()
        super(Folder, self).save(*args, **kwargs)

    def do_unique_slug(self):
        """
        Ensures that the slug is always unique for this post
        """
        if not self.id:
            # make sure we have a slug first
            if not len(self.slug.strip()):
                self.slug = slugify(self.name)

            self.slug = self.get_unique_slug(self.slug)
            return True

        return False

    def get_unique_slug(self, slug):
        """
        Iterates until a unique slug is found
        """
        orig_slug = slug
        counter = 1

        while True:
            folder = Folder.objects.filter(slug=slug)
            if not folder.exists():
                return slug

            slug = '%s-%s' % (orig_slug, counter)
            counter += 1

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_FOLDER, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)

    @property
    def hashed_parent(self):
        return hasher.encodeNumber(hasher.HASH_FOLDER, self.parent_id) if self.parent_id else None


# Bookmark stores bookmarked links
class Bookmark(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile, null=True)
    title = models.CharField(max_length=1024, db_index=True, default='')
    slug = models.SlugField(max_length=1024, db_index=True, default='')
    url = models.URLField(max_length=4096, default='')
    status = models.IntegerField(choices=BOOKMARK_STATUSES, default=BOOKMARK_NONE)
    feed_source = models.ForeignKey(FeedSource, related_name='bookmarks', null=True)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    folders = models.ManyToManyField(Folder, through='BookmarksInFolder', through_fields=('bookmark', 'folder'),
                                     symmetrical=False, related_name='bookmarks_in_folder')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.do_unique_slug()
        super(Bookmark, self).save(*args, **kwargs)

    def do_unique_slug(self):
        """
        Ensures that the slug is always unique for this post
        """
        if not self.id:
            if not len(self.slug.strip()):
                self.slug = slugify(self.title)

            return True

        return False

    class Meta:
        ordering = ['owner', 'feed_source', 'title']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_BOOKMARK, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)


# Stores bookmarks in folder
class BookmarksInFolder(models.Model):
    id = BigAutoField(primary_key=True)
    folder = models.ForeignKey(Folder)
    bookmark = models.ForeignKey(Bookmark)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)
    name = models.CharField(max_length=1024, default='')

    def __str__(self):
        return '%s in folder "%s"' % (self.bookmark.title, self.folder.name)

    class Meta:
        ordering = ['folder', '-updated_on']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_BOOKMARKINFOLDER, self.id)

    pass


# Post stores articles    
class Post(models.Model):
    id = BigAutoField(primary_key=True)
    poster = models.ForeignKey(Profile, null=True)
    title = models.TextField(max_length=1024, default='')
    author = models.TextField(max_length=1024, default='')
    slug = models.SlugField(max_length=1024, default='')
    content = models.TextField(default='')
    description = models.TextField(default='')
    thumbnails = models.CommaSeparatedIntegerField(max_length=4096)
    published_on = models.DateTimeField()
    is_published = models.BooleanField(default=False)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    categories = models.ManyToManyField(Category, related_name='posts')
    interests = models.ManyToManyField(Interest, related_name='posts')
    source = models.ForeignKey(Bookmark, related_name='content', null=True)
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        poster_name = self.poster.nick_name if self.poster else '<no poster>'
        source_id = self.source_id if self.source else '<no bookmark>'
        return "%s [%s - %s]" % (self.title, poster_name, source_id)

    class Meta:
        ordering = ['-published_on']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_POST, self.id)

    @property
    def hashed_poster(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.poster_id) if self.poster_id else None

    @property
    def hashed_source(self):
        if self.source_id is None:
            return None
        else:
            return hasher.encodeNumber(hasher.HASH_BOOKMARK, self.source_id) if self.source_id else None


# Share object to another user in the system
class Share(models.Model):
    id = BigAutoField(primary_key=True)
    person = models.ForeignKey(Profile, related_name='from_person', default=-1)
    to_person = models.ForeignKey(Profile, related_name='to_person', null=True)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    action = models.IntegerField(choices=SHARE_ACTIONS, default=SHARE_NONE)
    permissions = models.IntegerField(choices=SHARE_PERMISSIONS, default=SHARE_READ)
    is_accepted = models.NullBooleanField(default=None)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)

    def __str__(self):
        to_person = self.to_person.nick_name if self.to_person else 'public'
        return '%s - %s [%s - %s] : <%s> %s' % (self.person.nick_name, to_person, self.get_action_display(), self.get_permissions_display(), self.content_type, self.content_object)

    class Meta:
        ordering = ['-updated_on', 'person']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_SHARE, self.id)

    @property
    def hashed_person(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.person_id)

    @property
    def hashed_to_person(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.to_person_id) if self.to_person else None

    @property
    def hashed_content(self):
        return hasher.encodeNumber(self.content_type_id, self.object_id)


class Session(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile, related_name='sessions')
    session_key = models.CharField(max_length=1024, unique=True, db_index=True, default='')
    session_data = models.TextField(default='')
    expire_time = models.DateTimeField(default=timezone.now)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return '%s [%s]' % (self.session_key, self.owner.nick_name)

    class Meta:
        ordering = ['-updated_on']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_SESSION, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)

    @classmethod
    def verify(cls, token, user=None):
        try:
            session = Session.objects.get(session_key__iexact=token)
        except Session.DoesNotExist:
            # Session does not exist
            return None

        if user is None or session.owner_id == user.id:
            if session.is_deleted:
                # Session is expired
                return None

            if session.expire_time < timezone.now:
                # Session is expired
                session.is_deleted = True
                session.save()
                return None

            return session

        # Wrong identity
        return None

    @classmethod
    def error_str(cls, code):
        if code == SESSION_VALID:
            return 'Session is valid'
        elif code == SESSION_EXPIRED:
            return 'Session is expired'
        elif code == SESSION_NOT_EXIST:
            return 'Session does not exist'
        elif code == SESSION_WRONG_IDENTITY:
            return 'Session authentication failed'
        else:
            return 'Session got undefined error'

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()


class Photo(models.Model):
    id = BigAutoField(primary_key=True)
    owner = models.ForeignKey(Profile)
    caption = models.CharField(max_length=2048, default='')
    file = models.FileField(max_length=1024, upload_to=photo_filename)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    is_deleted = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)
    folder = models.CharField(max_length=8, default='')

    def __str__(self):
        return "%s [%s]" % (self.caption, self.owner.nick_name)

    class Meta:
        ordering = ['-updated_on']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_PHOTO, self.id)

    @property
    def hashed_owner(self):
        return hasher.encodeNumber(hasher.HASH_PROFILE, self.owner_id)

    def get_direct_link(self):
        link = 'photos/default/default.jpg'
        if self.file:
            link = str(self.file)
        return link


class Statistic(models.Model):
    id = BigAutoField(primary_key=True)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    views = models.IntegerField(default=0)
    shares = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)
    links = models.IntegerField(default=0)
    folders = models.IntegerField(default=0)
    created_on = models.DateTimeField(default=timezone.now)
    updated_on = models.DateTimeField(default=timezone.now)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return "likes: %s, dislikes: %s, views: %s, shares: %s, comments: %s [<%s> %s]" % (str(self.likes), str(self.dislikes), str(self.views), str(self.shares), str(self.comments), self.content_type, self.content_object)

    class Meta:
        ordering = ['content_type', 'object_id', '-updated_on']

    @property
    def hashed_id(self):
        return hasher.encodeNumber(hasher.HASH_ACTION, self.id)

    @property
    def hashed_content(self):
        return hasher.encodeNumber(self.content_type_id, self.object_id)


