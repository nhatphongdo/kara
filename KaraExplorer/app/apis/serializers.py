from app.serializers import *
from .models import *


class ItemSerializer(DynamicFieldsSerializer):
    id = serializers.CharField(source='hashed_id')
    name = serializers.CharField()
    slug = serializers.CharField()

    class Meta:
        model = ItemModel
        fields = ('id', 'name', 'slug')

    def to_representation(self, obj=None):
        if not hasattr(obj, 'name'):
            obj.name = None
            if hasattr(obj, 'title'):
                obj.name = obj.title

        return super(ItemSerializer, self).to_representation(obj)


class TagModelSerializer(DynamicFieldsSerializer):
    id = serializers.ReadOnlyField(source='hashed_id')
    tag = serializers.CharField()
    slug = serializers.SlugField()
    is_user_defined = serializers.BooleanField()
    links = BigIntegerField()
    folders = BigIntegerField()

    class Meta:
        model = TagModel
        fields = ('id', 'tag', 'slug', 'is_user_defined', 'links', 'folders')


class ActionModelSerializer(DynamicFieldsSerializer):
    updated_on = serializers.DateTimeField()
    username = serializers.CharField()

    class Meta:
        model = ActionModel
        fields = ('updated_on', 'username')


class CommentModelSerializer(DynamicFieldsSerializer):
    title = serializers.CharField()
    content = serializers.CharField()
    updated_on = serializers.DateTimeField()
    username = serializers.CharField()

    class Meta:
        model = CommentModel
        fields = ('title', 'content', 'updated_on', 'username')


class CategoryModelSerializer(DynamicFieldsSerializer):
    id = serializers.CharField()
    name = serializers.CharField()
    slug = serializers.SlugField()
    level = serializers.IntegerField()
    parent = serializers.CharField()
    children = RecursiveField(many=True)
    feeds_src = serializers.ListField(
        child=serializers.DictField()
    )
    num_post = BigIntegerField()

    class Meta:
        model = CategoryModel
        fields = ('id', 'name', 'slug', 'level', 'parent', 'children', 'feeds_src', 'num_post')


class FeedSourceModelSerializer(DynamicFieldsSerializer):
    id = serializers.CharField(source='hashed_id')
    name = serializers.CharField()
    url = serializers.URLField()
    icon = serializers.ReadOnlyField(source='hashed_icon_id')
    thumbnail = serializers.ReadOnlyField(source='hashed_thumbnail_id')
    cover = serializers.ReadOnlyField(source='hashed_cover_id')
    description = serializers.CharField()
    categories = serializers.ListField(
        source='categories_list',
        child=serializers.DictField()
    )

    class Meta:
        model = FeedSourceModel
        fields = ('id', 'name', 'url', 'icon', 'thumbnail', 'cover', 'description', 'categories')


class PostModelSerializer(DynamicFieldsSerializer):
    id = serializers.CharField(source='hashed_id')
    poster = serializers.CharField()
    name = serializers.CharField(source='title')
    slug = serializers.CharField()
    content = serializers.CharField()
    description = serializers.CharField()
    thumbnails = serializers.ListField(
        child=serializers.CharField()
    )
    published_on = serializers.DateTimeField()
    categories = serializers.ListField(
        source='categories_list',
        child=serializers.DictField()
    )
    interests = serializers.ListField(
        source='interests_list',
        child=serializers.DictField()
    )
    bookmark = serializers.DictField(source='source_bookmark')
    feed = serializers.DictField(source='source_feed')

    class Meta:
        model = PostModel
        fields = ('id', 'poster', 'name', 'slug', 'content', 'description',
                  'published_on', 'bookmark', 'feed', 'thumbnails', 'categories', 'interests')


class InterestModelSerializer(DynamicFieldsSerializer):
    id = serializers.CharField()
    name = serializers.CharField()
    slug = serializers.SlugField()
    parent = serializers.CharField()
    children = RecursiveField(many=True)
    num_post = BigIntegerField()

    class Meta:
        model = InterestModel
        fields = ('id', 'name', 'slug', 'parent', 'children', 'num_post')


class UserFolderModelSerializer(DynamicFieldsSerializer):
    id = serializers.CharField(source='hashed_id')
    owner = serializers.CharField(source='hashed_owner')
    name = serializers.CharField()
    slug = serializers.SlugField()
    description = serializers.CharField()
    is_user_defined = serializers.BooleanField()
    parent = serializers.CharField(source='hashed_parent')
    public = serializers.IntegerField()
    num_post = BigIntegerField()
    updated_on = serializers.DateTimeField()

    class Meta:
        model = FolderModel
        fields = ('id', 'owner', 'name', 'slug', 'description', 'is_user_defined',
                  'parent', 'public', 'num_post', 'updated_on')
        ordering = ['name']


class FolderModelSerializer(DynamicFieldsSerializer):
    id = serializers.CharField()
    owner = serializers.CharField()
    name = serializers.CharField()
    slug = serializers.SlugField()
    description = serializers.CharField()
    is_user_defined = serializers.BooleanField()
    parent = serializers.CharField()
    public = serializers.IntegerField()
    children = RecursiveField(many=True)
    num_post = BigIntegerField()
    updated_on = serializers.DateTimeField()

    class Meta:
        model = FolderModel
        fields = ('id', 'owner', 'name', 'slug', 'description', 'is_user_defined',
                  'parent', 'public', 'children', 'num_post', 'updated_on')


class BookmarkModelSerializer(DynamicFieldsSerializer):
    id = serializers.CharField(source='hashed_id')
    name = serializers.CharField(source='title')
    slug = serializers.CharField()
    description = serializers.CharField()
    url = serializers.URLField()
    status = serializers.IntegerField()
    folders = serializers.ListField(source='folders_list',
                                    child=serializers.DictField())
    thumbnails = serializers.ListField(child=serializers.CharField())
    updated_on = serializers.DateTimeField()

    class Meta:
        model = BookmarkModel
        fields = (
            'id', 'name', 'slug', 'description', 'url', 'status', 'folders', 'thumbnails', 'updated_on')


