from fields import BOOKMARK_NONE


class CategoryModel(object):
    def __init__(self, id='', name='', slug='', parent=None, children=None, feeds_src=None, num_post=0):
        self.id = id
        self.name = name
        self.slug = slug
        self.parent = parent
        self.children = children
        self.feeds_src = feeds_src
        self.num_post = num_post

    def __str__(self):
        return self.name
        
    class Meta:
        ordering = ['name']


class InterestModel(object):
    def __init__(self, id='', name='', slug='', parent=None, children=None, num_post=0):
        self.id = id
        self.name = name
        self.slug = slug
        self.parent = parent
        self.children = children
        self.num_post = num_post

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class FolderModel(object):
    def __init__(self, id='', owner='', name='', slug='', description='', is_user_defined=False, public=0, parent=None, children=None, num_post=0):
        self.id = id
        self.owner = owner
        self.name = name
        self.slug = slug
        self.description = description
        self.is_user_defined = is_user_defined
        self.public = public
        self.parent = parent
        self.children = children
        self.num_post = num_post

    def __str__(self):
        return self.name
        
    class Meta:
        ordering = ['name']


class BookmarkModel(object):
    def __init__(self, id='', title='', slug='', description='', url='', status=BOOKMARK_NONE, folders=None, thumbnails=None, likes=0, dislikes=0, shares=0, views=0, comments=0):
        self.id = id
        self.title = title
        self.slug = slug
        self.description = description
        self.url = url
        self.status = status
        self.folders = folders
        self.thumbnails = thumbnails
        self.likes = likes
        self.dislikes = dislikes
        self.shares = shares
        self.views = views
        self.comments = comments

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']


class PostModel(object):
    def __init__(self, id='', poster='', title='', slug='', content='', thumbnails=None, published_on=None, categories=None, source=None, likes=0, dislikes=0, shares=0, views=0, comments=0):
        self.id = id
        self.poster = poster
        self.title = title
        self.slug = slug
        self.content = content
        self.thumbnails = thumbnails
        self.categories = categories
        self.published_on = published_on
        self.likes = likes
        self.dislikes = dislikes
        self.shares = shares
        self.views = views
        self.comments = comments
        self.source = source

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']


class FeedSourceModel(object):
    def __init__(self, id='', name='', description='', url='', icon='', thumbnail='', categories=None):
        self.id = id
        self.name = name
        self.description = description
        self.url = url
        self.icon = icon
        self.thumbnail = thumbnail
        self.categories = categories

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class TagModel(object):
    def __init__(self, tag='', slug='', is_user_defined=False, count=0, content_type=0, object_id=0):
        self.tag = tag
        self.slug = slug
        self.content_type = content_type
        self.object_id = object_id
        self.is_user_defined = is_user_defined
        self.links = count
        self.folders = count
        self.updated_on = None
        self.username = None

    def __str__(self):
        return self.tag
        
    class Meta:
        ordering = ['-count', 'tag']


class ActionModel(object):
    def __init__(self, username='', updated_on=None):
        self.username = username
        self.updated_on = updated_on

    class Meta:
        ordering = ['-updated_on']


class ItemModel(object):
    def __init__(self, id=None, name='', slug=''):
        self.id = id
        self.name = name
        self.slug = slug

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class CommentModel(object):
    def __init__(self, title='', content='', username='', updated_on=None):
        self.title = title
        self.content = content
        self.username = username
        self.updated_on = updated_on

    class Meta:
        ordering = ['-updated_on']


