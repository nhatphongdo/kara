from django.conf.urls import patterns, url, include
from . import api

other_tags_api = [
    url(r'^$',
        api.OtherTags.as_view(), name='api_other_tags'),
    url(r'^page(?P<page>\d+)/$',
        api.OtherTags.as_view(), name='api_other_tags_page'),
    url(r'^page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.OtherTags.as_view(), name='api_other_tags_page_items')
]

user_tags_api = [
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.UserTags.as_view(), name='api_user_tags'),
    url(r'^(?P<username>[\w@_.-]+)/page(?P<page>\d+)/$',
        api.UserTags.as_view(), name='api_user_tags_page'),
    url(r'^(?P<username>[\w@_.-]+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.UserTags.as_view(), name='api_user_tags_page_items')
]

find_tag_api = [
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.FindTag.as_view(), name='api_find_tag'),
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FindTag.as_view(), name='api_find_tag_page'),
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FindTag.as_view(), name='api_find_tag_page_items'),
]

with_tag_api = [
    url(r'^(?P<slug>[\w-]+)/$',
        api.WithTag.as_view(), name='api_with_user_tag'),
    url(r'^(?P<slug>[\w-]+)/page(?P<page>\d+)/$',
        api.WithTag.as_view(), name='api_with_user_tag_page'),
    url(r'^(?P<slug>[\w-]+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.WithTag.as_view(), name='api_with_user_tag_page_items'),

    url(r'^(?P<slug>[\w-]+)/delete/$',
        api.WithTag.as_view(), name='api_with_user_tag_delete'),
    url(r'^(?P<slug>[\w-]+)/rename/$',
        api.WithTag.as_view(), name='api_with_user_tag_rename'),
]

favorite_api = [
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.Favorite.as_view(), name='api_tag_favorite'),
]

user_action_api = [
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.UserAction.as_view(), name='api_user_action')
]

user_comment_api = [
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.UserComment.as_view(), name='api_user_comment')
]

comments_api = [
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.Comments.as_view(), name='api_comments'),
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.Comments.as_view(), name='api_comments_page'),
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.Comments.as_view(), name='api_comments_page_items')
]

user_subscribe_api = [
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.UserSubscribe.as_view(), name='api_user_subscribe')
]

statistic_api = [
    url(r'^(?P<content_type>\w+)/slug-(?P<slug>[\w-]+)/$',
        api.StatisticView.as_view(), name='api_statistic_slug'),
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.StatisticView.as_view(), name='api_statistic')
]

un_subscribe_api = [
    url(r'^interests/$',
        api.UnSubscribeInterests.as_view(), name='api_un_subscribe_interests'),
    url(r'^interests/restore/$',
        api.UnSubscribeInterests.as_view(), name='api_un_subscribe_interests_restore'),
    url(r'^sources/$',
        api.UnSubscribeSources.as_view(), name='api_un_subscribe_sources'),
    url(r'^sources/restore/$',
        api.UnSubscribeSources.as_view(), name='api_un_subscribe_sources_restore'),
    url(r'^bookmarks/$',
        api.UnSubscribeBookmarks.as_view(), name='api_un_subscribe_bookmarks'),
    url(r'^bookmarks/restore/$',
        api.UnSubscribeBookmarks.as_view(), name='api_un_subscribe_bookmarks_restore')
]

find_user_api = [
    url(r'^$',
        api.FindFriends.as_view(), name='api_find_user_friends'),

    url(r'^subscriber/(?P<pk>\w+)/$',
        api.FindSubscriber.as_view(), name='api_find_user_subscriber'),
    url(r'^subscriber/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FindSubscriber.as_view(), name='api_find_user_subscriber_page'),
    url(r'^subscriber/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FindSubscriber.as_view(), name='api_find_user_subscriber_page_items'),

    url(r'^interested/(?P<pk>\w+)/$',
        api.FindUserInterested.as_view(), name='api_find_user_interested'),
    url(r'^interested/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FindUserInterested.as_view(), name='api_find_user_interested_page'),
    url(r'^interested/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FindUserInterested.as_view(), name='api_find_user_interested_page_items'),

    url(r'^liked/(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.FindUserLiked.as_view(), name='api_find_user_liked'),
    url(r'^liked/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FindUserLiked.as_view(), name='api_find_user_liked_page'),
    url(r'^liked/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FindUserLiked.as_view(), name='api_find_user_liked_page_items'),

    url(r'^disliked/(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.FindUserDisliked.as_view(), name='api_find_user_disliked'),
    url(r'^disliked/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FindUserDisliked.as_view(), name='api_find_user_disliked_page'),
    url(r'^disliked/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FindUserDisliked.as_view(), name='api_find_user_disliked_page_items'),

    url(r'^viewed/(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.FindUserViewed.as_view(), name='api_find_user_viewed'),
    url(r'^viewed/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FindUserViewed.as_view(), name='api_find_user_viewed_page'),
    url(r'^viewed/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FindUserViewed.as_view(), name='api_find_user_viewed_page_items'),

    url(r'^shared/(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.FindUserShared.as_view(), name='api_find_user_shared'),
    url(r'^shared/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FindUserShared.as_view(), name='api_find_user_shared_page'),
    url(r'^shared/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FindUserShared.as_view(), name='api_find_user_shared_page_items'),

    url(r'^commented/(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.FindUserCommented.as_view(), name='api_find_user_commented'),
    url(r'^commented/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FindUserCommented.as_view(), name='api_find_user_commented_page'),
    url(r'^commented/(?P<content_type>\w+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FindUserCommented.as_view(), name='api_find_user_commented_page_items'),
]

search_api = [
    url(r'^(?P<keyword>[\w _.-]+)/$',
        api.SearchKeyword.as_view(), name='api_search'),
    url(r'^(?P<keyword>[\w _.-]+)/page(?P<page>\d+)/$',
        api.SearchKeyword.as_view(), name='api_search_page'),
    url(r'^(?P<keyword>[\w _.-]+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.SearchKeyword.as_view(), name='api_search_page_items'),
]

photo_eule = [
    url(r'^$',
        api.EulePhotoView.as_view(), name='api_get_photos'),
    url(r'^page(?P<page>\d+)/$',
        api.EulePhotoView.as_view(), name='api_get_photos_page'),
    url(r'^page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.EulePhotoView.as_view(), name='api_get_photos_page_items'),
    url(r'^pk-(?P<pk>\w+)/$',
        api.EulePhotoView.as_view(), name='api_get_photos_pk'),
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.EulePhotoView.as_view(), name='api_get_photos_user'),
    url(r'^(?P<username>[\w@_.-]+)/page(?P<page>\d+)/$',
        api.EulePhotoView.as_view(), name='api_get_photos_user_page'),
    url(r'^(?P<username>[\w@_.-]+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.EulePhotoView.as_view(), name='api_get_photos_user_page_items'),
    url(r'^(?P<username>[\w@_.-]+)/pk-(?P<pk>\w+)/$',
        api.EulePhotoView.as_view(), name='api_get_photos_user_pk'),
]

statistic_eule = [
    url(r'^update/$',
        api.EuleStatisticUpdate.as_view(), name='api_update_statistic'),
    # url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/$',
    #     api.EuleStatisticUpdate.as_view(), name='api_get_statistic_of_type_pk')
]

count_eule = [
    url(r'^(?P<content_type>\w+)/$',
        api.EuleCount.as_view(), name='api_count_of_type'),
    url(r'^(?P<content_type>\w+)/(?P<pk>\w+)/$',
        api.EuleCount.as_view(), name='api_count_of_type_pk'),
    url(r'^(?P<content_type>\w+)/(?P<slug>[\w-]+)/$',
        api.EuleCount.as_view(), name='api_count_of_type_slug'),
]

post_api = [
    url(r'^(?P<pk>\w+)/$',
        api.PostView.as_view(), name='api_post_pk'),
]

"""
ActionServices:
    /api/post-on-wall/:username/
        post    -> add
"""

urlpatterns = patterns(
    '',
    url(r'^(?i)api/tags_sys/', include(other_tags_api)),
    url(r'^(?i)api/tags/', include(user_tags_api)),
    url(r'^(?i)api/action/', include(user_action_api)),
    url(r'^(?i)api/comment/', include(user_comment_api)),
    url(r'^(?i)api/comments/', include(comments_api)),
    url(r'^(?i)api/subscribe/', include(user_subscribe_api)),
    url(r'^(?i)api/statistic/', include(statistic_api)),
    url(r'^(?i)api/unsubscribe/', include(un_subscribe_api)),
    url(r'^(?i)api/find-user/', include(find_user_api)),
    url(r'^(?i)api/find-tag/', include(find_tag_api)),
    url(r'^(?i)api/with-tag/', include(with_tag_api)),
    url(r'^(?i)api/favorite/', include(favorite_api)),
    url(r'^(?i)api/search/', include(search_api)),
    url(r'^(?i)api/post/', include(post_api)),

    url(r'^(?i)eule/photos/', include(photo_eule)),
    url(r'^(?i)eule/statistic/', include(statistic_eule)),
    url(r'^(?i)eule/count/', include(count_eule)),

    url(r'^(?i)api/send-mail/(?P<email>[\w@_.-]+)/reset-password/$',
        api.SendMailResetPWD.as_view(), name='api_send_mail_reset_password'),
    url(r'^(?i)api/send-mail/(?P<email>[\w@_.-]+)/article-weekly/$',
        api.SendMailArticleWeekly.as_view(), name='api_send_mail_article_weekly'),
    url(r'^(?i)api/send-mail/(?P<email>[\w@_.-]+)/verify/$',
        api.SendMailVerify.as_view(), name='api_send_mail_verify'),
    url(r'^(?i)api/send-mail/(?P<email>[\w@_.-]+)/welcome/$',
        api.SendMailWelcome.as_view(), name='api_send_mail_welcome'),
    url(r'^(?i)api/post-on-wall/(?P<username>[\w@_.-]+)/$',
        api.ActionWallPost.as_view(), name='api_user_post_on_wall'),
)


