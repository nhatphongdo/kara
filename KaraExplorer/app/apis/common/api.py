import mandrill
import uuid
from datetime import timedelta
from django.db.models import Q, Count
from django.views.decorators.csrf import csrf_exempt
from itertools import chain
from rest_framework.response import Response
from rest_framework.views import APIView
from app.apis.serializers import *
from app.errors import *
from app.decorators import token_required
from app.utils import *


# ACTION
def save_action(user_id, action_id, content_type_id, object_id):
    try:
        act = Action(
            person_id=user_id,
            action=action_id,
            content_type_id=content_type_id,
            object_id=object_id
        )
        act.save()
        return True
    except ValueError:
        return False


def log_action(user_id, action, content_type_id, object_id):
    """
    :param user_id: id
    :param action: text
    :param content_type_id: id
    :param object_id: id
    :return: bool
    """
    action_id = action_id_of(action)

    if content_type_id and object_id:
        # print('ACTION: ', 'user_id', user_id, 'action', action,
        #       'content_type_id', content_type_id, 'action_id', action_id)
        if action_id:
            result = save_action(user_id, action_id, content_type_id, object_id)
            try:
                new_statistic = Statistic.objects.get(content_type_id=content_type_id, object_id=object_id)
            except ObjectDoesNotExist:
                new_statistic = Statistic(
                    content_type_id=content_type_id,
                    object_id=object_id
                )
                new_statistic.save()
            if action_id == ACTION_VIEW:
                new_statistic.views += 1
            if action_id == ACTION_SHARE:
                new_statistic.shares += 1

            actions_like = [ACTION_LIKE, ACTION_UNLIKE, ACTION_DISLIKE]
            actions = Action.objects.filter(Q(content_type_id=content_type_id), Q(object_id=object_id))
            actions = actions.filter(Q(person_id=user_id), Q(action__in=actions_like))
            actions = actions.order_by('-updated_on', '-id')
            actions = actions.values_list('action', flat=True)
            if action_id == ACTION_LIKE:
                if len(actions) > 1:
                    if ACTION_UNLIKE == actions[1]:
                        new_statistic.likes += 1
                    if ACTION_DISLIKE == actions[1]:
                        new_statistic.dislikes -= 1
                        new_statistic.likes += 1
                else:
                    new_statistic.likes += 1
            if action_id == ACTION_DISLIKE:
                if len(actions) > 1:
                    if ACTION_UNLIKE == actions[1]:
                        new_statistic.dislikes += 1
                    if ACTION_LIKE == actions[1]:
                        new_statistic.likes -= 1
                        new_statistic.dislikes += 1
                else:
                    new_statistic.dislikes += 1
            if action_id == ACTION_UNLIKE:
                if len(actions) > 1:
                    if ACTION_LIKE == actions[1]:
                        new_statistic.likes -= 1
                    if ACTION_DISLIKE == actions[1]:
                        new_statistic.dislikes -= 1

            new_statistic.save()

            return result
        else:
            return False
    return False


def log_action_object_hashed(user_id, action, content_type, object_hashed):
    """
    :param user_id: id
    :param action: text //action.get('action', None)
    :param content_type: text //action.get('content_type', None)
    :param object_hashed: hash_id //action.get('object', None)
    :return: bool
    :example: {"action": {"action": "like", "content_type": "post", "object": "exkf23l2"}}
    """
    content_type = content_type_of(content_type)
    try:
        object_id = hasher.decodeNumber(object_hashed)
    except ValueError:
        object_id = None

    return log_action(user_id, action, content_type.id, object_id) if content_type and object_id else False


# USER'S ACTION
class UserAction(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        """
        1.log action
        :param request:
        :param session:
        :param username:
        :return:
        """
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        # {"action": {"action": "like", "content_type": "post", "object": "exkf23l2"}}
        action_new = request.data.get('action', None)
        if action_new:
            action = action_new.get('action', None)
            content_type = action_new.get('content_type', None)
            object_hashed = action_new.get('object', None)
            return Response({'error': 'SUCCESS', 'params': params})\
                if log_action_object_hashed(user_id, action, content_type, object_hashed)\
                else Response({'error': 'FAILED', 'params': params})

        # get action
        return Response({'error': 'FAILED', 'params': params})


# COMMENT
def save_comment(user_id, title, content, content_type_id, object_id):
    try:
        cmt = Comment(
            person_id=user_id,
            title=title,
            content=content,
            content_type_id=content_type_id,
            object_id=object_id,
            updated_on=timezone.now()
        )
        cmt.save()
        return cmt
    except ValueError:
        return False


def add_comment_into_object(user_id, title, content, content_type_id, object_id):
    if content_type_id and object_id:
        # print('COMMENT: ', 'user_id', user_id, 'title', title, 'content', content,
        #       'content_type_id', content_type_id, 'object_id', object_id)
        if content:
            result = save_comment(user_id, title, content, content_type_id, object_id)
            try:
                new_statistic = Statistic.objects.get(content_type_id=content_type_id, object_id=object_id)
            except ObjectDoesNotExist:
                new_statistic = Statistic(
                    content_type_id=content_type_id,
                    object_id=object_id
                )
                new_statistic.save()
            new_statistic.comments += 1
            new_statistic.save()

            return result
        else:
            return False
    return False


def add_comment_into_object_hashed(user_id, title, content, content_type, object_hashed):
    content_type = content_type_of(content_type)
    try:
        object_id = hasher.decodeNumber(object_hashed)
    except ValueError:
        object_id = None

    return add_comment_into_object(user_id, title, content, content_type.id, object_id)\
        if content_type and object_id else False


# USER COMMENT
class UserComment(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        """
        1.add comment into object
        :param request:
        :param session:
        :param username:
        :return:
        """
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        # {"comment": {"title": "", "content": "123 alo alo 321", "content_type": "post", "object": "exkf23l2"}}
        comment_new = request.data.get('comment', None)
        if comment_new:
            title = comment_new.get('title', None)
            content = comment_new.get('content', None)
            content_type = comment_new.get('content_type', None)
            object_hashed = comment_new.get('object', None)
            comment = add_comment_into_object_hashed(user_id, title, content, content_type, object_hashed)
            if comment.id:
                # print(comment.id)
                comment = UserCommentsSerializer(comment, fields=('id', 'person', 'name', 'content', 'updated_on')).data
                return Response({
                    'error': 'SUCCESS',
                    'params': params,
                    'comment': comment
                })

        return Response({'error': 'FAILED', 'params': params})


class Comments(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, content_type=None, pk=None, page=1, items=10):
        params = request.data

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
            return Response({'error': 'FAILED', 'params': params, 'comments': []})

        comments = Comment.objects.filter(Q(content_type=content_type_of(content_type)), Q(object_id=pk))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            comments, paginator = get_paginator(comments, items, page)

            comments = UserCommentsSerializer(
                comments, many=True,
                fields=('id', 'person', 'name', 'content', 'updated_on')).data
            if request.user.is_authenticated():
                user_id = request.user.id
                update_status_statistic_for_list(user_id, 'comment', comments)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'comments': comments,
                'paginator': {
                    'unit': str(paginator.count) + ' source(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                }
            })
        else:
            comments = UserCommentsSerializer(
                comments, many=True,
                fields=('id', 'person', 'name', 'content', 'updated_on')).data
            if request.user.is_authenticated():
                user_id = request.user.id
                update_status_statistic_for_list(user_id, 'comment', comments)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'comments': comments
            })


# SUBSCRIBE - USER'S FEED-SOURCE
def save_user_feed_source(user_id, object_id, object_name, category):
    try:
        subscribe = FeedSourceOfUser(
            owner_id=user_id,
            feed_source_id=object_id,
            name=object_name
        )
        subscribe.save()
        add_tags_into_object(user_id, content_type_of('feedsourceofuser').id, subscribe.id, category, False, False)
        return subscribe.id
    except ValueError:
        return False


def subscribe_feed_source(user_id, source_id, object_name, category, is_deleted=False):
    if source_id:
        # print('SUBSCRIBE: ', 'user_id', user_id, 'source_id', source_id, 'object_name', object_name,
        #  'category', category, 'is_deleted', is_deleted)
        # try:
        #     subscribe = FeedSourceOfUser.objects.get(owner_id=user_id, feed_source_id=source_id)
        #     if is_deleted:
        #         subscribe.is_deleted=True
        #     elif subscribe.is_deleted:
        #         subscribe.is_deleted=False
        #     subscribe.save()
        #     return True
        # except ObjectDoesNotExist:
        #     return save_user_feed_source(user_id, source_id)
        subscribe = FeedSourceOfUser.objects.filter(Q(owner_id=user_id), Q(feed_source_id=source_id))
        if subscribe:
            item = subscribe.first()
            if is_deleted:
                subscribe.update(is_deleted=True)
            elif item.is_deleted:
                subscribe.update(is_deleted=False, name=object_name)
            add_tags_into_object(user_id, content_type_of('feedsourceofuser').id, item.id, category, False, False)
            return item.id
        else:
            return save_user_feed_source(user_id, source_id, object_name, category)

    return False


def subscribe_feed_source_hashed(user_id, source_hashed, name, category, is_deleted=False):
    try:
        source_id = hasher.decodeNumber(source_hashed)
        name = FeedSource.objects.get(pk=source_id).name if name is None else name
    except ValueError:
        source_id = None

    return subscribe_feed_source(user_id, source_id, name, category, is_deleted) if source_id else False


# USER'S FEED-SOURCE
class UserSubscribe(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        """
        1.subscribe a feed-source
        2.delete a user 's feed-source
        :param request:
        :param session:
        :param username:
        :return:
        """
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        # {"subscribe": {"object": "W4VUV3jd", "name": "", "tag_category": "Business"}}
        subscribe_new = request.data.get('subscribe', None)
        if subscribe_new:
            source = subscribe_new.get('object', None)
            name = subscribe_new.get('name', None)
            tag_category = subscribe_new.get('tag_category', None)
            # {"subscribe": {"object": "W4VUV3jd", "name": "", "tag_category": "Business", "is_deleted": "True"}}
            is_deleted = subscribe_new.get('is_deleted', None)
            is_deleted = (is_deleted.lower() == 'true') if type(is_deleted) is str else is_deleted
            return Response({'error': 'SUCCESS', 'params': params}) \
                if subscribe_feed_source_hashed(user_id, source, name, tag_category, is_deleted) \
                else Response({'error': 'FAILED', 'params': params})

        return Response({'error': 'FAILED', 'params': params})


# TAG
def get_tag_extra(list_object):
    return list_object.extra(
        select={
            'links': 'SELECT COUNT(tag_link.object_id) FROM {0} as tag_link '
                     'WHERE tag_link.content_type_id = {1} and {0}.tag = tag_link.tag'
            .format(tag_db, content_type_of('bookmark').id),
            'folders': 'SELECT COUNT(tag_folder.object_id) FROM {0} as tag_folder '
                       'WHERE tag_folder.content_type_id = {1} and {0}.tag = tag_folder.tag'
            .format(tag_db, content_type_of('folder').id)
        }
    )


def get_user_tags(user_id, is_extra=True, is_paginator=True, items=10, page=1):
    """
    :param user_id: int
    :return: [{tag:"tag", slug:"slug"}]
    """
    user_tags = Tag.objects.filter(Q(is_user_defined=True), Q(person_id=user_id), Q(is_deleted=False))
    user_tags = user_tags.filter(Q(content_type=content_type_of('folder')) | Q(content_type=content_type_of('bookmark')))
    user_tags = user_tags.values('tag', 'slug')
    user_tags = user_tags.annotate()

    if is_extra:
        user_tags = get_tag_extra(user_tags)
        user_tags = user_tags.values('tag', 'slug', 'links', 'folders')

        if is_paginator:
            list_tags, paginator = get_paginator(user_tags, items, page)

            serializer = TagModelSerializer(list_tags.object_list, many=True,
                                            fields=('tag', 'slug', 'links', 'folders'))
            return serializer.data, paginator
        else:
            serializer = TagModelSerializer(user_tags, many=True, fields=('tag', 'slug', 'links', 'folders'))
            return serializer.data
    else:
        if is_paginator:
            list_tags, paginator = get_paginator(user_tags, items, page)

            serializer = TagSerializer(list_tags.object_list, many=True, fields=('tag', 'slug'))
            return serializer.data, paginator
        else:
            serializer = TagSerializer(user_tags, many=True, fields=('tag', 'slug'))
            return serializer.data


def get_user_query_tags(user_id, query_tag=None):
    """
    :param user_id: int
    :return: [{tag:"tag", slug:"slug"}]
    """
    if query_tag:
        user_tags = Tag.objects.filter(Q(is_user_defined=True), Q(person_id=user_id), Q(is_deleted=False))
        user_tags = user_tags.filter(Q(tag__icontains=query_tag))
        user_tags = user_tags.values('tag')
        return set_items(user_tags, 'tag')
    else:
        return [{"tag": query_tag}]


def get_system_tags(is_extra=True, is_paginator=True, items=10, page=1):
    """
    :return: [{tag:"tag", slug:"slug"}]
    """
    system_tags = Tag.objects.filter(Q(is_user_defined=False), Q(is_deleted=False))
    system_tags = system_tags.values('tag', 'slug')
    system_tags = system_tags.annotate()

    if is_extra:
        system_tags = get_tag_extra(system_tags)
        system_tags = system_tags.values('tag', 'slug', 'links', 'folders')

        if is_paginator:
            list_tags, paginator = get_paginator(system_tags, items, page)

            serializer = TagModelSerializer(list_tags.object_list, many=True,
                                            fields=('tag', 'slug', 'links', 'folders'))
            return serializer.data, paginator
        else:
            serializer = TagModelSerializer(system_tags, many=True, fields=('tag', 'slug', 'links', 'folders'))
            return serializer.data
    else:
        if is_paginator:
            list_tags, paginator = get_paginator(system_tags, items, page)

            serializer = TagSerializer(list_tags.object_list, many=True, fields=('tag', 'slug'))
            return serializer.data, paginator
        else:
            serializer = TagSerializer(system_tags, many=True, fields=('tag', 'slug'))
            return serializer.data


def get_user_object_tags(user_id, content_type_id, object_id, is_extra=True, is_paginator=True, items=10, page=1):
    """
    :param user_id: int
    :param content_type: text //content_object.get('content_type', None)
    :param object_hashed: hash_id //content_object.get('object', None)
    :return: [{tag:"tag", slug:"slug"}]
    :example: {"content_object": {"content_type": "folder","object": "506UqDM9"}}
    """
    print(user_id, content_type_id, object_id, is_extra, is_paginator, items, page)
    if content_type_id and object_id:
        folder_tags = Tag.objects \
            .filter(Q(is_user_defined=True), Q(person_id=user_id), Q(content_type_id=content_type_id),
                    Q(object_id=object_id), Q(is_deleted=False)) \
            .values('tag', 'slug') \
            .annotate()
        if is_extra:
            folder_tags = get_tag_extra(folder_tags)
            folder_tags = folder_tags.values('tag', 'slug', 'links', 'folders')

            if is_paginator:
                list_tags, paginator = get_paginator(folder_tags, items, page)

                serializer = TagModelSerializer(list_tags.object_list, many=True,
                                                fields=('tag', 'slug', 'links', 'folders'))
                return serializer.data, paginator
            else:
                serializer = TagModelSerializer(folder_tags, many=True, fields=('tag', 'slug', 'links', 'folders'))
                return serializer.data
        else:
            if is_paginator:
                list_tags, paginator = get_paginator(folder_tags, items, page)

                serializer = TagSerializer(list_tags.object_list, many=True, fields=('tag', 'slug'))
                return serializer.data, paginator
            else:
                serializer = TagSerializer(folder_tags, many=True, fields=('tag', 'slug'))
                return serializer.data
    return False


def get_user_object_hashed_tags(user_id, content_type, object_hashed, is_extra=True, is_paginator=True, items=10, page=1):
    """
    :param user_id: int
    :param content_type: text //content_object.get('content_type', None)
    :param object_hashed: hash_id //content_object.get('object', None)
    :return: [{tag:"tag", slug:"slug"}]
    :example: {"content_object": {"content_type": "folder","object": "506UqDM9"}}
    """
    content_type = content_type_of(content_type)
    try:
        object_id = hasher.decodeNumber(object_hashed)
    except ValueError:
        object_id = None

    return get_user_object_tags(user_id, content_type.id, object_id, is_extra, is_paginator, items, page)\
        if content_type and object_id else False


def save_tag(user_id, tag, content_type_id, object_id, is_user_defined=False):
    try:
        new_tag = Tag(
            person_id=user_id,
            tag=tag,
            content_type_id=content_type_id,
            object_id=object_id,
            is_user_defined=is_user_defined
        )
        new_tag.save()
        return True
    except ValueError:
        return False


def add_tags_into_object(user_id, content_type_id, object_id, tags, is_system=False, is_deleted=False):
    """
    :param user_id: int
    :param tags: array
    :param is_system: bool, default: False
    :param is_delete: bool, default: False
    :return: bool
    """
    if not tags:
        return False
    for tag in tags:
        if tag is not None and tag == '':
            tag = None
        if tag:
            object_tag = Tag.objects \
                .filter(Q(is_user_defined=not is_system), Q(person_id=user_id), Q(content_type_id=content_type_id),
                        Q(object_id=object_id), Q(tag=tag))
            if object_tag:
                item = object_tag.first()
                if is_deleted:
                    object_tag.update(is_deleted=True)
                elif item.is_deleted:
                    object_tag.update(is_deleted=False)
            elif not is_deleted:
                result = save_tag(user_id, tag, content_type_id, object_id, not is_system)
                try:
                    new_statistic = Statistic.objects.get(content_type_id=content_type_id, object_id=object_id)
                except ObjectDoesNotExist:
                    new_statistic = Statistic(
                        content_type_id=content_type_id,
                        object_id=object_id
                    )
                    new_statistic.save()
                if content_type_id == content_type_of('folder'):
                    new_statistic.folders += 1
                if content_type_id == content_type_of('bookmark'):
                    new_statistic.links += 1
                if content_type_id == content_type_of('post'):
                    new_statistic.links += 1
                new_statistic.save()

                return result

    return False


def add_tags_into_object_hashed(user_id, content_type, object_hashed, tags, is_system=False, is_deleted=False):
    """
    :param user_id: int
    :param content_type: text //content_object.get('content_type', None)
    :param object_hashed: hash_id //content_object.get('object', None)
    :param tags: array
    :param is_system: bool, default: False
    :param is_delete: bool, default: False
    :return: bool
    example1: {"content_object": {"content_type": "folder","object": "506UqDM9"}, "tags": ["tag"]}
    example2: {"content_object": {"content_type": "folder","object": "506UqDM9"}, "sys_tags": ["Favorite"]}
    """
    content_type = content_type_of(content_type)
    try:
        object_id = hasher.decodeNumber(object_hashed)
    except ValueError:
        object_id = None

    return add_tags_into_object(user_id, content_type.id, object_id, tags, is_system, is_deleted) if content_type and object_id else False


# USER'S TAG
class UserTags(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, page=1, items=10):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function!"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        # {"is_extra": "True","is_paginator": "True"}
        # {"is_extra": "True","is_paginator": "False"}
        # {"is_extra": "False","is_paginator": "True"}
        # {"is_extra": "False","is_paginator": "False"}
        is_extra = request.data.get('is_extra', None)
        is_extra = (is_extra.lower() == 'true') if type(is_extra) is str else is_extra
        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator

        # {"content_object": {"content_type": "folder","object": "506UqDM9"}}
        content_object = request.data.get('content_object', None)
        # {"content_object": {"content_type": "folder","object": "506UqDM9"}, "tags": ["tag1", "tag2"]}
        tags_new = request.data.get('tags', None)
        # {"content_object": {"content_type": "folder","object": "506UqDM9"}, "sys_tags": ["Favorite"]}
        sys_tags_new = request.data.get('sys_tags', None)
        # {"content_object": {"content_type": "folder","object": "506UqDM9"},
        #  "sys_tags": ["Favorite"], "is_deleted": "True"}
        is_deleted = request.data.get('is_deleted', None)
        is_deleted = (is_deleted.lower() == 'true') if type(is_deleted) is str else is_deleted

        if content_object:
            content_type = content_object.get('content_type', None)
            object_hashed = content_object.get('object', None)

            if tags_new:
                # add_tags_into_object_hashed(user_id, content_type, object_hashed, tags_new)
                add_tags_into_object_hashed(user_id, content_type, object_hashed, tags_new, False, is_deleted)
            if sys_tags_new:
                add_tags_into_object_hashed(user_id, content_type, object_hashed, sys_tags_new, True, is_deleted)

            if is_paginator:
                user_object_tags, paginator = get_user_object_hashed_tags(user_id, content_type, object_hashed,
                                                                          is_extra, is_paginator, items, page)
                return Response({
                    'error': 'SUCCESS',
                    'params': params,
                    'user_object_tags': set_items(user_object_tags, 'tag'),
                    'paginator': {
                        'unit': str(paginator.count) + ' tags',
                        'totalItems': paginator.count,
                        'itemsPerPage': int(items),
                        'numPages': paginator.num_pages,
                        'currentPage': int(page)
                    }
                })
            else:
                user_object_tags = get_user_object_hashed_tags(user_id, content_type, object_hashed, is_extra, False,
                                                               items, page)
                return Response({
                    'error': 'SUCCESS',
                    'params': params,
                    'user_object_tags': set_items(user_object_tags, 'tag')
                })

        # {"query_tag": "tag"}
        query_tag = request.data.get('query_tag', None)
        if query_tag:
            return Response(get_user_query_tags(user_id, query_tag))

        if is_paginator:
            user_tags, paginator = get_user_tags(user_id, is_extra, is_paginator, items, page)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'user_tags': set_items(user_tags, 'tag'),
                'paginator': {
                    'title': 'Your Tags',
                    'unit': str(paginator.count) + ' tags',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                }
            })
        else:
            user_tags = get_user_tags(user_id, is_extra, False, items, page)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'user_tags': set_items(user_tags, 'tag')
            })


# SYSTEM'S TAG
class OtherTags(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, page=1, items=4):
        """
        1.get tags list
        2.get tags list on page and items/page
        3.get tags list basic or extra
        :param request:
        :param session:
        :param page:
        :param items:
        :return:
        """
        params = request.data

        system_tags_default = [
            {'tag': 'Share', 'slug': 'share'},
            {'tag': 'Favourite', 'slug': 'favourite'},
        ]

        # {"is_extra": "True","is_paginator": "True"}
        # {"is_extra": "True","is_paginator": "False"}
        # {"is_extra": "False","is_paginator": "True"}
        # {"is_extra": "False","is_paginator": "False"}
        is_extra = request.data.get('is_extra', None)
        is_extra = (is_extra.lower() == 'true') if type(is_extra) is str else is_extra
        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator

        if is_paginator:
            system_tags, paginator = get_system_tags(is_extra, is_paginator, items, page)
            if system_tags:
                system_tags = set_items(system_tags, 'tag')
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'system_tags_default': system_tags_default,
                'system_tags': system_tags,
                'paginator': {
                    'title': 'Other Tags',
                    'unit': str(len(system_tags)) + ' tags',
                    'totalItems': len(system_tags),
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                }
            })
        else:
            system_tags = get_system_tags(is_extra, False, items, page)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'system_tags_default': system_tags_default,
                'system_tags': set_items(system_tags, 'tag') if system_tags else system_tags_default
            })


def get_statistic(content_type, object_id):
    statistic = Statistic.objects.filter(Q(content_type_id=content_type_of(content_type)), Q(object_id=object_id))
    statistic = statistic.first()
    if not statistic:
        statistic = Statistic(
            content_type=content_type_of(content_type),
            object_id=object_id
        )
        statistic.save()

    serializer = StatisticSerializer(
        statistic,
        fields=('content_type', 'object_id', 'likes', 'dislikes', 'views', 'shares', 'comments', 'links', 'folders'))
    return serializer.data


def get_status(user_id, content_type, object_id):
    status = {
        'liked': False,
        'disliked': False,
        'viewed': False,
        'shared': False,
        'favourited': False
    }
    numb = {
        'posts': 0,
        'relate_sources': 0
    }

    actions = Action.objects.filter(Q(is_deleted=False), Q(person_id=user_id))
    actions = actions.filter(Q(content_type_id=content_type_of(content_type)), Q(object_id=object_id))
    actions = actions.order_by('-updated_on', '-id')
    actions = actions.values_list('action', flat=True)

    # print(actions)
    if ACTION_VIEW in actions:
        status['viewed'] = True
    if ACTION_SHARE in actions:
        status['shared'] = True
    if ACTION_FAVOURITE in actions:
        status['favourited'] = True

    actions_like = [ACTION_LIKE, ACTION_UNLIKE, ACTION_DISLIKE]
    actions = actions.filter(Q(action__in=actions_like))
    if len(actions):
        if ACTION_LIKE == actions[0]:
            status['liked'] = True
            status['disliked'] = False
        if ACTION_DISLIKE == actions[0]:
            status['disliked'] = True
            status['liked'] = False
        if ACTION_UNLIKE == actions[0]:
            status['liked'] = False
            status['disliked'] = False

    if 'post' == content_type:
        try:
            bookmark = Post.objects.get(id=object_id).source_id
            bookmarks = BookmarksInFolder.objects.filter(Q(is_deleted=False))
            bookmarks = bookmarks.filter(Q(folder__owner_id=user_id), Q(bookmark_id=bookmark))
            status['bookmarked'] = True if len(bookmarks) else False
        except ObjectDoesNotExist:
            status['bookmarked'] = False

    if 'bookmark' == content_type:
        try:
            bookmarks = BookmarksInFolder.objects.filter(Q(is_deleted=False))
            bookmarks = bookmarks.filter(Q(folder__owner_id=user_id), Q(bookmark_id=object_id))
            status['bookmarked'] = True if len(bookmarks) else False
        except ObjectDoesNotExist:
            status['bookmarked'] = False

    if 'feedsource' == content_type:
        try:
            subscribes = FeedSourceOfUser.objects.filter(Q(is_deleted=False))
            subscribes = subscribes.filter(Q(owner_id=user_id), Q(feed_source_id=object_id))
            status['subscribed'] = True if len(subscribes) else False
        except ObjectDoesNotExist:
            status['subscribed'] = False

        try:
            posts_viewed = Action.objects.filter(Q(is_deleted=False), Q(person_id=user_id))
            posts_viewed = posts_viewed.filter(Q(content_type=content_type_of('post')), Q(action=ACTION_VIEW))
            posts_viewed = posts_viewed.values_list('object_id', flat=True)
            posts_viewed = set(posts_viewed)
            posts = Post.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(source__feed_source_id=object_id))
            posts = posts.values_list('id', flat=True)
            posts = posts.filter(~Q(id__in=posts_viewed))
            numb['posts'] = len(posts)
        except ObjectDoesNotExist:
            numb['posts'] = 0

        try:
            sources_subscribed = FeedSourceOfUser.objects.filter(Q(is_deleted=False), Q(is_trash=False))
            sources_subscribed = sources_subscribed.filter(Q(owner_id=user_id))
            sources_subscribed = sources_subscribed.values_list('feed_source_id', flat=True)
            sources_subscribed = set(sources_subscribed)
            categories = FeedSource.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(id=object_id))
            categories = categories.values_list('categories__id', flat=True)
            sources = FeedSource.objects.filter(Q(is_deleted=False), Q(is_trash=False))
            sources = sources.filter(Q(categories__id__in=categories))
            sources = sources.filter(~Q(id__in=sources_subscribed))
            numb['relate_sources'] = len(sources)
        except ObjectDoesNotExist:
            numb['relate_sources'] = 0

    if 'interest' == content_type:
        try:
            subscribes = InterestsOfUser.objects.filter(Q(is_deleted=False), Q(owner_id=user_id))
            subscribes = subscribes.filter(Q(interest_id=object_id))
            status['subscribed'] = True if len(subscribes) else False
        except ObjectDoesNotExist:
            status['subscribed'] = False

        try:
            posts_viewed = Action.objects.filter(Q(is_deleted=False), Q(person_id=user_id))
            posts_viewed = posts_viewed.filter(Q(content_type=content_type_of('post')), Q(action=ACTION_VIEW))
            posts_viewed = posts_viewed.values_list('object_id', flat=True)
            posts_viewed = set(posts_viewed)
            posts = Post.objects.filter(Q(is_deleted=False), Q(interests__id=object_id))
            posts = posts.values_list('id', flat=True)
            posts = posts.filter(~Q(id__in=posts_viewed))
            numb['posts'] = len(posts)
        except ObjectDoesNotExist:
            numb['posts'] = 0

    if 'maininterest' == content_type:
        try:
            posts_viewed = Action.objects.filter(Q(is_deleted=False), Q(person_id=user_id))
            posts_viewed = posts_viewed.filter(Q(content_type=content_type_of('post')), Q(action=ACTION_VIEW))
            posts_viewed = posts_viewed.values_list('object_id', flat=True)
            posts_viewed = set(posts_viewed)
            interests = Interest.objects.filter(Q(is_deleted=False), Q(is_trash=False))
            interests = interests.filter(Q(parent_id=object_id), Q(interestsofuser__owner_id=user_id))
            interests = interests.values_list('id', flat=True)
            interests = set(interests)
            posts = Post.objects.filter(Q(is_deleted=False), Q(interests__id__in=interests))
            posts = posts.values_list('id', flat=True)
            posts = posts.filter(~Q(id__in=posts_viewed))
            numb['posts'] = len(posts)
        except ObjectDoesNotExist:
            numb['posts'] = 0

    return status, numb


def get_tag_status(user_id, content_type, slug):
    status = {
        'liked': False,
        'disliked': False,
        'viewed': False,
        'shared': False
    }
    numb = {
        'posts': 0,
        'relate_sources': 0
    }

    if 'tagcategory' == content_type:
        try:
            posts_viewed = Action.objects.filter(Q(is_deleted=False), Q(person_id=user_id))
            posts_viewed = posts_viewed.filter(Q(content_type=content_type_of('post')), Q(action=ACTION_VIEW))
            posts_viewed = posts_viewed.values_list('object_id', flat=True)
            posts_viewed = set(posts_viewed)
            sources_of_user = Tag.objects.filter(Q(is_deleted=False), Q(is_trash=False))
            sources_of_user = sources_of_user.filter(Q(content_type_id=content_type_of('feedsourceofuser')))
            sources_of_user = sources_of_user.filter(Q(slug__iexact=slug))
            sources_of_user = sources_of_user.values_list('object_id', flat=True)
            sources = FeedSourceOfUser.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(owner_id=user_id))
            sources = sources.values_list('feed_source_id', flat=True)
            sources = sources.filter(Q(id__in=sources_of_user))
            posts = Post.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(source__feed_source_id__in=sources))
            posts = posts.values_list('id', flat=True)
            posts = posts.filter(~Q(id__in=posts_viewed))
            numb['posts'] = len(posts)
        except ObjectDoesNotExist:
            numb['posts'] = 0

    return status, numb


def get_status_statistic(user_id, content_type, object_id=None, slug=None):
    statistic = {}
    status = {}
    numb = {}
    if content_type and object_id:
        if content_type != 'maininterest':
            statistic = get_statistic(content_type, object_id)
        status, numb = get_status(user_id, content_type, object_id)
    if content_type == 'tagcategory':
        statistic = {}
        status, numb = get_tag_status(user_id, content_type, slug)
    return statistic, status, numb


def update_status_statistic_for_list(user_id, content_type, list_items=None):
    for item in list_items:
        slug = item.get('slug', None)
        pk = item.get('id', None)
        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None
        statistic, status, numb = get_status_statistic(user_id, content_type, object_id, slug)
        item['statistic'] = statistic
        item['status'] = status
        item['numb'] = numb


class StatisticView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, content_type=None, pk=None, slug=None):
        params = request.data

        if request.user.is_authenticated():
            user_id = request.user.id

        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None

        # print(content_type, object_id, slug)

        statistic, status, numb = get_status_statistic(user_id, content_type, object_id, slug)
        return Response({
            'error': 'SUCCESS',
            'params': params,
            'statistic': statistic,
            'status': status,
            'numb': numb
        })


class EulePhotoView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, items=10, page=1):
        params = request.data
        static_params = {'username': username, 'pk': pk, 'items': items, 'page': page}

        # user_id
        user_id = request.user.id

        try:
            photo_id = hasher.decodeNumber(pk)
        except ValueError:
            photo_id = None
        pk_list = list(photo_id) if photo_id else []
        if pk:
            pk_list.append(pk)
        else:
            pk_list = request.data.get('photos_id', [])

        photos = Photo.objects.values_list('file', flat=True)
        if username:
            # print('photos of user ', username)
            photos = photos.filter(owner__user__username=username)

        if len(pk_list):
            # print('photos_pk in ', pk_list)
            photos = photos.filter(id__in=pk_list)

        # {"is_paginator": "true"}
        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            photos, paginator = get_paginator(photos)
            # print(photos.object_list)
            return Response({
                'error': 'SUCCESS',
                'static_params': static_params,
                'params': params,
                'paginator': {
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'photos': photos.object_list
            })

        return Response({
            'error': 'SUCCESS',
            'static_params': static_params,
            'params': params,
            'photos': photos
        })


def update_statistic_from_actions(items_count):
    for item in items_count:
        update_statistic_from_one_action(item['content_type'], item['object_id'], item['action'], item['action__count'])


def update_statistic_from_one_action(content_type, object_id, action, count=0):
    try:
        new_statistic = Statistic.objects.get(content_type_id=content_type, object_id=object_id)
    except ObjectDoesNotExist:
        new_statistic = Statistic(
            content_type_id=content_type,
            object_id=object_id
        )
        new_statistic.save()

    if action == ACTION_LIKE:
        new_statistic.likes = count
    if action == ACTION_DISLIKE:
        new_statistic.dislikes = count
    if action == ACTION_VIEW:
        new_statistic.views = count
    if action == ACTION_SHARE:
        new_statistic.shares = count
    new_statistic.save()


class EuleStatisticUpdate(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, content_type=None, pk=None):
        params = request.data
        static_params = {'username': username, 'content_type': content_type, 'pk': pk}

        # user_id
        user_id = request.user.id

        actions_count = Action.objects.filter(Q(is_deleted=False))
        actions_count = actions_count.values('content_type', 'object_id', 'action')
        actions_count = actions_count.annotate(Count('action'))
        actions_count = actions_count.order_by('content_type', 'object_id', 'action')

        update_statistic_from_actions(actions_count)

        items_count = Tag.objects.filter(Q(is_deleted=False))
        items_count = items_count.values('tag', 'content_type')
        items_count = items_count.annotate(Count('object_id'))
        items_count = items_count.order_by('tag', 'content_type')

        statistic = Statistic.objects.all()

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'static_params': static_params,
            'items_count': items_count,
            'statistic': StatisticSerializer(statistic, many=True).data
        })


def count_post_of_tag_category(user_id, category_slug):
    posts_viewed = Action.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(person_id=user_id))
    posts_viewed = posts_viewed.filter(Q(action=ACTION_VIEW), Q(content_type__model='post'))
    posts_viewed = posts_viewed.values_list('object_id', flat=True)
    fsou = Tag.objects.filter(Q(is_user_defined=True), Q(is_deleted=False), Q(is_trash=False))
    fsou = fsou.filter(Q(content_type__model='feedsourceofuser'), Q(slug=category_slug))
    fsou = fsou.values_list('object_id', flat=True)

    fs = FeedSourceOfUser.objects.filter(Q(id__in=fsou)).values_list('feed_source_id', flat=True)
    posts = Post.objects.filter(Q(is_deleted=False), Q(is_trash=False))
    posts = posts.filter(Q(source__feed_source__id__in=fs), ~Q(id__in=posts_viewed))
    posts = posts.count()
    return posts


class EuleCount(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, content_type=None, pk=None, slug=None):
        params = request.data
        static_params = {'username': username, 'content_type': content_type, 'pk': pk, 'slug': slug}

        # user_id
        user_id = request.user.id

        # tag_category = Tag.objects\
        #     .filter(Q(is_user_defined=True), Q(is_deleted=False), Q(is_trash=False))\
        #     .filter(Q(content_type__model='feedsourceofuser'))
        # if slug:
        #     tag_category = tag_category.filter(Q(slug=slug))
        # tag_category = tag_category.order_by('tag', 'slug')\
        #     .distinct('tag', 'slug')

        posts_viewed = Action.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(person_id=user_id))
        posts_viewed = posts_viewed.filter(Q(action=ACTION_VIEW), Q(content_type__model='post'))
        posts_viewed = posts_viewed.values_list('object_id', flat=True)
        fsou = Tag.objects.filter(Q(is_user_defined=True), Q(is_deleted=False), Q(is_trash=False))
        fsou = fsou.filter(Q(content_type__model='feedsourceofuser'))
        if slug:
            fsou = fsou.filter(Q(slug=slug))
        fsou = fsou.values_list('object_id', flat=True)
        fs = FeedSourceOfUser.objects.filter(Q(id__in=fsou)).values_list('feed_source_id', flat=True)
        posts = Post.objects.filter(Q(is_deleted=False), Q(is_trash=False))
        posts = posts.filter(Q(source__feed_source__id__in=fs), ~Q(id__in=posts_viewed))
        posts = posts.count()

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'static_params': static_params,
            'posts': posts,
            # 'tag_category': TagCategoryModelSerializer(tag_category, many=True).data
        })


class UnSubscribeInterests(APIView):
    """
    hint: {"list_object_id": []}
    hint: {"list_object_id": [], "restore": "True"}
    """

    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        params = request.data
        static_params = {'username': username}

        # user_id
        user_id = request.user.id

        list_object_id = request.data.get('list_object_id', [])
        i_list = []
        del_list = []
        for pk in list_object_id:
            try:
                id = hasher.decodeNumber(pk)
                i_list.append(id)
                del_list.append(pk)
            except ValueError:
                pass

        restore = request.data.get('restore', None)
        restore = (restore.lower() == 'true') if type(restore) is str else not restore
        try:
            select = InterestsOfUser.objects.filter(Q(owner_id=user_id), Q(interest_id__in=i_list))
            select = select.update(is_trash=restore, is_deleted=restore, updated_on=timezone.now())
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'static_params': static_params,
                'i_list': i_list,
                'del_list': del_list
            })
        except ObjectDoesNotExist:
            return Response({
                'error': 'FAILED',
                'params': params,
                'static_params': static_params,
                'i_list': i_list,
                'del_list': del_list
            })


class UnSubscribeSources(APIView):
    """
    hint: {"list_object_id": []}
    hint: {"list_object_id": [], "restore": "True"}
    """

    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        params = request.data
        static_params = {'username': username}

        # user_id
        user_id = request.user.id

        list_object_id = request.data.get('list_object_id', [])
        i_list = []
        del_list = []
        for pk in list_object_id:
            try:
                id = hasher.decodeNumber(pk)
                i_list.append(id)
                del_list.append(pk)
            except ValueError:
                pass

        restore = request.data.get('restore', None)
        restore = (restore.lower() == 'true') if type(restore) is str else not restore
        try:
            select = FeedSourceOfUser.objects.filter(Q(owner_id=user_id), Q(feed_source_id__in=i_list))
            select = select.update(is_trash=restore, is_deleted=restore, updated_on=timezone.now())
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'static_params': static_params,
                'i_list': i_list,
                'del_list': del_list
            })
        except ObjectDoesNotExist:
            return Response({
                'error': 'FAILED',
                'params': params,
                'static_params': static_params,
                'i_list': i_list,
                'del_list': del_list
            })


class UnSubscribeBookmarks(APIView):
    """
    hint: {"list_object_id": []}
    hint: {"list_object_id": [], "restore": "True"}
    """

    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        params = request.data
        static_params = {'username': username}

        # user_id
        user_id = request.user.id

        list_object_id = request.data.get('list_object_id', [])
        i_list = []
        del_list = []
        for pk in list_object_id:
            try:
                id = hasher.decodeNumber(pk)
                i_list.append(id)
                del_list.append(pk)
            except ValueError:
                pass

        restore = request.data.get('restore', None)
        restore = (restore.lower() == 'true') if type(restore) is str else not restore
        try:
            select = BookmarksInFolder.objects.filter(Q(folder_owner_id=user_id), Q(bookmark_id__in=i_list))
            select = select.update(is_trash=restore, is_deleted=restore, updated_on=timezone.now())
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'static_params': static_params,
                'i_list': i_list,
                'del_list': del_list
            })
        except ObjectDoesNotExist:
            return Response({
                'error': 'FAILED',
                'params': params,
                'static_params': static_params,
                'i_list': i_list,
                'del_list': del_list
            })


class FindFriends(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            users = User.objects.all()
        except ObjectDoesNotExist:
            users = None

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            users, paginator = get_paginator(users, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(users, many=True).data,
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(users, many=True).data
            })


class FindSubscriber(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, pk=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        owners = FeedSourceOfUser.objects.filter(Q(feed_source_id=pk)).values_list('owner_id', flat=True)
        subscriber = User.objects.filter(Q(id__in=owners))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            subscriber, paginator = get_paginator(subscriber, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(subscriber, many=True).data,
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(subscriber, many=True).data
            })


class FindUserInterested(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, pk=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        owners = InterestsOfUser.objects.filter(Q(interest_id=pk)).values_list('owner_id', flat=True)
        user_interested = User.objects.filter(Q(id__in=owners))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            user_interested, paginator = get_paginator(user_interested, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_interested, many=True).data,
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_interested, many=True).data
            })


class FindUserLiked(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, content_type=None, pk=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        actions = Action.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(action=ACTION_LIKE))
        owners = actions.filter(Q(content_type_id=content_type_of(content_type)), Q(object_id=pk))
        owners = owners.values_list('person_id', flat=True)
        user_liked = User.objects.filter(Q(id__in=owners))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            user_liked, paginator = get_paginator(user_liked, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_liked, many=True).data,
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_liked, many=True).data
            })


class FindUserDisliked(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, content_type=None, pk=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        actions = Action.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(action=ACTION_DISLIKE))
        owners = actions.filter(Q(content_type_id=content_type_of(content_type)), Q(object_id=pk))
        owners = owners.values_list('person_id', flat=True)
        user_disliked = User.objects.filter(Q(id__in=owners))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            user_disliked, paginator = get_paginator(user_disliked, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_disliked, many=True).data,
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_disliked, many=True).data
            })


class FindUserViewed(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, content_type=None, pk=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        actions = Action.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(action=ACTION_VIEW))
        owners = actions.filter(Q(content_type_id=content_type_of(content_type)), Q(object_id=pk))
        owners = owners.values_list('person_id', flat=True)
        user_viewed = User.objects.filter(Q(id__in=owners))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            user_viewed, paginator = get_paginator(user_viewed, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_viewed, many=True).data,
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_viewed, many=True).data
            })


class FindUserShared(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, content_type=None, pk=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        actions = Action.objects.filter(Q(is_deleted=False), Q(is_trash=False))
        actions = actions.filter(Q(action=ACTION_SHARE) | Q(action=ACTION_SHARE_SOCIAL))
        owners = actions.filter(Q(content_type_id=content_type_of(content_type)), Q(object_id=pk))
        owners = owners.values_list('person_id', flat=True)
        user_shared = User.objects.filter(Q(id__in=owners))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            user_shared, paginator = get_paginator(user_shared, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_shared, many=True).data,
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_shared, many=True).data
            })


class FindUserCommented(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, content_type=None, pk=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        comments = Comment.objects.filter(Q(is_deleted=False), Q(is_trash=False))
        owners = comments.filter(Q(content_type_id=content_type_of(content_type)), Q(object_id=pk))
        owners = owners.values_list('person_id', flat=True)
        user_commented = User.objects.filter(Q(id__in=owners))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            user_commented, paginator = get_paginator(user_commented, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_commented, many=True).data,
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'users': UserSerializer(user_commented, many=True).data
            })


class FindTag(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, content_type=None, pk=None, page=1, items=10):
        params = request.data

        user_id = request.user.id

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        tags = Tag.objects.filter(Q(is_deleted=False), Q(is_trash=False))
        tags = tags.filter(Q(content_type_id=content_type_of(content_type)), Q(object_id=pk)).values('tag', 'slug')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            tags, paginator = get_paginator(tags, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'tags': set_items(TagSerializer(tags, many=True, fields=('tag', 'slug')).data, 'tag'),
                'paginator': {
                    'unit': str(paginator.count) + ' subscriber(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'tags': set_items(TagSerializer(tags, many=True, fields=('tag', 'slug')).data, 'tag')
            })


class WithTag(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, slug=None, page=1, items=10):
        params = request.data
        user_id = request.user.id

        tags = Tag.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(person_id=user_id))
        tags = tags.filter(Q(slug__iexact=slug))

        rename = request.data.get('rename', None)
        if rename:
            for tag in tags:
                if tag.is_user_defined:
                    Tag.objects.create(
                        tag=rename,
                        slug=slugify(rename),
                        person_id=user_id,
                        is_user_defined=True,
                        content_type=tag.content_type,
                        object_id=tag.object_id,
                        created_on=tag.created_on,
                        updated_on=timezone.now()
                    )
            tags.filter(Q(is_user_defined=True)).update(is_deleted=True, is_trash=True, updated_on=timezone.now())
            return Response(True)

        object_folders = tags.filter(Q(content_type_id=content_type_of('folder')))
        object_folders = object_folders.values_list('object_id', flat=True)
        object_bookmarks = tags.filter(Q(content_type_id=content_type_of('bookmark')))
        object_bookmarks = object_bookmarks.values_list('object_id', flat=True)

        folders = Folder.objects.filter(Q(is_deleted=False), Q(is_trash=False))
        folders = folders.filter(Q(owner_id=user_id), Q(is_user_defined=True))
        folders = folders.filter(Q(id__in=object_folders)).order_by('-updated_on')
        folders = UserFolderSerializer(folders, many=True).data

        bookmarks = Bookmark.objects.filter(Q(is_deleted=False), Q(is_trash=False))
        bookmarks = bookmarks.filter(Q(id__in=object_bookmarks)).order_by('-updated_on')
        bookmarks = BookmarkSerializer(bookmarks, many=True).data

        mix = list(chain(folders, bookmarks))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            mix, paginator = get_paginator(mix, items, page)

            page_list_sub_folders = []
            page_list_bookmarks = []
            for item in mix.object_list:
                if 'url' in item:
                    item['type'] = 'bookmark'
                    update_status_statistic_for_list(user_id, 'bookmark', [item])
                    page_list_bookmarks.append(item)
                else:
                    item['type'] = 'folder'
                    update_status_statistic_for_list(user_id, 'folder', [item])
                    page_list_sub_folders.append(item)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'folders': page_list_sub_folders,
                'links': page_list_bookmarks,
                'mix': mix.object_list,
                'paginator': {
                    'unit': str(paginator.count) +
                            ' - ' + str(len(bookmarks)) + ' bookmark(s) and ' + str(len(folders)) + ' folder(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
            })
        else:
            page_list_sub_folders = []
            page_list_bookmarks = []
            for item in mix:
                if 'url' in item:
                    item['type'] = 'bookmark'
                    page_list_bookmarks.append(item)
                else:
                    item['type'] = 'folder'
                    page_list_sub_folders.append(item)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'folders': page_list_sub_folders,
                'links': page_list_bookmarks,
                'mix': mix
            })

    @csrf_exempt
    @token_required()
    def delete(self, request, session=None, slug=None):
        params = request.data

        user_id = request.user.id

        tags = Tag.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(is_user_defined=True), Q(person_id=user_id))
        tags = tags.filter(Q(slug__iexact=slug))
        tags.update(is_deleted=True, is_trash=True, updated_on=timezone.now())

        return Response({
            'error': 'SUCCESS',
            'params': params
        })


class Favorite(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, content_type=None, pk=None):
        params = request.data

        try:
            user_id = request.user.id

            pk = hasher.decodeNumber(pk)

            Tag.objects.create(
                tag='Favourite',
                slug=slugify('Favourite'),
                person_id=user_id,
                content_type=content_type_of(content_type),
                object_id=pk
            )
            log_action(user_id, 'favourite', content_type_of(content_type).id, pk)
            return Response({
                'error': 'SUCCESS',
                'favourited': True,
                'params': params
            })
        except ValueError:
            pass

        return Response({
            'error': 'FAILED',
            'favourited': False,
            'params': params
        })

    @csrf_exempt
    @token_required()
    def delete(self, request, session=None, username=None, content_type=None, pk=None):
        params = request.data

        try:
            user_id = request.user.id

            pk = hasher.decodeNumber(pk)

            Tag.objects.filter(
                tag='Favourite',
                slug=slugify('Favourite'),
                person_id=user_id,
                content_type=content_type_of(content_type),
                object_id=pk
            ).update(is_deleted=True, updated_on=timezone.now())
            log_action(user_id, 'un-favourite', content_type_of(content_type).id, pk)
            return Response({
                'error': 'SUCCESS',
                'favourited': False,
                'params': params
            })
        except ValueError:
            pass

        return Response({
            'error': 'FAILED',
            'favourited': True,
            'params': params
        })


def search_sources_with_keyword(user_id, key=None, is_paginator=False, items=10, page=1):
    sources = FeedSource.objects.filter(Q(is_deleted=False), Q(is_trash=False))
    if key:
        sources = FeedSource.objects.filter(Q(name__icontains=key) | Q(description__icontains=key))
    sources = sources.order_by('name')

    sources = sources.extra(
        select={
            'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                               'WHERE {0}.feedsource_id = {1}.id'
            .format(source_category_db, source_db, category_db,
                    concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                    .format('{0}.id'.format(category_db), '{0}.name'.format(category_db))))
        }
    )

    if is_paginator:
        sources, paginator = get_paginator(sources, items, page)

        sources = list(sources.object_list)
        for entry in sources:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f != None]

        serializer = FeedSourceModelSerializer(
            sources, many=True,
            fields=('id', 'name', 'thumbnail', 'cover', 'description', 'icon', 'categories'))
        update_status_statistic_for_list(user_id, 'feedsource', serializer.data)
        return serializer.data, paginator
    else:
        sources = list(sources)
        for entry in sources:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f != None]

        serializer = FeedSourceModelSerializer(
            sources, many=True,
            fields=('id', 'name', 'thumbnail', 'cover', 'description', 'icon', 'categories'))
        update_status_statistic_for_list(user_id, 'feedsource', serializer.data)
        return serializer.data


def search_interests_with_keyword(user_id, key=None, is_paginator=False, items=10, page=1):
    interests = Interest.objects.filter(Q(is_deleted=False), Q(is_trash=False), ~Q(parent_id=None))

    if key:
        interests = interests.order_by('parent__name', 'name')
        interests = interests.filter(Q(name__icontains=key) | Q(description__icontains=key))

    if is_paginator:
        interests, paginator = get_paginator(interests, items, page)

        interests = UserInterestSerializer(interests.object_list, many=True).data
        # for interest in interests:
        #     if interest['thumbnail']:
        #         id_hashed = hasher.encodeNumber(hasher.HASH_PHOTO, interest['thumbnail'])
        #         interest['thumbnail'] = id_hashed
        update_status_statistic_for_list(user_id, 'interest', interests)
        return interests, paginator
    else:
        interests = UserInterestSerializer(interests, many=True).data
        # for interest in interests:
        #     if interest['thumbnail']:
        #         id_hashed = hasher.encodeNumber(hasher.HASH_PHOTO, interest['thumbnail'])
        #         interest['thumbnail'] = id_hashed
        update_status_statistic_for_list(user_id, 'interest', interests)
        return interests


def search_folders_bookmarks_with_keyword(user_id, key=None, is_paginator=False, items=10, page=1):
    folders = Folder.objects.filter(Q(is_deleted=False), Q(is_trash=False))
    folders = folders.filter(Q(owner_id=user_id), Q(is_user_defined=True))
    folders = folders.order_by('-updated_on')
    if key:
        folders = folders.filter(Q(name__icontains=key) | Q(description__icontains=key))
    folders = UserFolderSerializer(folders, many=True).data

    bookmarks = Bookmark.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(owner_id=user_id))
    bookmarks = bookmarks.order_by('-updated_on')
    if key:
        bookmarks = bookmarks.filter(Q(title__icontains=key))
    bookmarks = BookmarkSerializer(bookmarks, many=True).data

    mix = list(chain(folders, bookmarks))

    if is_paginator:
        mix, paginator = get_paginator(mix, items, page)

        page_list_sub_folders = []
        page_list_bookmarks = []
        for item in mix.object_list:
            if 'url' in item:
                item['type'] = 'bookmark'
                update_status_statistic_for_list(user_id, 'bookmark', [item])
                page_list_bookmarks.append(item)
            else:
                item['type'] = 'folder'
                update_status_statistic_for_list(user_id, 'folder', [item])
                page_list_sub_folders.append(item)

        return {'folders': page_list_sub_folders,
                'links': page_list_bookmarks,
                'mix': mix.object_list}, paginator
    else:
        page_list_sub_folders = []
        page_list_bookmarks = []
        for item in mix:
            if 'url' in item:
                item['type'] = 'bookmark'
                update_status_statistic_for_list(user_id, 'bookmark', [item])
                page_list_bookmarks.append(item)
            else:
                item['type'] = 'folder'
                update_status_statistic_for_list(user_id, 'folder', [item])
                page_list_sub_folders.append(item)
        return {
            'folders': page_list_sub_folders,
            'links': page_list_bookmarks,
            'mix': mix
        }


def search_post_bookmarks_with_keyword(request, user_id, key=None, is_paginator=False, items=10, page=1):
    posts = Post.objects.filter(Q(is_deleted=False), Q(is_trash=False))
    if key:
        posts = posts.filter(Q(title__icontains=key) | Q(content__icontains=key))
    posts = posts.order_by('-published_on', 'title')

    posts = posts.extra(
        select={
            'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.poster_id'
            .format(user_db, post_db),
            'source_bookmark': 'SELECT {2} FROM {0} WHERE {0}.id = {1}.source_id'
            .format(bookmark_db, post_db, concatenate_fields('|', '{0}', '{1}')
                    .format('{0}.id'.format(bookmark_db), '{0}.url'.format(bookmark_db))),
            'source_feed': 'SELECT {3} FROM {0} LEFT OUTER JOIN {2} ON {0}.feed_source_id = {2}.id '
                           'WHERE {0}.id = {1}.source_id'
            .format(bookmark_db, post_db, source_db, concatenate_fields('|', '{0}', '{1}', '{2}', '{3}')
                    .format('{0}.id'.format(source_db), '{0}.name'.format(source_db), '{0}.url'.format(source_db),
                            '{0}.icon'.format(source_db))),
            'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                               'WHERE {0}.post_id = {1}.id'
            .format(post_category_db, post_db, category_db,
                    concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                    .format('{0}.id'.format(category_db), '{0}.name'.format(category_db)))),
            'interests_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.interest_id = {2}.id '
                              'WHERE {0}.post_id = {1}.id'
            .format(post_interest_db, post_db, interest_db,
                    concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                    .format('{0}.id'.format(interest_db), '{0}.name'.format(interest_db))))
        }
    )

    if request.user.is_authenticated():
        user_id = request.user.id
        user_actions = Action.objects.filter(Q(is_trash=False), Q(is_deleted=False), Q(person_id=user_id))
        user_views_post = user_actions.filter(Q(action=ACTION_VIEW), Q(content_type_id=content_type_of('post')))
        posts_viewed_list = user_views_post.values_list('object_id', flat=True)
        posts_viewed = posts.filter(Q(id__in=posts_viewed_list))
        posts_unread = posts.exclude(Q(id__in=posts_viewed_list))
        posts = list(chain(posts_unread, posts_viewed))

    if is_paginator:
        posts, paginator = get_paginator(posts, items, page)

        posts.object_list = list(posts.object_list)
        for entry in posts.object_list:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f != None]

            if entry.interests_list is not None:
                entry.interests_list = entry.interests_list.split(',')
                for idx, group in enumerate(entry.interests_list):
                    attrs = group.split(':')
                    if len(attrs) == 2:
                        entry.interests_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_INTEREST, attrs[0]),
                            'name': attrs[1]
                        }
                    else:
                        entry.interests_list[idx] = None
                entry.interests_list = [f for f in entry.interests_list if f != None]

            if entry.thumbnails is not None and entry.thumbnails != '':
                entry.thumbnails = entry.thumbnails.split(',')
                for idx, thumb_id in enumerate(entry.thumbnails):
                    entry.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id) if thumb_id else None
                    # entry.thumbnails[idx] = get_photo(thumb_id)
            else:
                entry.thumbnails = None

            if entry.source_bookmark is not None and entry.source_bookmark != '':
                bookmark_infos = entry.source_bookmark.split('|')
                if len(bookmark_infos) == 2:
                    entry.source_bookmark = {
                        'id': hasher.encodeNumber(hasher.HASH_BOOKMARK, bookmark_infos[0]),
                        'url': bookmark_infos[1]
                    }
                else:
                    entry.source_bookmark = None
            else:
                entry.source_bookmark = None

            if entry.source_feed is not None and entry.source_feed != '':
                feed_info = entry.source_feed.split('|')
                if len(feed_info) == 4:
                    entry.source_feed = {
                        'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_info[0]),
                        'name': feed_info[1],
                        'url': feed_info[2],
                        'icon': hasher.encodeNumber(hasher.HASH_PHOTO, feed_info[3])
                    }
                else:
                    entry.source_feed = None
            else:
                entry.source_feed = None

        serializer = PostModelSerializer(posts.object_list, many=True)

        update_status_statistic_for_list(user_id, 'post', serializer.data)

        return serializer.data, paginator
    else:
        for entry in posts:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f is not None]

            if entry.interests_list is not None:
                entry.interests_list = entry.interests_list.split(',')
                for idx, group in enumerate(entry.interests_list):
                    attrs = group.split(':')
                    if len(attrs) == 2:
                        entry.interests_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_INTEREST, attrs[0]),
                            'name': attrs[1]
                        }
                    else:
                        entry.interests_list[idx] = None
                entry.interests_list = [f for f in entry.interests_list if f is not None]

            if entry.thumbnails is not None and entry.thumbnails != '':
                entry.thumbnails = entry.thumbnails.split(',')
                for idx, thumb_id in enumerate(entry.thumbnails):
                    entry.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id) if thumb_id else None
                    # entry.thumbnails[idx] = get_photo(thumb_id)
            else:
                entry.thumbnails = None

            if entry.source_bookmark is not None and entry.source_bookmark != '':
                bookmark_infos = entry.source_bookmark.split('|')
                if len(bookmark_infos) == 2:
                    entry.source_bookmark = {
                        'id': hasher.encodeNumber(hasher.HASH_BOOKMARK, bookmark_infos[0]),
                        'url': bookmark_infos[1]
                    }
                else:
                    entry.source_bookmark = None
            else:
                entry.source_bookmark = None

            if entry.source_feed is not None and entry.source_feed != '':
                feed_info = entry.source_feed.split('|')
                if len(feed_info) == 4:
                    entry.source_feed = {
                        'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_info[0]),
                        'name': feed_info[1],
                        'url': feed_info[2],
                        'icon': hasher.encodeNumber(hasher.HASH_PHOTO, feed_info[3])
                    }
                else:
                    entry.source_feed = None
            else:
                entry.source_feed = None

        serializer = PostModelSerializer(posts, many=True)

        update_status_statistic_for_list(user_id, 'post', serializer.data)

        return serializer.data


class SearchKeyword(APIView):
    """
    hint: {"is_paginator": "true", "types":["post", "feedsource", "interest", "bookmark"]}
    """

    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, keyword=None, page=1, items=8):
        params = request.data
        static_params = {'username': username, 'keyword': keyword, 'items': items, 'page': page}
        if request.user.is_authenticated():
            user_id = request.user.id

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator

        try:
            types = list(request.data.get('types', ['post', 'feedsource', 'interest', 'bookmark']))
        except ValueError:
            types = ['post', 'feedsource', 'interest', 'bookmark']

        if is_paginator:
            content = {
                'error': 'SUCCESS',
                'static_params': static_params,
                'params': params,
                'posts': {},
                'paginator_posts': {},
                'sources': {},
                'paginator_sources': {},
                'interests': {},
                'paginator_interests': {},
                'mix': {},
                'paginator_mix': {}
            }
            if 'post' in types:
                posts, paginator_posts = search_post_bookmarks_with_keyword(request, user_id, keyword, is_paginator,
                                                                            items, page)
                content['posts'] = posts,
                content['paginator_posts'] = {
                    'unit': str(paginator_posts.count) + ' link(s)',
                    'totalItems': paginator_posts.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator_posts.num_pages,
                    'currentPage': int(page)
                }
            if 'feedsource' in types:
                sources, paginator_sources = search_sources_with_keyword(user_id, keyword, is_paginator, items, page)
                content['sources'] = sources,
                content['paginator_sources'] = {
                                                   'unit': str(paginator_sources.count) + ' subscriber(s)',
                                                   'totalItems': paginator_sources.count,
                                                   'itemsPerPage': int(items),
                                                   'numPages': paginator_sources.num_pages,
                                                   'currentPage': int(page)
                                               },
            if 'interest' in types:
                interests, paginator_interests = search_interests_with_keyword(user_id, keyword, is_paginator, items,
                                                                               page)
                content['interests'] = interests,
                content['paginator_interests'] = {
                    'unit': str(paginator_interests.count) + ' subscriber(s)',
                    'totalItems': paginator_interests.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator_interests.num_pages,
                    'currentPage': int(page)
                }
            if 'bookmark' in types:
                if user_id:
                    mix, paginator_mix = search_folders_bookmarks_with_keyword(user_id, keyword, is_paginator, items,
                                                                               page)
                else:
                    mix = {}
                    paginator_mix = {}
                content['mix'] = mix
                content['paginator_mix'] = {
                    'unit': str(paginator_mix.count) + ' subscriber(s)',
                    'totalItems': paginator_mix.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator_mix.num_pages,
                    'currentPage': int(page)
                }

            return Response(content)

        content = {
            'error': 'SUCCESS',
            'static_params': static_params,
            'params': params,
        }
        if 'post' in types:
            posts = search_post_bookmarks_with_keyword(request, user_id, keyword, is_paginator, items, page)
            content['posts'] = posts
        if 'feedsource' in types:
            sources = search_sources_with_keyword(user_id, keyword, is_paginator, items, page)
            content['sources'] = sources
        if 'interest' in types:
            interests = search_interests_with_keyword(user_id, keyword, is_paginator, items, page)
            content['interests'] = interests
        if 'bookmark' in types:
            if user_id:
                mix = search_folders_bookmarks_with_keyword(user_id, keyword, is_paginator, items, page)
            else:
                mix = {}
            content['mix'] = mix
        return Response(content)


class PostView(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, username=None, pk=None):
        params = request.data
        static_params = {'username': username, 'pk': pk}
        try:
            post_id = hasher.decodeNumber(pk)
        except ValueError:
            return False
        posts = Post.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(id=post_id))

        posts = posts.extra(
            select={
                'source_bookmark': 'SELECT {2} FROM {0} WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, concatenate_fields('|', '{0}', '{1}')
                        .format('{0}.id'.format(bookmark_db), '{0}.url'.format(bookmark_db))),
                'source_feed': 'SELECT {3} FROM {0} LEFT OUTER JOIN {2} ON {0}.feed_source_id = {2}.id '
                               'WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, source_db, concatenate_fields('|', '{0}', '{1}', '{2}', '{3}')
                        .format('{0}.id'.format(source_db), '{0}.name'.format(source_db), '{0}.url'.format(source_db),
                                '{0}.icon'.format(source_db))),
                'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                                   'WHERE {0}.post_id = {1}.id'
                .format(post_category_db, post_db, category_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(category_db), '{0}.name'.format(category_db)))),
                'interests_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.interest_id = {2}.id '
                                  'WHERE {0}.post_id = {1}.id'
                .format(post_interest_db, post_db, interest_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(interest_db), '{0}.name'.format(interest_db))))
            }
        )

        for entry in posts:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f != None]

            if entry.interests_list is not None:
                entry.interests_list = entry.interests_list.split(',')
                for idx, group in enumerate(entry.interests_list):
                    attrs = group.split(':')
                    if len(attrs) == 2:
                        entry.interests_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_INTEREST, attrs[0]),
                            'name': attrs[1]
                        }
                    else:
                        entry.interests_list[idx] = None
                entry.interests_list = [f for f in entry.interests_list if f != None]

            if entry.thumbnails is not None and entry.thumbnails != '':
                entry.thumbnails = entry.thumbnails.split(',')
                for idx, thumb_id in enumerate(entry.thumbnails):
                    entry.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id) if thumb_id else None
                    # entry.thumbnails[idx] = get_photo(thumb_id)
            else:
                entry.thumbnails = None

            if entry.source_bookmark is not None and entry.source_bookmark != '':
                bookmark_infos = entry.source_bookmark.split('|')
                if len(bookmark_infos) == 2:
                    entry.source_bookmark = {
                        'id': hasher.encodeNumber(hasher.HASH_BOOKMARK, bookmark_infos[0]),
                        'url': bookmark_infos[1]
                    }
                else:
                    entry.source_bookmark = None
            else:
                entry.source_bookmark = None

            if entry.source_feed is not None and entry.source_feed != '':
                feed_info = entry.source_feed.split('|')
                if len(feed_info) == 4:
                    entry.source_feed = {
                        'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_info[0]),
                        'name': feed_info[1],
                        'url': feed_info[2],
                        'icon': hasher.encodeNumber(hasher.HASH_PHOTO, feed_info[3])
                    }
                else:
                    entry.source_feed = None
            else:
                entry.source_feed = None

        serializer = PostModelSerializer(posts, many=True)
        if request.user.is_authenticated():
            user_id = request.user.id
            update_status_statistic_for_list(user_id, 'post', serializer.data)

        return Response({
            'params': params,
            'static_params': static_params,
            'post': serializer.data[0]
        })


class SendMailResetPWD(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, email=None):
        params = request.data
        if request.user.is_authenticated() and request.user.email == email:
            user = request.user
        else:
            try:
                user = User.objects.get(email=email)
            except ObjectDoesNotExist:
                return Response({})
        # create new session for this user
        old_session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id))
        old_session = old_session.update(is_deleted=True)
        session = Session(
            owner_id=user.id,
            session_key=uuid.uuid4()
        )

        session.expire_time = timezone.now() + timedelta(minutes=2 * 24 * 60)
        session.save()
        try:
            mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
            template_content = [{'name': 'editable', 'content': 'KARA'}]
            message = {
                'to': [{'email': user.email,
                        'name': user.first_name + ' ' + user.last_name,
                        'type': 'to'}],
                'track_clicks': True,
                'track_opens': True,
                'merge_vars': [{'rcpt': user.email,
                                'vars': [{'name': 'SESSIONKEY', 'content': str(session.session_key)},
                                         {'name': 'EMAIL', 'content': user.email},
                                         {'name': 'KARA', 'content': 'www.karaplus.com'}]}],
                'global_merge_vars': [{'name': 'SESSIONKEY', 'content': 'session_key'},
                                      {'name': 'EMAIL', 'content': 'user.email'},
                                      {'name': 'KARA', 'content': 'www.karaplus.com'}],

                'metadata': {'website': 'www.karaplus.com'},
                'recipient_metadata': [{'rcpt': user.email,
                                        'values': {'user_id': user.id}}],
                'tags': ['password-resets']
            }
            result = mandrill_client.messages.send_template(template_name='forgot-password',
                                                            template_content=template_content,
                                                            message=message,
                                                            async=False)
            return Response({
                'params': params,
                'result': result[0]
            })
        except mandrill.Error as e:
            return Response({
                'params': params,
                'message': 'A mandrill error occurred: %s - %s' % (e.__class__, e)
            })


class SendMailVerify(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, email=None):
        params = request.data
        if request.user.is_authenticated() and request.user.email == email:
            user = request.user
        else:
            try:
                user = User.objects.get(email=email)
            except ObjectDoesNotExist:
                return Response({})
        try:
            mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
            template_content = [{'name': 'editable', 'content': 'KARA'}]
            message = {
                'to': [{'email': user.email,
                        'name': user.first_name + ' ' + user.last_name,
                        'type': 'to'}],
                'track_clicks': True,
                'track_opens': True,
                'merge_vars': [{'rcpt': user.email,
                                'vars': [{'name': 'SESSIONKEY', 'content': str(session.session_key)},
                                         {'name': 'EMAIL', 'content': user.email},
                                         {'name': 'KARA', 'content': 'www.karaplus.com'}]}],
                'global_merge_vars': [{'name': 'SESSIONKEY', 'content': 'session_key'},
                                      {'name': 'EMAIL', 'content': 'user.email'},
                                      {'name': 'KARA', 'content': 'www.karaplus.com'}],

                'metadata': {'website': 'www.karaplus.com'},
                'recipient_metadata': [{'rcpt': user.email,
                                        'values': {'user_id': user.id}}],
                'tags': ['verify', 'sign up']
            }
            result = mandrill_client.messages.send_template(template_name='verify-email',
                                                            template_content=template_content,
                                                            message=message,
                                                            async=False)
            return Response({
                'params': params,
                'result': result[0]
            })
        except mandrill.Error as e:
            return Response({
                'params': params,
                'message': 'A mandrill error occurred: %s - %s' % (e.__class__, e)
            })


class SendMailArticleWeekly(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, email=None):
        params = request.data
        if request.user.is_authenticated() and request.user.email == email:
            user = request.user
        else:
            try:
                user = User.objects.get(email=email)
            except ObjectDoesNotExist:
                return Response({})
        try:
            mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
            template_content = [{'name': 'editable', 'content': 'KARA'}]
            message = {
                'to': [{'email': user.email,
                        'name': user.first_name + ' ' + user.last_name,
                        'type': 'to'}],
                'track_clicks': True,
                'track_opens': True,
                'merge_vars': [{'rcpt': user.email,
                                'vars': [{'name': 'SESSIONKEY', 'content': str(session.session_key)},
                                         {'name': 'FRIEND', 'content': user.first_name + ' ' + user.last_name},
                                         {'name': 'EMAIL', 'content': user.email},
                                         {'name': 'KARA', 'content': 'www.karaplus.com'}]}],
                'global_merge_vars': [{'name': 'SESSIONKEY', 'content': 'session_key'},
                                      {'name': 'EMAIL', 'content': 'user.email'},
                                      {'name': 'KARA', 'content': 'www.karaplus.com'}],

                'metadata': {'website': 'www.karaplus.com'},
                'recipient_metadata': [{'rcpt': user.email,
                                        'values': {'user_id': user.id}}],
                'tags': ['article', 'weekly']
            }
            result = mandrill_client.messages.send_template(template_name='new-article-per-week',
                                                            template_content=template_content,
                                                            message=message,
                                                            async=False)
            return Response({
                'params': params,
                'result': result[0]
            })
        except mandrill.Error as e:
            return Response({
                'params': params,
                'message': 'A mandrill error occurred: %s - %s' % (e.__class__, e)
            })


class SendMailWelcome(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, email=None):
        params = request.data
        if request.user.is_authenticated() and request.user.email == email:
            user = request.user
        else:
            try:
                user = User.objects.get(email=email)
            except ObjectDoesNotExist:
                return Response({})
        try:
            mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
            template_content = [{'name': 'editable', 'content': 'KARA'}]
            message = {
                'to': [{'email': user.email,
                        'name': user.first_name + ' ' + user.last_name,
                        'type': 'to'}],
                'track_clicks': True,
                'track_opens': True,
                'merge_vars': [{'rcpt': user.email,
                                'vars': [{'name': 'FRIEND', 'content': user.first_name + ' ' + user.last_name}]}],
                'global_merge_vars': [{'name': 'SESSIONKEY', 'content': 'session_key'},
                                      {'name': 'EMAIL', 'content': 'user.email'},
                                      {'name': 'KARA', 'content': 'www.karaplus.com'}],

                'metadata': {'website': 'www.karaplus.com'},
                'recipient_metadata': [{'rcpt': user.email,
                                        'values': {'user_id': user.id}}],
                'tags': ['welcome']
            }
            result = mandrill_client.messages.send_template(template_name='welcome-mail',
                                                            template_content=template_content,
                                                            message=message,
                                                            async=False)
            return Response({
                'params': params,
                'result': result[0]
            })
        except mandrill.Error as e:
            return Response({
                'params': params,
                'message': 'A mandrill error occurred: %s - %s' % (e.__class__, e)
            })


# ActionAPI
# actionWallPost
class ActionWallPost(APIView):
    """
    params = {
        username: $rootScope.wall_user.username,//wall_owner
        wall_post: {
            title: '',
            content: $scope.post_text
        }
    }
    """

    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        params = request.data
        # user_id
        user_id = request.user.id

        if request.user.username != username:
            try:
                wall_owner = User.objects.get(username__iexact=username)
            except ObjectDoesNotExist:
                return Response({
                    'success': False,
                    'params': params,
                    'msg': 'Not found poster'
                })
        else:
            wall_owner = request.user

        wall_post_new = request.data.get('wall_post', None)
        if wall_post_new:
            title = wall_post_new.get('title', '')
            content = wall_post_new.get('content', None)

            try:
                wall_post = WallPost(
                    poster_id=user_id,
                    owner_id=wall_owner.id,
                    title=title,
                    content=content
                )
                wall_post.save()
                wall_post_data = UserWallPostsSerializer(wall_post).data
                return Response({
                    'success': True,
                    'params': params,
                    'wall_post': wall_post_data
                })
            except ValueError:
                pass

        return Response({
            'success': False,
            'params': params
        })


