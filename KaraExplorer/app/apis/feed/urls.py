from django.conf.urls import patterns, url, include
from . import api

categories_api = [
    url(r'^structure/$',
        api.StructureCategories.as_view(), name='api_structure_categories'),
    url(r'^$',
        api.Categories.as_view(), name='api_main_categories'),
]

feeds_category_api = [
    url(r'^(?P<pk>\w+)/$',
        api.CategoryDetail.as_view(), name='api_feeds_category'),
    url(r'^(?P<pk>\w+)/posts/$',
        api.CategoryPosts.as_view(), name='api_feeds_category_posts'),
    url(r'^(?P<pk>\w+)/posts/page(?P<page>\d+)/$',
        api.CategoryPosts.as_view(), name='api_feeds_category_posts_page'),
    url(r'^(?P<pk>\w+)/posts/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.CategoryPosts.as_view(), name='api_feeds_category_posts_page_items'),
    url(r'^(?P<pk>\w+)/postsinsource/(?P<src>\w+)/$',
        api.CategoryPosts.as_view(), name='api_feeds_category_posts_in_source'),
    url(r'^(?P<pk>\w+)/postsinsource/(?P<src>\w+)/page(?P<page>\d+)/$',
        api.CategoryPosts.as_view(), name='api_feeds_category_posts_in_source_page'),
    url(r'^(?P<pk>\w+)/postsinsource/(?P<src>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.CategoryPosts.as_view(), name='api_feeds_category_posts_in_source_page_items'),
    url(r'^(?P<pk>\w+)/sources/$',
        api.CategorySources.as_view(), name='api_feeds_category_sources'),
    url(r'^(?P<pk>\w+)/sources/page(?P<page>\d+)/$',
        api.CategorySources.as_view(), name='api_feeds_category_sources_page'),
    url(r'^(?P<pk>\w+)/sources/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.CategorySources.as_view(), name='api_feeds_category_sources_page_items')
]

feeds_source_api = [
    url(r'^key-(?P<key>\w+)/$',
        api.FeedSourceWithKey.as_view(), name='api_feeds_source_key'),
    url(r'^key-(?P<key>\w+)/page(?P<page>\d+)/$',
        api.FeedSourceWithKey.as_view(), name='api_feeds_source_key_page'),
    url(r'^key-(?P<key>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FeedSourceWithKey.as_view(), name='api_feeds_source_key_page_items'),
    url(r'^(?P<pk>\w+)/$',
        api.FeedSourceDetail.as_view(), name='api_feeds_source'),

    url(r'^(?P<pk>\w+)/contents/$',
        api.FeedSourceContents.as_view(), name='api_feeds_source_contents'),
    url(r'^(?P<pk>\w+)/contents/page(?P<page>\d+)/$',
        api.FeedSourceContents.as_view(), name='api_feeds_source_contents_page'),
    url(r'^(?P<pk>\w+)/contents/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FeedSourceContents.as_view(), name='api_feeds_source_contents_page_items'),

    url(r'^(?P<pk>\w+)/likes/$',
        api.FeedSourceLikes.as_view(), name='api_feeds_source_likes'),
    url(r'^(?P<pk>\w+)/likes/page(?P<page>\d+)/$',
        api.FeedSourceLikes.as_view(), name='api_feeds_source_likes_page'),
    url(r'^(?P<pk>\w+)/likes/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FeedSourceLikes.as_view(), name='api_feeds_source_likes_page_items'),

    url(r'^(?P<pk>\w+)/dislikes/$',
        api.FeedSourceDislikes.as_view(),
        name='api_feeds_source_dislikes'),
    url(r'^(?P<pk>\w+)/dislikes/page(?P<page>\d+)/$',
        api.FeedSourceDislikes.as_view(), name='api_feeds_source_dislikes_page'),
    url(r'^(?P<pk>\w+)/dislikes/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FeedSourceDislikes.as_view(), name='api_feeds_source_dislikes_page_items'),

    url(r'^(?P<pk>\w+)/shares/$',
        api.FeedSourceShares.as_view(), name='api_feeds_source_shares'),
    url(r'^(?P<pk>\w+)/shares/page(?P<page>\d+)/$',
        api.FeedSourceShares.as_view(), name='api_feeds_source_shares_page'),
    url(r'^(?P<pk>\w+)/shares/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FeedSourceShares.as_view(), name='api_feeds_source_shares_page_items'),

    url(r'^(?P<pk>\w+)/views/$',
        api.FeedSourceViews.as_view(), name='api_feeds_source_views'),
    url(r'^(?P<pk>\w+)/views/page(?P<page>\d+)/$',
        api.FeedSourceViews.as_view(), name='api_feeds_source_views_page'),
    url(r'^(?P<pk>\w+)/views/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FeedSourceViews.as_view(), name='api_feeds_source_views_page_items'),

    url(r'^(?P<pk>\w+)/comments/$',
        api.FeedSourceComments.as_view(), name='api_feeds_source_comments'),
    url(r'^(?P<pk>\w+)/comments/page(?P<page>\d+)/$',
        api.FeedSourceComments.as_view(), name='api_feeds_source_comments_page'),
    url(r'^(?P<pk>\w+)/comments/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FeedSourceComments.as_view(), name='api_feeds_source_comments_page_items'),

    url(r'^(?P<pk>\w+)/tags/$',
        api.FeedSourceTags.as_view(), name='api_feeds_source_tags'),
    url(r'^(?P<pk>\w+)/tags/page(?P<page>\d+)/$',
        api.FeedSourceTags.as_view(), name='api_feeds_source_tags_page'),
    url(r'^(?P<pk>\w+)/tags/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FeedSourceTags.as_view(), name='api_feeds_source_tags_page_items')
]

urlpatterns = patterns(
    '',
    url(r'^(?i)api/categories/', include(categories_api)),

    url(r'^(?i)api/category/', include(feeds_category_api)),
    url(r'^(?i)api/source/', include(feeds_source_api)),
)


