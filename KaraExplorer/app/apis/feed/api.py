from django.db.models import Q
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from itertools import chain
from rest_framework.response import Response
from rest_framework.views import APIView
from app.apis.common.api import (update_status_statistic_for_list, )
from app.apis.serializers import *
from app.decorators import token_required
from app.errors import *
from app.utils import *


def create_or_update_category(new):
    name = new.get('name', '')
    slug = new.get('slug', slugify(name))
    parent = new.get('parent', None)
    parent_id = new.get('parent_id', None)
    is_deleted = new.get('is_deleted', False)
    is_root = new.get('is_root', False)
    new_name = new.get('new_name', '')

    if type(is_deleted) is str:
        if is_deleted.lower() == 'true':
            is_deleted = True
        else:
            is_deleted = False
    if type(is_root) is str:
        if is_root.lower() == 'true':
            is_root = True
        else:
            is_root = False
    if name is not None and name == '':
        name = None
    if new_name is not None and new_name == '':
        new_name = None
    if parent is not None and parent == '':
        parent = None
    if parent_id is not None and (parent_id == '' or parent_id.lower() == 'none'):
        parent_id = None
    # print('CATEGORY: ','name',name,'slug',slug,'parent',parent,'parent_id',parent_id,'is_deleted',is_deleted,'is_root',is_root,'new_name',new_name)

    if name:
        old_category = Category.objects.filter(Q(name=name))

        parent_category = None
        if parent:
            try:
                parent_category = Category.objects.get(name=parent)
            except ObjectDoesNotExist:
                # print("Either the category doesn't exist.")
                pass
        elif parent_id:
            try:
                pk = hasher.decodeNumber(parent_id)
                parent_category = Category.objects.get(pk=pk)
            except ObjectDoesNotExist:
                # print("Either the category doesn't exist.")
                pass

        if parent_category:
            parent_category = parent_category.id

        if old_category.exists():
            item = old_category.first()
            if new_name:
                old_category.update(
                    name=new_name,
                    slug=slugify(new_name),
                    is_deleted=False,
                )
            elif item.slug != slug or item.is_deleted:
                old_category.update(
                    name=name,
                    slug=slug,
                    is_deleted=False,
                )

            if is_root:
                old_category.update(
                    parent=None
                )
            elif parent_category:
                old_category.update(
                    parent_id=parent_category
                )

            if is_deleted:
                old_category.update(
                    is_deleted=is_deleted
                )
            return item.id
        else:
            category = Category(name=name, slug=slug)
            try:
                Category.objects.get(slug=slug)
            except ObjectDoesNotExist:
                category.slug = slugify(name)

            if parent_category:
                category.parent_id = parent_category

            category.save()

            return category.id


def get_category_info(pk):
    if pk is None or pk.lower() == 'none':
        info = {
            "id": 'None', "name": "All", "slug": None, "parent": None, "parents": []
        }
        return info

    try:
        pk = hasher.decodeNumber(pk)
    except ValueError:
        raise Http404

    category = Category.objects.filter(Q(is_deleted=False), Q(id=pk))

    category = category.extra(
        select={
            'parents': 'WITH RECURSIVE categories AS ('
                       '    SELECT root.id, root.name, root.slug, root.parent_id FROM {0} root WHERE root.id = {0}.id '
                       '    UNION ALL '
                       '    SELECT parent.id, parent.name, parent.slug, parent.parent_id '
                       '    FROM {0} parent INNER JOIN categories ON categories.parent_id = parent.id'
                       ') SELECT {1} FROM categories'
            .format(category_db, concatenate_rows(concatenate_fields(':', '{0}', '{1}', '{2}')
                                                 .format('categories.id', 'categories.slug', 'categories.name')))
        }
    )

    try:
        category_detail = category.first()
    except:
        raise Http404

    if category_detail.parents is not None and category_detail.parents != '':
        category_detail.parents = category_detail.parents.split(',')
        for idx, parent in enumerate(category_detail.parents):
            category_attrs = parent.split(':')
            if len(category_attrs) == 3:
                category_detail.parents[idx] = {
                    'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                    'slug': category_attrs[1],
                    'name': category_attrs[2]
                }
            else:
                category_detail.parents[idx] = None
        category_detail.parents = [f for f in category_detail.parents if f != None]

    serializer = CategorySerializer(
        category_detail, many=False,
        fields=('id', 'name', 'slug', 'parent', 'parents'))
    return serializer.data


class StructureCategories(APIView):
    def buildStructure(self, _parent, items_list, num_post):
        if _parent == -1:
            items = [entry for entry in items_list if (entry.parent_id is None)]
        else:
            items = [entry for entry in items_list if (entry.parent_id == _parent)]
        result = []
        num_post = num_post
        for item in items:
            children, total_post = self.buildStructure(item.id, items_list, item.num_post)
            new_model = CategoryModel(item.hashed_id, item.name, item.slug, item.hashed_parent, children,
                                      item.feeds_list, total_post)
            result.append(new_model)
            num_post = num_post + total_post

        return result, num_post

    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None):
        # insert
        categories_new = request.data.get('categories', None)
        if categories_new is not None:
            for item in categories_new:
                create_or_update_category(item)

        new_category = request.data.get('category', None)
        if new_category is not None:
            create_or_update_category(new_category)

        # get
        category = Category.objects.filter(Q(is_deleted=False))
        category = category.extra(
            select={
                'feeds_list': 'SELECT {0} FROM ( '
                              '    SELECT {2}.id, {2}.name, {2}.icon, count({6}.id) as num_post '
                              '    FROM {1} LEFT JOIN {2} ON {1}.feedsource_id = {2}.id '
                              '    LEFT JOIN {4} ON {4}.feed_source_id = {2}.id '
                              '    LEFT JOIN {5} ON {4}.id = {5}.source_id '
                              '    LEFT JOIN {6} ON {6}.post_id = {5}.id '
                              '    WHERE {1}.category_id = {3}.id AND {2}.is_deleted = false '
                              '    GROUP BY {2}.id, {2}.name '
                              ') as feedsource'
                .format(concatenate_rows(
                        concatenate_fields('|', '{0}', '{1}', '{2}', '{3}')
                        .format('feedsource.id', 'feedsource.name', 'feedsource.icon', 'feedsource.num_post'), ';'),
                        source_category_db, source_db, category_db, bookmark_db, post_db, post_category_db),
                'num_post': 'SELECT COUNT(*) FROM {0} WHERE {0}.category_id = {1}.id'
                .format(post_category_db, category_db)
            }
        )

        category_list = list(category)

        for cat in category_list:
            if cat.feeds_list is not None:
                cat.feeds_list = cat.feeds_list.split(';')
                for idx, feed_group in enumerate(cat.feeds_list):
                    feed_attr = feed_group.split('|')
                    if len(feed_attr) == 4:
                        cat.feeds_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_attr[0]),
                            'name': feed_attr[1],
                            'icon': feed_attr[2],
                            'num_post': feed_attr[3]
                        }
                    else:
                        cat.feeds_list[idx] = None
                cat.feeds_list = [f for f in cat.feeds_list if f != None]

        category_tree, num_post = self.buildStructure(-1, category_list, 0)

        serializer = CategoryModelSerializer(
            category_tree,
            many=True,
            fields=('id', 'name', 'slug', 'parent', 'children', 'feeds_src', 'num_post')
        )

        return Response({
            'error': 'SUCCESS',
            'structure_categories': serializer.data,
            'num_post': num_post
        })


class Categories(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None):
        # insert
        categories_new = request.data.get('categories', None)
        if categories_new is not None:
            for item in categories_new:
                create_or_update_category(item)

        new_category = request.data.get('category', None)
        if new_category is not None:
            create_or_update_category(new_category)

        # get
        category = Category.objects.filter(Q(is_deleted=False), Q(parent_id=None))

        serializer = CategorySerializer(category, many=True)

        return Response({
            'error': 'SUCCESS',
            'main_categories': serializer.data
        })


class CategoryDetail(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, pk=None):
        category_info = get_category_info(pk)
        return Response({
            'error': 'SUCCESS',
            'info': category_info
        })


class CategoryPosts(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, pk=None, src=None, page=1, items=10):
        if src is not None and (src == '' or src.lower() == 'none'):
            src = None
        if pk is not None and (pk == '' or pk.lower() == 'none'):
            pk = None

        if pk:
            try:
                pk = hasher.decodeNumber(pk)
            except ValueError:
                raise Http404

            queryset = Post.objects.filter(Q(categories__id=pk))
        else:
            queryset = Post.objects.all()

        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.poster_id'
                .format(user_db, post_db),
                'source_bookmark': 'SELECT {2} FROM {0} WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, concatenate_fields('|', '{0}', '{1}')
                        .format('{0}.id'.format(bookmark_db), '{0}.url'.format(bookmark_db))),
                'source_feed': 'SELECT {3} FROM {0} LEFT OUTER JOIN {2} ON {0}.feed_source_id = {2}.id '
                               'WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, source_db, concatenate_fields('|', '{0}', '{1}', '{2}', '{3}')
                        .format('{0}.id'.format(source_db), '{0}.name'.format(source_db), '{0}.url'.format(source_db),
                                '{0}.icon'.format(source_db))),
                'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                                   'WHERE {0}.post_id = {1}.id'
                .format(post_category_db, post_db, category_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(category_db), '{0}.name'.format(category_db)))),
                'interests_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.interest_id = {2}.id '
                                  'WHERE {0}.post_id = {1}.id'
                .format(post_interest_db, post_db, interest_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(interest_db), '{0}.name'.format(interest_db)))),
            }
        )

        queryset = queryset.exclude(categories=None)

        if src:
            try:
                src = hasher.decodeNumber(src)
                queryset = queryset.filter(Q(source__feed_source__id=src))
            except ValueError:
                pass

        itemsList, paginator = get_paginator(queryset, items, page)

        itemsList.object_list = list(itemsList.object_list)
        for entry in itemsList.object_list:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f != None]

            if entry.interests_list is not None:
                entry.interests_list = entry.interests_list.split(',')
                for idx, group in enumerate(entry.interests_list):
                    attrs = group.split(':')
                    if len(attrs) == 2:
                        entry.interests_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_INTEREST, attrs[0]),
                            'name': attrs[1]
                        }
                    else:
                        entry.interests_list[idx] = None
                entry.interests_list = [f for f in entry.interests_list if f != None]

            if entry.thumbnails is not None and entry.thumbnails != '':
                entry.thumbnails = entry.thumbnails.split(',')
                for idx, thumb_id in enumerate(entry.thumbnails):
                    entry.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id) if thumb_id else None
            else:
                entry.thumbnails = None

            if entry.source_bookmark is not None and entry.source_bookmark != '':
                bookmark_infos = entry.source_bookmark.split('|')
                if len(bookmark_infos) == 2:
                    entry.source_bookmark = {
                        'id': hasher.encodeNumber(hasher.HASH_BOOKMARK, bookmark_infos[0]),
                        'url': bookmark_infos[1]
                    }
                else:
                    entry.source_bookmark = None
            else:
                entry.source_bookmark = None

            if entry.source_feed is not None and entry.source_feed != '':
                feed_infos = entry.source_feed.split('|')
                if len(feed_infos) == 4:
                    entry.source_feed = {
                        'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_infos[0]),
                        'name': feed_infos[1],
                        'url': feed_infos[2],
                        'icon': hasher.encodeNumber(hasher.HASH_PHOTO, feed_infos[3]),
                    }
                else:
                    entry.source_feed = None
            else:
                entry.source_feed = None

        serializer = PostModelSerializer(itemsList.object_list, many=True)
        if request.user.is_authenticated():
            user_id = request.user.id
            update_status_statistic_for_list(user_id, 'post', serializer.data)

        return Response({
            'error': 'SUCCESS',
            'links': serializer.data,
            'paginator': {
                'unit': str(paginator.count) + ' link(s)',
                'totalItems': paginator.count,
                'itemsPerPage': int(items),
                'numPages': paginator.num_pages,
                'currentPage': int(page)
            }
        })


class CategorySources(APIView):
    """
    Manipulate feed sources
    """

    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            feedSources = FeedSource.objects.all()
        else:
            try:
                pk = hasher.decodeNumber(pk)
            except ValueError:
                raise Http404

            feedSources = FeedSource.objects.filter(categories__id__exact=pk)

        if request.user.is_authenticated():
            user_id = request.user.id
            feedSources = feedSources.filter(~Q(feedsourceofuser__is_deleted=False, feedsourceofuser__owner_id=user_id))

        feedSources = feedSources.extra(
            select={
                'categories_list': 'SELECT {3}  FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                                   'WHERE {0}.feedsource_id = {1}.id'
                .format(source_category_db, source_db, category_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(category_db), '{0}.name'.format(category_db))))
            }
        )

        itemsList, paginator = get_paginator(feedSources, items, page)

        itemsList.object_list = list(itemsList.object_list)
        for entry in itemsList.object_list:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f != None]

        serializer = FeedSourceModelSerializer(
            itemsList.object_list, many=True,
            fields=('id', 'name', 'thumbnail', 'cover', 'description', 'icon', 'categories', 'url'))
        if request.user.is_authenticated():
            user_id = request.user.id
            update_status_statistic_for_list(user_id, 'feedsource', serializer.data)

        return Response({
            'error': 'SUCCESS',
            'sources': serializer.data,
            'paginator': {
                'unit': str(paginator.count) + ' source(s)',
                'totalItems': paginator.count,
                'itemsPerPage': int(items),
                'numPages': paginator.num_pages,
                'currentPage': int(page)
            }
        })


class FeedSourceDetail(APIView):
    """
    Manipulate specific feed source
    """

    def get_source_info(self, pk):
        if pk is None or pk.lower() == 'none':
            info = {"id": 'None', "name": "All", "slug": None, "parent": None, "parents": []}
            return info

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        sources = FeedSource.objects.filter(Q(is_deleted=False), Q(id=pk))
        sources = sources.extra(
            select={
                'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                                   'WHERE {0}.feedsource_id = {1}.id'
                .format(source_category_db, source_db, category_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(category_db), '{0}.name'.format(category_db))))
            }
        )

        try:
            sources_detail = sources.first()
        except:
            raise Http404

        if sources_detail.categories_list is not None:
            sources_detail.categories_list = sources_detail.categories_list.split(',')
            for idx, source_group in enumerate(sources_detail.categories_list):
                source_attrs = source_group.split(':')
                if len(source_attrs) == 2:
                    sources_detail.categories_list[idx] = {
                        'id': hasher.encodeNumber(hasher.HASH_CATEGORY, source_attrs[0]),
                        'name': source_attrs[1]
                    }
                else:
                    sources_detail.categories_list[idx] = None
            sources_detail.categories_list = [f for f in sources_detail.categories_list if f != None]

        serializer = FeedSourceModelSerializer(
            sources_detail,
            fields=('id', 'name', 'url', 'icon', 'thumbnail', 'cover', 'description', 'categories'))
        return serializer.data

    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, pk=None):
        source_info = self.get_source_info(pk)
        if request.user.is_authenticated():
            user_id = request.user.id
            update_status_statistic_for_list(user_id, 'feedsource', [source_info])
        return Response({
            'error': 'SUCCESS',
            'info': source_info
        })


class FeedSourceContents(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': 'PARAMS',
                'message': "Feeds-source is None!"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        posts = Post.objects.filter(Q(source__feed_source__id=pk))
        posts = posts.order_by('-published_on', 'title')

        posts = posts.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.poster_id'
                .format(user_db, post_db),
                'source_bookmark': 'SELECT {2} FROM {0} WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, concatenate_fields('|', '{0}', '{1}')
                        .format('{0}.id'.format(bookmark_db), '{0}.url'.format(bookmark_db))),
                'source_feed': 'SELECT {3} FROM {0} LEFT OUTER JOIN {2} ON {0}.feed_source_id = {2}.id '
                               'WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, source_db, concatenate_fields('|', '{0}', '{1}', '{2}', '{3}')
                        .format('{0}.id'.format(source_db), '{0}.name'.format(source_db), '{0}.url'.format(source_db),
                                '{0}.icon'.format(source_db))),
                'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                                   'WHERE {0}.post_id = {1}.id'
                .format(post_category_db, post_db, category_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(category_db), '{0}.name'.format(category_db)))),
                'interests_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.interest_id = {2}.id '
                                  'WHERE {0}.post_id = {1}.id'
                .format(post_interest_db, post_db, interest_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(interest_db), '{0}.name'.format(interest_db))))
            }
        )

        if request.user.is_authenticated():
            user_id = request.user.id
            user_actions = Action.objects.filter(Q(is_trash=False), Q(is_deleted=False), Q(person_id=user_id))
            user_views_post = user_actions.filter(Q(action=ACTION_VIEW), Q(content_type_id=content_type_of('post')))
            posts_viewed_list = user_views_post.values_list('object_id', flat=True)
            posts_viewed = posts.filter(Q(id__in=posts_viewed_list))
            posts_unread = posts.exclude(Q(id__in=posts_viewed_list))
            posts = list(chain(posts_unread, posts_viewed))

        itemsList, paginator = get_paginator(posts, items, page)

        itemsList.object_list = list(itemsList.object_list)
        for entry in itemsList.object_list:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f != None]

            if entry.interests_list is not None:
                entry.interests_list = entry.interests_list.split(',')
                for idx, group in enumerate(entry.interests_list):
                    attrs = group.split(':')
                    if len(attrs) == 2:
                        entry.interests_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_INTEREST, attrs[0]),
                            'name': attrs[1]
                        }
                    else:
                        entry.interests_list[idx] = None
                entry.interests_list = [f for f in entry.interests_list if f != None]

            if entry.thumbnails is not None and entry.thumbnails != '':
                entry.thumbnails = entry.thumbnails.split(',')
                for idx, thumb_id in enumerate(entry.thumbnails):
                    entry.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id) if thumb_id else None
            else:
                entry.thumbnails = None

            if entry.source_bookmark is not None and entry.source_bookmark != '':
                bookmark_infos = entry.source_bookmark.split('|')
                if len(bookmark_infos) == 2:
                    entry.source_bookmark = {
                        'id': hasher.encodeNumber(hasher.HASH_BOOKMARK, bookmark_infos[0]),
                        'url': bookmark_infos[1]
                    }
                else:
                    entry.source_bookmark = None
            else:
                entry.source_bookmark = None

            if entry.source_feed is not None and entry.source_feed != '':
                feed_infos = entry.source_feed.split('|')
                if len(feed_infos) == 4:
                    entry.source_feed = {
                        'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_infos[0]),
                        'name': feed_infos[1],
                        'url': feed_infos[2],
                        'icon': hasher.encodeNumber(hasher.HASH_PHOTO, feed_infos[3])
                    }
                else:
                    entry.source_feed = None
            else:
                entry.source_feed = None

        serializer = PostModelSerializer(itemsList.object_list, many=True)
        if request.user.is_authenticated():
            user_id = request.user.id
            update_status_statistic_for_list(user_id, 'post', serializer.data)

        return Response({
            'error': 'SUCCESS',
            'links': serializer.data,
            'paginator': {
                'unit': str(paginator.count) + ' link(s)',
                'totalItems': paginator.count,
                'itemsPerPage': int(items),
                'numPages': paginator.num_pages,
                'currentPage': int(page)
            }
        })


class FeedSourceLikes(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        content_type = content_type_of('feedsource')
        queryset = Action.objects.filter(Q(content_type_id=content_type.id), Q(object_id=pk))
        queryset = queryset.filter(Q(action=ACTION_LIKE))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, action_db)})
        queryset = queryset.values('updated_on', 'username')

        itemsList, paginator = get_paginator(queryset, items, page)

        serializer = ActionModelSerializer(itemsList, many=True)

        return Response(serializer.data)


class FeedSourceDislikes(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        content_type = content_type_of('feedsource')
        queryset = Action.objects.filter(Q(content_type_id=content_type.id), Q(object_id=pk))
        queryset = queryset.filter(Q(action=ACTION_DISLIKE))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, action_db)})
        queryset = queryset.values('updated_on', 'username')

        itemsList, paginator = get_paginator(queryset, items, page)

        serializer = ActionModelSerializer(itemsList, many=True)

        return Response(serializer.data)


class FeedSourceShares(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        content_type = content_type_of('feedsource')
        queryset = Action.objects.filter(Q(content_type_id=content_type.id), Q(object_id=pk))
        queryset = queryset.filter(Q(action=ACTION_SHARE))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, action_db)})
        queryset = queryset.values('updated_on', 'username')

        itemsList, paginator = get_paginator(queryset, items, page)

        serializer = ActionModelSerializer(itemsList, many=True)

        return Response(serializer.data)


class FeedSourceViews(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        content_type = content_type_of('feedsource')
        queryset = Action.objects.filter(Q(content_type_id=content_type.id), Q(object_id=pk))
        queryset = queryset.filter(Q(action=ACTION_VIEW))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, action_db)})
        queryset = queryset.values('updated_on', 'username')

        itemsList, paginator = get_paginator(queryset, items, page)

        serializer = ActionModelSerializer(itemsList, many=True)

        return Response(serializer.data)


class FeedSourceComments(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        content_type = content_type_of('feedsource')
        queryset = Comment.objects.filter(Q(content_type_id=content_type.id), Q(object_id=pk))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, comment_db)})
        queryset = queryset.values('title', 'content', 'updated_on', 'username')

        itemsList, paginator = get_paginator(queryset, items, page)

        serializer = CommentModelSerializer(itemsList, many=True)

        return Response(serializer.data)


class FeedSourceTags(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        content_type = content_type_of('feedsource')
        queryset = Tag.objects.filter(Q(content_type_id=content_type.id), Q(object_id=pk))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, tag_db)})
        queryset = queryset.values('tag', 'slug', 'is_user_defined', 'updated_on', 'username')

        itemsList, paginator = get_paginator(queryset, items, page)

        serializer = TagModelSerializer(
            itemsList, many=True,
            fields=('tag', 'slug', 'is_user_defined', 'updated_on', 'username'))

        return Response(serializer.data)


class FeedSourceWithKey(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, key=None, page=1, items=10):
        params = request.data

        sources = FeedSource.objects.filter(Q(is_deleted=False), Q(is_trash=False))
        if key:
            sources = FeedSource.objects.filter(Q(name__icontains=key) | Q(description__icontains=key))

        sources = sources.extra(
            select={
                'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                                   'WHERE {0}.feedsource_id = {1}.id'
                .format(source_category_db, source_db, category_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(category_db), '{0}.name'.format(category_db))))
            }
        )

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            sources, paginator = get_paginator(sources, items, page)

            sources = list(sources.object_list)
            for entry in sources:
                if entry.categories_list is not None:
                    entry.categories_list = entry.categories_list.split(',')
                    for idx, category_group in enumerate(entry.categories_list):
                        category_attrs = category_group.split(':')
                        if len(category_attrs) == 2:
                            entry.categories_list[idx] = {
                                'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                                'name': category_attrs[1]
                            }
                        else:
                            entry.categories_list[idx] = None
                    entry.categories_list = [f for f in entry.categories_list if f != None]

            serializer = FeedSourceModelSerializer(
                sources, many=True,
                fields=('id', 'name', 'thumbnail', 'cover', 'description', 'icon', 'categories'))
            if request.user.is_authenticated():
                user_id = request.user.id
                update_status_statistic_for_list(user_id, 'feedsource', serializer.data)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'sources': serializer.data,
                'paginator': {
                    'unit': str(paginator.count) + ' source(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                }
            })
        else:
            sources = list(sources)
            for entry in sources:
                if entry.categories_list is not None:
                    entry.categories_list = entry.categories_list.split(',')
                    for idx, category_group in enumerate(entry.categories_list):
                        category_attrs = category_group.split(':')
                        if len(category_attrs) == 2:
                            entry.categories_list[idx] = {
                                'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                                'name': category_attrs[1]
                            }
                        else:
                            entry.categories_list[idx] = None
                    entry.categories_list = [f for f in entry.categories_list if f != None]

            serializer = FeedSourceModelSerializer(
                sources, many=True,
                fields=('id', 'name', 'thumbnail', 'cover', 'description', 'icon', 'categories'))
            if request.user.is_authenticated():
                user_id = request.user.id
                update_status_statistic_for_list(user_id, 'feedsource', serializer.data)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'sources': serializer.data
            })


