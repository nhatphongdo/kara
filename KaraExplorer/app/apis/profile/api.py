import mandrill
import uuid
from datetime import timedelta
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import (check_password, make_password, is_password_usable, )
from django.db.models import Q
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from itertools import chain
from rest_framework.response import Response
from rest_framework.views import APIView
from app.apis.common.api import (update_status_statistic_for_list, )
from app.apis.serializers import *
from app.decorators import token_required
from app.errors import *
from app.utils import *


# AuthServices
class AuthView(APIView):
    """
    POST    login
    DELETE  logout
    PUT     verify
    """

    @csrf_exempt
    def post(self, request):
        params = request.data
        username = request.data.get('username', None)
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        is_remembered = request.data.get('remember', False)

        if not (username or email):
            return Response({
                'error': AUTHENTICATION_ERROR_USERNAME,
                'params': params,
                'message': 'Missing username or email'
            })

        if password is None:
            return Response({
                'error': AUTHENTICATION_ERROR_PASSWORD,
                'params': params,
                'message': 'Missing password'
            })

        if email:
            try:
                user = User.objects.get(email=email)
            except ObjectDoesNotExist:
                return Response({
                    'error': AUTHENTICATION_ERROR_WRONG,
                    'params': params,
                    'message': 'Email availability'
                })

            if username:
                if username != user.username:
                    return Response({
                        'error': AUTHENTICATION_ERROR_WRONG,
                        'params': params,
                        'message': 'Wrong authentication'
                    })
            username = user.username

        user = authenticate(username=username, password=password)

        if user is not None:
            # the password verified for the user
            if not user.is_active:
                return Response({
                    'error': AUTHENTICATION_ERROR_SUSPENDED,
                    'params': params,
                    'message': 'User is suspended'
                })
        else:
            # the authentication system was unable to verify the username and password
            return Response({
                'error': AUTHENTICATION_ERROR_WRONG,
                'message': 'Wrong authentication'
            })

        login(request, user)

        # create new session for this user
        old_session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id))
        old_session = old_session.update(is_deleted=True)
        session = Session(
            owner_id=user.id,
            session_key=uuid.uuid4()
        )
        if is_remembered:
            session.expire_time = timezone.now() + timedelta(minutes=settings.LONG_PERIOD_SESSION)
        else:
            session.expire_time = timezone.now() + timedelta(minutes=settings.SHORT_PERIOD_SESSION)
        session.save()

        return Response({
            'error': AUTHENTICATION_SUCCESS,
            'params': params,
            'session': session.session_key,
            'expire_time': session.expire_time,
            'user': UserSerializer(user).data
        })

    @csrf_exempt
    @token_required()
    def put(self, request, session=None):
        return Response({
            'error': AUTHENTICATION_SUCCESS,
            'session': session.session_key,
            'expire_time': session.expire_time,
            'user': UserSerializer(request.user).data
        })

    @csrf_exempt
    @token_required()
    def delete(self, request, session=None):
        request.session.delete()
        logout(request)
        if session is not None:
            session.delete()

        return Response()


class Register(APIView):
    """
    POST    new user
    """

    @csrf_exempt
    def post(self, request):
        params = request.data
        email = request.data.get('email', None)
        password = request.data.get('password', None)

        if not email:
            return Response({
                'error': AUTHENTICATION_ERROR_USERNAME,
                'params': params,
                'message': 'Missing email'
            })
        else:
            try:
                user = User.objects.get(email=email)
                return Response({
                    'error': AUTHENTICATION_ERROR_WRONG,
                    'params': params,
                    'message': 'Please login!'
                })
            except ObjectDoesNotExist:
                pass

        if password is None:
            return Response({
                'error': AUTHENTICATION_ERROR_PASSWORD,
                'params': params,
                'message': 'Missing password'
            })

        try:
            user = User.objects.create_user(email, email, password)
            user.is_active = False
            user.save()
            Profile.objects.create(user=user, nick_name=email.split("@")[0])
        except ObjectDoesNotExist:
            return Response({
                'error': AUTHENTICATION_ERROR_WRONG,
                'params': params,
                'message': 'Try again'
            })

        user = authenticate(username=user.username, password=password)

        if user is not None:
            # the password verified for the user
            mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
            template_content = [{'name': 'editable', 'content': 'KARA'}]
            try:
                message = {
                    'to': [{'email': user.email,
                            'name': user.first_name + ' ' + user.last_name,
                            'type': 'to'}],
                    'track_clicks': True,
                    'track_opens': True,
                    'merge_vars': [{'rcpt': user.email,
                                    'vars': [{'name': 'FRIEND', 'content': user.first_name + ' ' + user.last_name}]}],
                    'global_merge_vars': [{'name': 'SESSIONKEY', 'content': 'session_key'},
                                          {'name': 'EMAIL', 'content': 'user.email'},
                                          {'name': 'KARA', 'content': 'www.karaplus.com'}],

                    'metadata': {'website': 'www.karaplus.com'},
                    'recipient_metadata': [{'rcpt': user.email,
                                            'values': {'user_id': user.id}}],
                    'tags': ['welcome']
                }
                result = mandrill_client.messages.send_template(template_name='welcome-mail',
                                                                template_content=template_content,
                                                                message=message,
                                                                async=False)
            except mandrill.Error as e:
                message = 'Try again. A mandrill error occurred: %s - %s' % (e.__class__, e)
            if not user.is_active:
                # create new session for this user
                old_session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id))
                old_session = old_session.update(is_deleted=True)
                session = Session(
                    owner_id=user.id,
                    session_key=uuid.uuid4()
                )

                session.expire_time = timezone.now() + timedelta(minutes=2 * 24 * 60)
                session.save()

                try:
                    message = {
                        'to': [{'email': user.email,
                                'name': user.first_name + ' ' + user.last_name,
                                'type': 'to'}],
                        'track_clicks': True,
                        'track_opens': True,
                        'merge_vars': [{'rcpt': user.email,
                                        'vars': [{'name': 'SESSIONKEY', 'content': str(session.session_key)},
                                                 {'name': 'EMAIL', 'content': user.email},
                                                 {'name': 'KARA', 'content': 'www.karaplus.com'}]}],
                        'global_merge_vars': [{'name': 'SESSIONKEY', 'content': 'session_key'},
                                              {'name': 'EMAIL', 'content': 'user.email'},
                                              {'name': 'KARA', 'content': 'www.karaplus.com'}],

                        'metadata': {'website': 'www.karaplus.com'},
                        'recipient_metadata': [{'rcpt': user.email,
                                                'values': {'user_id': user.id}}],
                        'tags': ['verify', 'sign up']
                    }
                    result = mandrill_client.messages.send_template(template_name='verify-email',
                                                                    template_content=template_content,
                                                                    message=message,
                                                                    async=False)
                except mandrill.Error as e:
                    message = 'Try again. A mandrill error occurred: %s - %s' % (e.__class__, e)
                return Response({
                    'error': AUTHENTICATION_ERROR_SUSPENDED,
                    'params': params,
                    'session': session.session_key,
                    'expire_time': session.expire_time,
                    'message': 'User is suspended'
                })
        else:
            # the authentication system was unable to verify the username and password
            return Response({
                'error': AUTHENTICATION_ERROR_WRONG,
                'message': 'Wrong authentication'
            })

        login(request, user)

        # create new session for this user
        old_session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id))
        old_session = old_session.update(is_deleted=True)
        session = Session(
            owner_id=user.id,
            session_key=uuid.uuid4()
        )

        session.expire_time = timezone.now() + timedelta(minutes=settings.SHORT_PERIOD_SESSION)
        session.save()

        return Response({
            'error': AUTHENTICATION_SUCCESS,
            'params': params,
            'session': session.session_key,
            'expire_time': session.expire_time,
            'user': UserSerializer(user).data
        })


class RegisterVerify(APIView):
    @csrf_exempt
    def post(self, request, session_key=None, email=None):
        static_params = {'session_key': session_key, 'email': email}
        params = request.data

        try:
            user = User.objects.get(email=email)
            session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id), Q(session_key=session_key))
            session = session.first()
            if session and session.expire_time > timezone.now():
                user.is_active = True
                user.save()
                session.is_deleted = True
                session.save()
        except ObjectDoesNotExist:
            return Response({
                'error': AUTHENTICATION_ERROR_WRONG,
                'params': params,
                'message': 'Try again'
            })

        if not user.is_active:
            try:
                # create new session for this user
                old_session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id))
                old_session = old_session.update(is_deleted=True)
                session = Session(
                    owner_id=user.id,
                    session_key=uuid.uuid4()
                )

                session.expire_time = timezone.now() + timedelta(minutes=2 * 24 * 60)
                session.save()
                mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
                template_content = [{'name': 'editable', 'content': 'KARA'}]
                message = {
                    'to': [{'email': user.email,
                            'name': user.first_name + ' ' + user.last_name,
                            'type': 'to'}],
                    'track_clicks': True,
                    'track_opens': True,
                    'merge_vars': [{'rcpt': user.email,
                                    'vars': [{'name': 'SESSIONKEY', 'content': str(session.session_key)},
                                             {'name': 'EMAIL', 'content': user.email},
                                             {'name': 'KARA', 'content': 'www.karaplus.com'}]}],
                    'global_merge_vars': [{'name': 'SESSIONKEY', 'content': 'session_key'},
                                          {'name': 'EMAIL', 'content': 'user.email'},
                                          {'name': 'KARA', 'content': 'www.karaplus.com'}],

                    'metadata': {'website': 'www.karaplus.com'},
                    'recipient_metadata': [{'rcpt': user.email,
                                            'values': {'user_id': user.id}}],
                    'tags': ['again', 'verify', 'sign up']
                }
                result = mandrill_client.messages.send_template(template_name='verify-email',
                                                                template_content=template_content,
                                                                message=message,
                                                                async=False)
            except mandrill.Error as e:
                message = 'Try again. A mandrill error occurred: %s - %s' % (e.__class__, e)
            return Response({
                'error': AUTHENTICATION_ERROR_SUSPENDED,
                'params': params,
                'session': session.session_key,
                'expire_time': session.expire_time,
                'message': 'User is suspended'
            })

        # create new session for this user
        old_session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id))
        old_session = old_session.update(is_deleted=True)
        session = Session(
            owner_id=user.id,
            session_key=uuid.uuid4()
        )

        session.expire_time = timezone.now() + timedelta(minutes=settings.SHORT_PERIOD_SESSION)
        session.save()

        return Response({
            'error': AUTHENTICATION_SUCCESS,
            'params': params,
            'session': session.session_key,
            'expire_time': session.expire_time,
            'user': UserSerializer(user).data
        })


class UserView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        params = request.data
        user_id = request.user.id

        if request.user.username != username:
            try:
                user = User.objects.get(username__iexact=username)
            except ObjectDoesNotExist:
                try:
                    pk = hasher.decodeNumber(username)
                except ValueError:
                    raise Http404('No user matches the given query.')
                if request.user.id != pk:
                    try:
                        user = User.objects.get(id=pk)
                    except ObjectDoesNotExist:
                        raise Http404('No user matches the given query.')
                else:
                    user = request.user
        else:
            user = request.user

        return Response({
            'error': AUTHENTICATION_SUCCESS,
            'session': session.session_key,
            'expire_time': session.expire_time,
            'params': params,
            'user': UserSerializer(user).data
        })


#
class UserInterestsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            interests = get_object_or_404(User, username__iexact=username).profile.interests
        else:
            interests = request.user.profile.interests

        interests = interests.filter(Q(is_deleted=False), Q(interestsofuser__is_deleted=False), ~Q(parent_id=None))
        interests = interests.order_by('parent__name', 'name')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            interests, paginator = get_paginator(interests, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' friends',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'interests': UserInterestSerializer(interests, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'interests': UserInterestSerializer(interests, many=True).data
            })


class UserMainInterestsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            interests = get_object_or_404(User, username__iexact=username).profile.interests
        else:
            interests = request.user.profile.interests

        interests = interests.filter(Q(interestsofuser__is_trash=False), Q(is_deleted=False), ~Q(parent_id=None))
        interests = interests.order_by('parent__name', 'name')

        user_main_interest = set(interests.values_list('parent', flat=True))
        user_main_interest = Interest.objects.filter(Q(id__in=user_main_interest))

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'user_main_interest': UserInterestSerializer(user_main_interest, many=True).data
        })


class UserChildInterestsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, name=None, items=10, page=1, trash=False):
        params = request.data

        user_id = request.user.id

        trash = True if trash else False
        if request.user.username != username:
            interests = get_object_or_404(User, username__iexact=username).profile.interests
        else:
            interests = request.user.profile.interests

        interests = interests.filter(Q(interestsofuser__is_deleted=False), Q(interestsofuser__is_trash=trash))
        interests = interests.filter(Q(is_deleted=False), ~Q(parent_id=None))
        interests = interests.order_by('parent__name', 'name')

        if name:
            interests = interests.filter(Q(parent__name=name))
        else:
            try:
                interest_id = hasher.decodeNumber(pk) if pk else None
            except ValueError:
                interest_id = None
            if interest_id:
                interests = interests.filter(Q(parent_id=interest_id))

        is_trash = request.data.get('is_trash', None)
        is_trash = (is_trash.lower() == 'true') if type(is_trash) is str else is_trash
        if is_trash:
            try:
                old = InterestsOfUser.objects.filter(Q(owner_id=user_id))
                old = old.filter(Q(interest_id__in=interests.values_list('id', flat=True)))
                old = old.update(is_trash=True)
                return Response({
                    'error': 'SUCCESS',
                    'params': params,
                    'message': 'moved to trash'
                })
            except ObjectDoesNotExist:
                return Response({
                    'error': 'FAILED',
                    'params': params,
                    'message': 'try again'
                })

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            interests, paginator = get_paginator(interests, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' friends',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'interests': UserInterestSerializer(interests, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'interests': UserInterestSerializer(interests, many=True).data
            })


class UserFriendsGroupsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None):
        params = request.data

        if request.user.username != username:
            friends_groups = get_object_or_404(User, username__iexact=username).profile.friendsgroup_set
        else:
            friends_groups = request.user.profile.friendsgroup_set

        friends_groups = friends_groups.filter(Q(is_deleted=False), Q(status=GROUP_FRIEND))
        friends_groups = friends_groups.order_by('-updated_on')

        if pk:
            try:
                group_id = hasher.decodeNumber(pk)
                friends_group = friends_groups.get(pk=group_id)
                return Response({
                    'error': 'SUCCESS',
                    'params': params,
                    'friends_group': UserFriendsGroupsSerializer(friends_group).data
                })
            except ValueError:
                pass

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'friends_groups': UserFriendsGroupsSerializer(friends_groups, many=True).data
        })


class UserFriendsInGroupView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        try:
            group_id = hasher.decodeNumber(pk)
        except ValueError:
            group_id = None

        friends_in_groups = request.user.profile.get_friends_in_group(group_id)
        friends_in_groups = friends_in_groups.filter(Q(is_deleted=False))
        friends_in_groups = friends_in_groups.order_by('-updated_on')
        return Response({
            'error': 'SUCCESS',
            'params': params,
            'friends_in_group': UserProfileSerializer(friends_in_groups, many=True).data
        })


class UserFollowingGroupsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None):
        params = request.data

        if request.user.username != username:
            following_groups = get_object_or_404(User, username__iexact=username).profile.friendsgroup_set
        else:
            following_groups = request.user.profile.friendsgroup_set

        following_groups = following_groups.filter(Q(is_deleted=False), Q(status=GROUP_FOLLOWING))
        following_groups = following_groups.order_by('-updated_on')

        if pk:
            try:
                group_id = hasher.decodeNumber(pk)
                following_groups = following_groups.get(pk=group_id)
                return Response({
                    'error': 'SUCCESS',
                    'params': params,
                    'following_groups': UserFriendsGroupsSerializer(following_groups).data
                })
            except ValueError:
                pass

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'following_groups': UserFriendsGroupsSerializer(following_groups, many=True).data
        })


class UserFollowingInGroupView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        try:
            group_id = hasher.decodeNumber(pk)
        except ValueError:
            group_id = None

        following_in_group = request.user.profile.get_following_in_group(group_id)
        following_in_group = following_in_group.filter(Q(is_deleted=False))
        following_in_group = following_in_group.order_by('-updated_on')
        return Response({
            'error': 'SUCCESS',
            'params': params,
            'following_in_group': UserProfileSerializer(following_in_group, many=True).data
        })


class UserFriendsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            friends = get_object_or_404(User, username__iexact=username).profile.get_friends()
        else:
            friends = request.user.profile.get_friends()

        friends = friends.filter(Q(is_deleted=False))
        friends = friends.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            friends, paginator = get_paginator(friends, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' friends',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'friends': UserProfileSerializer(friends, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'friends': UserProfileSerializer(friends, many=True).data
            })


class UserFriendRequestsSentView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            friend_requests = get_object_or_404(User, username__iexact=username).profile.get_friend_requests_sent()
        else:
            friend_requests = request.user.profile.get_friend_requests_sent()

        friend_requests = friend_requests.filter(Q(is_deleted=False))
        friend_requests = friend_requests.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            friend_requests, paginator = get_paginator(friend_requests, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' friends',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'friend_requests': UserProfileSerializer(friend_requests, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'friend_requests': UserProfileSerializer(friend_requests, many=True).data
            })


class UserFriendRequestsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            friend_requests = get_object_or_404(User, username__iexact=username).profile.get_friend_requests()
        else:
            friend_requests = request.user.profile.get_friend_requests()

        friend_requests = friend_requests.filter(Q(is_deleted=False))
        friend_requests = friend_requests.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            friend_requests, paginator = get_paginator(friend_requests, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' friends',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'friend_requests': UserProfileSerializer(friend_requests, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'friend_requests': UserProfileSerializer(friend_requests, many=True).data
            })


class UserFollowingView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            following = get_object_or_404(User, username__iexact=username).profile.get_following()
        else:
            following = request.user.profile.get_following()

        following = following.filter(Q(is_deleted=False))
        following = following.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            following, paginator = get_paginator(following, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' friends',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'following': UserProfileSerializer(following, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'following': UserProfileSerializer(following, many=True).data
            })


class UserPublicFoldersView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        folder_type = content_type_of('folder')
        public_permissions = share_permission_id('public')
        if request.user.username != username:
            folders = get_object_or_404(User, username__iexact=username).profile.from_person
        else:
            folders = request.user.profile.from_person

        folders = folders.filter(Q(is_deleted=False), Q(content_type=folder_type), Q(permissions=public_permissions))
        folders = folders.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            folders, paginator = get_paginator(folders, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'folders': UserPublicFolderSerializer(folders, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'folders': UserPublicFolderSerializer(folders, many=True).data
            })


def build_structure_folders(_parent, items_list):
    if _parent == -1:
        items = [entry for entry in items_list if (entry.parent_id is None)]
    else:
        items = [entry for entry in items_list if (entry.parent_id == _parent)]
    result = []
    for item in items:
        children = build_structure_folders(item.id, items_list)
        new_model = FolderModel(item.hashed_id, item.hashed_owner, item.name, item.slug, item.description,
                                item.is_user_defined, 0, item.hashed_parent, children)
        result.append(new_model)

    return result


class UserFoldersView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            folders = get_object_or_404(User, username__iexact=username).profile.folder_set
        else:
            folders = request.user.profile.folder_set

        folders = folders.filter(Q(is_deleted=False), Q(is_user_defined=True))
        folders = folders.order_by('parent_id', 'name')

        folders = build_structure_folders(-1, list(folders))

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'folders': FolderModelSerializer(
                folders, many=True,
                fields=('id', 'name', 'slug', 'description', 'parent', 'children')).data
        })


class UserSourcesView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            subscribes = get_object_or_404(User, username__iexact=username).profile.subscribes
        else:
            subscribes = request.user.profile.subscribes

        subscribes = subscribes.filter(Q(is_deleted=False))
        subscribes = subscribes.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            subscribes, paginator = get_paginator(subscribes, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'subscribes': UserFeedSourceSerializer(subscribes, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'subscribes': UserFeedSourceSerializer(subscribes, many=True).data
            })


class UserSourcesAliasView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            subscribes = get_object_or_404(User, username__iexact=username).profile.feedsourceofuser_set
        else:
            subscribes = request.user.profile.feedsourceofuser_set

        subscribes = subscribes.filter(Q(is_deleted=False), Q(feed_source__is_deleted=False))
        subscribes = subscribes.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            subscribes, paginator = get_paginator(subscribes, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'subscribes': UserFeedSourceAliasSerializer(subscribes, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'subscribes': UserFeedSourceAliasSerializer(subscribes, many=True).data
            })


class UserSourceAliasNameView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, category=None):
        params = request.data

        if request.user.username != username:
            subscribes = get_object_or_404(User, username__iexact=username).profile.feedsourceofuser_set
        else:
            subscribes = request.user.profile.feedsourceofuser_set

        subscribes = subscribes.filter(Q(is_deleted=False), Q(feed_source__is_deleted=False))
        subscribes = subscribes.order_by('-updated_on')

        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            pk = None
        subscribe = subscribes.filter(feed_source__id=pk)

        rename = request.data.get('rename', None)
        if rename:
            FeedSourceOfUser.objects.filter(id__in=subscribe.values_list('id', flat=True)).update(name=rename)

        category_name = request.data.get('category_name', None)
        if category and category_name:
            tag_categ = Tag.objects.filter(Q(content_type=content_type_of('feedsourceofuser')))
            tag_categ = tag_categ.filter(Q(object_id__in=subscribe.values_list('id', flat=True)))
            tag_categ = tag_categ.update(tag=category_name, slug=slugify(category_name))

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'subscribe': UserFeedSourceAliasSerializer(subscribe, many=True).data
        })


class UserBookmarksView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            bookmarks = get_object_or_404(User, username__iexact=username).profile.bookmark_set
        else:
            bookmarks = request.user.profile.bookmark_set

        bookmarks = bookmarks.filter(Q(is_deleted=False))
        bookmarks = bookmarks.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            bookmarks, paginator = get_paginator(bookmarks, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'bookmarks': UserBookmarkSerializer(bookmarks, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'bookmarks': UserBookmarkSerializer(bookmarks, many=True).data
            })


class UserWallPostsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        # user_id
        user_id = request.user.id

        if request.user.username != username:
            wall_posts = get_object_or_404(User, username__iexact=username).profile.owner
        else:
            wall_posts = request.user.profile.owner

        wall_posts = wall_posts.filter(Q(is_deleted=False))
        wall_posts = wall_posts.order_by('-updated_on', '-id')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            wall_posts, paginator = get_paginator(wall_posts, items, page)

            wall_posts = UserWallPostsSerializer(wall_posts, many=True).data
            update_status_statistic_for_list(user_id, 'wallpost', wall_posts)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'wall_posts': wall_posts
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'wall_posts': UserWallPostsSerializer(wall_posts, many=True).data
            })


class UserActionsView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        params = request.data

        if request.user.username != username:
            actions = get_object_or_404(User, username__iexact=username).profile.action_set
        else:
            actions = request.user.profile.action_set

        actions = actions.filter(Q(is_deleted=False))
        actions = actions.order_by('-updated_on', 'id')
        actions = actions.exclude(action__in=[4])

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            actions, paginator = get_paginator(actions, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' actions',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'actions': ActionSerializer(actions, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'actions': ActionSerializer(actions, many=True).data
            })


class UserCategoriesView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, name=None):
        params = request.data

        user_id = request.user.id

        if request.user.username != username:
            tag_set = get_object_or_404(User, username__iexact=username).profile.tag_set
            tag_categories = tag_set.filter(Q(is_deleted=False), Q(content_type_id=content_type_of('feedsourceofuser')))
            tag_categories = tag_categories.order_by('tag', '-updated_on')
        else:
            tag_set = request.user.profile.tag_set
            tag_categories = tag_set.filter(Q(is_deleted=False), Q(content_type_id=content_type_of('feedsourceofuser')))
            tag_categories = tag_categories.order_by('tag', '-updated_on')

            if name:
                if name == 'None':
                    return Response({
                        'error': 'SUCCESS',
                        'params': params,
                        'user_main_category': {
                            "name": "All",
                            "slug": None,
                            "parent": None,
                            "parents": [],
                            "statistic": {},
                            "status": {},
                            "numb": {}
                        }
                    })

                tag_category = tag_categories.filter(Q(slug__iexact=name))

                rename = request.data.get('rename', None)
                if rename:
                    tag_list = tag_category.values_list('id', flat=True)
                    name = slugify(rename)
                    Tag.objects.filter(Q(id__in=tag_list)).update(tag=rename, slug=name)
                    tag_category = tag_categories.filter(Q(slug__iexact=name))
                tag_category = UserTagCategorySerializer(tag_category, many=True, fields=('name', 'slug')).data

                out_put = set_items(tag_category, 'name')
                update_status_statistic_for_list(user_id, 'tagcategory', out_put)
                return Response({
                    'error': 'SUCCESS',
                    'params': params,
                    'user_main_category': out_put[0] if len(out_put) else {}
                })

            tag_categories = UserTagCategorySerializer(tag_categories, many=True, fields=('name', 'slug')).data

        user_main_categories = set_items(tag_categories, 'name')
        update_status_statistic_for_list(user_id, 'tagcategory', user_main_categories)

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'user_main_categories': user_main_categories
        })


class UserSourcesOfCategoryView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, name=None, items=10, page=1):
        params = request.data

        user_id = request.user.id

        if request.user.username != username:
            sources = get_object_or_404(User, username__iexact=username).profile.tag_set
            sources = sources.filter(Q(is_deleted=False), Q(content_type_id=content_type_of('feedsourceofuser')))
            sources = sources.filter(Q(slug__iexact=name))
            sources = sources.values_list('object_id', flat=True)
            subscribes = get_object_or_404(User, username__iexact=username).profile.subscribes
        else:
            sources = request.user.profile.tag_set
            sources = sources.filter(Q(is_deleted=False), Q(content_type_id=content_type_of('feedsourceofuser')))
            sources = sources.filter(Q(slug__iexact=name))
            sources = sources.values_list('object_id', flat=True)
            subscribes = request.user.profile.subscribes

        subscribes = subscribes.filter(Q(is_deleted=False), Q(feedsourceofuser__id__in=sources))
        subscribes = subscribes.order_by('-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            subscribes, paginator = get_paginator(subscribes, items, page)
            subscribes = UserFeedSourceSerializer(subscribes, many=True).data
            update_status_statistic_for_list(user_id, 'feedsource', subscribes)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'subscribes': subscribes
            })
        else:
            subscribes = UserFeedSourceSerializer(subscribes, many=True).data
            update_status_statistic_for_list(user_id, 'feedsource', subscribes)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'subscribes': subscribes
            })


class UserSourcesAliasOfCategoryView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, name=None, items=10, page=1):
        params = request.data

        user_id = request.user.id

        if request.user.username != username:
            sources = get_object_or_404(User, username__iexact=username).profile.tag_set
            sources = sources.filter(Q(is_deleted=False), Q(content_type_id=content_type_of('feedsourceofuser')))
            sources = sources.filter(Q(slug__iexact=name))
            sources = sources.values_list('object_id', flat=True)
            subscribes = get_object_or_404(User, username__iexact=username).profile.feedsourceofuser_set
            subscribes = subscribes.filter(Q(is_deleted=False), Q(feed_source__is_deleted=False), Q(id__in=sources))
            subscribes = subscribes.order_by('name', '-updated_on')
        else:
            sources = request.user.profile.tag_set
            sources = sources.filter(Q(is_deleted=False), Q(content_type_id=content_type_of('feedsourceofuser')))
            sources = sources.filter(Q(slug__iexact=name))
            sources = sources.values_list('object_id', flat=True)
            subscribes = request.user.profile.feedsourceofuser_set
            subscribes = subscribes.filter(Q(is_deleted=False), Q(feed_source__is_deleted=False), Q(id__in=sources))
            subscribes = subscribes.order_by('name', '-updated_on')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            subscribes, paginator = get_paginator(subscribes, items, page)
            subscribes = UserFeedSourceAliasSerializer(subscribes, many=True).data
            for item in subscribes:
                item.update(item['feed_source'])
                item['feed_source'].clear()
            update_status_statistic_for_list(user_id, 'feedsource', subscribes)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'subscribes': subscribes
            })
        else:
            subscribes = UserFeedSourceAliasSerializer(subscribes, many=True).data
            for item in subscribes:
                item.update(item['feed_source'])
                item['feed_source'].clear()
            update_status_statistic_for_list(user_id, 'feedsource', subscribes)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'subscribes': subscribes
            })


class UserPostOfCategoryView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, name=None, items=10, page=1):
        params = request.data

        user_id = request.user.id

        if request.user.username != username:
            sources = get_object_or_404(User, username__iexact=username).profile.tag_set
        else:
            sources = request.user.profile.tag_set

        sources = sources.filter(Q(is_deleted=False), Q(content_type_id=content_type_of('feedsourceofuser')))
        sources = sources.filter(Q(slug__iexact=name)) if name and name != 'None' else sources
        sources = sources.values_list('object_id', flat=True)
        posts = Post.objects.filter(Q(is_deleted=False), Q(source__feed_source__feedsourceofuser__id__in=sources))
        posts = posts.order_by('-published_on', 'title')

        posts = posts.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.poster_id'.format(user_db,post_db),
                'source_bookmark': 'SELECT {2} FROM {0} WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, concatenate_fields('|', '{0}', '{1}')
                        .format('{0}.id'.format(bookmark_db), '{0}.url'.format(bookmark_db))),
                'source_feed': 'SELECT {3} FROM {0} LEFT OUTER JOIN {2} ON {0}.feed_source_id = {2}.id '
                               'WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, source_db, concatenate_fields('|', '{0}', '{1}', '{2}', '{3}')
                        .format('{0}.id'.format(source_db), '{0}.name'.format(source_db), '{0}.url'.format(source_db),
                                '{0}.icon'.format(source_db))),
                'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                                   'WHERE {0}.post_id = {1}.id'
                .format(post_category_db, post_db, category_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(category_db), '{0}.name'.format(category_db)))),
                'interests_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.interest_id = {2}.id '
                                  'WHERE {0}.post_id = {1}.id'
                .format(post_interest_db, post_db, interest_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(interest_db), '{0}.name'.format(interest_db))))
            }
        )

        user_actions = Action.objects.filter(Q(is_trash=False), Q(is_deleted=False), Q(person_id=user_id))
        user_views_post = user_actions.filter(Q(action=ACTION_VIEW), Q(content_type_id=content_type_of('post')))
        posts_viewed_list = user_views_post.values_list('object_id', flat=True)
        posts_viewed = posts.filter(Q(id__in=posts_viewed_list))
        posts_unread = posts.exclude(Q(id__in=posts_viewed_list))
        posts = list(chain(posts_unread, posts_viewed))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            posts, paginator = get_paginator(posts, items, page)

            for entry in posts:
                if entry.categories_list is not None:
                    entry.categories_list = entry.categories_list.split(',')
                    for idx, category_group in enumerate(entry.categories_list):
                        category_attrs = category_group.split(':')
                        if len(category_attrs) == 2:
                            entry.categories_list[idx] = {
                                'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                                'name': category_attrs[1]
                            }
                        else:
                            entry.categories_list[idx] = None
                    entry.categories_list = [f for f in entry.categories_list if f != None]

                if entry.interests_list is not None:
                    entry.interests_list = entry.interests_list.split(',')
                    for idx, group in enumerate(entry.interests_list):
                        attrs = group.split(':')
                        if len(attrs) == 2:
                            entry.interests_list[idx] = {
                                'id': hasher.encodeNumber(hasher.HASH_INTEREST, attrs[0]),
                                'name': attrs[1]
                            }
                        else:
                            entry.interests_list[idx] = None
                    entry.interests_list = [f for f in entry.interests_list if f != None]

                if entry.source_bookmark is not None and entry.source_bookmark != '':
                    bookmark_infos = entry.source_bookmark.split('|')
                    if len(bookmark_infos) == 2:
                        entry.source_bookmark = {
                            'id': hasher.encodeNumber(hasher.HASH_BOOKMARK, bookmark_infos[0]),
                            'url': bookmark_infos[1]
                        }
                    else:
                        entry.source_bookmark = None
                else:
                    entry.source_bookmark = None

                if entry.source_feed is not None and entry.source_feed != '':
                    feed_infos = entry.source_feed.split('|')
                    if len(feed_infos) == 4:
                        entry.source_feed = {
                            'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_infos[0]),
                            'name': feed_infos[1],
                            'url': feed_infos[2],
                            'icon': hasher.encodeNumber(hasher.HASH_PHOTO, feed_infos[3])
                        }
                    else:
                        entry.source_feed = None
                else:
                    entry.source_feed = None

                if entry.thumbnails is not None and entry.thumbnails != '':
                    entry.thumbnails = entry.thumbnails.split(',')
                    for idx, thumb_id in enumerate(entry.thumbnails):
                        entry.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id) if thumb_id else None
                else:
                    entry.thumbnails = None

            posts = PostModelSerializer(posts, many=True).data
            update_status_statistic_for_list(user_id, 'post', posts)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                # 'posts': PostSerializer(posts, many=True).data
                'posts': posts
            })
        else:
            for entry in posts:
                if entry.categories_list is not None:
                    entry.categories_list = entry.categories_list.split(',')
                    for idx, category_group in enumerate(entry.categories_list):
                        category_attrs = category_group.split(':')
                        if len(category_attrs) == 2:
                            entry.categories_list[idx] = {
                                'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                                'name': category_attrs[1]
                            }
                        else:
                            entry.categories_list[idx] = None
                    entry.categories_list = [f for f in entry.categories_list if f != None]

                if entry.interests_list is not None:
                    entry.interests_list = entry.interests_list.split(',')
                    for idx, group in enumerate(entry.interests_list):
                        attrs = group.split(':')
                        if len(attrs) == 2:
                            entry.interests_list[idx] = {
                                'id': hasher.encodeNumber(hasher.HASH_INTEREST, attrs[0]),
                                'name': attrs[1]
                            }
                        else:
                            entry.interests_list[idx] = None
                    entry.interests_list = [f for f in entry.interests_list if f != None]

                if entry.source_bookmark is not None and entry.source_bookmark != '':
                    bookmark_infos = entry.source_bookmark.split('|')
                    if len(bookmark_infos) == 2:
                        entry.source_bookmark = {
                            'id': hasher.encodeNumber(hasher.HASH_BOOKMARK, bookmark_infos[0]),
                            'url': bookmark_infos[1]
                        }
                    else:
                        entry.source_bookmark = None
                else:
                    entry.source_bookmark = None

                if entry.source_feed is not None and entry.source_feed != '':
                    feed_infos = entry.source_feed.split('|')
                    if len(feed_infos) == 4:
                        entry.source_feed = {
                            'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_infos[0]),
                            'name': feed_infos[1],
                            'url': feed_infos[2],
                            'icon': hasher.encodeNumber(hasher.HASH_PHOTO, feed_infos[3])
                        }
                    else:
                        entry.source_feed = None
                else:
                    entry.source_feed = None

                if entry.thumbnails is not None and entry.thumbnails != '':
                    entry.thumbnails = entry.thumbnails.split(',')
                    for idx, thumb_id in enumerate(entry.thumbnails):
                        entry.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id) if thumb_id else None
                else:
                    entry.thumbnails = None

            posts = PostModelSerializer(posts, many=True).data
            update_status_statistic_for_list(user_id, 'post', posts)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                # 'posts': PostSerializer(posts, many=True).data
                'posts': posts
            })


class UpdateProfile(APIView):
    """
    hint: {"update":{"first_name":"","last_name":"","gender":0,"avatar":"","nick_name":"",
    "biography":"","birthday":"","country":"","language":""}}
    """

    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        params = request.data

        user = request.user

        if user:
            update = request.data.get('update', None)
            if update:
                # print(update)
                curr_user = User.objects.filter(Q(id=user.id))
                curr_profile = Profile.objects.filter(Q(user_id=user.id))
                first_name = update.get('first_name', None)
                if not first_name:
                    first_name = user.first_name
                last_name = update.get('last_name', None)
                if not last_name:
                    last_name = user.last_name
                curr_user.update(
                    first_name=first_name,
                    last_name=last_name
                )
                gender = update.get('gender', None)
                if not gender:
                    gender = user.profile.gender
                avatar = update.get('avatar', None)
                if not avatar:
                    avatar = user.profile.avatar
                nick_name = update.get('nick_name', None)
                if not nick_name:
                    nick_name = user.profile.nick_name
                biography = update.get('biography', None)
                if not biography:
                    biography = user.profile.biography
                birthday = update.get('birthday', None)
                if not birthday:
                    birthday = user.profile.birthday
                country = update.get('country', None)
                if not country:
                    country = user.profile.country
                language = update.get('language', None)
                if not language:
                    language = user.profile.language
                curr_profile.update(
                    updated_on=timezone.now(),
                    gender=gender,
                    avatar=avatar,
                    nick_name=nick_name,
                    biography=biography,
                    birthday=datetime.strptime(birthday, "%d/%m/%Y").date(),
                    country=country,
                    language=language
                )
                return Response({
                    'error': AUTHENTICATION_SUCCESS,
                    'session': session.session_key,
                    'expire_time': session.expire_time,
                    'params': params,
                    'user': UserSerializer(curr_user.first()).data
                })

        return Response({
            'error': AUTHENTICATION_SUCCESS,
            'session': session.session_key,
            'expire_time': session.expire_time,
            'params': params,
            'user': UserSerializer(user).data
        })


class FolderOfUser(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, items=10, page=1):
        static_params = {'username': username, 'items': items, 'page': page}
        params = request.data

        # user_id
        user_id = request.user.id

        folders = Folder.objects.filter(Q(is_trash=False), Q(is_deleted=False))
        folders = folders.filter(Q(is_user_defined=True), Q(owner_id=user_id))
        folders = folders.order_by('-updated_on', 'id')

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            folders, paginator = get_paginator(folders, items, page)

            return Response({
                'error': 'SUCCESS',
                'static_params': static_params,
                'params': params,
                'paginator': {
                    'unit': str(paginator.count) + ' folders',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                },
                'folders': UserFolderSerializer(folders, many=True).data
            })
        else:
            return Response({
                'error': 'SUCCESS',
                'static_params': static_params,
                'params': params,
                'folders': UserFolderSerializer(folders, many=True).data
            })


class ChangePassword(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        static_params = {'username': username}
        params = request.data

        # user_id
        user_id = request.user.id

        password = request.data.get('password', None)
        old_pwd = password.get('current', None)
        new_pwd = password.get('new', None)
        confirm_pwd = password.get('confirm', None)

        raw_pwd = new_pwd if new_pwd == confirm_pwd else None

        user = User.objects.get(id=user_id)
        if check_password(old_pwd, user.password) and raw_pwd:
            pwd = make_password(raw_pwd)
            if check_password(raw_pwd, pwd) and is_password_usable(pwd):
                user.password = pwd
                user.save()
                return Response({
                    'error': 'SUCCESS',
                    'static_params': static_params,
                    'params': params,
                })

        return Response({
            'error': 'FAILED',
            'static_params': static_params,
            'params': params,
            'message': 'Current password incorrect!' if new_pwd == confirm_pwd else 'Re confirm password, plz!'
        })


class UserSocialAccountView(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        static_params = {'username': username}
        params = request.data

        # user_id
        user_id = request.user.id

        soc_acc = request.user.social_auth.all()
        return Response({
            'error': 'FAILED',
            'static_params': static_params,
            'params': params,
            'social_accounts': UserSocialAccountSerializer(soc_acc, many=True, fields=('uid', 'provider')).data,
            'message': 'plz!'
        })


class ResetPassword(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, session_key=None, email=None):
        print('here')
        static_params = {'session_key': session_key, 'email': email}
        params = request.data

        password = request.data.get('password', None)
        new_pwd = password.get('new', None)
        confirm_pwd = password.get('confirm', None)

        raw_pwd = new_pwd if new_pwd == confirm_pwd else None

        try:
            user = User.objects.get(email=email)
            session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id), Q(session_key=session_key))
            print(session)
            session = session.first()
            if session and session.expire_time > timezone.now():
                session.is_deleted = True
                session.save()
                if raw_pwd:
                    pwd = make_password(raw_pwd)
                    if check_password(raw_pwd, pwd) and is_password_usable(pwd):
                        user.password = pwd
                        user.save()

                        user = authenticate(username=user.username, password=raw_pwd)
                        if user is not None:
                            # the password verified for the user
                            if not user.is_active:
                                return Response({
                                    'error': AUTHENTICATION_ERROR_SUSPENDED,
                                    'params': params,
                                    'message': 'User is suspended'
                                })
                        else:
                            # the authentication system was unable to verify the username and password
                            return Response({
                                'error': AUTHENTICATION_ERROR_WRONG,
                                'message': 'Wrong authentication'
                            })

                        login(request, user)

                        # create new session for this user
                        old_session = Session.objects.filter(Q(is_deleted=False), Q(owner_id=user.id))
                        old_session = old_session.update(is_deleted=True)
                        session = Session(
                            owner_id=user.id,
                            session_key=uuid.uuid4()
                        )
                        session.expire_time = timezone.now() + timedelta(minutes=settings.SHORT_PERIOD_SESSION)
                        session.save()

                        return Response({
                            'error': AUTHENTICATION_SUCCESS,
                            'params': params,
                            'session': session.session_key,
                            'expire_time': session.expire_time,
                            'user': UserSerializer(user).data
                        })
        except ObjectDoesNotExist:
            pass
        return Response({
            'error': AUTHENTICATION_ERROR_WRONG,
            'static_params': static_params,
            'params': params,
            'message': 'Wrong authentication'
        })


