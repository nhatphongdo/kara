from django.conf.urls import patterns, url, include
from . import api

api_profile = [
    url(r'^/$',
        api.UserView.as_view(), name='api_user_profile'),
    #
    url(r'^/update/$',
        api.UpdateProfile.as_view(), name='api_user_profile_update'),
    url(r'^/social-account/$',
        api.UserSocialAccountView.as_view(), name='api_user_social_account'),

    url(r'^/main-interests/$',
        api.UserMainInterestsView.as_view(), name='api_user_main_interests'),
    url(r'^/interests/$',
        api.UserInterestsView.as_view(), name='api_user_interests'),
    url(r'^/interests/page(?P<page>\w+)/$',
        api.UserInterestsView.as_view(), name='api_user_interests_page'),
    url(r'^/interests/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserInterestsView.as_view(), name='api_user_interests_page_items'),
    url(r'^/interests/(?P<pk>\w+)/$',
        api.UserChildInterestsView.as_view(), name='api_user_child_interests_pk'),
    url(r'^/interests/interest-(?P<name>\w+)/$',
        api.UserChildInterestsView.as_view(), name='api_user_child_interests_name'),
    url(r'^/interests/(?P<pk>\w+)/(?P<trash>trash)$',
        api.UserChildInterestsView.as_view(), name='api_user_child_interests_trash_pk'),
    url(r'^/interests/interest-(?P<name>\w+)/(?P<trash>trash)$',
        api.UserChildInterestsView.as_view(), name='api_user_child_interests_trash_name'),

    url(r'^/friends/$',
        api.UserFriendsView.as_view(), name='api_user_friends'),
    url(r'^/friends/page(?P<page>\w+)/$',
        api.UserFriendsView.as_view(), name='api_user_friends_page'),
    url(r'^/friends/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserFriendsView.as_view(), name='api_user_friends_page_items'),

    url(r'^/friends-groups/$',
        api.UserFriendsGroupsView.as_view(), name='api_user_friends_groups'),
    url(r'^/friends-groups/(?P<pk>\w+)/$',
        api.UserFriendsGroupsView.as_view(), name='api_user_friends_groups_pk'),
    url(r'^/friends-groups/(?P<pk>\w+)/friends/$',
        api.UserFriendsInGroupView.as_view(), name='api_user_friends_in_groups'),
    url(r'^/friends-groups/(?P<pk>\w+)/actions/$',
        api.UserFriendsInGroupView.as_view(), name='api_user_friends_groups_actions'),

    url(r'^/following-groups/$',
        api.UserFollowingGroupsView.as_view(), name='api_user_following_groups'),
    url(r'^/following-groups/(?P<pk>\w+)/$',
        api.UserFollowingGroupsView.as_view(), name='api_user_following_groups_pk'),
    url(r'^/following-groups/(?P<pk>\w+)/following/$',
        api.UserFollowingInGroupView.as_view(), name='api_user_following_in_groups'),
    url(r'^/following-groups/(?P<pk>\w+)/actions/$',
        api.UserFollowingInGroupView.as_view(), name='api_user_following_groups_actions'),

    url(r'^/friend-requests/$',
        api.UserFriendRequestsView.as_view(), name='api_user_friend_requests'),

    url(r'^/friend-requests-sent/$',
        api.UserFriendRequestsSentView.as_view(), name='api_user_friend_requests_sent'),

    url(r'^/following/$',
        api.UserFollowingView.as_view(), name='api_user_following'),
    url(r'^/following/page(?P<page>\w+)/$',
        api.UserFollowingView.as_view(), name='api_user_following_page'),
    url(r'^/following/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserFollowingView.as_view(), name='api_user_following_page_items'),

    url(r'^/folders/structure/$',
        api.UserFoldersView.as_view(), name='api_user_folders_structure'),

    url(r'^/folders/$',
        api.FolderOfUser.as_view(), name='api_user_folders'),
    url(r'^/folders/page(?P<page>\w+)/$',
        api.FolderOfUser.as_view(), name='api_user_folders_page'),
    url(r'^/folders/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.FolderOfUser.as_view(), name='api_user_folders_page_items'),

    url(r'^/public-folders/$',
        api.UserPublicFoldersView.as_view(), name='api_user_public_folders'),
    url(r'^/public-folders/page(?P<page>\w+)/$',
        api.UserPublicFoldersView.as_view(), name='api_user_public_folders_page'),
    url(r'^/public-folders/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserPublicFoldersView.as_view(), name='api_user_public_folders_page_items'),

    url(r'^/sources/$',
        api.UserSourcesView.as_view(), name='api_user_sources'),
    url(r'^/sources/page(?P<page>\w+)/$',
        api.UserSourcesView.as_view(), name='api_user_sources_page'),
    url(r'^/sources/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserSourcesView.as_view(), name='api_user_sources_page_items'),

    url(r'^/sources-alias/$',
        api.UserSourcesAliasView.as_view(), name='api_user_sources_alias'),
    url(r'^/sources-alias/(?P<pk>\w+)/$',
        api.UserSourceAliasNameView.as_view(), name='api_user_sources_alias_name'),
    url(r'^/sources-alias/(?P<pk>\w+)/move-to/(?P<category>[\w-]+)/$',
        api.UserSourceAliasNameView.as_view(), name='api_user_sources_alias_move_to'),
    url(r'^/sources-alias/page(?P<page>\w+)/$',
        api.UserSourcesAliasView.as_view(), name='api_user_sources_page'),
    url(r'^/sources-alias/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserSourcesAliasView.as_view(), name='api_user_sources_page_items'),

    url(r'^/bookmarks/$',
        api.UserBookmarksView.as_view(), name='api_user_bookmarks'),
    url(r'^/bookmarks/page(?P<page>\w+)/$',
        api.UserBookmarksView.as_view(), name='api_user_bookmarks_page'),
    url(r'^/bookmarks/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserBookmarksView.as_view(), name='api_user_bookmarks_page_items'),

    url(r'^/wall-posts/$',
        api.UserWallPostsView.as_view(), name='api_user_wall_posts'),
    url(r'^/wall-posts/page(?P<page>\w+)/$',
        api.UserWallPostsView.as_view(), name='api_user_wall_posts_page'),
    url(r'^/wall-posts/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserWallPostsView.as_view(), name='api_user_wall_posts_page_items'),

    url(r'^/actions/$',
        api.UserActionsView.as_view(), name='api_user_actions'),

    url(r'^/categories/$',
        api.UserCategoriesView.as_view(), name='api_user_categories'),
    url(r'^/categories/category-(?P<name>[\w-]+)/$',
        api.UserCategoriesView.as_view(), name='api_user_categories'),
    url(r'^/categories/category-(?P<name>[\w-]+)/rename/$',
        api.UserCategoriesView.as_view(), name='api_user_categories_rename'),
    url(r'^/categories/category-(?P<name>[\w-]+)/sources/$',
        api.UserSourcesOfCategoryView.as_view(), name='api_user_sources_category_name'),
    url(r'^/categories/category-(?P<name>[\w-]+)/sources/page(?P<page>\w+)/$',
        api.UserSourcesOfCategoryView.as_view(), name='api_user_sources_category_name_page'),
    url(r'^/categories/category-(?P<name>[\w-]+)/sources/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserSourcesOfCategoryView.as_view(), name='api_user_sources_category_name_page_items'),
    url(r'^/categories/category-(?P<name>[\w-]+)/sources-alias/$',
        api.UserSourcesAliasOfCategoryView.as_view(), name='api_user_sources_alias_category_name'),
    url(r'^/categories/category-(?P<name>[\w-]+)/sources-alias/page(?P<page>\w+)/$',
        api.UserSourcesAliasOfCategoryView.as_view(), name='api_user_sources_alias_category_name_page'),
    url(r'^/categories/category-(?P<name>[\w-]+)/sources-alias/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserSourcesAliasOfCategoryView.as_view(), name='api_user_sources_alias_category_name_page_items'),
    url(r'^/categories/category-(?P<name>[\w-]+)/posts/$',
        api.UserPostOfCategoryView.as_view(), name='api_user_posts_category_name'),
    url(r'^/categories/category-(?P<name>[\w-]+)/posts/page(?P<page>\w+)/$',
        api.UserPostOfCategoryView.as_view(), name='api_user_posts_category_name_page'),
    url(r'^/categories/category-(?P<name>[\w-]+)/posts/page(?P<page>\w+)/items(?P<items>\w+)/$',
        api.UserPostOfCategoryView.as_view(), name='api_user_posts_category_name_page_items'),

    url(r'^/password/change/$',
        api.ChangePassword.as_view(), name='api_user_change_password'),
]

"""
AuthServices:
    /api/auth/
        post    -> login
        put     -> verify
        delete  -> logout
    /api/register/
        post    -> join
    /api/profile/:username/
        post    -> get
"""

urlpatterns = patterns(
    '',
    url(r'^(?i)api/auth/$',
        api.AuthView.as_view(), name='api_authenticate'),
    url(r'^(?i)api/register/$',
        api.Register.as_view(), name='api_register'),
    url(r'^(?i)api/register-verify/(?P<session_key>[\w-]+)/(?P<email>[\w@_.-]+)/$',
        api.RegisterVerify.as_view(), name='api_register_verify'),
    url(r'^(?i)api/reset-password/(?P<session_key>[\w-]+)/(?P<email>[\w@_.-]+)/$',
        api.ResetPassword.as_view(), name='api_reset_password'),
    url(r'^(?i)api/profile/(?P<username>[\w@_.-]+)', include(api_profile)),
)


