import bs4
from itertools import chain
from bs4 import BeautifulSoup
from django.db.models import Q
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.views import APIView
from app.apis.common.api import (update_status_statistic_for_list, )
from app.apis.serializers import *
from app.decorators import token_required
from app.errors import *
from app.utils import *


def get_folder_by_name(user_id, object_name='', object_parent_id=None):
    try:
        old_folder = Folder.objects.filter(Q(owner_id=user_id), Q(is_user_defined=True), Q(parent_id=object_parent_id))
        old_folder = old_folder.filter(Q(name=object_name))
        return old_folder if len(old_folder) else None
    except ObjectDoesNotExist:
        return None


def get_folder(user_id, object_id):
    try:
        old_folder = Folder.objects.filter(Q(owner_id=user_id), Q(is_user_defined=True), Q(id=object_id))
        return old_folder if len(old_folder) else None
    except ObjectDoesNotExist:
        return None


def get_folder_by_id(user_id, object_id, object_parent_id=None):
    try:
        old_folder = Folder.objects.filter(Q(owner_id=user_id), Q(is_user_defined=True), Q(id=object_id))
        old_folder = old_folder.filter(Q(parent_id=object_parent_id))
        return old_folder if len(old_folder) else None
    except ObjectDoesNotExist:
        return None


def save_folder(user_id, name, description='', parent_id=None, is_user_defined=True):
    try:
        new_object = Folder(
            owner_id=user_id,
            name=name,
            description=description,
            parent_id=parent_id,
            is_user_defined=is_user_defined
        )
        new_object.save()
        return new_object.id
    except ValueError:
        # print('Save folder failed')
        return None


def delete_folder(user_id, object_id):
    # print('Begin Delete folder')
    old_folder = get_folder(user_id, object_id)
    if not old_folder:
        # print('Not find old folder')
        return None

    # print('With old folder ', len(old_folder), old_folder)
    parent_id = old_folder.first().parent_id

    try:
        old_folder.update(is_deleted=True)
        # print('Deleted folder!')
        return parent_id
    except ValueError:
        # print('Delete folder failed!')
        return parent_id


def copy_folder(user_id, object_id, parent_id=None):
    old_folder = get_folder(user_id, object_id)
    if not old_folder:
        # print('Not find old folder')
        return None

    # print('In old folder ', len(old_folder), old_folder)
    item = old_folder.first()

    if parent_id and get_folder(user_id, parent_id):
        # print('with target')
        pass
    else:
        # print('no target')
        # ## parent_id can not None
        # return None
        # ## parent_id can None
        pass

    n_folder_id = save_folder(user_id, item.name, item.description, None, True)
    children_objects = Folder.objects.filter(Q(is_deleted=False))
    children_objects = children_objects.filter(Q(owner_id=user_id), Q(is_user_defined=True), Q(parent_id=object_id))
    # print('children_objects',children_objects)
    list_bookmarks_id = BookmarksInFolder.objects.filter(Q(is_deleted=False), Q(folder_id=object_id))
    list_bookmarks_id = list_bookmarks_id.values_list('bookmark_id', flat=True)
    # print('list_bookmarks_id', list_bookmarks_id)
    for b_id in list_bookmarks_id:
        bif = BookmarksInFolder(bookmark_id=b_id, folder_id=n_folder_id)
        bif.save()

    if n_folder_id and len(children_objects):
        for f in children_objects:
            copy_folder(user_id, f.id, n_folder_id)

    Folder.objects.filter(id=n_folder_id).update(parent_id=parent_id)
    return parent_id


def move_folder(user_id, object_id, parent_id=None):
    old_folder = get_folder(user_id, object_id)
    if not old_folder:
        # print('Not find old folder')
        return None

    # print('With old folder ', len(old_folder), old_folder)
    item = old_folder.first()

    if parent_id and get_folder(user_id, parent_id):
        # print('with target')
        pass
    else:
        # print('no target')
        # ## parent_id can not None
        # return None
        # ## parent_id can None
        pass

    # need check dup name
    if get_folder_by_name(user_id, item.name, parent_id):
        # print('Existing name')
        return None
    old_folder.update(parent_id=parent_id)
    return parent_id


def todo_info_folder(user_id, object_id, object_parent_id=None, name='', description='', is_system=False):
    old_folder = get_folder_by_id(user_id, object_id, object_parent_id)
    if not old_folder:
        # print('Not find old folder || Create')
        return save_folder(user_id, name, description, object_parent_id, not is_system) if name else None

    # print('With old folder ', len(old_folder), old_folder)
    item_id = old_folder.first().id
    # rename
    if name:
        old_folder.update(name=name, slug=slugify(name), description=description, is_deleted=False)
    else:
        old_folder.update(description=description, is_deleted=False)
    return item_id


def extra_list_folder_object(list_object, list_attr=None):
    for attr in list_attr:
        if attr == 'public':
            list_object = list_object.extra(
                select={
                    'public': 'SELECT count(*) FROM {0} WHERE {0}.object_id = {1}.id AND {0}.action = 4'
                    .format(share_db, folder_db)})
        elif attr == 'num_post':
            list_object = list_object.extra(
                select={
                    'num_post': 'SELECT COUNT({0}.bookmark_id) FROM {0} WHERE {0}.folder_id = {1}.id'
                    .format(bookmark_folder, folder_db)})
        elif attr == 'parents':
            list_object = list_object.extra(
                select={
                    'parents': 'WITH RECURSIVE folders AS ('
                               '    SELECT root.id, root.name, root.slug, root.parent_id FROM {0} root '
                               '    WHERE root.id = {0}.id '
                               '    UNION ALL '
                               '    SELECT parent.id, parent.name, parent.slug, parent.parent_id '
                               '    FROM {0} parent INNER JOIN folders ON folders.parent_id = parent.id'
                               ') SELECT {1} FROM folders'
                    .format(folder_db, concatenate_rows(concatenate_fields(':', '{0}', '{1}', '{2}')
                                                       .format('folders.id', 'folders.slug', 'folders.name')))
                }
            )
    return list_object


def todo_extra_item_folder_object(item_object, list_attr=None):
    for attr in list_attr:
        if attr == 'parents':
            if item_object.parents is not None and item_object.parents != '':
                item_object.parents = item_object.parents.split(',')
                for idx, parent in enumerate(item_object.parents):
                    folder_attrs = parent.split(':')
                    if len(folder_attrs) == 3:
                        item_object.parents[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_FOLDER, folder_attrs[0]),
                            'slug': folder_attrs[1],
                            'name': folder_attrs[2]
                        }
                    else:
                        item_object.parents[idx] = None

                item_object.parents = [f for f in item_object.parents if f != None]
    return item_object


def todo_extra_list_folder_object(list_object, list_attr=None):
    for item_object in list_object:
        item_object = todo_extra_item_folder_object(item_object, list_attr)
    return list_object


def get_user_folders_in_object(user_id, object_id, is_extra=True, is_paginator=True, items=10, page=1):
    # print('user_id', user_id, 'object_id', object_id,
    #       'is_extra', is_extra, 'is_paginator', is_paginator, 'items', items, 'page', page)
    object_folders = Folder.objects.filter(Q(is_deleted=False), Q(owner_id=user_id),
                                           Q(is_user_defined=True), Q(parent_id=object_id))
    if is_extra:
        object_folders = extra_list_folder_object(object_folders, ['public', 'num_post'])

        if is_paginator:
            list_folders, paginator = get_paginator(object_folders, items, page)

            serializer = UserFolderModelSerializer(list_folders.object_list, many=True)
            return serializer.data, paginator
        else:
            serializer = UserFolderModelSerializer(object_folders, many=True)
            return serializer.data
    else:
        if is_paginator:
            list_folders, paginator = get_paginator(object_folders, items, page)

            serializer = UserFolderSerializer(list_folders.object_list, many=True)
            return serializer.data, paginator
        else:
            serializer = UserFolderSerializer(object_folders, many=True)
            return serializer.data


def get_user_folders_in_object_hashed(user_id, object_hashed, is_extra=True, is_paginator=True, items=10, page=1):
    try:
        object_id = hasher.decodeNumber(object_hashed)
    except ValueError:
        object_id = None

    return get_user_folders_in_object(user_id, object_id, is_extra, is_paginator, items, page)


def get_user_folder_info(user_id, object_id):
    old_folder = get_folder(user_id, object_id)
    if not old_folder:
        return {"id": 'None', "name": "All", "slug": None, "description": None, "parent": None, "public": 0}, []
    else:
        old_folder = extra_list_folder_object(old_folder, ['public', 'parents'])

        item = todo_extra_item_folder_object(old_folder.first(), ['parents'])

        serializer = FolderSerializer(
            item, many=False,
            fields=('id', 'name', 'slug', 'description', 'parent', 'public'))
        if item.is_deleted:
            return False, item.parents
        else:
            return serializer.data, item.parents


def build_structure_folders_with_num_post(_parent, items_list, num_post):
    if _parent == -1:
        items = [entry for entry in items_list if (entry.parent_id is None)]
    else:
        items = [entry for entry in items_list if (entry.parent_id == _parent)]
    result = []
    num_post = num_post
    for item in items:
        children, total_post = build_structure_folders_with_num_post(item.id, items_list, item.num_post)
        new_model = FolderModel(item.hashed_id, item.hashed_owner, item.name, item.slug, item.description,
                                item.is_user_defined, item.public, item.hashed_parent, children, total_post)
        result.append(new_model)
        num_post = num_post + total_post

    return result, num_post


def get_structure_folders_with_num_post(user_id):
    folders = Folder.objects.filter(Q(is_deleted=False), Q(owner_id=user_id), Q(is_user_defined=True))
    # is_extra
    folders_list = list(extra_list_folder_object(folders, ['public', 'num_post']))
    folders_tree, num_post = build_structure_folders_with_num_post(-1, folders_list, 0)

    serializer = FolderModelSerializer(
        folders_tree, many=True,
        fields=('id', 'name', 'slug', 'description', 'public', 'num_post', 'parent', 'children'))

    return serializer.data, num_post


# USER'S FOLDERS
class UserFolders(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, page=1, items=10):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function!"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        # copy_folder(user_id, 52, 51)
        # move_folder(user_id, 48, 89)

        # {"is_extra": "True","is_paginator": "True"}
        # {"is_extra": "True","is_paginator": "False"}
        # {"is_extra": "False","is_paginator": "True"}
        # {"is_extra": "False","is_paginator": "False"}
        is_extra = request.data.get('is_extra', None)
        is_extra = (is_extra.lower() == 'true') if type(is_extra) is str else is_extra
        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator

        # get
        object_hashed = pk
        if is_paginator:
            object_folders, paginator = \
                get_user_folders_in_object_hashed(user_id, object_hashed, is_extra, is_paginator, items, page)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'object_folders': object_folders,
                'paginator': {
                    'unit': str(paginator.count) + ' links',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                }
            })
        else:
            object_folders = get_user_folders_in_object_hashed(user_id, object_hashed, is_extra, False, items, page)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'object_folders': object_folders
            })


# USER'S STRUCTURE FOLDERS
class StructureFolders(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        # get
        structure_folders, num_post = get_structure_folders_with_num_post(user_id)

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'structure_folders': structure_folders,
            'num_post': num_post
        })


# USER'S FOLDERS INFORMATION
class UserFolderDetail(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, parent=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None
        try:
            folder_id = hasher.decodeNumber(parent)
        except ValueError:
            folder_id = None

        # insert
        # rename
        folder = request.data.get('folder', None)
        if folder:
            name = folder.get('name', None)
            description = folder.get('description', '')
            if name:
                folder_id = todo_info_folder(user_id, object_id, folder_id, name, description)
                if folder_id:
                    tags_list = set_items_value_list(request.data.get('tags', []), 'tag')
                    for tag in tags_list:
                        Tag.objects.create(
                            tag=tag, slug=slugify(tag),
                            person_id=user_id, is_user_defined=True, content_object=Folder.objects.get(pk=folder_id))

                    return Response({
                        'error': 'SUCCESS',
                        'params': params,
                        'folder_id': hasher.encodeNumber(hasher.HASH_FOLDER, folder_id),
                        'message': 'Please reload page!'
                    })
            return Response({
                'error': 'FAILED',
                'params': params,
            })

        # get info
        folder_info, breadcrumbs = get_user_folder_info(user_id, object_id)
        statistic = []
        return Response({
            'error': 'SUCCESS',
            'params': params,
            'info': folder_info,
            'breadcrumbs': breadcrumbs,
            'statistic': statistic
        })

    @csrf_exempt
    @token_required()
    def put(self, request, session=None, username=None, pk=None, parent=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None
        try:
            parent_id = hasher.decodeNumber(parent)
        except ValueError:
            parent_id = None

        # {"is_move": "False"}
        is_move = request.data.get('is_move', None)
        is_move = (is_move.lower() == 'true') if type(is_move) is str else is_move

        # ## parent_id can not None
        # if object_id and parent_id:
        # ## parent_id can None
        if object_id:
            if is_move:
                parent_id = move_folder(user_id, object_id, parent_id)
            else:
                parent_id = copy_folder(user_id, object_id, parent_id)

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'parent_id': hasher.encodeNumber(hasher.HASH_FOLDER, parent_id) if parent_id else parent_id,
            'message': 'Please reload page!'
        })

    @csrf_exempt
    @token_required()
    def delete(self, request, session=None, username=None, pk=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None

        parent_id = None
        if object_id:
            parent_id = delete_folder(user_id, object_id)

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'parent_id': parent_id,
            'message': 'Please reload page!'
        })


def handle_uploaded_file(f, filename):
    with open('uploads/' + filename, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def insert_folder(name, user_id, parent_id):
    old_folder = Folder.objects.filter(Q(name=name))
    if old_folder.exists():
        return old_folder.get().id

    if not name:
        return None

    folder = Folder(
        name=name, owner_id=user_id, parent_id=parent_id, slug=slugify(name),
        is_user_defined=True, updated_on=timezone.now())
    folder.save()
    return folder.id


def insert_new_bookmark(href, title, user_id, folder_id):
    old_bookmark = Bookmark.objects.filter(title=title, folders__id=folder_id, is_deleted=False)
    if old_bookmark.exists():
        return old_bookmark.get()

    folder = Folder.objects.get(id=folder_id)
    if not folder:
        return None

    bookmark = Bookmark()
    bookmark.title = title
    bookmark.slug = slugify(title)
    bookmark.url = href
    bookmark.owner_id = user_id
    bookmark.status = 0
    bookmark.save()

    bookmark_in_folder = BookmarksInFolder()
    bookmark_in_folder.bookmark_id = bookmark.id
    bookmark_in_folder.folder_id = folder.id
    bookmark_in_folder.save()

    return bookmark


def import_bookmark_from_html_str(soup, user_id, folder_id):
    if not type(soup) is bs4.element.Tag:
        return

    have_folder = False
    new_folder_id = 0
    for child in soup.children:
        if child.name == 'h3':
            have_folder = True
            new_folder_id = insert_folder(child.text, user_id, folder_id)
        elif child.name == 'dl':
            if have_folder:
                import_bookmark_from_html_str(child, user_id, new_folder_id)
                have_folder = False
            else:
                import_bookmark_from_html_str(child, user_id, folder_id)
        elif child.name == 'a':
            book = 'ok'
            if child['href'] and child.text:
                insert_new_bookmark(child['href'], child.text, user_id, folder_id)


# IMPORT USER'S BOOKMARKS
class ImportBookmark(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })

        folder_name = request.data.get('folderName', 'Import_' + str(timezone.now().date()))

        try:
            content = request.FILES['file']
            if content.size > (2 * 1024 * 1024):
                return Response({
                    'error': 'error',
                    'message': 'File is not valid!'
                })

            handle_uploaded_file(content, content.name)
            user = request.user
            folder_id = insert_folder(folder_name, user.id, None)

            if not folder_id:
                return Response({
                    'error': 'error',
                    'message': 'Folder is not valid!'
                })

            file_path = 'uploads/' + content.name

            with open(file_path, 'rb') as html:
                soup = BeautifulSoup(html)

                if soup.dl:
                    dl = soup.dl
                    for p in dl.findAll('p'):
                        p.unwrap()
                    for dt in dl.findAll('dt'):
                        dt.unwrap()
                    for hr in dl.findAll('hr'):
                        hr.unwrap()
                    import_bookmark_from_html_str(dl, user.id, folder_id)
                html.close()
            import os

            if os.path.exists(file_path):
                os.remove(file_path)

            return Response({
                'error': 'SUCCESS',
                'message': ''
            })
        except ValueError:
            return Response({
                'error': 'error',
                'message': 'Post is not valid!'
            })


def get_bookmark_in_folder(object_id, folder_id):
    try:
        old_bif = BookmarksInFolder.objects.filter(Q(bookmark_id=object_id), Q(folder_id=folder_id))
        return old_bif if len(old_bif) else None
    except ObjectDoesNotExist:
        return None


def get_user_bookmark(user_id, object_id):
    try:
        user_bookmark = Bookmark.objects.filter(Q(is_deleted=False), Q(owner_id=user_id), Q(id=object_id))
        return user_bookmark if len(user_bookmark) else None
    except ObjectDoesNotExist:
        return None


def save_bookmark(user_id, title, url):
    try:
        new_object = Bookmark(
            owner_id=user_id,
            title=title,
            url=url
        )
        new_object.save()
        return new_object.id
    except ValueError:
        # print('Save bookmark failed')
        return None


def delete_bookmark_in_folder(object_id, folder_id):
    # print('Begin Delete bookmark in folder')
    old_bif = get_bookmark_in_folder(object_id, folder_id)
    if not old_bif:
        # print('Not find old bif')
        return None

    # print('With old bif ', len(old_bif), old_bif)

    try:
        # old_bif.update(is_deleted=True)
        old_bif.delete()
        # print('Deleted bookmark')
        return folder_id
    except ValueError:
        # print('Delete bookmark failed')
        return folder_id


def delete_user_bookmark(user_id, object_id):
    # print('Begin Delete bookmark of user')
    user_bookmark = get_user_bookmark(user_id, object_id)
    if not user_bookmark:
        # print('Not find user\'s bookmark')
        return None

    # print('With user\'s bookmark ', len(user_bookmark), user_bookmark)

    try:
        user_bookmark.update(owner_id=None)
        # print('Deleted bookmark')
        return None
    except ValueError:
        # print('Delete bookmark failed')
        return None


# ok
def add_bookmark_to_folder(user_id, object_id, folder_id):
    old_folder = get_folder(user_id, folder_id)
    if not old_folder:
        # print('Not find old folder')
        return None, 'Not find old folder'

    old_bif = get_bookmark_in_folder(object_id, folder_id)
    if old_bif:
        old_bif.update(is_deleted=False)
        # print('Added bookmark (updated)')
        return True, 'Added bookmark (updated)'
    else:
        try:
            new_object_in_folder = BookmarksInFolder(
                bookmark_id=object_id,
                folder_id=folder_id
            )
            new_object_in_folder.save()
            return True, 'Added bookmark'
        except ValueError:
            # print('Add bookmark failed')
            return None, 'Add bookmark failed'


def move_bookmark_to_folder(user_id, object_id, folder_id, new_folder_id):
    try:
        delete_bookmark_in_folder(object_id, folder_id)
        add_bookmark_to_folder(user_id, object_id, new_folder_id)
        return new_folder_id
    except ValueError:
        return None


def todo_info_bookmark(user_id, object_id, title='', url=''):
    # print(object_id)
    old_bookmark = Bookmark.objects.filter(pk=object_id)
    if not old_bookmark:
        # print('Not find old bookmark || Create')
        return save_bookmark(user_id, title, url) if title and url else None

    # print('With old bookmark ', len(old_bookmark), old_bookmark)
    item_id = old_bookmark.first().id
    # rename
    if title and url:
        old_bookmark.update(title=title, slug=slugify(title), url=url, is_deleted=False)
    elif title:
        old_bookmark.update(title=title, is_deleted=False)
    elif url:
        old_bookmark.update(url=url, is_deleted=False)
    return item_id


def extra_list_bookmark_object(list_object, list_attr=None):
    for attr in list_attr:
        if attr == 'description':
            list_object = list_object.extra(
                select={
                    'description': 'SELECT {0}.description FROM {0} WHERE {0}.source_id = {1}.id '
                                   'ORDER BY {0}.updated_on DESC LIMIT 1'
                    .format(post_db, bookmark_db)})
        elif attr == 'thumbnails':
            list_object = list_object.extra(
                select={
                    'thumbnails': 'SELECT {0}.thumbnails FROM {0} WHERE {0}.source_id = {1}.id '
                                  'ORDER BY {0}.updated_on DESC LIMIT 1'
                    .format(post_db, bookmark_db)})
        elif attr == 'folders_list':
            list_object = list_object.extra(
                select={
                    'folders_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.folder_id = {2}.id '
                                    'WHERE {0}.bookmark_id = {1}.id'
                    .format(bookmark_folder, bookmark_db, folder_db,
                            concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                            .format('{0}.id'.format(folder_db), '{0}.name'.format(folder_db))))
                }
            )
        elif attr == 'username':
            list_object = list_object.extra(
                select={
                    'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.owner_id'.format(user_db, bookmark_db)
                }
            )

    return list_object


def todo_extra_list_bookmark_object(list_object, list_attr=None):
    for item_object in list_object:
        item_object = todo_extra_item_bookmark_object(item_object, list_attr)

    return list_object


def todo_extra_item_bookmark_object(item_object, list_attr=None):
    for attr in list_attr:
        if attr == 'thumbnails':
            if item_object.thumbnails != '' and item_object.thumbnails is not None:
                item_object.thumbnails = item_object.thumbnails.split(',')
                for idx, thumb_id in enumerate(item_object.thumbnails):
                    item_object.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id)
            else:
                item_object.thumbnails = None
        elif attr == 'folders_list':
            if item_object.folders_list is not None:
                item_object.folders_list = item_object.folders_list.split(',')
                for idx, folder_group in enumerate(item_object.folders_list):
                    folder_attrs = folder_group.split(':')
                    if len(folder_attrs) == 2:
                        item_object.folders_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_FOLDER, folder_attrs[0]),
                            'name': folder_attrs[1]
                        }
                    else:
                        item_object.folders_list[idx] = None
                item_object.folders_list = [f for f in item_object.folders_list if f != None]

    return item_object


def get_user_bookmarks_in_object(user_id, object_id, is_extra=True, is_paginator=True, items=10, page=1):
    # print('user_id', user_id, 'object_id', object_id,
    #       'is_extra', is_extra, 'is_paginator', is_paginator, 'items', items, 'page', page)
    bookmarks_in_folder = BookmarksInFolder.objects.filter(Q(is_deleted=False), Q(is_trash=False))
    object_bookmarks = Bookmark.objects.filter(Q(is_deleted=False), Q(is_trash=False))
    if object_id:
        bookmarks_in_folder = bookmarks_in_folder.filter(Q(folder_id=object_id))
        bookmarks_in_folder = bookmarks_in_folder.values_list('bookmark_id', flat=True)
        object_bookmarks = object_bookmarks.filter(id__in=bookmarks_in_folder).order_by('title', '-updated_on')
    else:
        bookmarks_in_folder = bookmarks_in_folder.values_list('bookmark_id', flat=True)
        object_bookmarks = object_bookmarks.filter(Q(owner_id=user_id))
        object_bookmarks = object_bookmarks.exclude(id__in=bookmarks_in_folder).order_by('title', '-updated_on')
    if is_extra:
        object_bookmarks = extra_list_bookmark_object(object_bookmarks, ['description', 'thumbnails', 'folders_list'])

        if is_paginator:
            list_bookmarks, paginator = get_paginator(object_bookmarks, items, page)

            list_bookmarks = todo_extra_list_bookmark_object(list_bookmarks, ['thumbnails', 'folders_list'])
            serializer = BookmarkModelSerializer(list_bookmarks.object_list, many=True)
            return serializer.data, paginator
        else:
            object_bookmarks = todo_extra_list_bookmark_object(object_bookmarks, ['thumbnails', 'folders_list'])
            serializer = BookmarkModelSerializer(object_bookmarks, many=True)
            return serializer.data
    else:
        if is_paginator:
            list_bookmarks, paginator = get_paginator(object_bookmarks, items, page)

            serializer = BookmarkSerializer(list_bookmarks.object_list, many=True)
            return serializer.data, paginator
        else:
            serializer = BookmarkSerializer(object_bookmarks, many=True)
            return serializer.data


def get_user_bookmarks_in_object_hashed(user_id, object_hashed, is_extra=True, is_paginator=True, items=10, page=1):
    try:
        object_id = hasher.decodeNumber(object_hashed)
    except ValueError:
        object_id = None

    return get_user_bookmarks_in_object(user_id, object_id, is_extra, is_paginator, items, page)


def get_bookmark_info(object_id):
    bookmark_detail = Bookmark.objects.filter(Q(is_deleted=False), Q(id=object_id))

    bookmark_detail = extra_list_bookmark_object(bookmark_detail,
                                                 ['username', 'description', 'thumbnails', 'folders_list'])

    bookmark = todo_extra_item_bookmark_object(bookmark_detail.first(), ['thumbnails', 'folders_list'])

    serializer = BookmarkModelSerializer(bookmark)
    if bookmark.is_deleted:
        return False
    else:
        return serializer.data


# USER'S FOLDERS AND BOOKMARKS
class FolderBookmark(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, page=1, items=10):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        folders = get_user_folders_in_object_hashed(user_id, pk, True, False)
        bookmarks = get_user_bookmarks_in_object_hashed(user_id, pk, True, False)
        list_result = list(chain(folders, bookmarks))

        page_list, paginator = get_paginator(list_result, items, page)

        page_list_sub_folders = []
        page_list_bookmarks = []
        page_list.object_list = list(page_list.object_list)
        for item in page_list.object_list:
            if 'url' in item:
                photos = []
                if item['thumbnails'] is not None and item['thumbnails'] != '':
                    for idx, thumb_id in enumerate(item['thumbnails']):
                        try:
                            pk = hasher.decodeNumber(thumb_id)
                            photo = get_photo(pk)
                            photos.append(str(photo))
                        except ValueError:
                            pass
                    item['thumbnails'] = photos
                else:
                    item['thumbnails'] = None
                item['type'] = 'bookmark'
                update_status_statistic_for_list(user_id, 'bookmark', [item])
                page_list_bookmarks.append(item)
            else:
                item['type'] = 'folder'
                update_status_statistic_for_list(user_id, 'folder', [item])
                page_list_sub_folders.append(item)

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'folders': page_list_sub_folders,
            'links': page_list_bookmarks,
            'mix': page_list.object_list,
            'paginator': {
                'unit': str(paginator.count)
                        + ' - ' + str(len(bookmarks)) + ' bookmark(s) and ' + str(len(folders)) + ' folder(s)',
                'totalItems': paginator.count,
                'itemsPerPage': int(items),
                'numPages': paginator.num_pages,
                'currentPage': int(page)
            }
        })


# USER'S BOOKMARKS
class UserBookmarks(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, page=1, items=10):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function!"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        # {"is_extra": "True","is_paginator": "True"}
        # {"is_extra": "True","is_paginator": "False"}
        # {"is_extra": "False","is_paginator": "True"}
        # {"is_extra": "False","is_paginator": "False"}
        is_extra = request.data.get('is_extra', None)
        is_extra = (is_extra.lower() == 'true') if type(is_extra) is str else is_extra
        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator

        # get
        object_hashed = pk
        if is_paginator:
            object_bookmarks, paginator = \
                get_user_bookmarks_in_object_hashed(user_id, object_hashed, is_extra, is_paginator, items, page)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'object_bookmarks': object_bookmarks,
                'paginator': {
                    'unit': str(paginator.count) + ' links',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                }
            })
        else:
            object_bookmarks = get_user_bookmarks_in_object_hashed(user_id, object_hashed, is_extra, False, items, page)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'object_bookmarks': object_bookmarks
            })


# USER'S BOOKMARK INFORMATION
class BookmarkDetail(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, folder=None):
        params = request.data

        # user_id
        user_id = request.user.id

        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None
        try:
            folder_id = hasher.decodeNumber(folder)
        except ValueError:
            folder_id = None

        # insert
        # rename
        bookmark = request.data.get('bookmark', None)
        if bookmark:
            title = bookmark.get('title', None)
            url = bookmark.get('url', '')
            if title and url:
                bookmark_id = todo_info_bookmark(user_id, object_id, title, url)
                if bookmark_id:
                    message = 'Please reload page!'
                    result, message = add_bookmark_to_folder(user_id, bookmark_id, folder_id)
                    tags_list = set_items_value_list(request.data.get('tags', []), 'tag')
                    for tag in tags_list:
                        Tag.objects.create(
                            tag=tag, slug=slugify(tag), person_id=user_id, is_user_defined=True,
                            content_object=Bookmark.objects.get(pk=bookmark_id))
                    return Response({
                        'error': 'SUCCESS',
                        'params': params,
                        'folder_id': hasher.encodeNumber(hasher.HASH_FOLDER,
                                                         folder_id) if folder_id and result else None,
                        'message': message
                    })
            return Response({
                'error': 'FAILED',
                'params': params,
            })

        # get info
        try:
            object_id = hasher.decodeNumber(pk)
            bookmark_info = get_bookmark_info(object_id)
            statistic = []
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'info': bookmark_info,
                'statistic': statistic
            })
        except ValueError:
            object_id = None
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'info': {}
            })


    @csrf_exempt
    @token_required()
    def put(self, request, session=None, username=None, pk=None, folder=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function!"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None
        try:
            folder_id = hasher.decodeNumber(folder)
        except ValueError:
            folder_id = None

        # {"is_move": "False"}
        is_move = request.data.get('is_move', None)
        is_move = (is_move.lower() == 'true') if type(is_move) is str else is_move
        old_folder = request.data.get('old_folder', None)
        try:
            old_folder_id = hasher.decodeNumber(old_folder)
        except ValueError:
            old_folder_id = None

        if object_id and folder_id:
            if is_move and old_folder_id:
                delete_bookmark_in_folder(object_id, old_folder_id)
            result, message = add_bookmark_to_folder(user_id, object_id, folder_id)
        else:
            return Response({
                'error': 'FAILED',
                'params': params,
            })

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'parent_id': hasher.encodeNumber(hasher.HASH_FOLDER, folder_id),
            'message': 'Please reload page!'
        })


    @csrf_exempt
    @token_required()
    def delete(self, request, session=None, username=None, pk=None, folder=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function!"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None
        try:
            folder_id = hasher.decodeNumber(folder)
        except ValueError:
            folder_id = None

        if object_id and folder_id:
            folder_id = delete_bookmark_in_folder(object_id, folder_id)
        elif object_id and (folder_id is None):
            delete_user_bookmark(user_id, object_id)
            # print('del user bookmark')

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'folder_id': folder_id,
            'message': 'Please reload page!'
        })


class BookmarkLikes(APIView):
    @csrf_exempt
    @token_required()
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        queryset = Bookmark.objects.filter(pk=pk)
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.owner_id'.format(user_db, bookmark_db)})
        try:
            bookmark = queryset.first()
        except:
            raise Http404

        content_type = content_type_of('bookmark')
        queryset = Action.objects.filter(Q(content_type_id=content_type.id), Q(action=ACTION_LIKE), Q(object_id=pk))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, action_db)})
        queryset = queryset.values('updated_on', 'username')

        items_list, paginator = get_paginator(queryset, items, page)

        serializer = ActionModelSerializer(items_list, many=True)

        return Response(serializer.data)


class BookmarkDislikes(APIView):
    @csrf_exempt
    @token_required()
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        queryset = Bookmark.objects.filter(pk=pk)
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.owner_id'.format(user_db, bookmark_db)})
        try:
            bookmark = queryset.first()
        except:
            raise Http404

        content_type = content_type_of('bookmark')
        queryset = Action.objects.filter(Q(content_type_id=content_type.id), Q(action=ACTION_DISLIKE), Q(object_id=pk))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, action_db)})
        queryset = queryset.values('updated_on', 'username')

        items_list, paginator = get_paginator(queryset, items, page)

        serializer = ActionModelSerializer(items_list, many=True)

        return Response(serializer.data)


class BookmarkShares(APIView):
    @csrf_exempt
    @token_required()
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        queryset = Bookmark.objects.filter(pk=pk)
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.owner_id'.format(user_db, bookmark_db)})
        try:
            bookmark = queryset.first()
        except:
            raise Http404

        content_type = content_type_of('bookmark')
        queryset = Action.objects.filter(Q(content_type_id=content_type.id), Q(action=ACTION_SHARE), Q(object_id=pk))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, action_db)})
        queryset = queryset.values('updated_on', 'username')

        items_list, paginator = get_paginator(queryset, items, page)

        serializer = ActionModelSerializer(items_list, many=True)

        return Response(serializer.data)


class BookmarkViews(APIView):
    @csrf_exempt
    @token_required()
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        queryset = Bookmark.objects.filter(pk=pk)
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.owner_id'.format(user_db, bookmark_db)})
        try:
            bookmark = queryset.first()
        except:
            raise Http404

        content_type = content_type_of('bookmark')
        queryset = Action.objects.filter(Q(content_type_id=content_type.id), Q(action=ACTION_VIEW), Q(object_id=pk))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, action_db)})
        queryset = queryset.values('updated_on', 'username')

        items_list, paginator = get_paginator(queryset, items, page)

        serializer = ActionModelSerializer(items_list, many=True)

        return Response(serializer.data)


class BookmarkComments(APIView):
    @csrf_exempt
    @token_required()
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        queryset = Bookmark.objects.filter(pk=pk)
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.owner_id'.format(user_db, bookmark_db)})
        try:
            bookmark = queryset.first()
        except:
            raise Http404

        content_type = content_type_of('bookmark')
        queryset = Comment.objects.filter(Q(content_type_id=content_type.id), Q(object_id=pk))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, comment_db)})
        queryset = queryset.values('title', 'content', 'updated_on', 'username')

        items_list, paginator = get_paginator(queryset, items, page)

        serializer = CommentModelSerializer(items_list, many=True)

        return Response(serializer.data)


class BookmarkTags(APIView):
    @csrf_exempt
    @token_required()
    def get(self, request, session=None, pk=None, page=1, items=10):
        if pk is None or pk.lower() == 'none':
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        try:
            pk = hasher.decodeNumber(pk)
        except ValueError:
            raise Http404

        content_type = content_type_of('bookmark')
        queryset = Tag.objects.filter(Q(content_type_id=content_type.id), Q(object_id=pk))
        queryset = queryset.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.person_id'.format(user_db, tag_db)})
        queryset = queryset.values('id', 'tag', 'slug', 'is_user_defined', 'updated_on', 'username')

        items_list, paginator = get_paginator(queryset, items, page)

        serializer = TagModelSerializer(items_list, many=True,
                                        fields=('id', 'tag', 'slug', 'is_user_defined', 'updated_on', 'username'))

        return Response(serializer.data)


# ADD BOOKMARK
class AddBookmark(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None, folder=None):
        static_params = {'username': username, 'pk': pk, 'folder': folder}
        params = request.data

        # user_id
        user_id = request.user.id

        try:
            folder_id = hasher.decodeNumber(folder)
        except ValueError:
            folder_inbox = Folder.objects.filter(Q(is_deleted=False), Q(is_trash=False))
            folder_inbox = folder_inbox.filter(Q(is_user_defined=True), Q(owner_id=user_id), Q(name='Inbox'))
            if folder_inbox.first():
                folder_id = folder_inbox.first().id
            else:
                inbox = Folder.objects.create(
                    name='Inbox', slug='inbox-of-user' + str(user_id),
                    owner_id=user_id, is_user_defined=True, description='')
                folder_id = inbox.id

        try:
            object_id = hasher.decodeNumber(pk)
        except ValueError:
            object_id = None
            return Response({
                'error': 'FAILED',
                'static_params': static_params,
                'params': params,
                'message': 'bookmark not found!'
            })

        # add bookmark
        bookmark = Bookmark.objects.get(pk=object_id)
        bif = BookmarksInFolder.objects.create(bookmark_id=object_id, folder_id=folder_id, name=bookmark.title)
        tags_list = set_items_value_list(request.data.get('tags', []), 'tag')
        for tag in tags_list:
            Tag.objects.create(
                tag=tag, slug=slugify(tag), person_id=user_id,
                is_user_defined=True, content_object=bookmark)

        return Response({
            'error': 'SUCCESS',
            'static_params': static_params,
            'params': params,
        })


class MultiSelectDelete(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })

        static_params = {'username': username}
        params = request.data
        user_id = request.user.id

        selected_list = params.get('selected_list')
        parent_id = params.get('parent_id')
        try:
            parent_id = hasher.decodeNumber(parent_id)
        except ValueError:
            parent_id = None

        folders_list = selected_list.get('folders')
        id_list = []
        if folders_list:
            for pk in folders_list:
                try:
                    fid = hasher.decodeNumber(pk)
                    id_list.append(fid)
                except ValueError:
                    pass
            folders = Folder.objects.filter(Q(is_deleted=False), Q(is_trash=False))
            folders = folders.filter(Q(owner_id=user_id), Q(is_user_defined=True))
            folders = folders.filter(Q(id__in=id_list))
            parent_id = folders.first().parent_id
            folders = folders.update(is_deleted=True, is_trash=True, updated_on=timezone.now())

        bookmarks_list = selected_list.get('bookmarks')
        id_list = []
        if bookmarks_list:
            for pk in bookmarks_list:
                try:
                    fid = hasher.decodeNumber(pk)
                    id_list.append(fid)
                except ValueError:
                    pass
            if parent_id:
                bookmarks_in_folder = BookmarksInFolder.objects.filter(Q(is_deleted=False), Q(is_trash=False))
                bookmarks_in_folder = bookmarks_in_folder.filter(Q(bookmark_id__in=id_list), Q(folder_id=parent_id))
                bookmarks_in_folder = bookmarks_in_folder.update(
                    is_deleted=True, is_trash=True, updated_on=timezone.now())
            else:
                bookmarks = Bookmark.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(owner_id=user_id))
                bookmarks = bookmarks.filter(Q(id__in=id_list))
                bookmarks = bookmarks.update(is_deleted=True, is_trash=True, updated_on=timezone.now())

        tags_list = selected_list.get('tags')
        if tags_list:
            tags = Tag.objects.filter(Q(is_deleted=False), Q(is_trash=False))
            tags = tags.filter(Q(person_id=user_id), Q(is_user_defined=True))
            tags = tags.filter(Q(slug__in=tags_list))
            tags = tags.update(is_deleted=True, is_trash=True, updated_on=timezone.now())

        return Response({
            'success': True,
            'static_params': static_params,
            'params': params
        })


class MultiSelectMoveTo(APIView):
    def ban_list(self, parent, items_list, output):
        if parent is None:
            items = [entry for entry in items_list if (entry.parent_id is None)]
        else:
            items = [entry for entry in items_list if (entry.parent_id == parent)]
        for item in items:
            output.append(item.id)
            self.ban_list(item.id, items_list, output)

    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, folder=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })

        static_params = {'username': username, 'folder': folder}
        params = request.data
        user_id = request.user.id

        try:
            folder = hasher.decodeNumber(folder)
        except ValueError:
            folder = None

        selected_list = params.get('selected_list')
        parent_id = params.get('parent_id')
        try:
            parent_id = hasher.decodeNumber(parent_id)
        except ValueError:
            parent_id = None

        folders_list = selected_list.get('folders')
        id_list = []
        moved_folders = []
        moved_folders_len = 0
        if folders_list:
            for pk in folders_list:
                try:
                    fid = hasher.decodeNumber(pk)
                    id_list.append(fid)
                except ValueError:
                    pass
            folders = Folder.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(is_user_defined=True))
            folders = folders.filter(Q(owner_id=user_id))
            all_user_folder = folders
            folders = folders.filter(Q(id__in=id_list))
            parent_id = folders.first().parent_id
            if folder:
                output = [parent_id]
                for f in folders:
                    output.append(f.id)
                    self.ban_list(f.id, all_user_folder, output)
                if folder not in output:
                    moved_folders_len = len(folders)
                    moved_folders = folders.values_list('name', flat=True)
                    folders = folders.update(parent_id=folder, updated_on=timezone.now())

        bookmarks_list = selected_list.get('bookmarks')
        id_list = []
        moved_bookmarks = []
        moved_bookmarks_len = 0
        if bookmarks_list:
            for pk in bookmarks_list:
                try:
                    fid = hasher.decodeNumber(pk)
                    id_list.append(fid)
                except ValueError:
                    pass
            if parent_id:
                bookmarks_in_folder = BookmarksInFolder.objects.filter(Q(is_deleted=False), Q(is_trash=False))
                bookmarks_in_folder = bookmarks_in_folder.filter(Q(bookmark_id__in=id_list), Q(folder_id=parent_id))
                if folder:
                    bookmarks_in_folder = bookmarks_in_folder.update(folder_id=folder, updated_on=timezone.now())
            else:
                bookmarks = Bookmark.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(owner_id=user_id))
                bookmarks = bookmarks.filter(Q(id__in=id_list))
                if folder:
                    for bookmark in bookmarks:
                        new_bif = BookmarksInFolder(
                            created_on=timezone.now(),
                            updated_on=timezone.now(),
                            ip_address=get_client_ip(request),
                            bookmark_id=bookmark.id,
                            folder_id=folder,
                            name=bookmark.title
                        )
                        new_bif.save()
                    moved_bookmarks_len = len(bookmarks)
                    moved_bookmarks = bookmarks.values_list('title', flat=True)

        return Response({
            'success': True,
            'static_params': static_params,
            'params': params,
            'moved_folders': moved_folders,
            'moved_folders_len': moved_folders_len,
            'moved_bookmarks': moved_bookmarks,
            'moved_bookmarks_len': moved_bookmarks_len,
        })


class MultiSelectCopyTo(APIView):
    def ban_list(self, parent, items_list, output):
        if parent is None:
            items = [entry for entry in items_list if (entry.parent_id is None)]
        else:
            items = [entry for entry in items_list if (entry.parent_id == parent)]
        for item in items:
            output.append(item.id)
            self.ban_list(item.id, items_list, output)

    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, folder=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })

        static_params = {'username': username, 'folder': folder}
        params = request.data
        user_id = request.user.id

        try:
            folder = hasher.decodeNumber(folder)
        except ValueError:
            folder = None

        selected_list = params.get('selected_list')
        parent_id = params.get('parent_id')
        try:
            parent_id = hasher.decodeNumber(parent_id)
        except ValueError:
            parent_id = None

        folders_list = selected_list.get('folders')
        id_list = []
        copied_folders = []
        copied_folders_len = 0
        if folders_list:
            for pk in folders_list:
                try:
                    fid = hasher.decodeNumber(pk)
                    id_list.append(fid)
                except ValueError:
                    pass
            folders = Folder.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(is_user_defined=True))
            folders = folders.filter(Q(owner_id=user_id))
            all_user_folder = folders
            folders = folders.filter(Q(id__in=id_list))
            parent_id = folders.first().parent_id
            if folder:
                output = [parent_id]
                for f in folders:
                    output.append(f.id)
                    self.ban_list(f.id, all_user_folder, output)
                if folder not in output:
                    copied_folders_len = len(folders)
                    copied_folders = folders.values_list('name', flat=True)
                    for f in folders:
                        copy_folder(user_id, f.id, folder)

        bookmarks_list = selected_list.get('bookmarks')
        id_list = []
        copied_bookmarks = []
        copied_bookmarks_len = 0
        if bookmarks_list:
            for pk in bookmarks_list:
                try:
                    fid = hasher.decodeNumber(pk)
                    id_list.append(fid)
                except ValueError:
                    pass
            bookmarks = Bookmark.objects.filter(Q(is_deleted=False), Q(is_trash=False), Q(owner_id=user_id))
            bookmarks = bookmarks.filter(Q(id__in=id_list))
            if folder:
                for bookmark in bookmarks:
                    new_bif = BookmarksInFolder(
                        created_on=timezone.now(),
                        updated_on=timezone.now(),
                        ip_address=get_client_ip(request),
                        bookmark_id=bookmark.id,
                        folder_id=folder,
                        name=bookmark.title + ' (copy)'
                    )
                    new_bif.save()
                copied_bookmarks_len = len(bookmarks)
                copied_bookmarks = bookmarks.values_list('title', flat=True)

        return Response({
            'success': True,
            'static_params': static_params,
            'params': params,
            'copied_folders': copied_folders,
            'copied_folders_len': copied_folders_len,
            'copied_bookmarks': copied_bookmarks,
            'copied_bookmarks_len': copied_bookmarks_len,
        })


