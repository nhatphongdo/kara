from django.conf.urls import patterns, url, include
from . import api

user_folders_api = [
    url(r'^(?P<username>[\w@_.-]+)/structure/$',
        api.StructureFolders.as_view(), name='api_structure_folders'),
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.UserFolders.as_view(), name='api_user_folders_none'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/$',
        api.UserFolders.as_view(), name='api_user_folders_pk'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.UserFolders.as_view(), name='api_user_folders_pk_page'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.UserFolders.as_view(), name='api_user_folders_pk_page_items'),
]

manage_folder_detail_api = [
    url(r'^(?P<username>[\w@_.-]+)/add-new-to/(?P<parent>\w+)/$',
        api.UserFolderDetail.as_view(), name='api_user_folder_create_new'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/(?P<parent>\w+)/rename/$',
        api.UserFolderDetail.as_view(), name='api_user_folder_rename'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/$',
        api.UserFolderDetail.as_view(), name='api_user_folder_detail'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/delete/$',
        api.UserFolderDetail.as_view(), name='api_user_folder_delete'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/copy-to/(?P<parent>\w+)/$',
        api.UserFolderDetail.as_view(), name='api_user_folder_copy_to'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/move-to/(?P<parent>\w+)/$',
        api.UserFolderDetail.as_view(), name='api_user_folder_move_to'),
]

manage_folder_bookmark_api = [
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.FolderBookmark.as_view(), name='api_user_folder_bookmark_none'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/$',
        api.FolderBookmark.as_view(), name='api_user_folder_bookmark'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.FolderBookmark.as_view(), name='api_user_folder_bookmark_page'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.FolderBookmark.as_view(), name='api_user_folder_bookmark_page_items'),
]

manage_bookmarks_api = [
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.UserBookmarks.as_view(), name='api_manage_bookmarks'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/$',
        api.UserBookmarks.as_view(), name='api_manage_bookmarks_folder'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/page(?P<page>\d+)/$',
        api.UserBookmarks.as_view(), name='api_manage_bookmarks_folder_page'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.UserBookmarks.as_view(), name='api_manage_bookmarks_folder_page_items')
]

manage_bookmarks_import_api = [
    url(r'^(?P<username>[\w@_.-]+)/$',
        api.ImportBookmark.as_view(), name='api_manage_bookmarks_import'),
]

manage_bookmark_detail_api = [
    url(r'^(?P<pk>\w+)/$',
        api.BookmarkDetail.as_view(), name='api_user_bookmark'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/(?P<folder>\w+)/delete/$',
        api.BookmarkDetail.as_view(), name='api_user_bookmark_delete'),
    url(r'^(?P<username>[\w@_.-]+)/add-new-to/(?P<folder>\w+)/$',
        api.BookmarkDetail.as_view(), name='api_user_bookmark_create_new'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/(?P<folder>\w+)/rename/$',
        api.BookmarkDetail.as_view(), name='api_user_bookmark_rename'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/copy-to/(?P<folder>\w+)/$',
        api.BookmarkDetail.as_view(), name='api_user_bookmark_copy_to'),
    url(r'^(?P<username>[\w@_.-]+)/(?P<pk>\w+)/move-to/(?P<folder>\w+)/$',
        api.BookmarkDetail.as_view(), name='api_user_bookmark_move_to'),

    url(r'^(?P<pk>\w+)/likes/$',
        api.BookmarkLikes.as_view(), name='api_manage_bookmark_likes'),
    url(r'^(?P<pk>\w+)/likes/page(?P<page>\d+)/$',
        api.BookmarkLikes.as_view(), name='api_manage_bookmark_likes_page'),
    url(r'^(?P<pk>\w+)/likes/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.BookmarkLikes.as_view(), name='api_manage_bookmark_likes_page_items'),

    url(r'^(?P<pk>\w+)/dislikes/$',
        api.BookmarkDislikes.as_view(), name='api_manage_bookmark_dislikes'),
    url(r'^(?P<pk>\w+)/dislikes/page(?P<page>\d+)/$',
        api.BookmarkDislikes.as_view(), name='api_manage_bookmark_dislikes_page'),
    url(r'^(?P<pk>\w+)/dislikes/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.BookmarkDislikes.as_view(), name='api_manage_bookmark_dislikes_page_items'),

    url(r'^(?P<pk>\w+)/shares/$',
        api.BookmarkShares.as_view(), name='api_manage_bookmark_shares'),
    url(r'^(?P<pk>\w+)/shares/page(?P<page>\d+)/$',
        api.BookmarkShares.as_view(), name='api_manage_bookmark_shares_page'),
    url(r'^(?P<pk>\w+)/shares/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.BookmarkShares.as_view(), name='api_manage_bookmark_shares_page_items'),

    url(r'^(?P<pk>\w+)/views/$',
        api.BookmarkViews.as_view(), name='api_manage_bookmark_views'),
    url(r'^(?P<pk>\w+)/views/page(?P<page>\d+)/$',
        api.BookmarkViews.as_view(), name='api_manage_bookmark_views_page'),
    url(r'^(?P<pk>\w+)/views/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.BookmarkViews.as_view(), name='api_manage_bookmark_views_page_items'),

    url(r'^(?P<pk>\w+)/comments/$',
        api.BookmarkComments.as_view(), name='api_manage_bookmark_comments'),
    url(r'^(?P<pk>\w+)/comments/page(?P<page>\d+)/$',
        api.BookmarkComments.as_view(), name='api_manage_bookmark_comments_page'),
    url(r'^(?P<pk>\w+)/comments/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.BookmarkComments.as_view(), name='api_manage_bookmark_comments_page_items'),

    url(r'^(?P<pk>\w+)/tags/$',
        api.BookmarkTags.as_view(), name='api_manage_bookmark_tags'),
    url(r'^(?P<pk>\w+)/tags/page(?P<page>\d+)/$',
        api.BookmarkTags.as_view(), name='api_manage_bookmark_tags_page'),
    url(r'^(?P<pk>\w+)/tags/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.BookmarkTags.as_view(), name='api_manage_bookmark_tags_page_items'),
]

urlpatterns = patterns(
    '',
    url(r'^(?i)api/folders/', include(user_folders_api)),
    url(r'^(?i)api/folder/', include(manage_folder_detail_api)),
    url(r'^(?i)api/folderbookmark/', include(manage_folder_bookmark_api)),

    url(r'^(?i)api/importbookmarks/', include(manage_bookmarks_import_api)),
    url(r'^(?i)api/bookmarks/', include(manage_bookmarks_api)),
    url(r'^(?i)api/bookmark/', include(manage_bookmark_detail_api)),

    url(r'^(?i)api/addbookmark/(?P<username>[\w@_.-]+)/(?P<pk>\w+)/(?P<folder>\w+)/$',
        api.AddBookmark.as_view(), name='api_add_bookmark'),

    url(r'^(?i)api/selected/(?P<username>[\w@_.-]+)/delete/$',
        api.MultiSelectDelete.as_view(), name='api_selected_delete'),
    url(r'^(?i)api/selected/(?P<username>[\w@_.-]+)/copy-to/(?P<folder>\w+)/$',
        api.MultiSelectCopyTo.as_view(), name='api_selected_copy_to_folder'),
    url(r'^(?i)api/selected/(?P<username>[\w@_.-]+)/move-to/(?P<folder>\w+)/$',
        api.MultiSelectMoveTo.as_view(), name='api_selected_move_to_folder')
)


