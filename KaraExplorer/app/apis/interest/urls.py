from django.conf.urls import patterns, url, include
from . import api

interests_api = [
    url(r'^structure/$',
        api.Interests.as_view(), name='api_interests_tree'),
    url(r'^main/$',
        api.MainInterests.as_view(), name='api_main_interests'),
    url(r'^interest-(?P<name>\w+)/$',
        api.ChildInterests.as_view(), name='api_child_interests_name'),
    url(r'^key-(?P<key>\w+)/$',
        api.InterestWithKey.as_view(), name='api_interests_key'),
    url(r'^key-(?P<key>\w+)/page(?P<page>\d+)/$',
        api.InterestWithKey.as_view(), name='api_interests_key_page'),
    url(r'^key-(?P<key>\w+)/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.InterestWithKey.as_view(), name='api_interests_key_page_items'),
    url(r'^(?P<pk>\w+)/$',
        api.ChildInterests.as_view(), name='api_child_interests_pk'),
]

interest_api = [
    url(r'^(?P<pk>\w+)/$',
        api.InterestDetail.as_view(), name='api_interests_info'),
    url(r'^(?P<pk>\w+)/posts/$',
        api.InterestPosts.as_view(), name='api_interests_posts'),
    url(r'^(?P<pk>\w+)/posts/page(?P<page>\d+)/$',
        api.InterestPosts.as_view(), name='api_interests_posts_page'),
    url(r'^(?P<pk>\w+)/posts/page(?P<page>\d+)/items(?P<items>\d+)/$',
        api.InterestPosts.as_view(), name='api_interests_posts_page_items'),
    url(r'^(?P<username>[\w@_.-]+)/pin/(?P<pk>\w+)/$',
        api.UserPinInterest.as_view(), name='api_pin_interest'),
]

urlpatterns = patterns(
    '',
    url(r'^(?i)api/interests/', include(interests_api)),
    url(r'^(?i)api/interest/', include(interest_api)),
)


