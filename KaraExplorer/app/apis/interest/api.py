from django.db.models import Q
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from itertools import chain
from rest_framework.response import Response
from rest_framework.views import APIView
from app.apis.common.api import (update_status_statistic_for_list, )
from app.apis.serializers import *
from app.decorators import token_required
from app.errors import *
from app.utils import *


def create_or_update_interest(new):
    name = new.get('name', '')
    slug = new.get('slug', slugify(name))
    parent = new.get('parent', None)
    parent_id = new.get('parent_id', None)
    is_deleted = new.get('is_deleted', False)
    is_root = new.get('is_root', False)
    new_name = new.get('new_name', '')

    if type(is_deleted) is str:
        if is_deleted.lower() == 'true':
            is_deleted = True
        else:
            is_deleted = False
    if type(is_root) is str:
        if is_root.lower() == 'true':
            is_root = True
        else:
            is_root = False
    if name is not None and name == '':
        name = None
    if new_name is not None and new_name == '':
        new_name = None
    if parent is not None and parent == '':
        parent = None
    if parent_id is not None and (parent_id == '' or parent_id.lower() == 'none'):
        parent_id = None
    # print('INTEREST: ','name',name,'slug',slug,'parent',parent,'parent_id',parent_id,'is_deleted',is_deleted,'is_root',is_root,'new_name',new_name)

    if name:
        old_interest = Interest.objects.filter(Q(name=name))

        parent_interest = None
        if parent:
            try:
                parent_interest = Interest.objects.get(name=parent)
            except ObjectDoesNotExist:
                # print("Either the interest doesn't exist.")
                pass
        elif parent_id:
            try:
                pk = hasher.decodeNumber(parent_id)
                parent_interest = Interest.objects.get(pk=pk)
            except ObjectDoesNotExist:
                # print("Either the interest doesn't exist.")
                pass

        if parent_interest:
            parent_interest = parent_interest.id

        if old_interest.exists():
            item = old_interest.first()
            if new_name:
                old_interest.update(
                    name=new_name,
                    slug=slugify(new_name),
                    is_deleted=False,
                )
            elif item.slug != slug or item.is_deleted:
                old_interest.update(
                    name=name,
                    slug=slug,
                    is_deleted=False,
                )

            if is_root:
                old_interest.update(
                    parent=None
                )
            elif parent_interest:
                old_interest.update(
                    parent_id=parent_interest
                )

            if is_deleted:
                old_interest.update(
                    is_deleted=is_deleted
                )
            return item.id
        else:
            interest = Interest(name=name, slug=slug)
            try:
                Interest.objects.get(slug=slug)
            except ObjectDoesNotExist:
                interest.slug = slugify(name)

            if parent_interest:
                interest.parent_id = parent_interest

            interest.save()

            return interest.id


def get_interest_info(pk):
    if pk is None or pk.lower() == 'none':
        info = {"id": 'None', "name": "All", "slug": None, "parent": None, "parents": [], "thumbnail": None}
        return info

    try:
        pk = hasher.decodeNumber(pk)
    except ValueError:
        raise Http404

    interest = Interest.objects.filter(Q(is_deleted=False), Q(id=pk))
    interest = interest.extra(
        select={
            'parents': 'WITH RECURSIVE interests AS ('
                       '    SELECT root.id, root.name, root.slug, root.parent_id FROM {0} root WHERE root.id = {0}.id '
                       '    UNION ALL '
                       '    SELECT parent.id, parent.name, parent.slug, parent.parent_id '
                       '    FROM {0} parent INNER JOIN interests ON interests.parent_id = parent.id'
                       ') SELECT {1} FROM interests'
            .format(interest_db,
                    concatenate_rows(concatenate_fields(':', '{0}', '{1}', '{2}')
                                    .format('interests.id', 'interests.slug', 'interests.name', 'interests.thumbnail')))
        }
    )

    try:
        interest_detail = interest.first()
    except:
        raise Http404

    if interest_detail.parents is not None and interest_detail.parents != '':
        interest_detail.parents = interest_detail.parents.split(',')

        for idx, parent in enumerate(interest_detail.parents):
            attrs = parent.split(':')
            if len(attrs) == 3:
                interest_detail.parents[idx] = {
                    'id': hasher.encodeNumber(hasher.HASH_CATEGORY, attrs[0]),
                    'slug': attrs[1],
                    'name': attrs[2]
                }
            else:
                interest_detail.parents[idx] = None
        interest_detail.parents = [f for f in interest_detail.parents if f != None]

    serializer = InterestSerializer(
        interest_detail, many=False,
        fields=('id', 'name', 'slug', 'parent', 'parents'))
    return serializer.data


def import_posts(new):
    title = new['title']
    url = new['url']
    content = new['content']
    interest = new['interest']
    if title is not None and url is not None and interest is not None \
            and title != '' and url != '' and interest != '':
        old_bookmark = Bookmark.objects.filter(Q(title=title), Q(url=url))
        if old_bookmark.exists():
            bookmark = old_bookmark.first()
            if bookmark.is_deleted:
                old_bookmark.update(is_deleted=False)
        else:
            bookmark = Bookmark(title=title, url=url, slug=slugify(title))
            bookmark.save()

        post_interest = Interest.objects.filter(Q(name=interest)).first()
        old_post = Post.objects.filter(Q(source_id=bookmark.id))
        if old_post.exists():
            post = old_post.first()
            if post.is_deleted:
                old_post.update(is_deleted=False)
                old_post.interests.add(post_interest)
                old_post.save()
            if post.source_id is None:
                old_post.update(source_id=bookmark.id)
        else:
            post = Post(title=title, content=content, slug=slugify(title), source_id=bookmark.id,
                        published_on=timezone.now())
            post.save()
            post.interests.add(post_interest)
            post.save()


def get_tree_interests():
    interest = Interest.objects.filter(Q(is_deleted=False))
    interest = interest.extra(
        select={
            'num_post': 'SELECT COUNT(*) FROM {0} WHERE {0}.interest_id = {1}.id'.format(post_interest_db, interest_db)
        }
    )

    interest_list = list(interest)
    interest_tree, num_post = buildStructure(-1, interest_list, 0)

    serializer = InterestModelSerializer(
        interest_tree,
        many=True,
        fields=('id', 'name', 'slug', 'level', 'parent', 'num_post', 'children')
    )
    return serializer, num_post


def buildStructure(_parent, items_list, num_post):
    if _parent == -1:
        items = [entry for entry in items_list if (entry.parent_id is None)]
    else:
        items = [entry for entry in items_list if (entry.parent_id == _parent)]
    result = []
    num_post = num_post
    for item in items:
        children, total_post = buildStructure(item.id, items_list, item.num_post)
        new_model = InterestModel(item.hashed_id, item.name, item.slug, item.hashed_parent, children, total_post)
        result.append(new_model)
        num_post = num_post + total_post

    return result, num_post


class Interests(APIView):
    """
    POST    get tree
    """

    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None):
        # insert
        interests_new = request.data.get('interests', None)
        if interests_new is not None:
            for item in interests_new:
                create_or_update_interest(item)

        new_interest = request.data.get('interest', None)
        if new_interest is not None:
            create_or_update_interest(new_interest)

        # get
        serializer, num_post = get_tree_interests()

        return Response({
            'error': 'SUCCESS',
            'tree': serializer.data,
            'num_post': num_post
        })


class InterestDetail(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, pk=None):
        interest_info = get_interest_info(pk)
        return Response({
            'error': 'SUCCESS',
            'info': interest_info
        })


class InterestPosts(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, pk=None, page=1, items=10):
        if 'posts' in request.data:
            posts = request.data['posts']
            if posts is not None:
                for item in posts:
                    import_posts(item)

        if pk is None or pk.lower() == 'none':
            interests = InterestsOfUser.objects.filter(owner_id=request.user.id).values_list('interest_id', flat=True)
        else:
            try:
                pk = hasher.decodeNumber(pk)
            except ValueError:
                raise Http404

            interests_valuelist = InterestsOfUser.objects.filter(Q(owner_id=request.user.id), Q(interest__parent_id=pk))
            interests_valuelist = interests_valuelist.values_list('interest_id', flat=True)
            interests = [pk]
            for interest in interests_valuelist:
                interests.append(interest)

        posts = Post.objects.filter(Q(interests__id__in=interests))
        posts = posts.order_by('-published_on', 'title')

        posts = posts.extra(
            select={
                'username': 'SELECT {0}.username FROM {0} WHERE {0}.id = {1}.poster_id'.format(user_db, post_db),
                'source_bookmark': 'SELECT {2} FROM {0} WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, concatenate_fields('|', '{0}', '{1}')
                        .format('{0}.id'.format(bookmark_db), '{0}.url'.format(bookmark_db))),
                'source_feed': 'SELECT {3} FROM {0} LEFT OUTER JOIN {2} ON {0}.feed_source_id = {2}.id '
                               'WHERE {0}.id = {1}.source_id'
                .format(bookmark_db, post_db, source_db, concatenate_fields('|', '{0}', '{1}', '{2}', '{3}')
                        .format('{0}.id'.format(source_db), '{0}.name'.format(source_db), '{0}.url'.format(source_db),
                                '{0}.icon'.format(source_db))),
                'categories_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.category_id = {2}.id '
                                   'WHERE {0}.post_id = {1}.id'
                .format(post_category_db, post_db, category_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(category_db), '{0}.name'.format(category_db)))),
                'interests_list': 'SELECT {3} FROM {0} LEFT JOIN {2} ON {0}.interest_id = {2}.id '
                                  'WHERE {0}.post_id = {1}.id'
                .format(post_interest_db, post_db,interest_db,
                        concatenate_rows(concatenate_fields(':', '{0}', '{1}')
                                        .format('{0}.id'.format(interest_db), '{0}.name'.format(interest_db))))
            }
        )

        # posts = posts.exclude(interests=None)
        if request.user.is_authenticated():
            user_id = request.user.id
            user_actions = Action.objects.filter(Q(is_trash=False), Q(is_deleted=False), Q(person_id=user_id))
            user_views_post = user_actions.filter(Q(action=ACTION_VIEW), Q(content_type_id=content_type_of('post')))
            posts_viewed_list = user_views_post.values_list('object_id', flat=True)
            posts_viewed = posts.filter(Q(id__in=posts_viewed_list))
            posts_unread = posts.exclude(Q(id__in=posts_viewed_list))
            posts = list(chain(posts_unread, posts_viewed))

        itemsList, paginator = get_paginator(posts, items, page)

        itemsList.object_list = list(itemsList.object_list)
        for entry in itemsList.object_list:
            if entry.categories_list is not None:
                entry.categories_list = entry.categories_list.split(',')
                for idx, category_group in enumerate(entry.categories_list):
                    category_attrs = category_group.split(':')
                    if len(category_attrs) == 2:
                        entry.categories_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_CATEGORY, category_attrs[0]),
                            'name': category_attrs[1]
                        }
                    else:
                        entry.categories_list[idx] = None
                entry.categories_list = [f for f in entry.categories_list if f != None]

            if entry.interests_list is not None:
                entry.interests_list = entry.interests_list.split(',')
                for idx, group in enumerate(entry.interests_list):
                    attrs = group.split(':')
                    if len(attrs) == 2:
                        entry.interests_list[idx] = {
                            'id': hasher.encodeNumber(hasher.HASH_INTEREST, attrs[0]),
                            'name': attrs[1]
                        }
                    else:
                        entry.interests_list[idx] = None
                entry.interests_list = [f for f in entry.interests_list if f != None]

            if entry.thumbnails is not None and entry.thumbnails != '':
                entry.thumbnails = entry.thumbnails.split(',')
                for idx, thumb_id in enumerate(entry.thumbnails):
                    entry.thumbnails[idx] = hasher.encodeNumber(hasher.HASH_PHOTO, thumb_id) if thumb_id else None
            else:
                entry.thumbnails = None

            if entry.source_bookmark is not None and entry.source_bookmark != '':
                bookmark_infos = entry.source_bookmark.split('|')
                if len(bookmark_infos) == 2:
                    entry.source_bookmark = {
                        'id': hasher.encodeNumber(hasher.HASH_BOOKMARK, bookmark_infos[0]),
                        'url': bookmark_infos[1]
                    }
                else:
                    entry.source_bookmark = None
            else:
                entry.source_bookmark = None

            if entry.source_feed is not None and entry.source_feed != '':
                feed_info = entry.source_feed.split('|')
                if len(feed_info) == 4:
                    entry.source_feed = {
                        'id': hasher.encodeNumber(hasher.HASH_FEEDSOURCE, feed_info[0]),
                        'name': feed_info[1],
                        'url': feed_info[2],
                        'icon': hasher.encodeNumber(hasher.HASH_PHOTO, feed_info[3])
                    }
                else:
                    entry.source_feed = None
            else:
                entry.source_feed = None

        serializer = PostModelSerializer(itemsList.object_list, many=True)
        if request.user.is_authenticated():
            user_id = request.user.id
            update_status_statistic_for_list(user_id, 'post', serializer.data)

        return Response({
            'error': 'SUCCESS',
            'links': serializer.data,
            'paginator': {
                'unit': str(paginator.count) + ' link(s)',
                'totalItems': paginator.count,
                'itemsPerPage': int(items),
                'numPages': paginator.num_pages,
                'currentPage': int(page)
            }
        })


class MainInterests(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None):
        params = request.data
        main_interests = Interest.objects.filter(Q(is_deleted=False), Q(parent_id=None))

        main_interests_data = UserInterestParentSerializer(main_interests, many=True).data
        # for interest in main_interests_data:
        #     if interest['thumbnail']:
        #         id_hashed = hasher.encodeNumber(hasher.HASH_PHOTO, interest['thumbnail'])
        #         interest['thumbnail'] = id_hashed

        return Response({
            'error': 'SUCCESS',
            'params': params,
            'main_interests': main_interests_data,
        })


class ChildInterests(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, pk=None, name=None):
        params = request.data
        interests = Interest.objects.filter(Q(is_deleted=False), Q(parent_id=None))
        if name:
            interests = Interest.objects.filter(Q(is_deleted=False), Q(parent__name=name))
        else:
            try:
                interest_id = hasher.decodeNumber(pk) if pk else None
            except ValueError:
                interest_id = None
            if interest_id:
                interests = Interest.objects.filter(Q(is_deleted=False), Q(parent_id=interest_id))

        if request.user.is_authenticated():
            user_id = request.user.id
            interests = interests.filter(~Q(interestsofuser__is_deleted=False))

        interests = UserInterestParentSerializer(interests, many=True).data
        # for interest in interests:
        #     if interest['thumbnail']:
        #         id_hashed = hasher.encodeNumber(hasher.HASH_PHOTO, interest['thumbnail'])
        #         interest['thumbnail'] = id_hashed
        if request.user.is_authenticated():
            user_id = request.user.id
            update_status_statistic_for_list(user_id, 'interest', interests)
        return Response({
            'error': 'SUCCESS',
            'params': params,
            'interests': interests,
        })


def pin_interest(user_id, interest):
    try:
        old = InterestsOfUser.objects.get(Q(owner_id=user_id), Q(interest_id=interest))
        if old.is_deleted or old.is_trash:
            old.is_deleted = False
            old.is_trash = False
            old.save()
            return old.id
    except ObjectDoesNotExist:
        iou = InterestsOfUser(
            owner_id=user_id,
            interest_id=interest
        )
        iou.save()
        return iou.id
    except ValueError:
        return False


def trash_interest(user_id, interest):
    try:
        old = InterestsOfUser.objects.get(Q(owner_id=user_id), Q(interest_id=interest))
        old.is_trash = True
        old.save()
        return old.id
    except ObjectDoesNotExist:
        return False


class UserPinInterest(APIView):
    @csrf_exempt
    @token_required()
    def post(self, request, session=None, username=None, pk=None):
        if request.user.username != username:
            return Response({
                'error': AUTHENTICATION_UNPERMISSIONS,
                'message': "You don't have permission(s) to access this function"
            })
        params = request.data

        # user_id
        user_id = request.user.id

        try:
            interest_id = hasher.decodeNumber(pk)
        except ValueError:
            interest_id = None

        is_trash = request.data.get('is_trash', None)
        is_trash = (is_trash.lower() == 'true') if type(is_trash) is str else is_trash
        if is_trash:
            iou = trash_interest(user_id, interest_id) if interest_id else False
            return Response({
                'error': 'SUCCESS' if iou else 'FAILED',
                'params': params,
                'message': 'moved to trash' if iou else 'try again'
            })

        iou = pin_interest(user_id, interest_id) if interest_id else False

        return Response({
            'error': 'SUCCESS' if iou else 'FAILED',
            'params': params,
            'message': 'added to your list' if iou else 'try again'
        })


class InterestWithKey(APIView):
    @csrf_exempt
    @token_required(check_session=True, check_user=False, login_url=None)
    def post(self, request, session=None, key=None, page=1, items=10):
        params = request.data

        interests = Interest.objects.filter(Q(is_deleted=False), Q(is_trash=False), ~Q(parent_id=None))

        if request.user.is_authenticated():
            user_id = request.user.id
            iou = InterestsOfUser.objects.filter(Q(owner_id=user_id), Q(is_deleted=False), Q(is_trash=False))
            iou = iou.values_list('interest_id', flat=True)
            interests = interests.filter(~Q(id__in=iou))

        if key:
            interests = interests.order_by('parent__name', 'name')
            interests = interests.filter(Q(name__icontains=key) | Q(description__icontains=key))

        is_paginator = request.data.get('is_paginator', None)
        is_paginator = (is_paginator.lower() == 'true') if type(is_paginator) is str else is_paginator
        if is_paginator:
            interests, paginator = get_paginator(interests, items, page)

            interests = UserInterestSerializer(interests, many=True).data
            if request.user.is_authenticated():
                user_id = request.user.id
                update_status_statistic_for_list(user_id, 'interest', interests)

            return Response({
                'error': 'SUCCESS',
                'params': params,
                'interests': interests,
                'paginator': {
                    'unit': str(paginator.count) + ' source(s)',
                    'totalItems': paginator.count,
                    'itemsPerPage': int(items),
                    'numPages': paginator.num_pages,
                    'currentPage': int(page)
                }
            })
        else:
            interests = UserInterestSerializer(interests, many=True).data
            if request.user.is_authenticated():
                user_id = request.user.id
                update_status_statistic_for_list(user_id, 'interest', interests)
            return Response({
                'error': 'SUCCESS',
                'params': params,
                'interests': interests
            })


