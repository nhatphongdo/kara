from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    '',
    # url for common feature
    url(r'', include('app.apis.common.urls')),

    # url for feed section
    url(r'', include('app.apis.feed.urls')),

    # url for interest section
    url(r'', include('app.apis.interest.urls')),

    # url for manage section
    url(r'', include('app.apis.bookmark.urls')),

    # url for profile/people section
    url(r'', include('app.apis.profile.urls')),
)
