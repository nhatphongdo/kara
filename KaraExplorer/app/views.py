from django.conf import settings
from django.shortcuts import render
from django.template import RequestContext


def home(request):
    if request.user.is_authenticated():
        values = {
            'title': 'Homepage',
            'version': settings.VERSION,
            'user': request.user
        }
        return render(request,
                      'layout.html',
                      context_instance=RequestContext(request, values))
    else:
        values = {
            'title': 'Login',
            'version': settings.VERSION,
            'user': request.user
        }
        return render(request,
                      'index.html',
                      context_instance=RequestContext(request, values))


def layout_login(request):
    values = {}
    return render(request,
                  'login.html',
                  context_instance=RequestContext(request, values))


def empty_content(request):
    return render(request,
                  'layout/empty_content.html',
                  context_instance=RequestContext(request, {}))


def empty_sidebar(request):
    return render(request,
                  'layout/empty_sidebar.html',
                  context_instance=RequestContext(request, {}))


def layout_footer(request):
    return render(request,
                  'layout/footer.html',
                  context_instance=RequestContext(request, {}))


def layout_top_menu(request):
    if request.user.is_authenticated():
        return render(request,
                      'layout/top-menu.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_top_menu.html',
                      context_instance=RequestContext(request, {}))


def layout_right_sidebar(request):
    if request.user.is_authenticated():
        return render(request,
                      'layout/right-sidebar.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_sidebar.html',
                      context_instance=RequestContext(request, {}))


def layout_left_sidebar(request):
    if request.user.is_authenticated():
        return render(request,
                      'layout/left-sidebar.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_sidebar.html',
                      context_instance=RequestContext(request, {}))


def layout_feeds_sidebar(request):
    if request.user.is_authenticated():
        return render(request,
                      'layout/feeds-sidebar.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_sidebar.html',
                      context_instance=RequestContext(request, {}))


def layout_interests_sidebar(request):
    if request.user.is_authenticated():
        return render(request,
                      'layout/interests-sidebar.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_sidebar.html',
                      context_instance=RequestContext(request, {}))


def layout_people_sidebar(request):
    if request.user.is_authenticated():
        return render(request,
                      'layout/people-sidebar.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_sidebar.html',
                      context_instance=RequestContext(request, {}))


def web_screen_content(request):
    return render(request,
                  'popup_view/webscreen_content.html',
                  context_instance=RequestContext(request, {}))


def link_detail_content(request):
    return render(request,
                  'popup_view/linkdetail_content.html',
                  context_instance=RequestContext(request, {}))


"""
content view
"""


def main_view(request):
    if request.user.is_authenticated():
        return render(request,
                      'layout/main-view.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_content.html',
                      context_instance=RequestContext(request, {}))


def feed_view(request):
    if request.user.is_authenticated():
        return render(request,
                      'feeds/feed_view.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_content.html',
                      context_instance=RequestContext(request, {}))


def interest_view(request):
    if request.user.is_authenticated():
        return render(request,
                      'interests/interest_view.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_content.html',
                      context_instance=RequestContext(request, {}))


def manage_view(request):
    if request.user.is_authenticated():
        return render(request,
                      'manage/manage_view.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_content.html',
                      context_instance=RequestContext(request, {}))


def people_view(request):
    if request.user.is_authenticated():
        return render(request,
                      'people/people_view.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_content.html',
                      context_instance=RequestContext(request, {}))


def profile_view(request):
    if request.user.is_authenticated():
        return render(request,
                      'profile/profile_view.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_content.html',
                      context_instance=RequestContext(request, {}))


def search(request):
    if request.user.is_authenticated():
        return render(request,
                      'search/search_view.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_content.html',
                      context_instance=RequestContext(request, {}))


def profile_setting_view(request):
    if request.user.is_authenticated():
        return render(request,
                      'profile/profile-setting.html',
                      context_instance=RequestContext(request, {}))
    else:
        return render(request,
                      'layout/empty_content.html',
                      context_instance=RequestContext(request, {}))


"""
all popup dialog
"""


def addlink(request):
    return render(request,
                  'popup/addlink.html',
                  context_instance=RequestContext(request, {}))


def backup(request):
    return render(request,
                  'popup/backuponline.html',
                  context_instance=RequestContext(request, {}))


def bookmark(request):
    return render(request,
                  'popup/importbookmark.html',
                  context_instance=RequestContext(request, {}))


def collaborate(request):
    return render(request,
                  'popup/collaborate.html',
                  context_instance=RequestContext(request, {}))


def createfeedfolder(request):
    return render(request,
                  'popup/createfeedfolder.html',
                  context_instance=RequestContext(request, {}))


def createfolder(request):
    return render(request,
                  'popup/createfolder.html',
                  context_instance=RequestContext(request, {}))


def createlink(request):
    return render(request,
                  'popup/createlink.html',
                  context_instance=RequestContext(request, {}))


def delete(request):
    return render(request,
                  'popup/delete.html',
                  context_instance=RequestContext(request, {}))


def follow(request):
    return render(request,
                  'popup/follow.html',
                  context_instance=RequestContext(request, {}))


def privacy(request):
    return render(request,
                  'popup/privacy.html',
                  context_instance=RequestContext(request, {}))


def rename(request):
    return render(request,
                  'popup/rename.html',
                  context_instance=RequestContext(request, {}))


def rename_user_feed_source(request):
    return render(request,
                  'popup/rename_user_feedsource.html',
                  context_instance=RequestContext(request, {}))


def rename_user_category(request):
    return render(request,
                  'popup/rename_user_category.html',
                  context_instance=RequestContext(request, {}))


def create_user_category(request):
    return render(request,
                  'popup/create_user_category.html',
                  context_instance=RequestContext(request, {}))


def rename_bookmark(request):
    return render(request,
                  'popup/bookmarkrename.html',
                  context_instance=RequestContext(request, {}))


def search_popup(request):
    return render(request,
                  'popup/search.html',
                  context_instance=RequestContext(request, {}))


def share(request):
    return render(request,
                  'popup/share.html',
                  context_instance=RequestContext(request, {}))


def subscribe(request):
    return render(request,
                  'popup/subscribe.html',
                  context_instance=RequestContext(request, {}))


def tags(request):
    return render(request,
                  'popup/managetags.html',
                  context_instance=RequestContext(request, {}))


def add_tag(request):
    return render(request,
                  'popup/addtag.html',
                  context_instance=RequestContext(request, {}))


def rename_tag(request):
    return render(request,
                  'popup/rename_tag.html',
                  context_instance=RequestContext(request, {}))


def votedown(request):
    return render(request,
                  'popup/votedown.html',
                  context_instance=RequestContext(request, {}))


def show_static(request):
    return render(request,
                  'popup/show_static.html',
                  context_instance=RequestContext(request, {}))


def change_password(request):
    return render(request,
                  'popup/change_password.html',
                  context_instance=RequestContext(request, {}))


def reset_password(request):
    return render(request,
                  'popup/reset_password.html',
                  context_instance=RequestContext(request, {}))


def change_cover(request):
    return render(request,
                  'popup/change_cover.html',
                  context_instance=RequestContext(request, {}))


def select_dest_folder(request):
    return render(request,
                  'popup/select_dest_folder.html',
                  context_instance=RequestContext(request, {}))

"""
all popup view
"""


def linkdetail(request):
    return render(request,
                  'popup_view/linkdetail.html',
                  context_instance=RequestContext(request, {}))


def webscreen(request):
    return render(request,
                  'popup_view/webscreen.html',
                  context_instance=RequestContext(request, {}))


def add_browser_interests(request):
    return render(request,
                  'popup_view/add-browser-interests.html',
                  context_instance=RequestContext(request, {}))


def editfeed(request):
    return render(request,
                  'feeds/edit/edit-entry.html',
                  context_instance=RequestContext(request, {}))


"""
xenon view
"""


def manage_cards_view(request):
    return render(request,
                  'manage/manage-card.html',
                  context_instance=RequestContext(request, {}))


def manage_list_view(request):
    return render(request,
                  'manage/manage-list.html',
                  context_instance=RequestContext(request, {}))


def manage_tag(request):
    return render(request,
                  'manage/manage-tag_cloud.html',
                  context_instance=RequestContext(request, {}))


def manage_content_tag(request):
    return render(request,
                  'manage/manage-tag_cloud.html',
                  context_instance=RequestContext(request, {}))


def feed_manage_view(request):
    return render(request,
                  'feeds/feed_manage_view.html',
                  context_instance=RequestContext(request, {}))


def interest_manage_view(request):
    return render(request,
                  'interests/interest_manage_view.html',
                  context_instance=RequestContext(request, {}))


def feed_cards_view(request):
    return render(request,
                  'feeds/feed-card.html',
                  context_instance=RequestContext(request, {}))


def feed_list_view(request):
    return render(request,
                  'feeds/feed-list.html',
                  context_instance=RequestContext(request, {}))


def feed_post_cards_view(request):
    return render(request,
                  'feeds/feed-post-card.html',
                  context_instance=RequestContext(request, {}))


def feed_post_list_view(request):
    return render(request,
                  'feeds/feed-post-list.html',
                  context_instance=RequestContext(request, {}))


def interest_cards_view(request):
    return render(request,
                  'interests/interest-card.html',
                  context_instance=RequestContext(request, {}))


def interest_list_view(request):
    return render(request,
                  'interests/interest-list.html',
                  context_instance=RequestContext(request, {}))


def add_people_view(request):
    return render(request,
                  'people/add-people.html',
                  context_instance=RequestContext(request, {}))


def manage_people_view(request):
    return render(request,
                  'people/people_manage_view.html',
                  context_instance=RequestContext(request, {}))


"""
include template
"""


def bookmark_card(request):
    return render(request,
                  'layout/include/bookmark-card.html',
                  context_instance=RequestContext(request, {}))


def persons_list(request):
    return render(request,
                  'people/friend-requested.html',
                  context_instance=RequestContext(request, {}))


def person_list_request_sent(request):
    return render(request,
                  'people/friend-request-sent.html',

                  context_instance=RequestContext(request, {}))


def manage_people_folder(request):
    return render(request,
                  'people/manage-people-folder.html',
                  context_instance=RequestContext(request, {}))


def manage_following_list(request):
    return render(request,
                  'people/manage-following.html',
                  context_instance=RequestContext(request, {}))


def people_card(request):
    return render(request,
                  'people/people-card.html',
                  context_instance=RequestContext(request, {}))


def people_list(request):
    return render(request,
                  'people/people-list.html',
                  context_instance=RequestContext(request, {}))


def people_post_card(request):
    return render(request,
                  'people/people-post-card.html',
                  context_instance=RequestContext(request, {}))


def people_post_list(request):
    return render(request,
                  'people/people-post-list.html',
                  context_instance=RequestContext(request, {}))


def bookmark_list(request):
    return render(request,
                  'layout/include/bookmark-list.html',
                  context_instance=RequestContext(request, {}))


def folder_card(request):
    return render(request,
                  'layout/include/folder-card.html',
                  context_instance=RequestContext(request, {}))


def folder_list(request):
    return render(request,
                  'layout/include/folder-list.html',
                  context_instance=RequestContext(request, {}))


def feed_card(request):
    return render(request,
                  'layout/include/feed-card.html',
                  context_instance=RequestContext(request, {}))


def feed_list(request):
    return render(request,
                  'layout/include/feed-list.html',
                  context_instance=RequestContext(request, {}))


def feed_post_card(request):
    return render(request,
                  'layout/include/feed-post-card.html',
                  context_instance=RequestContext(request, {}))


def feed_post_list(request):
    return render(request,
                  'layout/include/feed-post-list.html',
                  context_instance=RequestContext(request, {}))


def feed_popup_post_list(request):
    return render(request,
                  'popup/feed-popup/feed-list.html',
                  context_instance=RequestContext(request, {}))


def interest_card(request):
    return render(request,
                  'layout/include/interest-card.html',
                  context_instance=RequestContext(request, {}))


def interest_list(request):
    return render(request,
                  'layout/include/interest-list.html',
                  context_instance=RequestContext(request, {}))


def interest_post_card(request):
    return render(request,
                  'layout/include/interest-post-card.html',
                  context_instance=RequestContext(request, {}))


def interest_post_list(request):
    return render(request,
                  'layout/include/interest-post-list.html',
                  context_instance=RequestContext(request, {}))


def feed_source_list(request):
    return render(request,
                  'layout/include/feed-source-list.html',
                  context_instance=RequestContext(request, {}))


def search_all(request):
    return render(request,
                  'search/search_all.html',
                  context_instance=RequestContext(request, {}))


def search_web(request):
    return render(request,
                  'search/search_web.html',
                  context_instance=RequestContext(request, {}))


def search_interest(request):
    return render(request,
                  'search/search_interest.html',
                  context_instance=RequestContext(request, {}))


def search_bookmark(request):
    return render(request,
                  'search/search_bookmark.html',
                  context_instance=RequestContext(request, {}))


def search_post(request):
    return render(request,
                  'search/search_post.html',
                  context_instance=RequestContext(request, {}))


"""
templates of feed
"""


def tpl_cards(request):
    return render(request,
                  'feeds/cardview.html',
                  context_instance=RequestContext(request, {}))


def tpl_add_feed_source(request):
    return render(request,
                  'feeds/add-feed.html',
                  context_instance=RequestContext(request, {}))


"""
templates of interest
"""


def tpl_add_interest(request):
    return render(request,
                  'interests/add-interests.html',
                  context_instance=RequestContext(request, {}))


"""
templates of manage
"""


def tpl_tagcloud(request):
    return render(request,
                  'manage/tagcloud.html',
                  context_instance=RequestContext(request, {}))


def tpl_detectbrokenlink(request):
    return render(request,
                  'manage/detectbrokenlink.html',
                  context_instance=RequestContext(request, {}))


def tpl_detectduplicationlink(request):
    return render(request,
                  'manage/detectduplicationlink.html',
                  context_instance=RequestContext(request, {}))


"""
templates of people
"""


def tpl_wall(request):
    return render(request,
                  'profile/tabs/profile_wall.html',
                  context_instance=RequestContext(request, {}))


def tpl_folder(request):
    return render(request,
                  'profile/tabs/profile_folder.html',
                  context_instance=RequestContext(request, {}))


def tpl_link(request):
    return render(request,
                  'profile/tabs/profile_link.html',
                  context_instance=RequestContext(request, {}))


def tpl_web(request):
    return render(request,
                  'profile/tabs/profile_web.html',
                  context_instance=RequestContext(request, {}))


def tpl_interest(request):
    return render(request,
                  'profile/tabs/profile_interest.html',
                  context_instance=RequestContext(request, {}))


def tpl_people(request):
    return render(request,
                  'profile/tabs/profile_people.html',
                  context_instance=RequestContext(request, {}))


def tpl_dashboard(request):
    return render(request,
                  'profile/tabs/profile_dashboard.html',
                  context_instance=RequestContext(request, {}))


