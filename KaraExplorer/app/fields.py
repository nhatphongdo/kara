from django.core import exceptions
from django.conf import settings
from django.db.models import fields, CharField
from django.utils import timezone


def concatenate_rows(field, separator=','):
    if settings.DATABASES['default']['ENGINE'].endswith('mysql'):
        sql = "GROUP_CONCAT({0} SEPARATOR '" + separator + "')"
        return sql.format(field)
    elif settings.DATABASES['default']['ENGINE'].endswith('oracle'):
        sql = "LISTAGG({0}, '" + separator + "') WITHIN GROUP (ORDER BY {0})"
        return sql.format(field)
    elif settings.DATABASES['default']['ENGINE'].endswith('postgres') \
            or settings.DATABASES['default']['ENGINE'].endswith('postgresql_psycopg2'):
        sql = "STRING_AGG({0}, '" + separator + "')"
        return sql.format(field)
    elif settings.DATABASES['default']['ENGINE'].endswith('sqlite3'):
        sql = "GROUP_CONCAT({0})"
        return sql.format(field)
    else:
        raise NotImplemented


def concatenate_fields(separator, *args):
    if settings.DATABASES['default']['ENGINE'].endswith('mysql'):
        sql = 'CONCAT('
        for idx, arg in enumerate(args):
            if idx < len(args) - 1:
                sql += arg + ", '" + separator + "', "
            else:
                sql += arg

        sql += ')'
        return sql
    elif settings.DATABASES['default']['ENGINE'].endswith('oracle'):
        sql = ''
        for idx, arg in enumerate(args):
            if idx < len(args) - 1:
                sql += arg + " || '" + separator + "' ||"
            else:
                sql += arg

        return sql
    elif settings.DATABASES['default']['ENGINE'].endswith('postgres') or settings.DATABASES['default'][
        'ENGINE'].endswith('postgresql_psycopg2'):
        sql = ''
        for idx, arg in enumerate(args):
            if idx < len(args) - 1:
                sql += arg + " || '" + separator + "' ||"
            else:
                sql += arg

        return sql
    elif settings.DATABASES['default']['ENGINE'].endswith('sqlite3'):
        sql = ''
        for idx, arg in enumerate(args):
            if idx < len(args) - 1:
                sql += arg + " || '" + separator + "' ||"
            else:
                sql += arg

        return sql
    else:
        raise NotImplemented


def avatar_filename(instance, filename):
    now = timezone.now()
    return '/'.join(['avatar', instance.user.username, now.strftime('%Y'), now.strftime('%m'), now.strftime('%d'),
                     now.strftime('%H%M%S.%f') + '_' + filename])


def upload_file_filename(instance, filename):
    now = timezone.now()
    return '/'.join([instance.owner.user.username, now.strftime('%Y'), now.strftime('%m'), now.strftime('%d'),
                     now.strftime('%H%M%S.%f') + '_' + filename])


def feed_source_icon_filename(instance, filename):
    now = timezone.now()
    return '/'.join(['feed_source_icons', instance.id, now.strftime('%Y'), now.strftime('%m'), now.strftime('%d'),
                     now.strftime('%H%M%S.%f') + '_' + filename])


def feed_source_thumbnail_filename(instance, filename):
    now = timezone.now()
    return '/'.join(['feed_sources', instance.id, now.strftime('%Y'), now.strftime('%m'), now.strftime('%d'),
                     now.strftime('%H%M%S.%f') + '_' + filename])


def feed_source_cover_filename(instance, filename):
    now = timezone.now()
    return '/'.join(['feed_sources_cover', instance.id, now.strftime('%Y'), now.strftime('%m'), now.strftime('%d'),
                     now.strftime('%H%M%S.%f') + '_' + filename])


def interest_thumbnail_filename(instance, filename):
    now = timezone.now()
    return '/'.join(['interests', instance.id, now.strftime('%Y'), now.strftime('%m'), now.strftime('%d'),
                     now.strftime('%H%M%S.%f') + '_' + filename])


def photo_filename(instance, filename):
    now = timezone.now()
    return '/'.join(['photos', instance.owner.user.username, now.strftime('%Y'), now.strftime('%m'), now.strftime('%d'),
                     now.strftime('%H%M%S.%f') + '_' + filename])


# Gender choices
GENDER_MALE = 1
GENDER_FEMALE = 2
GENDER_OTHER = 3
GENDER_CHOICE = (
    (GENDER_MALE, 'Male'),
    (GENDER_FEMALE, 'Female'),
    (GENDER_OTHER, 'Other'),
)


# Message statuses
MESSAGE_UNREAD = 1
MESSAGE_READ = 2
MESSAGE_DELETED = 3
MESSAGE_IMPORTANT = 4
MESSAGE_STATUSES = (
    (MESSAGE_UNREAD, 'Unread'),
    (MESSAGE_READ, 'Read'),
    (MESSAGE_DELETED, 'Deleted'),
    (MESSAGE_IMPORTANT, 'Important'),
)


# FriendsGroup statuses
GROUP_FRIEND = 1
GROUP_FOLLOWING = 2
GROUP_STATUSES = (
    (GROUP_FRIEND, 'Friend'),
    (GROUP_FOLLOWING, 'Following'),
)


# Relationship statuses
RELATIONSHIP_FRIEND_REQUESTED = 1
RELATIONSHIP_FRIEND = 2
RELATIONSHIP_BLOCKED = 3
RELATIONSHIP_FOLLOWING = 4
RELATIONSHIP_STATUSES = (
    (RELATIONSHIP_FRIEND_REQUESTED, 'Friend requested'),
    (RELATIONSHIP_FRIEND, 'Friend'),
    (RELATIONSHIP_BLOCKED, 'Blocked'),
    (RELATIONSHIP_FOLLOWING, 'Following'),
)


# Action types
ACTION_UNDEFINED = 0
ACTION_SHARE = 1
ACTION_LIKE = 2
ACTION_DISLIKE = 3
ACTION_VIEW = 4
ACTION_DOWNLOAD = 5
ACTION_FAVOURITE = 6
ACTION_SHARE_SOCIAL = 7
ACTION_UNLIKE = 8
ACTION_UN_FAVOURITE = 9
ACTION_TYPES = (
    (ACTION_UNDEFINED, 'Undefined'),
    (ACTION_SHARE, 'Share'),
    (ACTION_SHARE_SOCIAL, 'Share Social'),
    (ACTION_LIKE, 'Like'),
    (ACTION_DISLIKE, 'Dislike'),
    (ACTION_UNLIKE, 'Unlike'),
    (ACTION_VIEW, 'View'),
    (ACTION_DOWNLOAD, 'Download'),
    (ACTION_FAVOURITE, 'Favourite'),
    (ACTION_UN_FAVOURITE, 'Un-Favourite'),
)


# Bookmark statuses
BOOKMARK_NONE = 0
BOOKMARK_FAVOURITE = 1
BOOKMARK_IMPORTANT = 2
BOOKMARK_STATUSES = (
    (BOOKMARK_NONE, 'None'),
    (BOOKMARK_FAVOURITE, 'Favourite'),
    (BOOKMARK_IMPORTANT, 'Important'),
)


# Share actions
SHARE_NONE = 0
SHARE_ACTIONS = (
    (SHARE_NONE, 'None'),
)

# Share permissions
SHARE_READ = 1
SHARE_WRITE = 2
SHARE_ADMIN = 3
SHARE_PUBLIC = 4
SHARE_PERMISSIONS = (
    (SHARE_READ, 'Read'),
    (SHARE_WRITE, 'Write'),
    (SHARE_ADMIN, 'Admin'),
    (SHARE_PUBLIC, 'Public'),
)

SESSION_VALID = 0
SESSION_EXPIRED = 1
SESSION_NOT_EXIST = 2
SESSION_WRONG_IDENTITY = 3

LANGUAGES = (
    ('ab', 'Abkhaz - аҧсуа'),
    ('aa', 'Afar - Afaraf'),
    ('af', 'Afrikaans - Afrikaans'),
    ('ak', 'Akan - Akan'),
    ('sq', 'Albanian - Shqip'),
    ('am', 'Amharic - አማርኛ'),
    ('ar', 'Arabic - العربية'),
    ('an', 'Aragonese - Aragonés'),
    ('hy', 'Armenian - Հայերեն'),
    ('as', 'Assamese - অসমীয়া'),
    ('av', 'Avaric - авар мацӀ, магӀарул мацӀ'),
    ('ae', 'Avestan - avesta'),
    ('ay', 'Aymara - aymar aru'),
    ('az', 'Azerbaijani - azərbaycan dili'),
    ('bm', 'Bambara - bamanankan'),
    ('ba', 'Bashkir - башҡорт теле'),
    ('eu', 'Basque - euskara, euskera'),
    ('be', 'Belarusian - Беларуская'),
    ('bn', 'Bengali - বাংলা'),
    ('bh', 'Bihari - भोजपुरी'),
    ('bi', 'Bislama - Bislama'),
    ('bs', 'Bosnian - bosanski jezik'),
    ('br', 'Breton - brezhoneg'),
    ('bg', 'Bulgarian - български език'),
    ('my', 'Burmese - ဗမာစာ'),
    ('ca', 'Catalan; Valencian - Català'),
    ('ch', 'Chamorro - Chamoru'),
    ('ce', 'Chechen - нохчийн мотт'),
    ('ny', 'Chichewa; Chewa; Nyanja - chiCheŵa, chinyanja'),
    ('zh', 'Chinese - 中文 (Zhōngwén), 汉语, 漢語'),
    ('cv', 'Chuvash - чӑваш чӗлхи'),
    ('kw', 'Cornish - Kernewek'),
    ('co', 'Corsican - corsu, lingua corsa'),
    ('cr', 'Cree - ᓀᐦᐃᔭᐍᐏᐣ'),
    ('hr', 'Croatian - hrvatski'),
    ('cs', 'Czech - česky, čeština'),
    ('da', 'Danish - dansk'),
    ('dv', 'Divehi; Dhivehi; Maldivian; - ދިވެހި'),
    ('nl', 'Dutch - Nederlands, Vlaams'),
    ('en', 'English - English'),
    ('eo', 'Esperanto - Esperanto'),
    ('et', 'Estonian - eesti, eesti keel'),
    ('ee', 'Ewe - Eʋegbe'),
    ('fo', 'Faroese - føroyskt'),
    ('fj', 'Fijian - vosa Vakaviti'),
    ('fi', 'Finnish - suomi, suomen kieli'),
    ('fr', 'French - français, langue française'),
    ('ff', 'Fula; Fulah; Pulaar; Pular - Fulfulde, Pulaar, Pular'),
    ('gl', 'Galician - Galego'),
    ('ka', 'Georgian - ქართული'),
    ('de', 'German - Deutsch'),
    ('el', 'Greek, Modern - Ελληνικά'),
    ('gn', 'Guaraní - Avañeẽ'),
    ('gu', 'Gujarati - ગુજરાતી'),
    ('ht', 'Haitian; Haitian Creole - Kreyòl ayisyen'),
    ('ha', 'Hausa - Hausa, هَوُسَ'),
    ('he', 'Hebrew (modern) - עברית'),
    ('hz', 'Herero - Otjiherero'),
    ('hi', 'Hindi - हिन्दी, हिंदी'),
    ('ho', 'Hiri Motu - Hiri Motu'),
    ('hu', 'Hungarian - Magyar'),
    ('ia', 'Interlingua - Interlingua'),
    ('id', 'Indonesian - Bahasa Indonesia'),
    ('ie', 'Interlingue - Originally called Occidental; then Interlingue after WWII'),
    ('ga', 'Irish - Gaeilge'),
    ('ig', 'Igbo - Asụsụ Igbo'),
    ('ik', 'Inupiaq - Iñupiaq, Iñupiatun'),
    ('io', 'Ido - Ido'),
    ('is', 'Icelandic - Íslenska'),
    ('it', 'Italian - Italiano'),
    ('iu', 'Inuktitut - ᐃᓄᒃᑎᑐᑦ'),
    ('ja', 'Japanese - 日本語 (にほんご／にっぽんご)'),
    ('jv', 'Javanese - basa Jawa'),
    ('kl', 'Kalaallisut, Greenlandic - kalaallisut, kalaallit oqaasii'),
    ('kn', 'Kannada - ಕನ್ನಡ'),
    ('kr', 'Kanuri - Kanuri'),
    ('ks', 'Kashmiri - कश्मीरी, كشميري‎'),
    ('kk', 'Kazakh - Қазақ тілі'),
    ('km', 'Khmer - ភាសាខ្មែរ'),
    ('ki', 'Kikuyu, Gikuyu - Gĩkũyũ'),
    ('rw', 'Kinyarwanda - Ikinyarwanda'),
    ('ky', 'Kirghiz, Kyrgyz - кыргыз тили'),
    ('kv', 'Komi - коми кыв'),
    ('kg', 'Kongo - KiKongo'),
    ('ko', 'Korean - 한국어 (韓國語), 조선말 (朝鮮語)'),
    ('ku', 'Kurdish - Kurdî, كوردی‎'),
    ('kj', 'Kwanyama, Kuanyama - Kuanyama'),
    ('la', 'Latin - latine, lingua latina'),
    ('lb', 'Luxembourgish, Letzeburgesch - Lëtzebuergesch'),
    ('lg', 'Luganda - Luganda'),
    ('li', 'Limburgish, Limburgan, Limburger - Limburgs'),
    ('ln', 'Lingala - Lingála'),
    ('lo', 'Lao - ພາສາລາວ'),
    ('lt', 'Lithuanian - lietuvių kalba'),
    ('lu', 'Luba-Katanga - '),
    ('lv', 'Latvian - latviešu valoda'),
    ('gv', 'Manx - Gaelg, Gailck'),
    ('mk', 'Macedonian - македонски јазик'),
    ('mg', 'Malagasy - Malagasy fiteny'),
    ('ms', 'Malay - bahasa Melayu, بهاس ملايو‎'),
    ('ml', 'Malayalam - മലയാളം'),
    ('mt', 'Maltese - Malti'),
    ('mi', 'Māori - te reo Māori'),
    ('mr', 'Marathi (Marāṭhī) - मराठी'),
    ('mh', 'Marshallese - Kajin M̧ajeļ'),
    ('mn', 'Mongolian - монгол'),
    ('na', 'Nauru - Ekakairũ Naoero'),
    ('nv', 'Navajo, Navaho - Diné bizaad, Dinékʼehǰí'),
    ('nb', 'Norwegian Bokmål - Norsk bokmål'),
    ('nd', 'North Ndebele - isiNdebele'),
    ('ne', 'Nepali - नेपाली'),
    ('ng', 'Ndonga - Owambo'),
    ('nn', 'Norwegian Nynorsk - Norsk nynorsk'),
    ('no', 'Norwegian - Norsk'),
    ('ii', 'Nuosu - ꆈꌠ꒿ Nuosuhxop'),
    ('nr', 'South Ndebele - isiNdebele'),
    ('oc', 'Occitan - Occitan'),
    ('oj', 'Ojibwe, Ojibwa - ᐊᓂᔑᓈᐯᒧᐎᓐ'),
    ('cu', 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic - ѩзыкъ словѣньскъ'),
    ('om', 'Oromo - Afaan Oromoo'),
    ('or', 'Oriya - ଓଡ଼ିଆ'),
    ('os', 'Ossetian, Ossetic - ирон æвзаг'),
    ('pa', 'Panjabi, Punjabi - ਪੰਜਾਬੀ, پنجابی‎'),
    ('pi', 'Pāli - पाऴि'),
    ('fa', 'Persian - فارسی'),
    ('pl', 'Polish - polski'),
    ('ps', 'Pashto, Pushto - پښتو'),
    ('pt', 'Portuguese - Português'),
    ('qu', 'Quechua - Runa Simi, Kichwa'),
    ('rm', 'Romansh - rumantsch grischun'),
    ('rn', 'Kirundi - kiRundi'),
    ('ro', 'Romanian, Moldavian, Moldovan - română'),
    ('ru', 'Russian - русский язык'),
    ('sa', 'Sanskrit (Saṁskṛta) - संस्कृतम्'),
    ('sc', 'Sardinian - sardu'),
    ('sd', 'Sindhi - सिन्धी, سنڌي، سندھی‎'),
    ('se', 'Northern Sami - Davvisámegiella'),
    ('sm', 'Samoan - gagana faa Samoa'),
    ('sg', 'Sango - yângâ tî sängö'),
    ('sr', 'Serbian - српски језик'),
    ('gd', 'Scottish Gaelic; Gaelic - Gàidhlig'),
    ('sn', 'Shona - chiShona'),
    ('si', 'Sinhala, Sinhalese - සිංහල'),
    ('sk', 'Slovak - slovenčina'),
    ('sl', 'Slovene - slovenščina'),
    ('so', 'Somali - Soomaaliga, af Soomaali'),
    ('st', 'Southern Sotho - Sesotho'),
    ('es', 'Spanish; Castilian - español, castellano'),
    ('su', 'Sundanese - Basa Sunda'),
    ('sw', 'Swahili - Kiswahili'),
    ('ss', 'Swati - SiSwati'),
    ('sv', 'Swedish - svenska'),
    ('ta', 'Tamil - தமிழ்'),
    ('te', 'Telugu - తెలుగు'),
    ('tg', 'Tajik - тоҷикӣ, toğikī, تاجیکی‎'),
    ('th', 'Thai - ไทย'),
    ('ti', 'Tigrinya - ትግርኛ'),
    ('bo', 'Tibetan Standard, Tibetan, Central - བོད་ཡིག'),
    ('tk', 'Turkmen - Türkmen, Түркмен'),
    ('tl', 'Tagalog - Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔'),
    ('tn', 'Tswana - Setswana'),
    ('to', 'Tonga (Tonga Islands) - faka Tonga'),
    ('tr', 'Turkish - Türkçe'),
    ('ts', 'Tsonga - Xitsonga'),
    ('tt', 'Tatar - татарча, tatarça, تاتارچا‎'),
    ('tw', 'Twi - Twi'),
    ('ty', 'Tahitian - Reo Tahiti'),
    ('ug', 'Uighur, Uyghur - Uyƣurqə, ئۇيغۇرچە‎'),
    ('uk', 'Ukrainian - українська'),
    ('ur', 'Urdu - اردو'),
    ('uz', 'Uzbek - zbek, Ўзбек, أۇزبېك‎'),
    ('ve', 'Venda - Tshivenḓa'),
    ('vi', 'Vietnamese - Tiếng Việt'),
    ('vo', 'Volapük - Volapük'),
    ('wa', 'Walloon - Walon'),
    ('cy', 'Welsh - Cymraeg'),
    ('wo', 'Wolof - Wollof'),
    ('fy', 'Western Frisian - Frysk'),
    ('xh', 'Xhosa - isiXhosa'),
    ('yi', 'Yiddish - ייִדיש'),
    ('yo', 'Yoruba - Yorùbá'),
    ('za', 'Zhuang, Chuang - Saɯ cueŋƅ, Saw cuengh')
)


# Big Auto Field
class BigAutoField(fields.AutoField):
    def db_type(self, connection):
        if settings.DATABASES['default']['ENGINE'].endswith('mysql'):
            return "bigint AUTO_INCREMENT"
        elif settings.DATABASES['default']['ENGINE'].endswith('oracle'):
            return "NUMBER(19)"
        elif settings.DATABASES['default']['ENGINE'].endswith('postgres') \
                or settings.DATABASES['default']['ENGINE'].endswith('postgresql_psycopg2'):
            return "serial"
        elif settings.DATABASES['default']['ENGINE'].endswith('sqlite3'):
            return "integer"
        else:
            raise NotImplemented

    def get_internal_type(self):
        return "BigAutoField"

    def to_python(self, value):
        if value is None:
            return value
        try:
            return int(value)
        except (TypeError, ValueError):
            raise exceptions.ValidationError(("This value must be a long integer."))


class LanguageField(CharField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 5)
        kwargs.setdefault('choices', LANGUAGES)
        super(CharField, self).__init__(*args, **kwargs)

    def get_internal_type(self):
        return "CharField"


