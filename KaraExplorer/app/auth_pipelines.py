import uuid
import mandrill
import requests
from datetime import timedelta
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from requests import HTTPError
from .models import Profile, Session
from .utils import gender_id


def create_profile(strategy, details, backend, response, uid, user=None, *args, **kwargs):
    # print('uid: ', uid)
    # print('is_new: ', kwargs.get('is_new'))
    # print('new_association: ', kwargs.get('new_association'))
    # print('details: ', details)
    # print('response: ', response)
    avatar_default = 'avatar/default/1.jpg'
    api = 'http://static.karaplus.com/api/upload_url'
    url = None
    avatar = None
    # print('backend: ', backend.name)
    if backend.name == 'facebook':
        # print('facebook: ', backend.name == 'facebook')
        url = "http://graph.facebook.com/%s/picture?type=large" % uid
        # print('url: ', url)
    if backend.name == 'google-oauth2':
        # print('facebook: ', backend.name == 'google-oauth2')
        url = response['image'].get('url').split('?')[0]
        # print('url: ', url)
    if backend.name == 'twitter':
        # print('facebook: ', backend.name == 'twitter')
        url = response.get('profile_image_url', '').replace('_normal', '')
        # print('url: ', url)

    if kwargs.get('is_new'):
        try:
            # print('will create profile!!!!!!')
            profile = Profile.objects.create(user=user, nick_name=user.username)
            if url:
                # try:
                #     avatar = requests.get(url, params={'type': 'large'})
                #     avatar.raise_for_status()
                # except HTTPError:
                #     pass
                # else:
                #     profile.avatar.save('{0}_{1}.jpg'.format(user.username, backend.name), ContentFile(avatar.content))
                #     profile.save()
                try:
                    avatar = requests.post(api, params={'type': 'avatar', 'file': url})
                    avatar.raise_for_status()
                    avatar_json = avatar.json()
                    thumb_id = 0
                    if avatar_json:
                        thumb_id = avatar_json.get('image').get('id')
                except HTTPError:
                    pass
                else:
                    profile.avatar = str(thumb_id)
                    profile.save()

            gender = language = birthday = None
            if backend.name == 'facebook':
                gender = gender_id(response['gender'])
                language = response['locale']
                birthday = response['birthday']
            if backend.name == 'google-oauth2':
                gender = gender_id(response.get('gender', 'other'))
                language = response['language']

            if gender and language and birthday:
                profile.gender = gender
                profile.language = language
                # profile.birthday = birthday
                profile.save()

            # create new session for this user
            session = Session(
                owner_id=user.id,
                session_key=uuid.uuid4()
            )
            session.expire_time = timezone.now() + timedelta(minutes=settings.SHORT_PERIOD_SESSION)
            session.save()

            mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
            template_content = [{'name': 'editable', 'content': 'KARA'}]
            message = {
                'to': [{'email': user.email,
                       'name': user.first_name + ' ' + user.last_name,
                       'type': 'to'}],
                'track_clicks': True,
                'track_opens': True,
                'merge_vars': [{'rcpt': user.email,
                               'vars': [{'name': 'FRIEND', 'content': user.first_name + ' ' + user.last_name}]}],
                'global_merge_vars': [{'name': 'SESSIONKEY', 'content': 'session_key'},
                                      {'name': 'EMAIL', 'content': 'user.email'},
                                      {'name': 'KARA', 'content': 'www.karaplus.com'}],

                'metadata': {'website': 'www.karaplus.com'},
                'recipient_metadata': [{'rcpt': user.email,
                                       'values': {'user_id': user.id}}],
                'tags': ['welcome']
            }
            result = mandrill_client.messages.send_template(template_name='welcome-mail',
                                                            template_content=template_content,
                                                            message=message,
                                                            async=False)
        except mandrill.Error as e:
            print('A mandrill error occurred: %s - %s' % (e.__class__, e))
        except ObjectDoesNotExist:
            return False
    else:
        try:
            profile = Profile.objects.get(user=user)
            # print('avatar/default/1.jpg' is profile.avatar, profile.avatar, profile.avatar == 'avatar/default/1.jpg')
            if 0 == profile.avatar or profile.avatar is None:
                if url:
                    # try:
                    #     avatar = requests.get(url, params={'type': 'large'})
                    #     avatar.raise_for_status()
                    # except HTTPError:
                    #     pass
                    # else:
                    #     profile.avatar.save('{0}_{1}.jpg'.format(user.username, backend.name), ContentFile(avatar.content))
                    #     profile.save()
                    try:
                        avatar = requests.post(api, params={'type': 'avatar', 'file': url})
                        avatar.raise_for_status()
                        thumb_id = avatar.json().get('image').get('id')
                    except HTTPError:
                        pass
                    else:
                        profile.avatar = str(thumb_id)
                        profile.save()
        except ObjectDoesNotExist:
            # print('will re-create profile!!!!!!')
            profile = Profile.objects.create(user=user, nick_name=user.username)
            if url:
                # try:
                #     avatar = requests.get(url, params={'type': 'large'})
                #     avatar.raise_for_status()
                # except HTTPError:
                #     pass
                # else:
                #     profile.avatar.save('{0}_{1}.jpg'.format(user.username, backend.name), ContentFile(avatar.content))
                #     profile.save()
                try:
                    avatar = requests.post(api, params={'type': 'avatar', 'file': url})
                    avatar.raise_for_status()
                    thumb_id = avatar.json().get('image').get('id')
                except HTTPError:
                    pass
                else:
                    profile.avatar = str(thumb_id)
                    profile.save()

            gender = language = birthday = None
            if backend.name == 'facebook':
                gender = gender_id(response['gender'])
                language = response['locale']
                birthday = response['birthday']
            if backend.name == 'google-oauth2':
                gender = gender_id(response['gender'])
                language = response['language']

            if gender and language and birthday:
                profile.gender = gender
                profile.language = language
                # profile.birthday = birthday
                profile.save()
        except ValueError:
            return False


