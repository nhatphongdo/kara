"""
Example of mandrill service in python (http://mandrill.com/)
Description of usage in python:
Russian: http://www.lexev.org/2014/send-email-django-project-mandrill-service/
English: http://www.lexev.org/en/2014/send-email-django-project-mandrill-service/
"""

# ======
# Django
# ======

# install
# -------
# pip install django-mandrill

# settings.py
# -----------
# INSTALLED_APPS += ('django_mandrill',)
# EMAIL_BACKEND = 'django_mandrill.mail.backends.mandrillbackend.EmailBackend'
# MANDRILL_API_KEY = "valid api key"

# mandrill_mail.py
# ----------------
from django_mandrill.mail.mandrillmail import MandrillTemplateMail


def send_mandrill_email(template_name, email_to, context=None, curr_site=None):
    if context is None:
        context = {}
    message = {
        'to': [],
        'global_merge_vars': []
    }
    for em in email_to:
        message['to'].append({'email': em})
    for k, v in context.items():
        message['global_merge_vars'].append(
            {'name': k, 'content': v}
        )
    MandrillTemplateMail(template_name, [], message).send()


send_mandrill_email('kara1', ["ntphat691@gmail.com"], context={'Name': "Phat Nguyen"})


# ================
# Standalone usage
# ================

# install
# -------
# pip install mandrill

# send email
# ----------
import mandrill

API_KEY = 'P_dpIUzDdI-LuDPhHIK2rA'


def send_mail(template_name, email_to, context):
    mandrill_client = mandrill.Mandrill(API_KEY)
    message = {
        'to': [],
        'global_merge_vars': []
    }
    for em in email_to:
        message['to'].append({'email': em})
    for k, v in context.iteritems():
        message['global_merge_vars'].append(
            {'name': k, 'content': v}
        )
    mandrill_client.messages.send_template(template_name, [], message)


send_mail('kara1', ["ntphat691@gmail.com"], context={'Name': "Phat Nguyen"})


# ================
# Standalone usage
# ================

# send email
# ----------
# Python has built-in libraries for handling email over SMTP: smtplib and email.
# We will also use the os library to so we can access our environment variables.
# ----------
import os
import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

msg = MIMEMultipart('alternative')

msg['Subject'] = "kara plus"
msg['From'] = "Phat Nguyen <ntphat6691@gmail.com>"
msg['To'] = "ntphat691@gmail.com"

text = "Mandrill speaks plaintext"
part1 = MIMEText(text, 'plain')

html = "<em>Mandrill speaks <strong>HTML</strong></em>"
part2 = MIMEText(html, 'html')

username = 'ntphat691'
password = 'P_dpIUzDdI-LuDPhHIK2rA'

msg.attach(part1)
msg.attach(part2)

s = smtplib.SMTP('smtp.mandrillapp.com', 587)

s.login(username, password)
s.sendmail(msg['From'], msg['To'], msg.as_string())

s.quit()