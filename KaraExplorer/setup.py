from distutils.core import setup

setup(
    name='KaraExplorer',
    version='0.0.1',
    packages=['app', 'app.libraries', 'app.migrations', 'app.templatetags', 'feeds', 'profile', 'manage',
              'interests', 'KaraExplorer'],
    url='http://vietdev.vn',
    license='',
    author='Do Nhat Phong',
    author_email='phong.do@vietdev.vn',
    description=''
)
