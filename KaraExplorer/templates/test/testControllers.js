﻿'use strict';

angular.module('test.controllers', [])
    .controller('testController', function ($scope) {
        $scope.models = {
            selected: null,
            templates: [
                {type: "man", name: 0},
                {type: "woman", name: 0},
                {type: "unknown", name: 0},
                {type: "group", name: 0, lists: []}
            ],
            zones: [
                {name: "Men", allowedTypes: [], max: 9, lists: []},
                {name: "Women", allowedTypes: [], max: 9, lists: []},
                {name: "People", allowedTypes: [], max: 9, lists: []}
            ]
        };

        $scope.toggleType = function (list, type) {
            var index = jQuery.inArray(type, list) + 1;
            if (index)
                list.splice(index - 1, 1);
            else
                list.push(type);
        };
        $scope.hasType = function (list, type) {
            var has = jQuery.inArray(type, list) >= 0;
            console.log('disable ', has);
            return has;
        };
        $scope.dragStart = function (item) {
            console.log('start drag ', item);
        };
        $scope.dragEnd = function (item) {
            console.log('end drag ', item);
        };
        $scope.dragSelected = function (item) {
            console.log('selected ', item);
            $scope.models.selected = item;
        };
        $scope.dragMoved = function (list, index) {
            console.log('moved ', list[index]);
            list.splice(index, 1);
        };
        $scope.dragCopied = function (item) {
            item.name = item.name + 1;
            console.log('copied ', item);
        };
        $scope.dragCanceled = function (item) {
            console.log('canceled ', item);
        };
        $scope.dragOver = function (event, index, type, external) {
            console.log('dragged over the list.');
            //console.log(event, index, type);
            return true;
        };
        $scope.dropOver = function (event, index, item, type, external) {
            console.log('dropped over the list.');
            //console.log(event, index, item, type);
            return item;
        };
        $scope.inserted = function (event, index, item, type) {
            console.log('inserted into the list.');
            //console.log(event, index, item, type);
        };
    })
;
