# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Setup

1. Install PTVS (Python tools for Visual Studio), select correct installer according to VS version
2. Open solution file. If everything is installed correctly, project can be loaded and appear in Solution Explorer window.

* Configuration

Python uses virtual environment for running application. In solution folder, there is existence virtual environment. We can install or remove the other libraries / packages in this virtual environment.

This project use these techniques:
1. LESS for styles sheet (http://lesscss.org)
2. CoffeeScript to scripting (http://coffeescript.org)
3. Django for front-end and back-end framework (http://www.djangobook.com)

* Dependencies


* Database configuration

At the moment, application uses SQLite 3 as DB system. It's file-based so it's no need to configure DB system. But as a Django application, we need to use migration whenever we modify database's structure to keep the system consistent.

* How to run tests

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact